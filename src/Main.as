package {
	
	import caurina.transitions.properties.ColorShortcuts;
	import caurina.transitions.properties.DisplayShortcuts;
	import caurina.transitions.properties.TextShortcuts;
	
	import com.lookingInSpeakingOut.flashcms.AppFacade;
	import com.lookingInSpeakingOut.flashcms.data.StartupData;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	[SWF ( backgroundColor=0xc2c27c, frameRate=31, width=1015, height=600, pageTitle="Looking In Speaking Out" ) ]
	[Frame ( factoryClass="com.lookingInSpeakingOut.flashcms.SiteLoader" ) ]

	
	/**
	 * Application Launcher
	 * @author ghostmonk 28/12/2008 
	 * 
	 */
	public class Main extends Sprite {
		
		
		
		public static const NAME:String = "Main";
		private const _configURL:String = "xmlData/config.xml";
		
		
		
		/**
		 * An instance of Main initializes Tweener shortcuts and waits to be added to stage before
		 * calling the startup method on AppFacade
		 * 
		 */
		public function Main() {
			
			DisplayShortcuts.init();
			TextShortcuts.init();
			ColorShortcuts.init();
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
		}
		
		
		
		private function onAddedToStage( e:Event ):void {
			
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
			var startupData:StartupData = new StartupData( stage, _configURL );
			AppFacade.getInstance( NAME ).startup( startupData );
			
		}
		
		
		
	}
}