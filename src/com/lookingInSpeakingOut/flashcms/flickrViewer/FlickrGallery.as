package com.lookingInSpeakingOut.flashcms.flickrViewer {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickrGalleryEvent;
	import com.lookingInSpeakingOut.flashcms.ui.ColorSwapButton;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.system.Security;
	import flash.ui.Keyboard;
	
	
	/**
	 * 
	 * @author ghostmonk 30/05/2009
	 * 
	 */
	public class FlickrGallery extends PhotoGalleryAsset {
		
		
		
		private var _closeBtn:FrameLabelButton;
		private var _thumbs:FlickrThumbCollection;
		private var _isThumbStripOpen:Boolean;
		private var _mainImage:MainFlickrImage;
		private var _thumbScrollMax:int;
		private var _thumbScrollMin:int;
		private var _visibleWidth:Number;
		private var _scrollDistance:Number;
		
		private var _nextBtn:FrameLabelButton;
		private var _previousBtn:FrameLabelButton;
		private var _scrollRightBtn:ColorSwapButton;
		private var _scrollLeftBtn:ColorSwapButton;
		
		private var _currentId:String;
		
		
		
		public function FlickrGallery( policyFiles:XML ) {
			
			loadPolicyFiles( policyFiles );
			
			_closeBtn = new FrameLabelButton( close );
			_closeBtn.addClickCallback( buildOut );
			
			_visibleWidth = thumbStrip.thumbHolder.width;
			_thumbScrollMax = thumbStrip.thumbHolder.x;
			
			_nextBtn = new FrameLabelButton( next );
			_nextBtn.addClickCallback( loadNewMain );
			_previousBtn = new FrameLabelButton( previous );
			_previousBtn.addClickCallback( loadNewMain );
			
			thumbStrip.addEventListener( MouseEvent.MOUSE_OVER, thumbStripMouseEvent );
			thumbStrip.addEventListener( MouseEvent.MOUSE_OUT, thumbStripMouseEvent );
			
			_scrollLeftBtn = new ColorSwapButton( thumbStrip.scrollLeft, thumbStrip.scrollLeft.arrow, thumbStrip.scrollLeft.bg, scrollThumbs );
			_scrollRightBtn = new ColorSwapButton( thumbStrip.scrollRight, thumbStrip.scrollRight.arrow, thumbStrip.scrollRight.bg, scrollThumbs );
			
			scaleX = scaleY = 0;
			
		}
		
		
		
		public function set thumbs( value:FlickrThumbCollection ):void {
			
			_mainImage = new MainFlickrImage( value, infoPanel );
			imageHolder.addChild( _mainImage );
			_isThumbStripOpen = false;
			_thumbs = value;
			_scrollDistance = Math.floor( _visibleWidth / _thumbs.getThumb( 0 ).fullWidth ) * _thumbs.getThumb( 0 ).fullWidth;
			
		}
		
		
		
		public function onThumbClick( e:FlickrGalleryEvent ):void {
			
			setCurrentImage( e.id );
			
		}
		
		
		
		public function buildIn( event:Event = null ):void {
			
			enableKeys();
			Tweener.addTween (
				this, {	
					scaleX:1, 
					scaleY:1, 
					time:0.3, 
					onUpdate:dispatchEvent, 
					onUpdateParams:[ new Event( Event.RESIZE ) ], 
					transition:Equations.easeOutBack,
					onComplete:positionThumbs
				}
			);
			
		}
		
		
		
		public function buildOut( event:Event = null, sendNotification:Boolean = true ):void {
			
			disableKeys();
			Tweener.addTween (
				this, {	
					scaleX:0, 
					scaleY:0, 
					time:0.3, 
					onUpdate:dispatchEvent, 
					onUpdateParams:[ new Event( Event.RESIZE ) ], 
					onComplete:onBuildOutComplete, 
					onCompleteParams:[ sendNotification ]
				}
			);
			
			closeThumbStrip( 0 );
			_thumbs.enableThumbs();
		}
		
		
		
		private function onBuildOutComplete( sendNotification:Boolean ):void {
			
			_mainImage.clean();
			stage.removeChild( this );
			if( sendNotification ) {
				dispatchEvent( new Event( FlickrGalleryEvent.REMOVE_FROM_VIEW ) );
			}
			
		}
		
		
		
		private function enableKeys():void {
			
			stage.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );	
				
		}
		
		
		
		private function disableKeys():void {
			
			stage.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			stage.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			
		}
		
		
		
		private function onKeyDown( e:KeyboardEvent ):void {
			
			disableKeys();
			stage.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			
			switch( e.keyCode ) {
				
				case Keyboard.RIGHT:
					setNextImage();
					break;
				case Keyboard.LEFT:
					setPreviousImage();
					break;
				default: 
					break;
				
			} 
		}
		
		
		private function onKeyUp( e:KeyboardEvent ):void {
			
			disableKeys();
			enableKeys();
			
		}
		
		
		
		private function loadPolicyFiles( xml:XML ):void {
			
			for each( var file:XML in xml.file ) {
				Security.loadPolicyFile( file );
			}
			
		}
		
		
		
		private function positionThumbs():void {
			
			thumbStrip.thumbHolder.x = _thumbScrollMax;
			
			for each( var thumb:FlickrThumb in _thumbs.list ) {
				
				thumb.buildIn( thumb.fullWidth * thumb.index, 0 );
				thumbStrip.thumbHolder.addChild( thumb.view );
			}
			
			_thumbScrollMin = _thumbScrollMax - thumb.view.x + _visibleWidth - 60;
			setCurrentImage( _currentId );
			
		}
		
		
		
		private function thumbStripMouseEvent( e:MouseEvent ) : void {
			
			if( e.type == MouseEvent.MOUSE_OVER ) {
				openThumbStrip( 0 );
			}
			else if( e.type == MouseEvent.MOUSE_OUT ) {
				closeThumbStrip( 0 );
			}
			
		}
		
		
		
		private function closeThumbStrip( delay:Number ):void {
			
			_isThumbStripOpen = false;
			tweenThumbVisibility( -thumbStripMask.height + thumbStrip.thumbReveal.height, delay, 0.3 );
			
			Tweener.addTween( thumbStrip.thumbReveal, { y:0, delay:delay, time:0.3 } );
			Tweener.addTween( thumbStrip.thumbReveal.arrow, { rotation:-90, delay:delay, time:0.3 } );
			
		}
		
		
		
		private function openThumbStrip( delay:Number ):void {
			
			_isThumbStripOpen = true;
			tweenThumbVisibility( 0, delay, 0.3 );
			
			var yPos:Number = thumbStrip.thumbHolder.height - 3; 
			
			Tweener.addTween( thumbStrip.thumbReveal, { y:yPos, delay:delay, time:0.3 } );
			Tweener.addTween( thumbStrip.thumbReveal.arrow, { rotation:90, delay:delay, time:0.3 } );
			
		}
		
		
		
		private function tweenThumbVisibility( maskY:Number, delay:Number, time:Number ):void {
			
			Tweener.addTween( thumbStripMask, { y:maskY, time:time,	delay:delay } );
			
		}
		
		
		
		private function loadNewMain( e:MouseEvent ):void {
			
			if( e.target == next ) {
				setNextImage();
			}
			else {
				setPreviousImage();
			}
			
		}
		
		
		
		private function setNextImage():void {
			
			setCurrentImage( _thumbs.getNext( _currentId ).id );
			
		}
		
		
		
		private function setPreviousImage():void {
			
			setCurrentImage( _thumbs.getPrevious( _currentId ).id );
			
		}
		
		
		
		private function setCurrentImage( thumbId:String ):void {
			
			_currentId = _mainImage.currentImageById = thumbId;
			_thumbs.enableThumbs( _currentId );
			
			var fThumb:FlickrThumb = _thumbs.getThumbById( _currentId );
			var currentThumb:MovieClip = _thumbs.getThumbById( _currentId ).view;
			var maskPoint:Point = thumbStrip.localToGlobal( new Point( thumbStrip.holderMask.x, thumbStrip.holderMask.y ) );
			var thumbPoint:Point = currentThumb.parent.localToGlobal( new Point( currentThumb.x, currentThumb.y ) );
			var testValue:Number = maskPoint.x - thumbPoint.x;
			
			if( testValue > 1 ) {
				scrollIntoView( testValue );
			}
			else if( testValue < - ( thumbStrip.holderMask.width - currentThumb.width * 1.5 )) {
				scrollIntoView( testValue + ( thumbStrip.holderMask.width - currentThumb.width) );
			}
			
		}
		
		
		
		private function scrollThumbs( e:MouseEvent ):void {
			
			var scrollXDest:Number;
			
			if( e.target == thumbStrip.scrollLeft ) {
				 scrollXDest = Math.min( thumbStrip.thumbHolder.x + _scrollDistance, _thumbScrollMax );
			}
			else {
				 scrollXDest = Math.max( thumbStrip.thumbHolder.x - _scrollDistance, _thumbScrollMin );
			}
			
			if( !Tweener.isTweening( thumbStrip.thumbHolder ) ) {
				Tweener.addTween( thumbStrip.thumbHolder, { x:scrollXDest, time:0.3	} );
			}
			
		}
		
		
		
		private function scrollIntoView( xDest:Number ):void {
			
			var scrollXDest:Number = Math.min( _thumbScrollMax, Math.max( thumbStrip.thumbHolder.x + xDest, _thumbScrollMin ) );
			Tweener.addTween( thumbStrip.thumbHolder, { x:scrollXDest, time:0.3	} );
			
		}
		
		
		
	}
}