package com.lookingInSpeakingOut.flashcms.globals {
	
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.net.sendToURL;
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class ErrorLogger {
		
		
		
		private static const SCRIPT:String = "log/sendAndLogError.php"; 
		private static const DEBUG:Boolean = true;
		
		
		
		/**
		 * 
		 * @param msg
		 * 
		 */
		public static function error( msg:String ):void {
			
			if( DEBUG ) {
				throw new Error( msg );	
			}
			else {
				sendError( msg );	
			}
			
		}
		
		
		
		private static function sendError( msg:String ):void {
			
			var variables:URLVariables = new URLVariables();
			variables.msg = msg;
			
			var request:URLRequest = new URLRequest( SCRIPT );
			request.method = URLRequestMethod.POST;
			request.data = variables;
			
			try {
				sendToURL( request );
			}
			catch( e:Error ) {
				trace( "ErrorLog is throwing Errors" );
			}
			
		}



	}
}