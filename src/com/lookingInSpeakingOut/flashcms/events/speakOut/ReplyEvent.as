package com.lookingInSpeakingOut.flashcms.events.speakOut {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 2009-10-02
	 * 
	 */
	public class ReplyEvent extends Event {
		
		
		
		public static const SPEAKOUT_REPLY:String = "speakoutReply";
		
		private var _parentId:int;
		
		
		
		public function ReplyEvent( type:String, parentId:int, bubbles:Boolean = false, cancelable:Boolean = false ) {
			
			_parentId = parentId;
			super(type, bubbles, cancelable);
			
		}
		
		
		
		public function get parentId() : int {
			
			return _parentId;
			
		}
		
		
		
		override public function clone() : Event {
			
			return new ReplyEvent( type, parentId, bubbles, cancelable );
			
		}
		
		
		
	}
}