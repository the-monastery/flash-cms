package com.lookingInSpeakingOut.flashcms.framework {
	
	public class FrameworkEvents {
		
		public static const NAME:String = "VideoPlayerMediator";
		public static const STAGE_RESIZE:String = "stageResize";
		public static const MINOR_CLOSE_BTN_CLICK:String = "minorCloseBtnClick";
		public static const SHOW_CURRENT_SECTIONS:String = "showCurrentSections";
		
	}
}