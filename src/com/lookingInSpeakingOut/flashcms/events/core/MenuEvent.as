package com.lookingInSpeakingOut.flashcms.events.core {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class MenuEvent extends Event {
		
		
		
		public static const MORPH:String = "com.lookingspeakingout.flashcms.events.morph";
		public static const INITIALIZED:String = "com.lookingspeakingout.flashcms.events.initialized";
		
		
		private var _isOpen:Boolean;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get isOpen():Boolean {
			 
			return _isOpen;
			 
		}
		
		
		
		/**
		 * 
		 * @param type		- MORPH
		 * @param isOpen	- isOpen is an indicator if the menuContainer will be open or closed
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function MenuEvent(type:String, isOpen:Boolean, bubbles:Boolean=false, cancelable:Boolean=false) {
			
			super( type, bubbles, cancelable );
			_isOpen = isOpen;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		override public function clone():Event {
			
			return new MenuEvent( type, isOpen );
			
		}
		
		
		
	}
}