package com.lookingInSpeakingOut.flashcms.rootView {
	
	import assets.title.TitleAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.composed.ClickableSprite;
	import com.lookingInSpeakingOut.flashcms.events.LISOEvent;
	
	import flash.events.Event;
	import flash.events.MouseEvent;

	[Event (name="lookingInApp", type="com.lookingInSpeakingOut.flashcms.events.LISOEvent")]
	[Event (name="speakingOutApp", type="com.lookingInSpeakingOut.flashcms.events.LISOEvent")]

	/**
	 * Interactive functionality for main title
	 * @author ghostmonk 2009-09-24
	 * 
	 */
	public class Title extends TitleAsset {
		
		
		
		private var _isLookingIn:Boolean;
		private var _outPos:Number;
		private var _clickableSprite:ClickableSprite;
		
		
		
		public function Title() {
			
			_isLookingIn = true;
			_clickableSprite = new ClickableSprite( this, onClick );
			disable();
			_outPos = speakingOut.y;
			alpha = 0;
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
		}
		
		
		
		public function enable() : void {
			
			_clickableSprite.enable();
			
		}
		
		
		
		public function disable() : void {
			
			_clickableSprite.disable();
			
		}
		
		
		
		public function onAddedToStage( e:Event ) : void {
			
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			Tweener.addTween( this, { alpha:1, time:1, transition:Equations.easeNone } );
			
		}
		
		
		
		public function setLookingIn() : void {
			
			_isLookingIn = true;
			Tweener.addTween( speakingOut, { scaleX:0.5, scaleY:0.5, y:_outPos, time:0.3 } );
			Tweener.addTween( lookingIn, { scaleX:1, scaleY:1, y:0, time:0.3, alpha:1 } );
			dispatchEvent( new LISOEvent( LISOEvent.LOOKING_IN_APP ) );
			
		}
		
		
		
		public function setSpeakingOut() : void {
			
			_isLookingIn = false;
			Tweener.addTween( lookingIn, { scaleX:0.5, scaleY:0.5, y:_outPos, time:0.3, alpha:0.5 } );
			Tweener.addTween( speakingOut, { scaleX:1, scaleY:1, y:0, time:0.3 } );
			dispatchEvent( new LISOEvent( LISOEvent.SPEAKING_OUT_APP ) );
			
		}
		
		
		
		private function onClick( e:MouseEvent ) : void {
			
			swapSections();
			
		}
		
		
		
		private function swapSections() : void {
			
			if( _isLookingIn ) {
				setSpeakingOut();
			}
			else {
				setLookingIn();
			}
			
		}
		
		
		
	}
}