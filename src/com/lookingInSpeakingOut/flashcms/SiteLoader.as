package com.lookingInSpeakingOut.flashcms {
		
	import com.ghostmonk.utils.BaseContextMenu;
	import com.lookingInSpeakingOut.flashcms.cmsUtils.SiteLoaderBase;
	
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	
	[SWF ( backgroundColor=0xc2c27c, frameRate=31, width=1015, height=600, pageTitle="Looking In Speaking Out" ) ]


	/**
	 * Standard Site loader with progress bar that loads in Main.swf
	 * 
	 * <p>Additionally, creates a new Flash Contextual menu with links to the author's sites.</p>nb  
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class SiteLoader extends SiteLoaderBase {	
		
		
		private var _preloader:Preloader;
		
		/**
		 * stage align and scalemode are set and loader is added to the stage. 
		 * 
		 */		
		public function SiteLoader() {
			
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			_preloader = new Preloader();
			addChild( _preloader );
			
			super();
			
			var menu:BaseContextMenu = new BaseContextMenu( this );
			menu.addLink( "Design: NotSoSimpleton", "http://www.notsosimpleton.com" );
			menu.addLink( "Programming: Ghostmonk", "http://selectedwork.ghostmonk.com" );
			
			onResize( null );
			stage.addEventListener( Event.RESIZE, onResize );
			
		}
		
		
		
		override protected function updateLoader( percent:Number ) : void {
			
			_preloader.gotoAndStop( Math.ceil( percent * _preloader.totalFrames ) );
			
		}
		
		
		
		private function onResize( e:Event ):void {
			
			_preloader.x = stage.stageWidth/2;
			_preloader.y = stage.stageHeight/2;
			
		}
		
		
		
	}
}
