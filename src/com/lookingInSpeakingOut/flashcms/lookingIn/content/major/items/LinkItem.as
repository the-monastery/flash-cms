package com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.LinkData;
	
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.TextFieldAutoSize;
	
	import items.links.LinkItemAsset;

	/**
	 * A simple clickable displayObject with a title and description that opens a blank page via navigateToURL
	 *  
	 * @author ghostmonk
	 * 
	 */
	public class LinkItem extends LinkItemAsset {
		
		
		private var _url:URLRequest;
		
		
		
		/**
		 * 
		 * @param data
		 * 
		 */
		public function LinkItem( data:LinkData ) {
			
			background.alpha = 0;
			title.text = data.title;
			description.text = data.description;
			description.autoSize = TextFieldAutoSize.LEFT;
			
			//Remove extra characters to keep description text box a max of 2 lines long
			if( description.numLines > 2 ) {
				
				var numChars:int = description.getLineLength( 0 ) + description.getLineLength( 1 ) - 4;
				description.text = "";
				description.text = data.description.substring( 0, numChars ) + "...";
				
			}
			
			visitsite.y = description.y + description.height;
			background.height = visitsite.y + visitsite.height + 2;
			_url = new URLRequest( data.url );
			
			mouseChildren = false;
			alpha = 0;
			
		}
		
		
		
		/**
		 * Use this to disable and remove from the DisplayList. 
		 * 
		 */
		public function buildOut():void {
			
			disable();
			alpha = 0;
			
			if( parent ) {
				
				parent.removeChild( this );
				
			}
			
		}
		
		
		
		/**
		 * Fade in and enable navigation click 
		 * 
		 */
		public function buildIn():void {
			
			Tweener.addTween( 
				this, {
					alpha:1, 
					delay:0.2, 
					time:0.2, 
					transition:Equations.easeNone, 
					onComplete:enable
				}
			);
			
		}
		
		
		
		private function enable():void {
			
			buttonMode = true;
			addEventListener( MouseEvent.CLICK, onClick );
			addEventListener( MouseEvent.ROLL_OVER, onRollover );
			addEventListener( MouseEvent.ROLL_OUT, onRollout );
			
		}
		
		
		
		private function disable():void {
			
			buttonMode = false;
			removeEventListener( MouseEvent.CLICK, onClick );
			removeEventListener( MouseEvent.ROLL_OVER, onRollover );
			removeEventListener( MouseEvent.ROLL_OUT, onRollout );
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
			
			navigateToURL( _url, "_blank" );
			
		}
		
		
		
		private function onRollover( e:MouseEvent ):void {
			
			Tweener.addTween( 
				background, { 
					alpha:1, 
					time:0.3, 
					transition:Equations.easeNone 
				} 
			);
			
		}
		
		
		
		private function onRollout( e:MouseEvent ):void {
			
			Tweener.addTween( 
				background, { 
					alpha:0, 
					time:0.3, 
					transition:Equations.easeNone 
				} 
			);
			
		}	
		
		
		
	}
}