package com.lookingInSpeakingOut.flashcms.speakingOut {
	
	public class TempPositionGraph {
		
		public static const MAP:Array = [
			
			{ x:834, y:160, s:0.9 }, //1
			{ x:897, y:147, s:0.9 }, //2
			{ x:976, y:120, s:0.9 }, //3
			{ x:893, y:213, s:1.3 }, //4
			{ x:952, y:185, s:0.9 }, //5
			{ x:1017, y:180, s:0.9 }, //6
			{ x:893, y:274, s:1.3 }, //7
			{ x:956, y:253, s:1.3 }, //8
			{ x:1024, y:247, s:1.3 }, //9
			{ x:1092, y:231, s:1.3 }, //10
			{ x:1164, y:239, s:1.3 }, //11
			{ x:1237, y:239, s:1.3 }, //12
			{ x:1342, y:180, s:0.9 }, //13
			{ x:1037, y:301, s:1.5 }, //14
			{ x:1116, y:298, s:1.5 }, //15
			{ x:1193, y:304, s:1.5 }, //16
			{ x:1261, y:300, s:1.5 }, //17
			{ x:1323, y:271, s:1.3 }, //18
			{ x:1381, y:256, s:1.3 }, //19
			{ x:1383, y:197, s:1.5 } //20
			
		]

	}
}