package com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.net.AssetLoader;
	import com.ghostmonk.net.SimpleURLLoader;
	import com.lookingInSpeakingOut.flashcms.cmsUtils.CleanHTML;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.RSSData;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	
	import items.news.NewsItemAsset;
	
	[Event (name="itemClicked", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	[Event (name="buildOutComplete", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	[Event (name="animationUpdate", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class NewsItem extends NewsItemAsset {
		
		
		private var _hasImage:Boolean;
		private var _shortDescription:String;
		private var _imageLoader:AssetLoader;
		private var _request:URLRequest;
		private var _shortHeight:Number;
		private var _yPos:Number;
		private var _isFull:Boolean;
		
		
		
		
		/**
		 * 
		 * @param data
		 * 
		 */
		public function NewsItem( data:RSSData ) {
			
			_yPos = 0;
			
			bodyCopy.autoSize = TextFieldAutoSize.LEFT;
			_hasImage = data.photoRequest != null;
			_shortDescription = data.shortDescription;
			if( _hasImage ) {
				_imageLoader = new AssetLoader( data.photoRequest.url, onImageLoaded, onImageError );
				removeChild( bodyCopy );
			}
			else {
				setShortDescription( _shortDescription );
			}
			
			title.htmlText = "<b>" + data.title + "</b>";
			title.mouseEnabled = false;
			
			//date.htmlText = "Posted by <b>" + data.author + "</b> on " + data.date;
			date.htmlText = "Posted on " + data.date;
			date.mouseEnabled = false;
			
			_request = data.fullLink;
			
			//mouseChildren = false;
			
			_shortHeight = background.height;
			enable();
			
			x = width;
			addEventListener( Event.ADDED_TO_STAGE, buildIn );
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get isFullDisplay():Boolean {
			 
			return _isFull;
			 
		}
		
		
		
		/**
		 * 
		 * @param value
		 * 
		 */
		public function set yPos( value:Number ):void {
			
			y = _yPos = value;
			 
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function buildIn( e:Event = null ):void {
			
			if( !_isFull ) {			
				Tweener.addTween( this, { x:0, y:_yPos, time:0.4 } );
			}	
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function buildOut():void {
			
			Tweener.addTween(
				this, {
					x:-width-20, 
					time:0.2, 
					onComplete:parent.removeChild, 
					onCompleteParams:[ this ]
				}
			);
			
		}
		
		
		
		public function setShortDisplay():void {
			
			_isFull = false;
			Tweener.addTween(
				background, {	
					height:_shortHeight, 
					time:0.3, 
					onUpdate:dispatchEvent, 
					onUpdateParams:[ new SectionEvent( SectionEvent.SECTION_VIEW_UPDATE ) ],
					onComplete:enable
				}
			);
			
			var text:String = _hasImage ? "" : _shortDescription;
			Tweener.addTween( bodyCopy, { _text:text, time:0.3 } );
			
		}
		
		
		
		private function setFullDisplay( data:String ):void {
			
			data = CleanHTML.replaceStrongWithBold( data );
			data = CleanHTML.replacePTageWith2BR( data );
			data = CleanHTML.addUnderlineToLinks( data );
			
			_isFull = true;
			addChild( bodyCopy );
			bodyCopy.htmlText = data;
			var newHeight:Number = _hasImage ? background.height + bodyCopy.height + 10 : bodyCopy.height + title.height + date.height + 20;
			
			Tweener.addTween(
				background, {
					height:newHeight, 
					time:1, 
					onUpdate:dispatchEvent, 
					onUpdateParams:[ new SectionEvent( SectionEvent.SECTION_VIEW_UPDATE ) ]
				}
			);
			
			var buildText:String = bodyCopy.text;
			bodyCopy.htmlText = "";
			
			Tweener.addTween( bodyCopy, { _text:buildText, time:1, transition:Equations.easeNone, onComplete:setHTMLText, onCompleteParams:[bodyCopy,data] } );
			
		}
		
		
		private function setHTMLText( field:TextField, text:String ) : void {
			
			field.htmlText = text;
			trace( text );
			
		}
		
		
		
		private function setShortDescription( text:String ) : void {
			
			removeChild( screen );
			removeChild( holder );
			removeChild( imageMask );
			bodyCopy.y = holder.y;
			bodyCopy.htmlText = text;
			background.height = bodyCopy.y + bodyCopy.height + 20;
			
		}
		
		
		
		private function enable():void {
			
			buttonMode = true;
			addEventListener( MouseEvent.CLICK, onLoadText );
			addEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			addEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			screen.visible = true;
			
		} 
		
		
		
		private function disable():void {
			
			buttonMode = false;
			removeEventListener( MouseEvent.CLICK, onLoadText );
			removeEventListener( MouseEvent.MOUSE_OVER, onMouseOver );
			removeEventListener( MouseEvent.MOUSE_OUT, onMouseOut );
			screen.visible = false;
			
		} 
		
		
		
		private function onMouseOver( e:MouseEvent ):void {
			
			Tweener.addTween(
				screen, {
					alpha:0.5, 
					time:0.2, 
					transition:Equations.easeNone
				}
			);
				
		}
		
		
		
		private function onMouseOut( e:MouseEvent ):void {
			
			Tweener.addTween(
				screen, {
					alpha:1, 
					time:0.2, 
					transition:Equations.easeNone
				}
			);
			
		}
		
		
		
		private function onImageLoaded( bitmap:Bitmap ):void {
			
			var bit:Bitmap = new Bitmap( new BitmapData( holder.width, holder.height, true, 0 ) );
			bit.bitmapData.draw( bitmap );
			holder.addChild( bit );
			bitmap.alpha = 0;
			Tweener.addTween( bitmap, { alpha:1, time:0.2, transition:Equations.easeNone } );
			
		}
		
		
		
		private function onLoadText( e:MouseEvent ):void {
			
			dispatchEvent( new SectionEvent( SectionEvent.ITEM_CLICKED ) );
			new SimpleURLLoader( _request, setFullDisplay );
			
			Tweener.addTween(
				this, {
					y:0, 
					time:0.3, 
					onUpdate:dispatchEvent, 
					onUpdateParams:[ new SectionEvent( SectionEvent.SECTION_VIEW_UPDATE ) ]
				}
			);
			
			disable();
		}
		
		
		
		private function onImageError( e:IOErrorEvent ):void {
			trace( e.text );
		}



	}
}