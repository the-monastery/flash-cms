package com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.CategoryData;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.LinkData;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	
	import flash.events.MouseEvent;
	
	import items.links.LinksCategoryAsset;
	
	[Event (name="animationUpdate", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	
	/**
	 * A clickable display object that opens up to a list of url 
	 * links with titles, descriptions and external URL actions.
	 *  
	 * @author ghostmonk
	 * 
	 */
	public class LinksCategoryItem extends LinksCategoryAsset {
		
		
		private const PADDING:Number = 5;
		
		private var _isOpen:Boolean;
		private var _openHeight:Number;
		private var _closedHeight:Number;
		private var _links:Array;
		
		
		
		/**
		 * Use this class in conjunction with components.content.type.Categorized object.
		 * 
		 * <p>LinksCategoryItem is passed to the constructer as a class reference</p>
		 * 
		 * @param data - a data structure used to create a dynamic number of LinksItems
		 * 
		 */
		public function LinksCategoryItem( data:CategoryData ) {
			
			_isOpen = false;
			_links = [];
			
			initLinks( data );
			_closedHeight = height;
			
			title.label.text = data.catTitle;
			title.buttonMode = true;
			title.mouseChildren = false;
			title.addEventListener( MouseEvent.CLICK, onClick );
			
		}
		
		
		
		/**
		 * Calls buildOut on each LinkItem and resizes background to a closed state
		 * <p>dispatches an animation update as it closes, which is useful for notifying its container</p>
		 * 
		 */
		public function close():void {
			
			for each( var item:LinkItem in _links ) {
				item.buildOut();
			}
			
			Tweener.addTween( 
				background, {
					height:_closedHeight, 
					time:0.3, 
					onUpdate:dispatchAnimationUpdate
				}
			);
			
			Tweener.addTween( 
				title.indicator, {
					rotation:0, 
					time:0.3, 
					transition:Equations.easeNone
				}
			);
			
		}
		
		
		
		/**
		 * Scales open its background to a size that accomidates the number of LinkItems.
		 * <p>Upon completion, each LinkItem is added to the DisplayList</p>
		 * <p>dispatches an animation update as it opens, which is useful for notifying its container</p>
		 * 
		 */
		private function open():void {
				
			Tweener.addTween(
				background, {
					height:_openHeight, 
					time:0.3, 
					onUpdate:dispatchAnimationUpdate, 
					onComplete:addLinks
				}
			);
			
			Tweener.addTween(
				title.indicator, {
					rotation:180, 
					time:0.3, 
					transition:Equations.easeNone
				}
			);
			
			
		}
		
		
		
		private function addLinks():void {
			
			for each( var item:LinkItem in _links ) {
				
				addChild( item );
				item.buildIn();
				
			}
			
		}
		
		
		
		private function initLinks( data:CategoryData ):void {
			
			_openHeight = height;
			var yPos:int = title.y + title.height + PADDING;
			
			for each( var linkData:LinkData in data.items ) {
				
				var link:LinkItem = new LinkItem( linkData );
				link.y =  yPos;
				link.x = title.x + ( title.width - link.width ) / 2;
				yPos += link.height;
				_openHeight += link.height;
				_links.push( link );
				
			}
			
			_openHeight += PADDING;
		}
		
		
		
		private function dispatchAnimationUpdate():void {
			
			dispatchEvent( new SectionEvent( SectionEvent.SECTION_VIEW_UPDATE ) );
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
			
			_isOpen = !_isOpen;
			
			if( _isOpen ) {
				open();
			}
			else {
				close();
			}
			
		}
		
		
	}
}