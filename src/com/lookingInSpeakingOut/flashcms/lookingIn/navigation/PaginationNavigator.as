package com.lookingInSpeakingOut.flashcms.lookingIn.navigation {
	
	import com.lookingInSpeakingOut.flashcms.events.core.PaginationEvent;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	[Event (name="pageNavigation", type="com.lookingInSpeakingOut.flashcms.events.PaginationEvent")]
	
	/**
	 * Controls the pagination of posts.
	 * Maintains the position and dispatches events indicating forward and 
	 * backward movement through the aggregate pages 
	 * @author ghostmonk
	 * 
	 */
	public class PaginationNavigator extends AggregationNavigatorAsset {
		
		
		
		private var _totalPages:int;
		private var _currentPage:int;
		
		
		
		/**
		 * 
		 * @param totalPages
		 * 
		 */
		public function PaginationNavigator( totalPages:int ) {
			
			_totalPages = totalPages;
			_currentPage = 0;
			setView();
			
			createButton( previousBtn, onPrevious );
			createButton( nextBtn, onNext );
				
		}
		
		
		
		private function setView() : void {
			
			if( _currentPage == 0 ) {
				removeChild( previousBtn );
				addChild( nextBtn );
			}
			else if( _currentPage == _totalPages - 1 ) {
				removeChild( nextBtn );
				addChild( previousBtn );
			}
			else if( _totalPages > 2 ) {
				addChild( nextBtn );
				addChild( previousBtn );
			}
			
		}
		
		
		
		private function createButton( btn:Sprite, call:Function ) : void {
			
			btn.buttonMode = true;
			btn.mouseChildren = false;
			btn.addEventListener( MouseEvent.CLICK, call );
			
		} 
		
		
		
		private function onPrevious( e:MouseEvent  ) : void {
			
			_currentPage--;
			dispatch();
			setView();
			
		}
		
		
		
		private function onNext( e:MouseEvent ) : void {
			
			_currentPage++;
			dispatch();
			setView();
			
		}
		
		
		
		private function dispatch() : void {
			
			dispatchEvent( new PaginationEvent( PaginationEvent.PAGE_NAVIGATION, _currentPage ) );
			
		}
		
		
		
	}
}