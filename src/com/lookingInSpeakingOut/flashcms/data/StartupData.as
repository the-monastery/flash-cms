package com.lookingInSpeakingOut.flashcms.data {
	
	import flash.display.Stage;
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class StartupData {
		
		
		
		private var _stage:Stage;
		private var _configURL:String;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get stage():Stage {
			
			return _stage;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get configURL():String {
			
			return _configURL;
			
		}
		
		
		
		/**
		 * 
		 * @param stage
		 * @param conf
		 * 
		 */
		public function StartupData( stage:Stage, conf:String ) {
			
			_stage = stage;
			_configURL = conf;
		
		}



	}
}