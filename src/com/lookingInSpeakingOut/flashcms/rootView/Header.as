package com.lookingInSpeakingOut.flashcms.rootView {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.Stage;
	import flash.events.MouseEvent;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class Header extends AnimatedHeader {
		
		
		
		private var _headerClips:Array;
		private var _stageWidth:Number;
		private var _stageHeight:Number;
		
		
		
		public function Header() {
					
			mouseChildren = false;
			mouseEnabled = false;
			
		}
		
		
		
		public function fadeIn() : void {
			
			Tweener.addTween( this, {alpha:1, time:0.3, transition:Equations.easeNone} );
			
		}
		
		
		
		public function fadeOut() : void {
			
			Tweener.addTween( this, {alpha:0.3, time:0.3, transition:Equations.easeNone} );
				
		}
		
		
		
		public function position( stage:Stage, vSpace:Number ):void {
			
			x = ( stage.stageWidth - width ) * 0.5; 
			y = Math.max( -( lotus.y ), Math.min( ( ( vSpace - height ) * 0.5 ), 0 ) );
			
		}
		
		
		
		private function onClick( e:MouseEvent ) : void {
			
			trace( e );
			
		}
		
		
		
		
	}
}