package com.lookingInSpeakingOut.flashcms.lookingIn.content.major {
	
	import com.ghostmonk.media.video.events.VideoLoadEvent;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.RSSData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items.VideoItem;
	
	[Event (name="loadVideo", type="com.ghostmonk.media.video.events.VideoLoadEvent")]
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class VideosContent extends BaseContent {
		
		private const BASE_URL:String = "";//"http://www.lookinginspeakingout.com/";
		private const DEFAULT_IMG:String = "assets/images/videos/default.jpg";
		
		private var _items:Array;
		private var _videoCallback:Function;
		
		
		
		public function VideosContent( sectionW:int, sectionH:int ) {
		
			super( sectionW, sectionH );
			
		}
		
		
		
		override public function removeItems() : void {
			
			for each( var item:VideoItem in _items ) {
				item.buildOut();
			}
			
			_items = [];
		} 
		
		
		
		/**
		 * 
		 * 
		 */
		override public function listView():void {
			
		}
		
		
		
		override protected function onDataLoaded( data:String ):void {
			
			var xml:XML = XML( data );
			_items = [];
			var yPos:Number = 0;
			for each( var item:XML in xml.channel.item ) {
				
				var videoItem:VideoItem = new VideoItem( new RSSData( item, BASE_URL, DEFAULT_IMG ) );
				_items.push( videoItem );
				videoItem.yPos = yPos;
				
				yPos += videoItem.height + 10;
				
				videoItem.addEventListener( VideoLoadEvent.LOAD_VIDEO, onVideoLoad );
				videoItem.addEventListener( SectionEvent.SECTION_VIEW_UPDATE, onAnimationUpdate );
				videoItem.addEventListener( DisplayEvent.BUILD_OUT_COMPLETE, onBuildOutComplete );
				addChild( videoItem );
				
			}
			
			hasAggregatePages( xml.totalPages, yPos );
			
			super.onDataLoaded( data );
			
		}
		
		
		
		private function onVideoLoad( e:VideoLoadEvent ):void {
			
			dispatchEvent( e );
			
		}
		
		
		
		private function onAnimationUpdate( e:SectionEvent ):void {
			
		}
		
		
		
		private function onBuildOutComplete( e:SectionEvent ):void {
			
		}
		
		
	}
}