package com.lookingInSpeakingOut.flashcms.data.speakingOutData {
	import flash.geom.Point;
	
	
	/**
	 * 
	 * @author ghostmonk 2009-09-20
	 * 
	 */
	public class XMLMessageData  {		
		
		
		
		private var _data:XML;
		
		
		
		public function XMLMessageData( data:XML ) {
				
			_data = data;	
				
		}
		
		
		
		public function deleteData() : void {
			
			_data = null;
			
		}
		
		
		
		public function get id() : int {
			
			return _data.@id;
			
		}
		
		
		
		public function get name() : String {
			
			return _data.user;
			
		}
		
		
		
		public function get text() : String {
			
			return _data.text;
			
		}
		
		
		
		public function get position() : Point {
			
			return new Point( _data.position.@x, _data.position.@y );
			
		}
		
		
		
		public function get date() : String {
			
			return _data.date;
			
		}
		
		
		
		public function get responseCount() : int {
			
			return _data.@replies;
			
		}
		
		
		
		public function toString() : String {
			
			return "\nID: " + id +
				"\nName: " + name +
				"\nText: " + text +
				"\nPoint: " + position +
				"\nDate: " + date;
			
		}
		
		
		
	}
}