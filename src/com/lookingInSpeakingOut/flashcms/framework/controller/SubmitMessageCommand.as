package com.lookingInSpeakingOut.flashcms.framework.controller {
	
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.SubmitMessageData;
	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	/**
	 * 
	 * @author ghostmonk 2009-10-01
	 * 
	 */
	public class SubmitMessageCommand extends SimpleCommand {
		
		
		override public function execute( note:INotification ) : void {
			
			var data:SubmitMessageData = note.getBody() as SubmitMessageData;
			
			var urlLoader:URLLoader = new URLLoader();
			//urlLoader.addEventListener( Event.COMPLETE, onComplete );
			
			var variables:URLVariables = new URLVariables();
			variables.isTrunk = data.isTrunk;
			variables.parentId = data.parentId;
			variables.name = data.name;
			variables.xPos = data.xPos;
			variables.yPos = data.yPos;
			variables.text = data.text;
			
			var request:URLRequest = new URLRequest( data.endpoint );
			request.method = URLRequestMethod.POST;
			request.data = variables;
			
			//urlLoader.load( request );
			
		}
		
		
		
		private function onComplete( e:Event ) : void {
			
			trace( "did anything work?" + e.target.data );
			
		}
		
	}
}