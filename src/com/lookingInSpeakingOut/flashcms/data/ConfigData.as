package com.lookingInSpeakingOut.flashcms.data {
	
	import items.ReturnTabAsset;
	
	
	/**
	 * Structure for containing refrences to data used in the application
	 * 
	 * @author ghostmonk 14/02/2009
	 * 
	 */
	public class ConfigData {
		
		
		private var _configXML:XML;
		
		
		/**
		 * 
		 * @param xml
		 * 
		 */
		public function ConfigData( xml:XML ) {
			
			_configXML = xml;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get background():XML {
			 
			return _configXML.background[ 0 ];
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get navContainer():XML {
			 
			return _configXML.navContainer[ 0 ];
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get majorSections():XML {
			
			return _configXML.majorSections[ 0 ];
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get minorSections():XML {
			 
			return _configXML.minorSections[ 0 ];
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get video():XML {
			 
			return _configXML.videoPlayer[ 0 ];
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get policyFiles():XML {
			
			return _configXML.policyFiles[0];
				
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get security():XML {
			
			return _configXML.security[0];
				
		}
		
		
		
		public function get speakingOut() : XML {
			
			return _configXML.speakingOut[ 0 ];
			
		}



	}
}