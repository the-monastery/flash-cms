package com.lookingInSpeakingOut.flashcms.framework.view {
	
	import com.lookingInSpeakingOut.flashcms.events.LISOEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickrGalleryEvent;
	import com.lookingInSpeakingOut.flashcms.flickrViewer.FlickrGallery;
	import com.lookingInSpeakingOut.flashcms.framework.FrameworkEvents;
	
	import flash.events.Event;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class FlickrGalleryMediator extends Mediator {
		
		
		
		public static const NAME:String = "FlickrGalleryMediator";
		
		private var stageMediator:StageMediator;
		
		
		
		public function FlickrGalleryMediator( viewComponent:FlickrGallery ) {
			
			super( NAME, viewComponent );
			
			gallery.addEventListener( Event.RESIZE, position );
			gallery.addEventListener( FlickrGalleryEvent.REMOVE_FROM_VIEW, onRemovedFromView );
			
		}
		
		
		
		public function get gallery():FlickrGallery {
			
			return viewComponent as FlickrGallery;
			
		}
		
		
		
		override public function onRegister():void {
			
		 	stageMediator = facade.retrieveMediator( StageMediator.NAME ) as StageMediator;
		 		
		}
		
		
		
		override public function listNotificationInterests():Array {
			
			return [
				FlickrGalleryEvent.PHOTO_GALLERY_DATA,
				FlickrGalleryEvent.PHOTO_GALLERY_CLICK,
				NavigationEvent.MAJOR_SECTION,
				NavigationEvent.MINOR_SECTION,
				LISOEvent.SPEAKING_OUT_APP,
				FrameworkEvents.STAGE_RESIZE
			]
			
		}
		
		
		
		override public function handleNotification( note:INotification ):void {
			
			
			switch ( note.getName() ) {
				
				case FlickrGalleryEvent.PHOTO_GALLERY_CLICK:
					onThumbClick( note.getBody() as FlickrGalleryEvent );
					break;
					
				case FlickrGalleryEvent.PHOTO_GALLERY_DATA:
					gallery.thumbs = ( note.getBody() as FlickrGalleryEvent ).thumbCollection;
					break;
					
				case FrameworkEvents.STAGE_RESIZE:
					position();
					break;
				
				case LISOEvent.SPEAKING_OUT_APP:
				case NavigationEvent.MAJOR_SECTION:
				case NavigationEvent.MINOR_SECTION: 
					if( gallery.stage ) gallery.buildOut( null, false );
					break;
				
				default:
					trace( "Unhanled event: " + note.getName() );
				
			}
			
		}
		
		
		
		private function onThumbClick( e:FlickrGalleryEvent ):void {
			
			if( e.revealGallery ) buildIn();
			gallery.onThumbClick( e );
			
		}
		
		
		private function buildIn():void {
			
			stageMediator.addChildUnderNavBar( gallery );
			gallery.buildIn();
			
		}
		
		
		
		private function position( e:Event = null ):void {
			
			gallery.x = ( stageMediator.stage.stageWidth - ( gallery.imageHolder.width * gallery.scaleX ) ) * 0.5;
			gallery.y = ( stageMediator.stage.stageHeight - ( gallery.imageHolder.height * gallery.scaleY ) ) * 0.5 - 25;
			
		}
		
		
		private function onRemovedFromView( e:Event ):void {
			
			sendNotification( FrameworkEvents.SHOW_CURRENT_SECTIONS );
			
		}
		
		
		
	}
}