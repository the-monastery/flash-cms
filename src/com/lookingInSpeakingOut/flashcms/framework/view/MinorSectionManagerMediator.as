package com.lookingInSpeakingOut.flashcms.framework.view {
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.LISOEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.framework.FrameworkEvents;
	import com.lookingInSpeakingOut.flashcms.lookingIn.clients.MinorSectionManager;
	
	import flash.display.Stage;
	import flash.events.Event;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	/**
	 *  
	 * @author ghostmonk
	 * 
	 */
	public class MinorSectionManagerMediator extends Mediator {
		
		
		public static const NAME:String = "MinorSectionManagerMediator";
		
		
		
		/**
		 * 
		 * @param viewComponent
		 * 
		 */
		public function MinorSectionManagerMediator( viewComponent:MinorSectionManager ) {
			
			super( NAME, viewComponent );
			manager.addEventListener( DisplayEvent.CLOSE_BTN_CLICKED, onCloseBtnClicked );
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get manager():MinorSectionManager {
			
			return viewComponent as MinorSectionManager;
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		override public function onRegister():void {
			
			StageMediator( facade.retrieveMediator( StageMediator.NAME ) ).addChildUnderNavBar( manager );
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		override public function listNotificationInterests():Array {
			
			return [	
				NavigationEvent.MINOR_SECTION,
				NavigationEvent.MAJOR_SECTION,
				FrameworkEvents.STAGE_RESIZE,
				LISOEvent.SPEAKING_OUT_APP
			];
			
		}
		
		
		
		/**
		 * 
		 * @param note
		 * 
		 */
		override public function handleNotification( note:INotification ):void {
			
			switch( note.getName() ) {
				
				case NavigationEvent.MINOR_SECTION:		
					minorSectionCall( note );		
					break;
				
				case LISOEvent.SPEAKING_OUT_APP:
				case NavigationEvent.MAJOR_SECTION:		
					majorSectionCall();			
					break;
					
				case FrameworkEvents.STAGE_RESIZE:		
					onStageResize( note );		
					break;
					
			}
			
		}
		
		
		
		private function minorSectionCall( note:INotification ):void {
			
			var data:SectionData = note.getBody() as SectionData;
			manager.createSection( data );
			
		}
		
		
		
		private function majorSectionCall():void {
			
			manager.clear();
			
		}
		
		
		
		private function onCloseBtnClicked( e:Event ):void {
			
			sendNotification( FrameworkEvents.MINOR_CLOSE_BTN_CLICK );
			
		}
		
		
		
		private function onStageResize( note:INotification ):void {
			
			manager.position( note.getBody() as Stage );
			
		}
		
		
		
	}
}