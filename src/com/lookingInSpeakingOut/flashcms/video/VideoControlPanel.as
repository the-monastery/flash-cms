package com.lookingInSpeakingOut.flashcms.video {
	
	import com.ghostmonk.events.PercentageEvent;
	import com.ghostmonk.events.ToggleEvent;
	import com.ghostmonk.media.video.events.VideoControlEvent;
	import com.ghostmonk.ui.graveyard.TimeDisplay;
	import com.ghostmonk.ui.graveyard.VolumeContol;
	import com.ghostmonk.ui.graveyard.buttons.SimpleMovieClipButton;
	import com.ghostmonk.ui.graveyard.idecomposed.VideoScrubBar;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.FullScreenEvent;
	import flash.events.MouseEvent;
	
	import video.asset.ControlsAsset;
	
	[Event(name="change", type="com.ghostmonk.events.PercentageEvent")]
	[Event(name="play", type="com.ghostmonk.events.video.VideoControlEvent")]
	[Event(name="pause", type="com.ghostmonk.events.video.VideoControlEvent")]
	[Event(name="fullScreen", type="flash.events.FullScreenEvent")]
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class VideoControlPanel extends EventDispatcher {
		
		
		
		private var _controls:ControlsAsset;
		private var _scrubBar:VideoScrubBar;
		private var _playToggle:PlayToggleBtn;
		private var _timeDisplay:TimeDisplay;
		private var _volumeControl:VolumeContol;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get view():ControlsAsset {
			 
			return _controls;
			 
		}
		
		
		
		/**
		 * 
		 * @param view
		 * 
		 */
		public function VideoControlPanel( view:ControlsAsset ) {
			
			_controls = view;
			
			_controls.track.scaleX = 1;
			_scrubBar = new VideoScrubBar( _controls.playhead, _controls.track, _controls.durationBar );
			_volumeControl = new VolumeContol( _controls.volumeCtrl );
			
			new SimpleMovieClipButton( _controls.fullscreenBtn, onFullScreen );
			_playToggle = new PlayToggleBtn( _controls.playToggle, onPlayToggle );
			_timeDisplay = new TimeDisplay( _controls.timeDisplay );
			
			_scrubBar.addEventListener( PercentageEvent.CHANGE, onScrub );
			_scrubBar.addEventListener( VideoControlEvent.PAUSE, onVideoPause );
			_scrubBar.addEventListener( VideoControlEvent.PLAY, onVideoPlay );
			
		}
		
		
		
		/**
		 * 
		 * @param duration
		 * 
		 */
		public function setTotalDuration( duration:Number ):void {
			
			_timeDisplay.setTotalTime( duration );
			
		}
		
		
		
		/**
		 * 
		 * @param percent
		 * 
		 */
		public function setScrubHead( percent:Number ):void {
			
			_timeDisplay.setTimeByPercent( percent );
			_scrubBar.setPlayhead( percent );
			
		}
		
		
		
		/**
		 * 
		 * @param e
		 * 
		 */
		public function onLoadProgress( e:PercentageEvent ):void {
			
			_scrubBar.onLoadProgress( e.percent );
			
		}
		
		
		
		/**
		 * 
		 * @param totalReset
		 * 
		 */
		public function reset( totalReset:Boolean ):void {
			
			_playToggle.setToPause();
			_scrubBar.setPlayhead( 0 );
			_timeDisplay.reset( totalReset );
		
		}
		
		
		
		private function onScrub( e:PercentageEvent ):void {
			
			_timeDisplay.setTimeByPercent( e.percent );
			dispatchEvent( e );
			
		}
		
		
		
		private function onVideoPause( e:Event = null ):void {
			
			dispatchEvent( new VideoControlEvent( VideoControlEvent.PAUSE ) );
			
		}
		
		
		
		private function onVideoPlay( e:Event = null ):void {
			
			if( _playToggle.isPlay ) {
				dispatchEvent( new VideoControlEvent( VideoControlEvent.PLAY ) );
			}
			
		}
		
		
		
		private function onPlayToggle( e:ToggleEvent ):void {
			
			if( e.isTrue ) {
				onVideoPlay();
			} 
			else {
				onVideoPause();
			}
			
		}
		
		
		
		private function onFullScreen( e:MouseEvent ):void {
			
			dispatchEvent( new FullScreenEvent( FullScreenEvent.FULL_SCREEN ) );
			
		}
		
		
		
	}
}