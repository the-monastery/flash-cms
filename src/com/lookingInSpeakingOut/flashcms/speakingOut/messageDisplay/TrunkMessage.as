package com.lookingInSpeakingOut.flashcms.speakingOut.messageDisplay {
	
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.XMLMessageData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.speakOut.ReplyEvent;
	
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.text.TextFieldAutoSize;
	import flash.utils.Timer;
	
	import speakingout.assets.msgTrunk.TrunkMsgAsset;

	[Event ( name="speakoutReply", type="com.lookingInSpeakingOut.flashcms.events.speakOut.ReplyEvent" )]
	[Event ( name="buildInComplete", type="com.lookingInSpeakingOut.flashcms.events.DisplayEvent" )]

	/**
	 * 
	 * @author ghostmonk 19-09-2009
	 * 
	 */
	public class TrunkMessage extends TrunkMsgAsset {
		
		
		private var _replyBtn:FrameLabelButton;
		private var _opentimer:Timer;
		private var _replyId:int;
		
		
		public function TrunkMessage() {
			
			alpha = 0;
			//_replyBtn = new FrameLabelButton( replyBtn );	
			//_replyBtn.addClickCallback( onReply );
			//_replyBtn.enable();
			mainText.autoSize = TextFieldAutoSize.LEFT;
			_opentimer = new Timer( 300, 1 );
			_opentimer.addEventListener( TimerEvent.TIMER_COMPLETE, onTimerComplete );
			
		}
		
		
		
		public function get replyId() : int {
			
			return _replyId;
			
		}
		
		
		
		public function buildIn( dest:Point ) : void {
			
			Tweener.removeTweens( this );
			scaleX = scaleY = 0;
			Tweener.addTween( this, { alpha:1, x:dest.x, y:dest.y, scaleX:1, scaleY:1, delay:0.2, time:0.3, onComplete:buildInComplete } );
			
		}
		
		
		
		public function buildOut( dest:Point ) : void {
			
			if( _opentimer ) {
				_opentimer.stop();
			}
			
			Tweener.addTween( this, { alpha:0, x:dest.x, y:dest.y, scaleX:0, scaleY:0, time:0.3, onComplete:removeFromParent } );
			Tweener.addTween( this, { alpha:0, scaleX:0, scaleY:0, time:0.2 } );
			
		}
		
		
		
		public function setData( data:XMLMessageData ) : void {
			
			mainText.text = data.text;
			postInfo.text = "Posted By: " + data.name + "\n" + "on " + data.date;
			//commentCount.text = "Replies: " + data.responseCount;
			_replyId = data.id;
			resizeAssetsToData();
			mainText.cacheAsBitmap = true;
			postInfo.cacheAsBitmap = true;
			//commentCount.cacheAsBitmap = true;
			
			scaleX = 0;
			scaleY = 0;
			
		}
		
		
		
		private function removeFromParent() : void {
			
			if( parent ) {
				parent.removeChild( this );
			}
			
		}
		
		
		
		private function resizeAssetsToData() : void {
			
			textBG.height = mainText.y + mainText.height + postInfo.height;
			postInfo.y = mainText.y + mainText.height;
			//commentCount.y = textBG.y + textBG.height;
			background.height = textBG.y + textBG.height + 38;
			//replyBtn.y = commentCount.y + 13;
			//hitarea.height = background.height;
			//hitarea.y = background.y;
			
		}
		
		
		
		private function onReply( e:MouseEvent ) : void {
			
			dispatchEvent( new ReplyEvent( ReplyEvent.SPEAKOUT_REPLY, _replyId ) );
			
		}
		
		
		
		private function buildInComplete() : void {
			
			if( _opentimer ) {			
				_opentimer.start();
			}
			
		}
		
		
		private function onTimerComplete( e:TimerEvent ) : void {
			
			_opentimer.removeEventListener( TimerEvent.TIMER_COMPLETE, onTimerComplete );
			_opentimer = null;
			dispatchEvent( new DisplayEvent( DisplayEvent.BUILD_IN_COMPLETE ) );			
			
		}
		
		
	}
}