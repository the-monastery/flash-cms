<?php

function printTableRows( $msqliResult, $currentPage ) {
	
	print <<< END
	<table cellspacing="0">
	<tr>
		<th>ID</th>
		<th>Name</th>
		<th>Story Text</th>
		<th>Date</th>
		<th>Is Mediated</th>
		<th>Is Approved</th>
		<th>Approve</th>
		<th>Reject</th>
	</tr>

END;
	
	$altBGTracker = 0;
	
	while( $row = mysqli_fetch_assoc( $msqliResult ) ){
		
		$name = stripslashes( $row[name] );
		$maintext = stripslashes( $row[text] );
		$date = stripslashes( $row[date] );
		$isMediated = stripslashes( $row[isMediated] );
		$isApproved = stripslashes( $row[isApproved] );
		$msgId = stripslashes( $row[id] );
		
		$pageArg = isset( $currentPage ) ? '&page='.$currentPage : '';
		
		$mediatedStatus = $isMediated == 1 ? "Yes" : "No";
		$approvedStatus = $isApproved == 1 ? "Yes" : "No";
		$rowClass = $altBGTracker % 2 == 0 ? "even" : "odd";
		
		$approveImg = "<a href='?approve=1&id=$msgId$pageArg'><img src='imgs/approve.gif' /></a>";
		$rejectImg = "<a href='?approve=0&id=$msgId$pageArg'><img src='imgs/deny.gif' /></a>";
		
		if( $isMediated == 1 ) {
			$approveImg = $isApproved == 1 ? "<img src='imgs/approveDisabled.gif' />" : $approveImg;
			$rejectImg = $isApproved == 1 ? $rejectImg : "<img src='imgs/denyDisabled.gif' />";
		}
		
		
		print <<< END

	<tr class="$rowClass">
		<td>$msgId</td>
		<td>$name</td>
		<td width="80%">$maintext</td>
		<td>$date</td>
		<td valign="center" align="center" width="35">$mediatedStatus</td>
		<td valign="center" align="center" width="35">$approvedStatus</td>
		<td valign="center" align="center">$approveImg</td>
		<td valign="center" align="center">$rejectImg</td>
	</tr>

END;
		$altBGTracker++;
	} 
	
	print <<< END
	
	</table>
END;

}

?>