package com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items {
	
	import caurina.transitions.Tweener;
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.ResourceData;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	
	import flash.events.DataEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	
	import items.resources.ResourceItemAsset;
	
	[Event (name="animationUpdate", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	
	/**
	 * Display object representing an data resource on the web (like a story, blog post etc)
	 * <p>Displays a Title, author and description of resource</p>  
	 * <p>dispatches a DataNotificationEvent when clicked, carrying a url as a parameter</p>
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class ResourceItem extends ResourceItemAsset {
		
		
		
		private const AUTHOR_PREFIX:String = "Posted By: ";
		
		
		private var _request:URLRequest;
		private var _openYPosition:Number;
		
		
		
		/**
		 * 
		 * @param value
		 * 
		 */
		public function set openYPosition( value:Number ):void {
			
			_openYPosition = value;
			
		}
		
		
		
		/**
		 * 
		 * @param data
		 * 
		 */
		public function ResourceItem( data:ResourceData ) {
			
			
			_request = data.request;
			_openYPosition = 0;
			
			description.text = data.description;
			title.text = data.title;
			author.text = AUTHOR_PREFIX + data.author + ": " + data.date;
			alpha = 0;
			
			mouseChildren = false;
			disable();
			//store x coordinate position for use in transition animations
		}
		
		
		
		/**
		 * enable button like interactivity and animate in
		 * 
		 * @param parent
		 * @param delay
		 * 
		 */
		public function buildIn():void
		{
			enable();
			
			Tweener.addTween (
				this, {
					y:_openYPosition,
					scaleY:1,
					alpha:1,
					time:0.3,
					onUpdate:animUpdate,
					onComplete:animUpdate
				}
			);
			
		}
		
		
		
		/**
		 * disable button like interactivity and animate out removing from display list when complete
		 * 
		 */
		public function buildOut():void {
			
			Tweener.addTween (
				this, {
					y:0,
					scaleY:0,
					alpha:0,
					time:0.3,
					onUpdate:animUpdate,
					onComplete:onTransitionComplete
				}
			);
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function enable():void {
			
			buttonMode = true;
			addEventListener( MouseEvent.CLICK, onClick );
			addEventListener( MouseEvent.ROLL_OVER, onRollover );
			addEventListener( MouseEvent.ROLL_OUT, onRollout );
			
		}
		
		
		
		private function disable():void {
			
			buttonMode = false;
			removeEventListener( MouseEvent.CLICK, onClick );
			removeEventListener( MouseEvent.ROLL_OVER, onRollover );
			removeEventListener( MouseEvent.ROLL_OUT, onRollout );
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
			
			disable();
			dispatchEvent( new DataEvent( DataEvent.DATA, false, false, _request.url ) );
			
		}
		
		
		
		private function onRollout( e:MouseEvent ):void {
			
		}
		
		
		
		private function onRollover( e:MouseEvent ):void {
			
		}
		
		
		
		private function animUpdate():void {
			
			dispatchEvent( new SectionEvent( SectionEvent.SECTION_VIEW_UPDATE ) );
			
		}
		
		
		
		private function onTransitionComplete():void {
			
			if( parent ) {
				parent.removeChild( this );
			}
			
			animUpdate();			
			
		}
		
		
		
	}
}