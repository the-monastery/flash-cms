package com.lookingInSpeakingOut.flashcms.lookingIn.sections {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.events.MeasureEvent;
	import com.ghostmonk.media.video.events.VideoLoadEvent;
	import com.ghostmonk.ui.composed.ClickableSprite;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.PaginationEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickrGalleryEvent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.ContentDefinition;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.Categories;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.DiariesContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.GalleryContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.NewsContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.ResourcesContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.VideosContent;
	import com.lookingInSpeakingOut.flashcms.ui.ScrollerAsset;
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	
	import items.ReturnTabAsset;
	
	[Event (name="loadVideo", type="com.ghostmonk.events.video.VideoLoadEvent")]
	[Event (name="photoGalleryClick", type="com.lookingspeakingout.flashcms.events.FlickrGalleryEvent")]
	[Event (name="photoGalleryData", type="com.lookingspeakingout.flashcms.events.FlickrGalleryEvent")]
	
	
	/**
	 * Content Display
	 * 
	 * @author ghostmonk 03/01/2009
	 * 
	 */
	public class MajorSection extends BaseSection {
		
		
		
		private var _returnTab:ClickableSprite;
		private var _request:URLRequest;
		private var _label:String;
		
		
		
		/**
		 * 
		 * @param data
		 * @param onClose
		 * 
		 */
		public function MajorSection( data:SectionData, closeBtn:MovieClip, scrollerAsset:ScrollerAsset ) {
			
			_label = data.label;
			super( data, closeBtn, scrollerAsset );
			
			_returnTab = new ClickableSprite( new ReturnTabAsset(), showList );
			_returnTab.view.x = _returnTab.view.width;
			
			content = createContent( ContentDefinition.getDefinition( data.factoryId ), data.contentWidth, data.contentHeight );
			content.addEventListener( SectionEvent.SECTION_VIEW_UPDATE, onDisplayUpdate );
			content.addEventListener( MeasureEvent.HEIGHT_READY, onContentReady );	
			
			_request = data.request;
			content.load( _request );
			
		}
		
		
		
		override public function createContent( type:ContentDefinition, width:int, height:int ) : BaseContent {
			
			var output:BaseContent;
			switch( type ) {
				
				case ContentDefinition.DIARIES :
					output = new DiariesContent( width, height );
					break;
				
				case ContentDefinition.GALLERY :
					output = new GalleryContent( width, height );
					output.addEventListener( SectionEvent.ITEM_CLICKED, showReturnTab );
					output.addEventListener( FlickrGalleryEvent.PHOTO_GALLERY_CLICK, onPhotoGallery );
					output.addEventListener( FlickrGalleryEvent.PHOTO_GALLERY_DATA, onPhotoGallery );
					break;
				
				case ContentDefinition.NEWS :
					output = new NewsContent( width, height );
					output.addEventListener( SectionEvent.ITEM_CLICKED, showReturnTab );
					output.addEventListener( PaginationEvent.PAGE_NAVIGATION, onPageNavigation );
					break;
				
				case ContentDefinition.RESOURCES :
					output = new ResourcesContent( width, height );
					output.addEventListener( SectionEvent.ITEM_CLICKED, showReturnTab );
					break;
				
				case ContentDefinition.VIDEO :
					output = new VideosContent( width, height );
					output.addEventListener( VideoLoadEvent.LOAD_VIDEO, onVideoLoad );
					output.addEventListener( PaginationEvent.PAGE_NAVIGATION, onPageNavigation );
					break;
				
				case ContentDefinition.WEB_LINKS :
					output = new Categories( width, height );
					break;
				
			}
			
			return output;
			
		}
		
		

		public function positionTitleX( xVal:Number ):void {
			
			Tweener.addTween( title, { x:xVal, time:0.3 } );
			
		}
		
		
		
		public function positionTitleRight():void {
			
			title.x = configWidth - title.testWidth( _label.toString() ) - 20;
			
		}
		
		

		override protected function doLayout( event:Event = null ):void {
			
			title.y = -( title.height + 40 );	
			closeBtn.view.x = configWidth;
			closeBtn.view.y = 0;
			closeBtn.view.alpha = 0.75;
			content.x = 15;
			content.y = 15;
			scroller.view.x = configWidth - 23;
			scroller.view.y = content.y + 5;		
			
			displayTweens.createBuildIn( _label.toString(), configWidth - 23, content.y + 5 );
			displayTweens.createBuildOut( this, configWidth * 0.5, configHeight * 0.5, 0.1 );
			
			super.doLayout( event );
			
		}
		
		
		
		private function showReturnTab( event:SectionEvent = null ):void {
			
			resetScroll();
			
			_returnTab.view.alpha = 0;
			_returnTab.view.y = ( configHeight - _returnTab.view.height ) / 2;
			
			Tweener.addTween( _returnTab.view, { alpha:1, x:0, time:0.3, transition:Equations.easeNone } );
			bg.addChild( _returnTab.view );
			
		}
		
		
		
		private function showList( e:MouseEvent ):void {
			
			Tweener.addTween(
				_returnTab.view, {
					alpha:0, 
					x:_returnTab.view.width, 
					time:0.3, 
					transition:Equations.easeNone,
					onUpdate:onDisplayUpdate,
					onUpdateParams:[ null ], 
					onComplete:bg.removeChild, onCompleteParams:[_returnTab.view]
				}
			);
			
			content.listView();
			
		}
		
		
		
		private function onDisplayUpdate( event:Event ):void {
			
			resetScroll();
			
			if( content.fullHeight <= content.height ) {
				disableScrollWheel()
			}
			else {
				enableScrollWheel(); 	
			}
			
			scroller.setUp( content.fullHeight, content.height );
			
		}
		
		
		
		private function onPageNavigation( e:PaginationEvent ) : void {
			
			content.removeItems();
			showLoadIndicator();
			
			if( e.page == 0 ) {
				content.load( _request );
			}
			else {
				var url:String = _request.url.split( "." )[0] + "-" + e.page + "0.xml";
				content.load( new URLRequest( url ) );
			}
			
		}
		
		
		
		private function onVideoLoad( e:VideoLoadEvent ):void {
			
			dispatchEvent(e);
			
		}
		
		
		
		private function onPhotoGallery( e:FlickrGalleryEvent ):void {
			
			dispatchEvent( e );
			
		}
		
		
		
	}
}