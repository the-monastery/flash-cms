package com.lookingInSpeakingOut.flashcms.ui {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.composed.ClickableSprite;
	
	import flash.display.Sprite;
	import flash.geom.Point;

	public class HelpBtn extends ClickableSprite {
		
		
		
		private var _buildInPoint:Point;
		private var _buildOutPoint:Point
		private var _fullWidth:Number;
		private var _fullHeight:Number;
		
		
		
		public function HelpBtn(view:Sprite, clickCall:Function) {
			
			super(view, clickCall);
			_buildInPoint = new Point( view.x, view.y );
			_buildOutPoint = new Point( view.x + _fullWidth * 0.5, view.y + _fullHeight * 0.5 );
			_fullWidth = view.width;
			_fullHeight = view.height;
			
			view.alpha = 0;
			
		}
		
		
		
		public function buildIn() : void {
			
			Tweener.addTween( view, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function buildOut() : void {
			
			Tweener.addTween( view, { alpha:0, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
	}
}