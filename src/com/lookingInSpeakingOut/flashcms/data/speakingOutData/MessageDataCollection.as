package com.lookingInSpeakingOut.flashcms.data.speakingOutData {
	
	/**
	 * 
	 * @author ghostmonk 2009-09-20
	 * 
	 */
	public class MessageDataCollection {
		
		
		private var _messages:Array;
		private var _totalPages:int;
		private var _currentPage:int;
		
		
		public function MessageDataCollection() {
			
			_messages = new Array();
			
		}
		
		
		
		public function get totalPages() : int {
			
			return _totalPages;
			
		}
		
		
		
		public function get currentPage() : int {
			
			return _currentPage;
			
		}
		
		
		
		public function get messages() : Array {
			
			return _messages;
			
		}
		
		
		
		public function get ids() : Array {
			
			var output:Array = new Array();
			
			for each( var data:XMLMessageData in _messages ) {
				output.push( data.id );
			}
			
			return output;
			
		}
		
		
		
		public function createMessages( xml:XML ) : void {
				
			deleteMessages();
			_totalPages = xml.@totalPages;
			_currentPage = xml.@currentPage;
			for each( var msgData:XML in xml.message ) {
				var data:XMLMessageData = new XMLMessageData( msgData )
				_messages.push( data );
			}
			
		}
		
		
		
		public function deleteMessages() : void {
			
			for each( var item:XMLMessageData in _messages ) {
				item.deleteData();
				item = null;
			}
			
			_messages.length = 0;
			_messages = null;
			_messages = new Array();
			
		}
		
		
		
		public function toString() : String {
			
			var output:String = "";
			for each( var data:XMLMessageData in _messages ) {
				output += "\n" + data.toString();
			}
			return output;
			
		}
		

	}
}