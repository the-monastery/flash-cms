package com.lookingInSpeakingOut.flashcms.framework.controller {
	
	import background.CloudBG;
	
	import com.ghostmonk.display.graphics.shapes.Polygon;
	import com.lookingInSpeakingOut.flashcms.AppFacade;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.flickrViewer.FlickrGallery;
	import com.lookingInSpeakingOut.flashcms.framework.model.ConfigProxy;
	import com.lookingInSpeakingOut.flashcms.framework.view.FlickrGalleryMediator;
	import com.lookingInSpeakingOut.flashcms.framework.view.LandscapeMediator;
	import com.lookingInSpeakingOut.flashcms.framework.view.MajorSectionManagerMediator;
	import com.lookingInSpeakingOut.flashcms.framework.view.MinorSectionManagerMediator;
	import com.lookingInSpeakingOut.flashcms.framework.view.NavContainerMediator;
	import com.lookingInSpeakingOut.flashcms.framework.view.StageMediator;
	import com.lookingInSpeakingOut.flashcms.framework.view.VideoPlayerMediator;
	import com.lookingInSpeakingOut.flashcms.globals.Fonts;
	import com.lookingInSpeakingOut.flashcms.lookingIn.clients.MajorSectionManager;
	import com.lookingInSpeakingOut.flashcms.lookingIn.clients.MinorSectionManager;
	import com.lookingInSpeakingOut.flashcms.navigation.Menu;
	import com.lookingInSpeakingOut.flashcms.navigation.NavBackground;
	import com.lookingInSpeakingOut.flashcms.navigation.NavContainer;
	import com.lookingInSpeakingOut.flashcms.rootView.Header;
	import com.lookingInSpeakingOut.flashcms.rootView.Sky;
	import com.lookingInSpeakingOut.flashcms.rootView.Title;
	import com.lookingInSpeakingOut.flashcms.speakingOut.Landscape;
	import com.lookingInSpeakingOut.flashcms.video.VideoPlayer;
	
	import flash.display.Bitmap;
	import flash.display.Stage;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	
	/**
	 * this Command initializes components of the application after the configuration file has been loaded
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class CreationCommand extends SimpleCommand {
		
		
		
		private var _timer:Timer;
		private var _startingSection1:XML;
		private var _startingSection2:XML;
		
		
		
		override public function execute( notification:INotification ):void {
			
			var configData:ConfigProxy = facade.retrieveProxy( ConfigProxy.NAME ) as ConfigProxy;	
			var containerNode:XML = configData.majorSections.section[ 0 ].container[ 0 ];
			var landscape:Landscape = new Landscape( configData.messageSubmitEndpoint, configData.helpData.title, configData.helpData.mainText );
			
			createStageMediator( notification.getBody() as Stage, landscape );
			createLandscapeMediator( landscape );
			createNavigation( configData.navContainer, configData.majorSections, configData.minorSections, configData.arrowColor );
			createMajorSections( containerNode.@width, containerNode.@height );
			createMinorSections();		
			createVideoPlayer();
			createFlickerGallery( configData.policyFiles );
			
			_startingSection1 = configData.majorSections.section[ 0 ];
			_startingSection2 = configData.majorSections.section[ 1 ];
			startMainSectionTimer();
			
		}
		
		
		
		private function startMainSectionTimer() : void {
			
			_timer = new Timer( 4500, 1 );
			_timer.addEventListener( TimerEvent.TIMER, onTimer );
			_timer.start();
			
		}
		
		
		private function onTimer( e:TimerEvent ) : void {
			
			_timer.stop();
			_timer.removeEventListener( TimerEvent.TIMER, onTimer );
			_timer = null;
			
			sendNotification( AppFacade.SHOW_LOOKING_IN, { section1:_startingSection1, section2:_startingSection2 } );
			
		}
		
		
		
		private function createStageMediator( stage:Stage, landscape:Landscape ) : void {
			
			var clouds:Bitmap = new Bitmap( new CloudBG( 0, 0 ) );
			var title:Title = new Title();
			title.tabEnabled = false;
			register( new StageMediator( stage, new Sky( clouds ), landscape, new Header(), title ) );
			
		}
		
		
		
		private function createLandscapeMediator( landscape:Landscape ) : void {
			
			register( new LandscapeMediator( landscape ) );
			
		}
		
		
		
		private function createMajorSections( majorWidth:Number, majorHeight:Number ) : void {
			
			register( new MajorSectionManagerMediator( new MajorSectionManager( majorWidth, majorHeight ) ) );
			
		}
		
		
		
		private function createMinorSections() : void {
			
			register( new MinorSectionManagerMediator( new MinorSectionManager() ) );
			
		}
		
		
		
		private function createNavigation( config:XML, majorSections:XML, minorSections:XML, arrowColor:uint ) : void {
			
			var navBg:NavBackground = new NavBackground( config );
			var mainMenu:Menu = new Menu(  NavigationEvent.MAJOR_SECTION, majorSections, Fonts.BIG_ED.fontName, true );
			var subMenu:Menu = new Menu(  NavigationEvent.MINOR_SECTION, minorSections, Fonts.VERDANA.fontName );
			
			mainMenu.tabChildren = false;
			subMenu.tabChildren = false;
			
			register(
				new NavContainerMediator(
					new NavContainer( navBg, mainMenu, subMenu, new Polygon( 3, 9, arrowColor, -90, 4 / 7 ) )
				)
			);
			
		}
		
		
		
		private function createVideoPlayer() : void {
			
			facade.registerMediator( new VideoPlayerMediator( new VideoPlayer() ) );
			
		}
		
		
		
		private function createFlickerGallery( policyFiles:XML ) : void {
			
			facade.registerMediator( new FlickrGalleryMediator( new FlickrGallery( policyFiles ) ) );
			
		}
		
		
		
		private function register( item:Mediator ) : void {
			
			facade.registerMediator( item );
			
		}
		
		
	}
}