#
# Looking In Speaking Out Database Dump
# MODx 0.9.6.3 (rev 4565)
# 
# Host: external-db.s52268.gridserver.com
# Generation Time: Oct 12, 2009 am31 10:56
# Server version: 4.1.25-Debian_mt1
# PHP Version: 5.2.6
# Database : `db52268_modX`
#

# --------------------------------------------------------

#
# Table structure for table `modx_active_users`
#

CREATE TABLE `modx_active_users` (
  `internalKey` int(9) NOT NULL default '0',
  `username` varchar(50) NOT NULL default '',
  `lasthit` int(20) NOT NULL default '0',
  `id` int(10) default NULL,
  `action` varchar(10) NOT NULL default '',
  `ip` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data about active users.';

#
# Dumping data for table `modx_active_users`
#

INSERT INTO `modx_active_users` VALUES ('1','admin','1255370174','','93','70.55.106.69');
INSERT INTO `modx_active_users` VALUES ('2','aDavis','1254948576','','8','24.78.210.169');

# --------------------------------------------------------

#
# Table structure for table `modx_categories`
#

CREATE TABLE `modx_categories` (
  `id` int(11) NOT NULL auto_increment,
  `category` varchar(45) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Categories to be used snippets,tv,chunks, etc';

#
# Dumping data for table `modx_categories`
#


# --------------------------------------------------------

#
# Table structure for table `modx_document_groups`
#

CREATE TABLE `modx_document_groups` (
  `id` int(10) NOT NULL auto_increment,
  `document_group` int(10) NOT NULL default '0',
  `document` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `document` (`document`),
  KEY `document_group` (`document_group`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_document_groups`
#


# --------------------------------------------------------

#
# Table structure for table `modx_documentgroup_names`
#

CREATE TABLE `modx_documentgroup_names` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  `private_memgroup` tinyint(4) default '0' COMMENT 'determine whether the document group is private to manager users',
  `private_webgroup` tinyint(4) default '0' COMMENT 'determines whether the document is private to web users',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_documentgroup_names`
#


# --------------------------------------------------------

#
# Table structure for table `modx_event_log`
#

CREATE TABLE `modx_event_log` (
  `id` int(11) NOT NULL auto_increment,
  `eventid` int(11) default '0',
  `createdon` int(11) NOT NULL default '0',
  `type` tinyint(4) NOT NULL default '1' COMMENT '1- information, 2 - warning, 3- error',
  `user` int(11) NOT NULL default '0' COMMENT 'link to user table',
  `usertype` tinyint(4) NOT NULL default '0' COMMENT '0 - manager, 1 - web',
  `source` varchar(50) NOT NULL default '',
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `user` (`user`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1 COMMENT='Stores event and error logs';

#
# Dumping data for table `modx_event_log`
#

INSERT INTO `modx_event_log` VALUES ('1','1','1231604282','3','0','0','Ditto 2.1.0','/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/assets/snippets/ditto/formats/\'rss\' .format.inc.php does not exist. Please check the file.');
INSERT INTO `modx_event_log` VALUES ('2','1','1236900870','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('3','1','1236900888','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('4','1','1236900944','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('5','1','1236901087','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('6','1','1236901097','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>3</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('7','1','1236901137','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR, expecting \',\' or \';\'</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR, expecting \',\' or \';\' in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('8','1','1236901155','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR, expecting \',\' or \';\'</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR, expecting \',\' or \';\' in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('9','1','1236901198','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('10','1','1236901288','3','0','0','testing - Snippet','<b>syntax error, unexpected T_OBJECT_OPERATOR</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_OBJECT_OPERATOR in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('11','1','1236904019','3','0','0','testing - Snippet','<b>syntax error, unexpected \'[\'</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected \'[\' in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>2</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('12','1','1236906272','3','0','0','testing - Snippet','<b>syntax error, unexpected T_STRING, expecting \')\'</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_STRING, expecting \')\' in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>17</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('13','1','1237850637','3','0','0','testing - Snippet','<b>syntax error, unexpected T_VARIABLE</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_VARIABLE in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>6</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('14','1','1237850657','3','0','0','testing - Snippet','<b>syntax error, unexpected T_VARIABLE</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_VARIABLE in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>6</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('15','1','1237852472','3','0','0','testing - Snippet','<b>syntax error, unexpected T_LNUMBER, expecting \']\'</b><br /><br /> <br />\r\n<b>Parse error</b>:  syntax error, unexpected T_LNUMBER, expecting \']\' in <b>/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/manager/includes/document.parser.class.inc.php(769) : eval()\'d code</b> on line <b>5</b><br />\r\n');
INSERT INTO `modx_event_log` VALUES ('16','1','1244414035','3','0','0','Ditto 2.1.0','/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/assets/snippets/ditto/formats/.format.inc.php does not exist. Please check the file.');
INSERT INTO `modx_event_log` VALUES ('17','1','1244414038','3','0','0','Ditto 2.1.0','/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/assets/snippets/ditto/formats/.format.inc.php does not exist. Please check the file.');
INSERT INTO `modx_event_log` VALUES ('18','1','1249759717','3','0','0','testing - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> 1237672336Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('19','1','1249759780','3','0','0','testing - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> 2Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('20','1','1249759887','3','0','0','testing - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> 2Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('21','1','1249760030','3','0','0','testing - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> 83Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('22','1','1249760143','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> 2Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('23','1','1249760315','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison DavisSat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('24','1','1249760363','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('25','1','1249760424','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('26','1','1249760451','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('27','1','1249760457','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('28','1','1249760693','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('29','1','1249771880','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('30','1','1249775947','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('31','1','1249779290','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('32','1','1250267450','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('33','1','1251862819','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('34','1','1251862955','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('35','1','1251862980','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('36','1','1251942573','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('37','1','1252512084','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Nicholas Hillier Sun, 22-03-2009');
INSERT INTO `modx_event_log` VALUES ('38','1','1252512297','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('39','1','1252881502','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('40','1','1252978656','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('41','1','1252978660','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('42','1','1252978667','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('43','1','1252978671','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('44','1','1253048606','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('45','0','1253071260','3','0','0','Parser','\r\n              <html><head><title>MODx Content Manager  &raquo; </title>\r\n              <style>TD, BODY { font-size: 11px; font-family:verdana; }</style>\r\n              <script type=\'text/javascript\'>\r\n              function copyToClip()\r\n              {\r\n                holdtext.innerText = sqlHolder.innerText;\r\n                Copied = holdtext.createTextRange();\r\n                Copied.execCommand(\'Copy\');\r\n              }\r\n            </script>\r\n              </head><body>\r\n              <h3 style=\'color:red\'>&laquo; MODx Parse Error &raquo;</h3>\r\n                    <table border=\'0\' cellpadding=\'1\' cellspacing=\'0\'>\r\n                    <tr><td colspan=\'3\'>MODx encountered the following error while attempting to parse the requested resource:</td></tr>\r\n                    <tr><td colspan=\'3\'><b style=\'color:red;\'>&laquo; Execution of a query to the database failed - You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'pub_date\' at line 4 &raquo;</b></td></tr><tr><td colspan=\'3\'><b style=\'color:#999;font-size: 9px;\'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SQL:&nbsp;<span id=\'sqlHolder\'>SELECT DISTINCT sc.id,sc.published FROM `db52268_modX`.`modx_site_content` sc\r\n		LEFT JOIN `db52268_modX`.`modx_document_groups` dg on dg.document = sc.id\r\n		WHERE sc.id IN (28,29,30,31,45,46,118,119,120,121,122,130,131,132,133,134,135,136,137,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,176,181,186) AND sc.published=1 AND sc.deleted=0 AND sc.isfolder = 0\r\n		AND (sc.privateweb=0) GROUP BY sc.id ORDER BY sc.id pub_date  </span></b>\r\n                    <br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=\'javascript:copyToClip();\' style=\'color:#821517;font-size: 9px; text-decoration: none\'>[Copy SQL to ClipBoard]</a><textarea id=\'holdtext\' style=\'display:none;\'></textarea></td></tr><tr><td>&nbsp;</td></tr><tr><td colspan=\'3\'><b>Parser timing</b></td></tr><tr><td>&nbsp;&nbsp;MySQL: </td><td><i>0.0156 s</i></td><td>(<i>3 Requests</i>)</td></tr><tr><td>&nbsp;&nbsp;PHP: </td><td><i>0.0842 s</i></td><td>&nbsp;</td></tr><tr><td>&nbsp;&nbsp;Total: </td><td><i>0.0997 s</i></td><td>&nbsp;</td></tr></table></body></html>');
INSERT INTO `modx_event_log` VALUES ('46','1','1254828884','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('47','1','1254828892','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('48','1','1254828895','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('49','1','1254828899','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('50','1','1254971773','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('51','1','1255028220','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('52','1','1255217069','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');
INSERT INTO `modx_event_log` VALUES ('53','1','1255365342','3','0','0','authorDate - Snippet','<b>date() [<a href=\'function.date\'>function.date</a>]: It is not safe to rely on the system\'s timezone settings. Please use the date.timezone setting, the TZ environment variable or the date_default_timezone_set() function. In case you used any of those methods and you are still getting this warning, you most likely misspelled the timezone identifier. We selected \'America/Los_Angeles\' for \'PDT/-7.0/DST\' instead</b><br /><br /> Alison Davis Sat, 21-03-2009');

# --------------------------------------------------------

#
# Table structure for table `modx_keyword_xref`
#

CREATE TABLE `modx_keyword_xref` (
  `content_id` int(11) NOT NULL default '0',
  `keyword_id` int(11) NOT NULL default '0',
  KEY `content_id` (`content_id`),
  KEY `keyword_id` (`keyword_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Cross reference bewteen keywords and content';

#
# Dumping data for table `modx_keyword_xref`
#


# --------------------------------------------------------

#
# Table structure for table `modx_manager_log`
#

CREATE TABLE `modx_manager_log` (
  `id` int(10) NOT NULL auto_increment,
  `timestamp` int(20) NOT NULL default '0',
  `internalKey` int(10) NOT NULL default '0',
  `username` varchar(255) default NULL,
  `action` int(10) NOT NULL default '0',
  `itemid` varchar(10) default '0',
  `itemname` varchar(255) default NULL,
  `message` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4665 DEFAULT CHARSET=latin1 COMMENT='Contains a record of user interaction.';

#
# Dumping data for table `modx_manager_log`
#

INSERT INTO `modx_manager_log` VALUES ('1','1229918219','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2','1229918251','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('3','1229918251','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('4','1229918253','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('5','1229918261','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('6','1229918261','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('7','1229918264','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('8','1229918270','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('9','1229918272','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('10','1229918280','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('11','1229918284','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('12','1229918292','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('13','1229918299','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('14','1229918301','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('15','1229918305','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('16','1229918308','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('17','1229918314','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('18','1229918319','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('19','1229918337','1','admin','67','-','-','Removing locks');
INSERT INTO `modx_manager_log` VALUES ('20','1229918338','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('21','1229918341','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('22','1229918343','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('23','1229918355','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('24','1229918357','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('25','1229918360','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('26','1229918362','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('27','1229919467','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('28','1229946699','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('29','1230403578','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('30','1230404504','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('31','1230514980','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('32','1230514995','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('33','1230515034','1','admin','22','9','NewsPublisher','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('34','1230515066','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('35','1230515068','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('36','1230515075','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('37','1230515084','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('38','1230515087','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('39','1230515090','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('40','1230515091','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('41','1230515093','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('42','1230515102','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('43','1230520864','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('44','1230520866','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('45','1230520887','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('46','1230520912','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('47','1230520946','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('48','1230520955','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('49','1230520961','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('50','1230520966','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('51','1230520976','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('52','1230520983','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('53','1230520986','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('54','1230520988','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('55','1230520989','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('56','1230520996','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('57','1230521002','1','admin','67','-','-','Removing locks');
INSERT INTO `modx_manager_log` VALUES ('58','1230521005','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('59','1230521022','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('60','1231462830','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('61','1231462849','1','admin','3','1','MODx CMS Install Success','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('62','1231462856','1','admin','3','1','MODx CMS Install Success','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('63','1231462922','1','admin','3','1','MODx CMS Install Success','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('64','1231462929','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('65','1231464795','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('66','1231464828','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('67','1231464830','1','admin','3','1','Untitled document','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('68','1231464865','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('69','1231464873','1','admin','3','1','Untitled document','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('70','1231464875','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('71','1231464914','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('72','1231464917','1','admin','3','1','home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('73','1231464952','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('74','1231464964','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('75','1231464969','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('76','1231464976','1','admin','61','2','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('77','1231464999','1','admin','3','1','home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('78','1231465003','1','admin','3','1','home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('79','1231465004','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('80','1231465010','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('81','1231465013','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('82','1231465029','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('83','1231465031','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('84','1231465037','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('85','1231465040','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('86','1231465052','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('87','1231465054','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('88','1231465066','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('89','1231465069','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('90','1231465074','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('91','1231465084','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('92','1231465088','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('93','1231465099','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('94','1231465113','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('95','1231465121','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('96','1231465123','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('97','1231465129','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('98','1231465132','1','admin','5','2','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('99','1231465135','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('100','1231465136','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('101','1231465142','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('102','1231465149','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('103','1231465152','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('104','1231465155','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('105','1231465164','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('106','1231465188','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('107','1231465200','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('108','1231465203','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('109','1231465207','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('110','1231465213','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('111','1231465219','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('112','1231465222','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('113','1231465224','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('114','1231465391','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('115','1231465397','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('116','1231465405','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('117','1231465412','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('118','1231465416','1','admin','3','3','videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('119','1231465421','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('120','1231465423','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('121','1231465426','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('122','1231465428','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('123','1231465429','1','admin','3','3','videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('124','1231465431','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('125','1231465436','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('126','1231465438','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('127','1231465440','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('128','1231465443','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('129','1231465444','1','admin','3','6','links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('130','1231465446','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('131','1231465529','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('132','1231465537','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('133','1231465540','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('134','1231465592','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('135','1231465596','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('136','1231465673','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('137','1231465684','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('138','1231465721','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('139','1231465725','1','admin','19','-','New template','Creating a new template');
INSERT INTO `modx_manager_log` VALUES ('140','1231465745','1','admin','20','-','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('141','1231465746','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('142','1231465749','1','admin','16','3','Minimal Template','Editing template');
INSERT INTO `modx_manager_log` VALUES ('143','1231465802','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('144','1231465819','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('145','1231465832','1','admin','61','3','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('146','1231465835','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('147','1231465838','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('148','1231465842','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('149','1231465845','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('150','1231465848','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('151','1231465850','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('152','1231465853','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('153','1231465857','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('154','1231465860','1','admin','5','2','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('155','1231465862','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('156','1231465864','1','admin','3','3','videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('157','1231465866','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('158','1231465873','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('159','1231465876','1','admin','5','3','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('160','1231465879','1','admin','3','3','videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('161','1231465881','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('162','1231465882','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('163','1231465887','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('164','1231465890','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('165','1231465891','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('166','1231465894','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('167','1231465899','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('168','1231465904','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('169','1231465906','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('170','1231465908','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('171','1231465909','1','admin','3','6','links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('172','1231465918','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('173','1231465923','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('174','1231465926','1','admin','5','6','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('175','1231465929','1','admin','3','6','links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('176','1231465930','1','admin','3','7','terms','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('177','1231465932','1','admin','27','7','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('178','1231465937','1','admin','27','7','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('179','1231465940','1','admin','5','7','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('180','1231465943','1','admin','3','7','terms','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('181','1231465944','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('182','1231465946','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('183','1231465950','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('184','1231465953','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('185','1231465964','1','admin','61','4','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('186','1231465970','1','admin','61','5','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('187','1231465976','1','admin','3','6','links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('188','1231465982','1','admin','61','7','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('189','1231465992','1','admin','61','6','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('190','1231466000','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('191','1231466008','1','admin','16','3','Minimal Template','Editing template');
INSERT INTO `modx_manager_log` VALUES ('192','1231466021','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('193','1231466023','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('194','1231466058','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('195','1231466059','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('196','1231466191','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('197','1231466193','1','admin','16','3','Minimal Template','Editing template');
INSERT INTO `modx_manager_log` VALUES ('198','1231466215','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('199','1231466218','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('200','1231466225','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('201','1231466225','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('202','1231466228','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('203','1231466231','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('204','1231466244','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('205','1231466247','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('206','1231466270','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('207','1231466295','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('208','1231466305','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('209','1231466308','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('210','1231466318','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('211','1231466320','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('212','1231466325','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('213','1231466331','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('214','1231466334','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('215','1231466353','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('216','1231466418','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('217','1231466425','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('218','1231466428','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('219','1231466431','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('220','1231466437','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('221','1231466438','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('222','1231466444','1','admin','5','2','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('223','1231466445','1','admin','3','3','videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('224','1231466448','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('225','1231466452','1','admin','5','3','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('226','1231466453','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('227','1231466455','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('228','1231466465','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('229','1231466467','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('230','1231466468','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('231','1231466470','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('232','1231466474','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('233','1231466476','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('234','1231466484','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('235','1231466487','1','admin','3','6','links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('236','1231466489','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('237','1231466493','1','admin','5','6','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('238','1231466496','1','admin','3','6','links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('239','1231466497','1','admin','3','7','terms','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('240','1231466499','1','admin','27','7','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('241','1231466504','1','admin','5','7','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('242','1231466507','1','admin','3','7','terms','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('243','1231466524','1','admin','3','7','terms','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('244','1231466564','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('245','1231466566','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('246','1231466579','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('247','1231466583','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('248','1231466588','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('249','1231466596','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('250','1231466599','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('251','1231466606','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('252','1231466662','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('253','1231466684','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('254','1231466687','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('255','1231466692','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('256','1231466693','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('257','1231466769','1','admin','3','7','terms','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('258','1231466771','1','admin','27','7','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('259','1231466801','1','admin','3','7','terms','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('260','1231466808','1','admin','27','7','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('261','1231466813','1','admin','5','7','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('262','1231466816','1','admin','3','7','termsofuse','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('263','1231466817','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('264','1231466825','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('265','1231466838','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('266','1231466843','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('267','1231466851','1','admin','61','8','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('268','1231466858','1','admin','61','9','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('269','1231466892','1','admin','3','9','privacypolicy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('270','1231466894','1','admin','27','9','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('271','1231466941','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('272','1231466943','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('273','1231467064','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('274','1231467068','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('275','1231467070','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('276','1231467078','1','admin','5','2','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('277','1231467081','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('278','1231467091','1','admin','3','3','videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('279','1231467093','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('280','1231467098','1','admin','5','3','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('281','1231467101','1','admin','3','3','videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('282','1231467102','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('283','1231467104','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('284','1231467109','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('285','1231467113','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('286','1231467114','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('287','1231467116','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('288','1231467120','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('289','1231467123','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('290','1231467124','1','admin','3','6','links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('291','1231467126','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('292','1231467131','1','admin','5','6','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('293','1231467133','1','admin','3','6','links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('294','1231467135','1','admin','3','7','termsofuse','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('295','1231467136','1','admin','27','7','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('296','1231467142','1','admin','5','7','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('297','1231467144','1','admin','3','7','termsofuse','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('298','1231467146','1','admin','3','8','about','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('299','1231467148','1','admin','27','8','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('300','1231467152','1','admin','5','8','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('301','1231467155','1','admin','3','8','about','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('302','1231467156','1','admin','3','9','privacypolicy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('303','1231467158','1','admin','27','9','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('304','1231467163','1','admin','5','9','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('305','1231467166','1','admin','3','9','privacypolicy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('306','1231467483','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('307','1231467484','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('308','1231467490','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('309','1231467497','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('310','1231469018','1','admin','3','3','videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('311','1231469019','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('312','1231469020','1','admin','3','4','resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('313','1231469029','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('314','1231469032','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('315','1231471944','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('316','1231502766','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('317','1231546831','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('318','1231598464','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('319','1231598469','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('320','1231598681','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('321','1231598684','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('322','1231598686','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('323','1231598751','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('324','1231598753','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('325','1231598758','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('326','1231598760','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('327','1231598764','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('328','1231598825','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('329','1231598879','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('330','1231598882','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('331','1231598886','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('332','1231599089','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('333','1231599122','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('334','1231599132','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('335','1231600285','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('336','1231600290','1','admin','22','3','Ditto','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('337','1231600329','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('338','1231601505','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('339','1231601522','1','admin','22','3','Ditto','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('340','1231601796','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('341','1231601813','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('342','1231601815','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('343','1231602586','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('344','1231602595','1','admin','22','3','Ditto','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('345','1231602604','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('346','1231602605','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('347','1231602608','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('348','1231602641','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('349','1231602644','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('350','1231602646','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('351','1231602647','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('352','1231602657','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('353','1231602666','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('354','1231602670','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('355','1231602743','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('356','1231602746','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('357','1231602762','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('358','1231602772','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('359','1231602775','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('360','1231602780','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('361','1231602849','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('362','1231602852','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('363','1231602875','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('364','1231603010','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('365','1231603013','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('366','1231603024','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('367','1231603189','1','admin','3','1','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('368','1231603190','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('369','1231603213','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('370','1231603215','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('371','1231603217','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('372','1231603219','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('373','1231603224','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('374','1231603232','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('375','1231603235','1','admin','3','10','news','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('376','1231603237','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('377','1231603249','1','admin','5','10','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('378','1231603252','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('379','1231603258','1','admin','61','10','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('380','1231603265','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('381','1231603282','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('382','1231603291','1','admin','61','11','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('383','1231603294','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('384','1231603296','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('385','1231603337','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('386','1231603340','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('387','1231603352','1','admin','94','11','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('388','1231603352','1','admin','3','12','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('389','1231603356','1','admin','3','12','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('390','1231603357','1','admin','27','12','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('391','1231603365','1','admin','5','12','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('392','1231603369','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('393','1231603375','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('394','1231603378','1','admin','94','12','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('395','1231603378','1','admin','3','13','Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('396','1231603382','1','admin','94','13','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('397','1231603382','1','admin','3','14','Duplicate of Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('398','1231603388','1','admin','94','14','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('399','1231603388','1','admin','3','15','Duplicate of Duplicate of Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('400','1231603393','1','admin','3','13','Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('401','1231603395','1','admin','27','13','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('402','1231603402','1','admin','5','13','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('403','1231603405','1','admin','3','14','Duplicate of Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('404','1231603405','1','admin','3','13','Item 03','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('405','1231603407','1','admin','27','13','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('406','1231603410','1','admin','3','14','Duplicate of Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('407','1231603412','1','admin','27','14','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('408','1231603420','1','admin','5','14','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('409','1231603423','1','admin','3','14','Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('410','1231603423','1','admin','3','15','Duplicate of Duplicate of Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('411','1231603428','1','admin','27','15','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('412','1231603440','1','admin','5','15','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('413','1231603443','1','admin','3','15','item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('414','1231603447','1','admin','61','12','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('415','1231603453','1','admin','61','13','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('416','1231603457','1','admin','61','14','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('417','1231603463','1','admin','61','15','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('418','1231603724','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('419','1231603727','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('420','1231604072','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('421','1231604075','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('422','1231604111','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('423','1231604238','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('424','1231604240','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('425','1231604251','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('426','1231604276','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('427','1231604279','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('428','1231604302','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('429','1231604424','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('430','1231604427','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('431','1231604432','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('432','1231604484','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('433','1231604487','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('434','1231604551','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('435','1231604573','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('436','1231604576','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('437','1231604582','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('438','1231604591','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('439','1231604594','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('440','1231604597','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('441','1231604647','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('442','1231604650','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('443','1231604659','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('444','1231604684','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('445','1231604687','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('446','1231604730','1','admin','5','10','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('447','1231604733','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('448','1231604737','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('449','1231604739','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('450','1231604776','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('451','1231604779','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('452','1231604786','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('453','1231604833','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('454','1231604836','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('455','1231604841','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('456','1231604897','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('457','1231604901','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('458','1231604911','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('459','1231605091','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('460','1231605094','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('461','1231605100','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('462','1231605108','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('463','1231605111','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('464','1231605200','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('465','1231605201','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('466','1231605257','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('467','1231605259','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('468','1231605496','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('469','1231605498','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('470','1231605515','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('471','1231605518','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('472','1231605522','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('473','1231605533','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('474','1231605535','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('475','1231605544','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('476','1231605547','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('477','1231605552','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('478','1231605564','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('479','1231605567','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('480','1231605569','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('481','1231605571','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('482','1231605585','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('483','1231605588','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('484','1231605590','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('485','1231605611','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('486','1231605614','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('487','1231606398','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('488','1231606400','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('489','1231606408','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('490','1231606411','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('491','1231606411','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('492','1231606414','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('493','1231606419','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('494','1231606424','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('495','1231606424','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('496','1231606427','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('497','1231606437','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('498','1231606445','1','admin','5','2','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('499','1231606448','1','admin','3','2','features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('500','1231606454','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('501','1231606464','1','admin','5','2','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('502','1231606467','1','admin','3','2','Features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('503','1231606474','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('504','1231606481','1','admin','5','3','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('505','1231606484','1','admin','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('506','1231606485','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('507','1231606492','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('508','1231606495','1','admin','3','4','Resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('509','1231606500','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('510','1231606506','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('511','1231606509','1','admin','3','5','diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('512','1231606559','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('513','1231606568','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('514','1231606571','1','admin','3','5','Diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('515','1231606656','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('516','1231606667','1','admin','5','6','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('517','1231606671','1','admin','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('518','1231606697','1','admin','27','7','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('519','1231606709','1','admin','6','7','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('520','1231606715','1','admin','6','8','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('521','1231606722','1','admin','6','9','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('522','1231606732','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('523','1231606744','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('524','1231606745','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('525','1231606759','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('526','1231606767','1','admin','61','16','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('527','1231606773','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('528','1231606784','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('529','1231606787','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('530','1231606798','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('531','1231606807','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('532','1231606849','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('533','1231606869','1','admin','3','16','Feeds','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('534','1231606876','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('535','1231606894','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('536','1231606917','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('537','1231606922','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('538','1231606928','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('539','1231606937','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('540','1231606940','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('541','1231607190','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('542','1231607229','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('543','1231607232','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('544','1231607251','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('545','1231607265','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('546','1231607308','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('547','1231607313','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('548','1231607326','1','admin','27','19','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('549','1231607337','1','admin','5','19','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('550','1231607340','1','admin','3','19','Features Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('551','1231607343','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('552','1231607351','1','admin','5','18','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('553','1231607354','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('554','1231607604','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('555','1231607606','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('556','1231607612','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('557','1231607614','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('558','1231607620','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('559','1231607621','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('560','1231607626','1','admin','3','19','Features Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('561','1231607628','1','admin','27','19','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('562','1231607642','1','admin','3','16','Feeds','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('563','1231607648','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('564','1231607676','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('565','1231607689','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('566','1231607700','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('567','1231607701','1','admin','51','20','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('568','1231607707','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('569','1231607709','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('570','1231607717','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('571','1231607720','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('572','1231607801','1','admin','94','11','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('573','1231607802','1','admin','3','21','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('574','1231607806','1','admin','51','21','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('575','1231607813','1','admin','52','21','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('576','1231607821','1','admin','3','21','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('577','1231607823','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('578','1231607837','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('579','1231607840','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('580','1231607845','1','admin','61','21','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('581','1231607868','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('582','1231607878','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('583','1231607884','1','admin','61','22','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('584','1231607923','1','admin','94','21','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('585','1231607924','1','admin','3','23','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('586','1231607927','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('587','1231607933','1','admin','94','21','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('588','1231607933','1','admin','3','24','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('589','1231607942','1','admin','94','21','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('590','1231607942','1','admin','3','25','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('591','1231607948','1','admin','94','21','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('592','1231607949','1','admin','3','26','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('593','1231607954','1','admin','3','23','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('594','1231607958','1','admin','27','23','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('595','1231607965','1','admin','5','23','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('596','1231607968','1','admin','3','23','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('597','1231607971','1','admin','27','24','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('598','1231607981','1','admin','5','24','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('599','1231607984','1','admin','3','24','Item 03','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('600','1231607989','1','admin','27','25','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('601','1231608000','1','admin','5','25','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('602','1231608003','1','admin','3','25','Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('603','1231608004','1','admin','27','26','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('604','1231608011','1','admin','5','26','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('605','1231608015','1','admin','3','26','Item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('606','1231608018','1','admin','61','23','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('607','1231608023','1','admin','61','24','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('608','1231608029','1','admin','61','25','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('609','1231608033','1','admin','61','26','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('610','1231608129','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('611','1231608139','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('612','1231608143','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('613','1231608148','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('614','1231608150','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('615','1231608155','1','admin','3','27','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('616','1231608162','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('617','1231608166','1','admin','5','27','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('618','1231608169','1','admin','3','27','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('619','1231608171','1','admin','94','27','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('620','1231608171','1','admin','3','28','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('621','1231608175','1','admin','94','27','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('622','1231608176','1','admin','3','29','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('623','1231608180','1','admin','94','27','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('624','1231608181','1','admin','3','30','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('625','1231608185','1','admin','94','27','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('626','1231608185','1','admin','3','31','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('627','1231608195','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('628','1231608199','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('629','1231608206','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('630','1231608219','1','admin','27','32','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('631','1231608223','1','admin','5','32','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('632','1231608227','1','admin','3','32','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('633','1231608231','1','admin','94','32','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('634','1231608231','1','admin','3','33','Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('635','1231608235','1','admin','27','33','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('636','1231608241','1','admin','94','33','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('637','1231608241','1','admin','3','34','Duplicate of Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('638','1231608247','1','admin','94','32','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('639','1231608247','1','admin','3','35','Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('640','1231608255','1','admin','94','32','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('641','1231608256','1','admin','3','36','Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('642','1231608326','1','admin','61','27','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('643','1231608332','1','admin','3','28','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('644','1231608336','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('645','1231608346','1','admin','5','28','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('646','1231608349','1','admin','3','28','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('647','1231608349','1','admin','3','29','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('648','1231608351','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('649','1231608359','1','admin','5','29','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('650','1231608362','1','admin','3','30','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('651','1231608362','1','admin','3','29','Item 03','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('652','1231608364','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('653','1231608368','1','admin','3','30','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('654','1231608371','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('655','1231608379','1','admin','5','30','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('656','1231608382','1','admin','3','30','Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('657','1231608382','1','admin','3','31','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('658','1231608385','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('659','1231608393','1','admin','5','31','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('660','1231608396','1','admin','3','31','Item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('661','1231608401','1','admin','61','28','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('662','1231608406','1','admin','61','29','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('663','1231608411','1','admin','61','30','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('664','1231608417','1','admin','61','31','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('665','1231608438','1','admin','3','33','Duplicate of Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('666','1231608440','1','admin','27','33','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('667','1231608447','1','admin','5','33','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('668','1231608450','1','admin','3','33','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('669','1231608451','1','admin','27','34','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('670','1231608458','1','admin','5','34','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('671','1231608461','1','admin','3','34','Item 03','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('672','1231608461','1','admin','27','35','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('673','1231608470','1','admin','5','35','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('674','1231608473','1','admin','3','35','Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('675','1231608502','1','admin','27','36','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('676','1231608740','1','admin','5','36','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('677','1231608743','1','admin','3','36','Item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('678','1231608752','1','admin','61','32','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('679','1231608757','1','admin','27','32','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('680','1231608762','1','admin','5','32','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('681','1231608765','1','admin','3','32','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('682','1231608767','1','admin','61','33','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('683','1231608777','1','admin','61','34','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('684','1231608783','1','admin','61','35','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('685','1231608790','1','admin','61','36','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('686','1231608822','1','admin','3','22','Footer Content','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('687','1231610129','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('688','1231610151','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('689','1231610160','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('690','1231610169','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('691','1231610186','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('692','1231610199','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('693','1231610231','1','admin','61','37','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('694','1231610239','1','admin','61','38','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('695','1231610243','1','admin','61','39','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('696','1231610247','1','admin','3','22','Footer Content','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('697','1231610249','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('698','1231610253','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('699','1231610258','1','admin','3','2','Features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('700','1231610261','1','admin','3','27','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('701','1231610263','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('702','1231610267','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('703','1231610269','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('704','1231610273','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('705','1231610276','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('706','1231610276','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('707','1231610280','1','admin','5','38','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('708','1231610284','1','admin','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('709','1231610287','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('710','1231610291','1','admin','5','39','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('711','1231610294','1','admin','3','39','Privacy Policy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('712','1231610802','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('713','1231610804','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('714','1231610955','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('715','1231610980','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('716','1231611051','1','admin','302','-','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('717','1231611052','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('718','1231611055','1','admin','301','1','[*image*]','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('719','1231611060','1','admin','302','1','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('720','1231611060','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('721','1231611066','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('722','1231611068','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('723','1231611069','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('724','1231611152','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('725','1231611265','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('726','1231611267','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('727','1231611268','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('728','1231612084','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('729','1231612086','1','admin','301','1','[*image*]','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('730','1231612112','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('731','1231612116','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('732','1231612127','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('733','1231612130','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('734','1231612133','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('735','1231612134','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('736','1231612139','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('737','1231612152','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('738','1231612156','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('739','1231612160','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('740','1231612166','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('741','1231612168','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('742','1231612169','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('743','1231612171','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('744','1231612174','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('745','1231612177','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('746','1231612180','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('747','1231612182','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('748','1231612183','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('749','1231612185','1','admin','301','1','[*image*]','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('750','1231612337','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('751','1231612339','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('752','1231612339','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('753','1231612570','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('754','1231612604','1','admin','200','-','-','Viewing phpInfo()');
INSERT INTO `modx_manager_log` VALUES ('755','1231612715','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('756','1231612824','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('757','1231612831','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('758','1231612891','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('759','1231612894','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('760','1231612894','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('761','1231612913','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('762','1231612930','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('763','1231612937','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('764','1231612959','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('765','1231612963','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('766','1231612965','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('767','1231612966','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('768','1231612984','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('769','1231612987','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('770','1231612991','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('771','1231612996','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('772','1231613079','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('773','1231613084','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('774','1231613086','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('775','1231613087','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('776','1231613212','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('777','1231613372','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('778','1231613374','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('779','1231613375','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('780','1231613375','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('781','1231613635','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('782','1231613687','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('783','1231613694','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('784','1231613699','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('785','1231613701','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('786','1231613722','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('787','1231613732','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('788','1231613763','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('789','1231615358','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('790','1231615362','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('791','1231615366','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('792','1231615367','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('793','1231615368','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('794','1231615513','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('795','1231615830','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('796','1231615836','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('797','1231615836','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('798','1231615840','1','admin','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('799','1231615843','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('800','1231615843','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('801','1231615846','1','admin','3','39','Privacy Policy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('802','1231615851','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('803','1231615851','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('804','1231620836','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('805','1231620841','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('806','1231620852','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('807','1231620852','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('808','1231620864','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('809','1231620866','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('810','1231620867','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('811','1231620895','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('812','1231620898','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('813','1231620904','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('814','1231620956','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('815','1231620960','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('816','1231620962','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('817','1231620964','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('818','1231620978','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('819','1231620982','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('820','1231620987','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('821','1231620995','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('822','1231621049','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('823','1231621053','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('824','1231621062','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('825','1231621062','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('826','1231621067','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('827','1231621071','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('828','1231621077','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('829','1231621090','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('830','1231621091','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('831','1231621098','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('832','1231621100','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('833','1231621112','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('834','1231621114','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('835','1231621117','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('836','1231621120','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('837','1231621123','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('838','1231621200','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('839','1231621205','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('840','1231621209','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('841','1231621231','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('842','1231621243','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('843','1231621247','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('844','1231621271','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('845','1231621274','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('846','1231621348','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('847','1231621351','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('848','1231621353','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('849','1231621381','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('850','1231621385','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('851','1231621388','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('852','1231621391','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('853','1231621411','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('854','1231621413','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('855','1231621419','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('856','1231621421','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('857','1231621425','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('858','1231621439','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('859','1231621440','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('860','1231621566','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('861','1231621608','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('862','1231621611','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('863','1231621612','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('864','1231621614','1','admin','5','10','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('865','1231621623','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('866','1231621626','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('867','1231621626','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('868','1231621629','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('869','1231621794','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('870','1231621797','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('871','1231622145','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('872','1231622145','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('873','1231622150','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('874','1231622153','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('875','1231622155','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('876','1231622156','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('877','1231622197','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('878','1231622200','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('879','1231622203','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('880','1231622210','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('881','1231622256','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('882','1231622258','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('883','1231622261','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('884','1231622264','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('885','1231622265','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('886','1231622331','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('887','1231622334','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('888','1231622365','1','admin','28','-','-','Changing password');
INSERT INTO `modx_manager_log` VALUES ('889','1231622372','1','admin','10','-','-','Viewing/ composing messages');
INSERT INTO `modx_manager_log` VALUES ('890','1231622394','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('891','1231622404','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('892','1231622407','1','admin','12','1','admin','Editing user');
INSERT INTO `modx_manager_log` VALUES ('893','1231622424','1','admin','32','1','-','Saving user');
INSERT INTO `modx_manager_log` VALUES ('894','1231622425','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('895','1231622478','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('896','1231622480','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('897','1231622482','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('898','1231622485','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('899','1231622487','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('900','1231622488','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('901','1231622528','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('902','1231622531','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('903','1231622559','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('904','1231622566','1','admin','5','21','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('905','1231622569','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('906','1231626465','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('907','1231626469','1','admin','77','-','New Chunk','Creating a new Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('908','1231626629','1','admin','79','-','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('909','1231626629','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('910','1231626639','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('911','1231626641','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('912','1231626675','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('913','1231626678','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('914','1231626700','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('915','1231626706','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('916','1231626707','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('917','1231626728','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('918','1231626731','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('919','1231626750','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('920','1231626760','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('921','1231626763','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('922','1231627050','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('923','1231627053','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('924','1231627053','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('925','1231627056','1','admin','78','2','NewRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('926','1231627231','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('927','1231627232','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('928','1231627242','1','admin','78','2','NewRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('929','1231627249','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('930','1231627250','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('931','1231627251','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('932','1231627256','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('933','1231627259','1','admin','27','21','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('934','1231627281','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('935','1231627283','1','admin','78','2','NewRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('936','1231627292','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('937','1231627293','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('938','1231627365','1','admin','78','2','NewRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('939','1231627377','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('940','1231627377','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('941','1231627384','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('942','1231627386','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('943','1231627395','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('944','1231627399','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('945','1231627581','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('946','1231627583','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('947','1231627587','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('948','1231627589','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('949','1231627593','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('950','1231627598','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('951','1231627608','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('952','1231627614','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('953','1231627616','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('954','1231627617','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('955','1231627623','1','admin','5','18','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('956','1231627626','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('957','1231627626','1','admin','27','19','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('958','1231627631','1','admin','5','19','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('959','1231627634','1','admin','3','19','Features Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('960','1231627635','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('961','1231627641','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('962','1231627645','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('963','1231627676','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('964','1231627680','1','admin','27','12','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('965','1231627681','1','admin','27','12','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('966','1231627697','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('967','1231627700','1','admin','301','1','[*image*]','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('968','1231627728','1','admin','302','1','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('969','1231627728','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('970','1231627755','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('971','1231627762','1','admin','302','1','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('972','1231627763','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('973','1231627767','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('974','1231627771','1','admin','27','12','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('975','1231627771','1','admin','27','12','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('976','1231627785','1','admin','27','13','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('977','1231627785','1','admin','27','13','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('978','1231627797','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('979','1231627798','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('980','1231627821','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('981','1231627838','1','admin','61','40','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('982','1231627849','1','admin','51','11','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('983','1231627860','1','admin','52','11','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('984','1231627885','1','admin','51','12','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('985','1231627891','1','admin','52','12','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('986','1231627896','1','admin','51','13','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('987','1231627915','1','admin','27','13','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('988','1231627916','1','admin','27','13','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('989','1231627921','1','admin','51','13','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('990','1231627925','1','admin','52','13','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('991','1231627931','1','admin','27','14','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('992','1231627931','1','admin','27','14','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('993','1231627933','1','admin','5','14','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('994','1231627936','1','admin','3','14','Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('995','1231627936','1','admin','51','14','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('996','1231627954','1','admin','52','14','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('997','1231627958','1','admin','51','15','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('998','1231627968','1','admin','52','15','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('999','1231627979','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1000','1231627980','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1001','1231628032','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1002','1231628044','1','admin','61','41','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1003','1231628052','1','admin','51','11','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1004','1231628054','1','admin','52','11','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1005','1231628061','1','admin','51','12','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1006','1231628065','1','admin','52','12','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1007','1231628070','1','admin','51','13','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1008','1231628074','1','admin','52','13','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1009','1231628078','1','admin','51','14','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1010','1231628083','1','admin','52','14','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1011','1231628089','1','admin','51','15','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1012','1231628092','1','admin','52','15','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1013','1231628150','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1014','1231628216','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1015','1231628219','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1016','1231628221','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1017','1231628245','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1018','1231628246','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1019','1231628257','1','admin','5','40','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1020','1231628261','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1021','1231628281','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1022','1231628282','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1023','1231628333','1','admin','5','40','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1024','1231628336','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1025','1231628356','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1026','1231628357','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1027','1231628377','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1028','1231628383','1','admin','61','42','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1029','1231628403','1','admin','27','42','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1030','1231628404','1','admin','27','42','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1031','1231628418','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1032','1231628419','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1033','1231628477','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1034','1231628484','1','admin','61','43','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1035','1231628694','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1036','1231628702','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1037','1231628710','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1038','1231628729','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1039','1231628732','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1040','1231628762','1','admin','62','42','-','Un-publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1041','1231628783','1','admin','6','43','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('1042','1231628790','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('1043','1231628945','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1044','1231628947','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1045','1231628974','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1046','1231628978','1','admin','12','1','admin','Editing user');
INSERT INTO `modx_manager_log` VALUES ('1047','1231628995','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1048','1231628996','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1049','1231629023','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1050','1231629023','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1051','1231629043','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1052','1231629049','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1053','1231629050','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1054','1231629079','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1055','1231629095','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1056','1231629096','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1057','1231629115','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1058','1231629606','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1059','1231629606','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1060','1231629623','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1061','1231629705','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1062','1231629706','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1063','1231631923','1','admin','3','22','Footer Content','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1064','1231631929','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1065','1231631929','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1066','1231631962','1','admin','5','22','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1067','1231631965','1','admin','3','22','Org Information','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1068','1231632038','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1069','1231632039','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1070','1231633419','1','admin','6','42','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('1071','1231633425','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('1072','1231633441','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1073','1231633441','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1074','1231633448','1','admin','5','10','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1075','1231633451','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1076','1231633454','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1077','1231633455','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1078','1231633459','1','admin','5','2','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1079','1231633463','1','admin','3','2','Features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1080','1231633471','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1081','1231633472','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1082','1231633477','1','admin','5','16','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1083','1231633480','1','admin','3','16','Feeds','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1084','1231633516','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1085','1231633517','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1086','1231633521','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1087','1231633525','1','admin','3','4','Resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1088','1231633529','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1089','1231633530','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1090','1231633535','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1091','1231633538','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1092','1231633538','1','admin','3','5','Diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1093','1231633541','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1094','1231633542','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1095','1231633550','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1096','1231633554','1','admin','3','5','Diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1097','1231633569','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1098','1231633570','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1099','1231633572','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1100','1231633573','1','admin','27','2','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1101','1231633575','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1102','1231633575','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1103','1231633581','1','admin','5','3','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1104','1231633584','1','admin','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1105','1231633589','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1106','1231633590','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1107','1231633594','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1108','1231633596','1','admin','3','5','Diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1109','1231633598','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1110','1231633599','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1111','1231633602','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1112','1231633603','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1113','1231633607','1','admin','5','6','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1114','1231633611','1','admin','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1115','1231633613','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1116','1231633613','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1117','1231633618','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1118','1231633618','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1119','1231633627','1','admin','5','16','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1120','1231633630','1','admin','3','16','Feeds','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1121','1231633682','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1122','1231633683','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1123','1231633700','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1124','1231633709','1','admin','61','44','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1125','1231633716','1','admin','51','21','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1126','1231633720','1','admin','52','21','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1127','1231633727','1','admin','51','23','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1128','1231633731','1','admin','52','23','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1129','1231633737','1','admin','51','24','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1130','1231633740','1','admin','52','24','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1131','1231633744','1','admin','51','25','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1132','1231633746','1','admin','52','25','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1133','1231633751','1','admin','51','26','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1134','1231633754','1','admin','52','26','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1135','1231633765','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1136','1231633766','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1137','1231633777','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1138','1231633784','1','admin','61','45','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1139','1231633788','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1140','1231633789','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1141','1231633814','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1142','1231633822','1','admin','51','27','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1143','1231633825','1','admin','52','27','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1144','1231633834','1','admin','61','46','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1145','1231633856','1','admin','51','28','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1146','1231633859','1','admin','52','28','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1147','1231633865','1','admin','51','29','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1148','1231633871','1','admin','52','29','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1149','1231633876','1','admin','51','30','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1150','1231633880','1','admin','52','30','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1151','1231633887','1','admin','51','31','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1152','1231633890','1','admin','52','31','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1153','1231633902','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1154','1231633903','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1155','1231633920','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1156','1231633927','1','admin','3','46','January','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1157','1231633928','1','admin','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1158','1231633929','1','admin','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1159','1231633936','1','admin','27','47','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1160','1231633939','1','admin','27','47','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1161','1231633942','1','admin','61','47','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1162','1231633947','1','admin','51','21','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1163','1231633949','1','admin','52','21','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1164','1231633954','1','admin','51','23','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1165','1231633956','1','admin','52','23','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1166','1231633959','1','admin','51','24','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1167','1231633962','1','admin','52','24','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1168','1231633966','1','admin','51','25','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1169','1231633969','1','admin','51','25','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1170','1231633972','1','admin','52','25','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1171','1231633978','1','admin','51','26','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1172','1231633981','1','admin','52','26','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1173','1231633994','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1174','1231633994','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1175','1231634008','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1176','1231634014','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1177','1231634014','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1178','1231634027','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1179','1231634033','1','admin','51','32','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1180','1231634035','1','admin','52','32','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1181','1231634039','1','admin','51','33','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1182','1231634043','1','admin','52','33','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1183','1231634047','1','admin','51','34','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1184','1231634050','1','admin','52','34','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1185','1231634057','1','admin','51','35','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1186','1231634060','1','admin','52','35','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1187','1231634065','1','admin','51','36','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1188','1231634068','1','admin','52','36','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1189','1231634073','1','admin','61','48','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1190','1231634079','1','admin','61','49','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1191','1231637011','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1192','1231637027','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1193','1231637038','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1194','1231637042','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1195','1231637044','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1196','1231637057','1','admin','5','18','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1197','1231637060','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1198','1231637061','1','admin','27','19','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1199','1231637072','1','admin','5','19','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1200','1231637076','1','admin','3','19','Features Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1201','1231637077','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1202','1231637082','1','admin','5','18','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1203','1231637085','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1204','1231637085','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1205','1231637091','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1206','1231637094','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1207','1231637095','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1208','1231637100','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1209','1231637111','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1210','1231637115','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1211','1231637184','1','admin','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1212','1231637186','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1213','1231637187','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1214','1231637196','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1215','1231637205','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1216','1231637208','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1217','1231686346','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1218','1231686369','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1219','1231686369','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1220','1231686371','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1221','1231686399','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1222','1231686399','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1223','1231686512','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1224','1231686516','1','admin','3','50','Untitled document','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1225','1231686521','1','admin','27','50','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1226','1231686527','1','admin','5','50','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1227','1231686530','1','admin','3','50','test','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1228','1231686532','1','admin','61','50','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1229','1231686559','1','admin','6','50','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('1230','1231686568','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('1231','1231722815','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1232','1231722835','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1233','1231722891','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1234','1231722969','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1235','1231722973','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1236','1231722974','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1237','1231722976','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1238','1231722983','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1239','1231722984','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1240','1231722994','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1241','1231722997','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1242','1231723002','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1243','1231723003','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1244','1231723012','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1245','1231723012','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1246','1231723019','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1247','1231723023','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1248','1231723026','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1249','1231723049','1','admin','3','22','Org Information','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1250','1231723053','1','admin','3','22','Org Information','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1251','1231723087','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1252','1231723087','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1253','1231723088','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1254','1231723089','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1255','1231723116','1','admin','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1256','1231723118','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1257','1231723119','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1258','1231723197','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1259','1231723212','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1260','1231723213','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1261','1231723232','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1262','1231723247','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1263','1231723248','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1264','1231723250','1','admin','3','13','Item 03','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1265','1231723250','1','admin','3','14','Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1266','1231723251','1','admin','3','15','item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1267','1231723254','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1268','1231723257','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1269','1231723270','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1270','1231723277','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1271','1231723278','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1272','1231723280','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1273','1231723282','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1274','1231723284','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1275','1231723313','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1276','1231723316','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1277','1231723324','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1278','1231723601','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1279','1231723602','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1280','1231723607','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1281','1231723607','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1282','1231723632','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1283','1231723632','1','admin','5','40','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1284','1231723632','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1285','1231723635','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1286','1231723636','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1287','1231723637','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1288','1231723653','1','admin','5','40','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1289','1231723656','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1290','1231723666','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1291','1231723668','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1292','1231723669','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1293','1231723689','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1294','1231723950','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1295','1231723952','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1296','1231724279','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1297','1231724296','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1298','1231724338','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1299','1231724339','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1300','1231724353','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1301','1231724356','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1302','1231724429','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1303','1231724448','1','admin','27','12','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1304','1231724449','1','admin','27','12','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1305','1231724451','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1306','1231724460','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1307','1231724605','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1308','1231724608','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1309','1231724611','1','admin','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1310','1231724614','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1311','1231727952','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1312','1231728005','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1313','1231770226','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1314','1231771919','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1315','1231771926','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1316','1231771950','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1317','1231771966','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1318','1231771971','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1319','1231771977','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('1320','1232160962','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1321','1232162058','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1322','1232162067','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('1323','1232162091','1','admin','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1324','1232162095','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1325','1232162096','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1326','1232162103','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1327','1232162109','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1328','1232162114','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1329','1232162119','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('1330','1232162158','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1331','1232162167','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('1332','1232162179','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1333','1232162184','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('1334','1232162258','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1335','1232162263','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('1336','1232162450','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1337','1232162456','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('1338','1232162467','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1339','1232231011','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1340','1232231084','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1341','1232231086','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1342','1232231089','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1343','1232231091','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1344','1232231095','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1345','1232231177','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1346','1232231184','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1347','1232231199','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1348','1232231200','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1349','1232231201','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1350','1232231203','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1351','1232238747','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1352','1232238935','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1353','1232238945','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1354','1232238948','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1355','1232238960','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1356','1232243559','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1357','1232243565','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1358','1232296266','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1359','1232298823','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1360','1232298962','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1361','1232321645','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1362','1232321909','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1363','1232322011','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1364','1232322176','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1365','1232322262','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1366','1232325061','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1367','1232325140','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('1368','1232327075','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1369','1232327080','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1370','1232327086','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1371','1232331235','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1372','1232331238','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1373','1232331241','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1374','1232331252','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1375','1232331255','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1376','1232331257','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1377','1232331260','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1378','1232331263','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1379','1232331275','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1380','1232331277','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1381','1232331280','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1382','1232331284','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1383','1232331286','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1384','1232331288','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1385','1232331290','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1386','1232331292','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1387','1232331296','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1388','1232331298','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1389','1232331312','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1390','1232331322','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1391','1232331325','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1392','1232333311','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1393','1232333643','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1394','1232333704','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1395','1232333735','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1396','1232333748','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1397','1232375318','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1398','1232375408','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1399','1232413746','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1400','1232413764','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1401','1232413772','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1402','1232416540','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1403','1232416549','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1404','1232416565','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1405','1232416570','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1406','1232416577','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1407','1232416580','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1408','1232416582','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1409','1232416586','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1410','1232416593','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1411','1232416593','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1412','1232416602','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1413','1232416607','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1414','1232416608','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1415','1232416611','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1416','1232416632','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1417','1232416633','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1418','1232416637','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1419','1232416677','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1420','1232416678','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1421','1232416680','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1422','1232416757','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1423','1232416769','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1424','1232416769','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1425','1232416862','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1426','1232416963','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1427','1232417040','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1428','1232417066','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1429','1232417078','1','admin','19','-','New template','Creating a new template');
INSERT INTO `modx_manager_log` VALUES ('1430','1232417266','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1431','1232417270','1','admin','19','-','New template','Creating a new template');
INSERT INTO `modx_manager_log` VALUES ('1432','1232417285','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1433','1232417287','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('1434','1232417294','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1435','1232417297','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1436','1232417301','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1437','1232417303','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1438','1232417311','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1439','1232417314','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1440','1232417324','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1441','1232417331','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1442','1232417345','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1443','1232417352','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1444','1232417366','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1445','1232417372','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1446','1232417377','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1447','1232417427','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1448','1232417440','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1449','1232417445','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1450','1232417456','1','admin','87','-','New web user','Create new web user');
INSERT INTO `modx_manager_log` VALUES ('1451','1232417484','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1452','1232417551','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1453','1232417559','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1454','1232417561','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1455','1232417567','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1456','1232417570','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1457','1232417651','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1458','1232417771','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1459','1232417800','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1460','1232417805','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1461','1232417861','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1462','1232417869','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1463','1232417879','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1464','1232417888','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1465','1232417896','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1466','1232417903','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1467','1232417912','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1468','1232417923','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1469','1232417927','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1470','1232418008','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1471','1232418014','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1472','1232418017','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1473','1232418101','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1474','1232418135','1','admin','67','-','-','Removing locks');
INSERT INTO `modx_manager_log` VALUES ('1475','1232418142','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('1476','1232418149','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1477','1232418157','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1478','1232418193','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1479','1232418196','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1480','1232418201','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1481','1232418303','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1482','1232418316','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1483','1232418321','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1484','1232418325','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1485','1232418328','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1486','1232418331','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1487','1232418334','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('1488','1232418338','1','admin','67','-','-','Removing locks');
INSERT INTO `modx_manager_log` VALUES ('1489','1232418340','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1490','1232418346','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1491','1232418348','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1492','1232418353','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1493','1232418370','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1494','1232418384','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1495','1232418450','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1496','1232418455','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1497','1232418475','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1498','1232418475','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1499','1232418476','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1500','1232418558','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1501','1232418569','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1502','1232418582','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1503','1232418586','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1504','1232418700','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1505','1232418707','1','admin','3','12','Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1506','1232419146','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1507','1232419160','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1508','1232419163','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1509','1232419219','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1510','1232419225','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1511','1232419239','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1512','1232419311','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1513','1232419318','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('1514','1232419449','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1515','1232419462','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1516','1232419471','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1517','1232419479','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1518','1232419487','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1519','1232419510','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1520','1232419519','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1521','1232419532','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1522','1232419535','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1523','1232419536','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1524','1232419544','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1525','1232419547','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('1526','1232419558','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1527','1232419562','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1528','1232419572','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1529','1232419578','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1530','1232419580','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1531','1232419593','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1532','1232419603','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1533','1232419609','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('1534','1232419989','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1535','1232419991','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1536','1232419998','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1537','1232420034','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1538','1232420049','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1539','1232420053','1','admin','19','-','New template','Creating a new template');
INSERT INTO `modx_manager_log` VALUES ('1540','1232420069','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1541','1232420079','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1542','1232420094','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1543','1232420101','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1544','1232420106','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1545','1232420110','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1546','1232420113','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1547','1232420119','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1548','1232420246','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1549','1232420258','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1550','1232420260','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1551','1232420263','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1552','1232420265','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1553','1232420281','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1554','1232420282','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1555','1232420298','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1556','1232420311','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1557','1232420324','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1558','1232420327','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1559','1232420334','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1560','1232420340','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1561','1232420388','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1562','1232420679','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1563','1232420694','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1564','1232420700','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1565','1232420710','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1566','1232420724','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1567','1232420728','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1568','1232420732','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1569','1232420772','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1570','1232420785','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1571','1232420788','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1572','1232420792','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1573','1232420795','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1574','1232420887','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1575','1232420893','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1576','1232420930','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1577','1232420936','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1578','1232420937','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1579','1232420946','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1580','1232420948','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1581','1232420988','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1582','1232421055','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1583','1232421090','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1584','1232421092','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1585','1232421093','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1586','1232421130','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1587','1232421133','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1588','1232421135','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1589','1232421138','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1590','1232421257','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1591','1232421259','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1592','1232421261','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1593','1232421263','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1594','1232421345','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1595','1232421353','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1596','1232421359','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1597','1232421398','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('1598','1232421417','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1599','1232421424','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1600','1232421429','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1601','1232421444','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1602','1232421447','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1603','1232421452','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1604','1232421455','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1605','1232421463','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1606','1232421467','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1607','1232421471','1','admin','67','-','-','Removing locks');
INSERT INTO `modx_manager_log` VALUES ('1608','1232421479','1','admin','67','-','-','Removing locks');
INSERT INTO `modx_manager_log` VALUES ('1609','1232421486','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1610','1232421488','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('1611','1232421491','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1612','1232421501','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1613','1232421506','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1614','1232421518','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1615','1232421523','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1616','1232421528','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1617','1232421533','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1618','1232453988','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1619','1232496324','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1620','1232538955','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1621','1232586735','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1622','1232593738','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1623','1232593796','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1624','1232593974','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1625','1232594683','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1626','1232594684','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1627','1232594686','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1628','1232594721','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1629','1232594722','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1630','1232594724','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1631','1232594724','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1632','1232594789','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1633','1232594790','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1634','1232594794','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1635','1232594800','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1636','1232594803','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1637','1232594804','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1638','1232594806','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1639','1232594811','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1640','1232594819','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1641','1232594912','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1642','1232594912','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1643','1232594915','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1644','1232594917','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1645','1232594918','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1646','1232594921','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1647','1232594927','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1648','1232594927','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1649','1232594928','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1650','1232594982','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1651','1232595009','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1652','1232595010','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1653','1232595011','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1654','1232595012','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1655','1232595016','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1656','1232595023','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1657','1232595024','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('1658','1232595029','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1659','1232595056','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1660','1232595061','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1661','1232595065','1','admin','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1662','1232595078','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1663','1232595081','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1664','1232595086','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1665','1232605427','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1666','1232605438','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1667','1232605488','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1668','1232605499','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1669','1232605501','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1670','1232605502','1','admin','114','-','-','View event log');
INSERT INTO `modx_manager_log` VALUES ('1671','1232605557','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1672','1232605558','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1673','1232605559','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1674','1232605565','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1675','1232605571','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1676','1232606158','1','admin','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1677','1232606159','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1678','1232606162','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1679','1232606170','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1680','1232606176','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1681','1232606182','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1682','1232606477','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1683','1232606483','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1684','1232610131','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1685','1232644202','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1686','1232644246','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1687','1232644250','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1688','1232644256','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1689','1232644258','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1690','1232644262','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1691','1232644265','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1692','1232644267','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1693','1232644269','1','admin','40','-','-','Editing Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1694','1232644271','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1695','1232644279','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('1696','1232644282','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1697','1232644284','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('1698','1232644285','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('1699','1232644288','1','admin','67','-','-','Removing locks');
INSERT INTO `modx_manager_log` VALUES ('1700','1232644290','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1701','1232644364','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1702','1232676149','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1703','1232676168','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1704','1232717929','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1705','1232717942','1','admin','3','21','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1706','1232717949','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1707','1232717958','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1708','1232717969','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1709','1233945583','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1710','1233945616','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1711','1233945622','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('1712','1233945636','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1713','1233949637','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1714','1233949645','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1715','1234111659','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1716','1234111673','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1717','1234111677','1','admin','11','-','New user','Creating a user');
INSERT INTO `modx_manager_log` VALUES ('1718','1234111722','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1719','1234111724','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('1720','1234111848','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('1721','1234112496','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1722','1234112498','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('1723','1234115887','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('1724','1234117674','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('1725','1234117709','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1726','1234117714','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('1727','1234117796','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1728','1234117803','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1729','1234117850','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1730','1234117857','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1731','1234117964','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1732','1234118018','1','admin','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1733','1234118024','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1734','1234118466','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1735','1234118472','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1736','1234118479','1','admin','19','-','New template','Creating a new template');
INSERT INTO `modx_manager_log` VALUES ('1737','1234118491','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1738','1234118495','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1739','1234118512','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1740','1234118515','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('1741','1234118585','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1742','1234118593','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1743','1234118595','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('1744','1234118664','1','admin','36','-','-','Saving role');
INSERT INTO `modx_manager_log` VALUES ('1745','1234118664','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1746','1234118670','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1747','1234118671','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1748','1234118673','1','admin','11','-','New user','Creating a user');
INSERT INTO `modx_manager_log` VALUES ('1749','1234118801','1','admin','32','-','-','Saving user');
INSERT INTO `modx_manager_log` VALUES ('1750','1234118822','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1751','1234118862','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1752','1234118864','1','admin','35','2','Content Manger','Editing role');
INSERT INTO `modx_manager_log` VALUES ('1753','1234118939','1','admin','36','2','-','Saving role');
INSERT INTO `modx_manager_log` VALUES ('1754','1234118939','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1755','1234118948','2','contentManager','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1756','1234118953','2','contentManager','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1757','1234118966','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1758','1234118973','2','contentManager','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1759','1234118984','2','contentManager','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1760','1234119000','2','contentManager','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1761','1234119015','2','contentManager','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1762','1234119018','2','contentManager','87','-','New web user','Create new web user');
INSERT INTO `modx_manager_log` VALUES ('1763','1234119047','2','contentManager','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1764','1234119051','2','contentManager','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1765','1234119057','2','contentManager','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1766','1234119085','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1767','1234119112','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1768','1234119135','2','contentManager','61','51','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1769','1234119143','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1770','1234119148','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1771','1234119163','2','contentManager','6','51','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('1772','1234119171','2','contentManager','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('1773','1234119935','2','contentManager','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1774','1234120034','2','contentManager','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1775','1234120041','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1776','1234120149','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1777','1234120178','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1778','1234120182','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('1779','1234120203','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1780','1234120230','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1781','1234120232','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1782','1234128002','2','contentManager','9','-','-','Viewing help');
INSERT INTO `modx_manager_log` VALUES ('1783','1234128078','2','contentManager','28','-','-','Changing password');
INSERT INTO `modx_manager_log` VALUES ('1784','1234128088','1','admin','28','-','-','Changing password');
INSERT INTO `modx_manager_log` VALUES ('1785','1234128148','2','contentManager','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1786','1234128163','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1787','1234128184','2','contentManager','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1788','1234128190','2','contentManager','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1789','1234128194','2','contentManager','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1790','1234128196','2','contentManager','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1791','1234128202','2','contentManager','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1792','1234128208','2','contentManager','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1793','1234128220','2','contentManager','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1794','1234128233','2','contentManager','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1795','1234128235','2','contentManager','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1796','1234128239','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1797','1234137857','2','contentManager','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1798','1234137860','2','contentManager','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1799','1234137864','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1800','1234137874','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1801','1234137884','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1802','1234138128','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1803','1234138389','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1804','1234139355','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1805','1234139556','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1806','1234139557','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1807','1234139560','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1808','1234139766','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1809','1234140158','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1810','1234140502','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1811','1234140567','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1812','1234140694','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1813','1234140757','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1814','1234140762','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1815','1234140918','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1816','1234140931','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('1817','1234140934','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('1818','1234140936','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1819','1234140947','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('1820','1234140950','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1821','1234140961','1','admin','112','2','QuickEdit','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1822','1234140963','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('1823','1234140969','1','admin','91','-','-','Editing Web Access Permissions');
INSERT INTO `modx_manager_log` VALUES ('1824','1234140978','1','admin','83','-','-','Exporting a document to HTML');
INSERT INTO `modx_manager_log` VALUES ('1825','1234140989','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('1826','1234140995','1','admin','67','-','-','Removing locks');
INSERT INTO `modx_manager_log` VALUES ('1827','1234140997','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('1828','1234141003','1','admin','53','-','-','Viewing system info');
INSERT INTO `modx_manager_log` VALUES ('1829','1234141009','1','admin','13','-','-','Viewing logging');
INSERT INTO `modx_manager_log` VALUES ('1830','1234141032','2','contentManager','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1831','1234141036','2','contentManager','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1832','1234141066','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1833','1234141074','2','contentManager','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('1834','1234141090','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1835','1234141111','2','contentManager','71','-','-','Searching');
INSERT INTO `modx_manager_log` VALUES ('1836','1234141114','2','contentManager','26','-','-','Refreshing site');
INSERT INTO `modx_manager_log` VALUES ('1837','1234141119','2','contentManager','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1838','1234141126','2','contentManager','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1839','1234141129','2','contentManager','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1840','1234141131','2','contentManager','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1841','1234141135','2','contentManager','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1842','1234141139','2','contentManager','70','-','-','Viewing site schedule');
INSERT INTO `modx_manager_log` VALUES ('1843','1234141154','2','contentManager','28','-','-','Changing password');
INSERT INTO `modx_manager_log` VALUES ('1844','1234152771','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1845','1234152782','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1846','1234152793','2','contentManager','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1847','1234153313','2','contentManager','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1848','1234153955','2','contentManager','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1849','1234222253','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1850','1234222264','2','contentManager','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1851','1234222269','2','contentManager','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1852','1234222270','2','contentManager','3','4','Resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1853','1234222273','2','contentManager','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1854','1234222274','2','contentManager','3','16','Feeds','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1855','1234222468','2','contentManager','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1856','1234283522','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1857','1234283534','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1858','1234283612','2','contentManager','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1859','1234283612','2','contentManager','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1860','1234283620','2','contentManager','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1861','1234283621','2','contentManager','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1862','1234283916','2','contentManager','3','45','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1863','1234283945','2','contentManager','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('1864','1234283958','2','contentManager','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1865','1234283965','2','contentManager','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('1866','1234283973','2','contentManager','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1867','1234283987','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1868','1234283993','2','contentManager','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1869','1234283996','2','contentManager','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1870','1234283997','2','contentManager','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1871','1234284013','2','contentManager','3','27','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1872','1234284017','2','contentManager','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1873','1234288387','2','contentManager','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1874','1234622855','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1875','1234622867','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1876','1234622911','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1877','1234622911','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1878','1234622920','2','contentManager','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1879','1234622930','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1880','1234622947','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1881','1234622952','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('1882','1234622959','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('1883','1234622962','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1884','1234622964','1','admin','35','2','Content Manger','Editing role');
INSERT INTO `modx_manager_log` VALUES ('1885','1234623045','1','admin','36','2','-','Saving role');
INSERT INTO `modx_manager_log` VALUES ('1886','1234623045','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('1887','1234623048','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1888','1234623054','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1889','1234623066','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1890','1234623091','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1891','1234623091','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1892','1234623106','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1893','1234623151','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1894','1234623152','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1895','1234623155','2','contentManager','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1896','1234623174','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1897','1234623175','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1898','1234623185','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1899','1234623194','2','contentManager','6','53','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('1900','1234623199','2','contentManager','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('1901','1234623222','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1902','1234623232','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1903','1234623233','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1904','1234623254','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1905','1234623255','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1906','1234623277','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1907','1234623288','2','contentManager','3','54','contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1908','1234623290','2','contentManager','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1909','1234623291','2','contentManager','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1910','1234623300','2','contentManager','5','54','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1911','1234623303','2','contentManager','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1912','1234623307','2','contentManager','61','54','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1913','1234623402','2','contentManager','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1914','1234623427','2','contentManager','3','22','Org Information','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1915','1234623429','2','contentManager','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1916','1234623430','2','contentManager','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1917','1234623441','2','contentManager','5','22','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1918','1234623444','2','contentManager','3','22','Org Information','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1919','1234623447','2','contentManager','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1920','1234623448','2','contentManager','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1921','1234623453','2','contentManager','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1922','1234623454','2','contentManager','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1923','1234623460','2','contentManager','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1924','1234623460','2','contentManager','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1925','1234623463','2','contentManager','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1926','1234623464','2','contentManager','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1927','1234623470','2','contentManager','5','54','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1928','1234623473','2','contentManager','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1929','1234634608','2','contentManager','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1930','1234660920','2','contentManager','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('1931','1234832532','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1932','1234832543','2','contentManager','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1933','1234832565','2','contentManager','5','11','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1934','1234832568','2','contentManager','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1935','1234832573','2','contentManager','3','15','item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1936','1234832576','2','contentManager','27','15','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1937','1234832576','2','contentManager','27','15','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1938','1234832591','2','contentManager','5','15','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1939','1234832594','2','contentManager','3','15','item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1940','1235334966','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('1941','1235334989','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1942','1235334992','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1943','1235335015','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1944','1235335021','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1945','1235335028','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1946','1235335029','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1947','1235335036','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1948','1235335049','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1949','1235335058','1','admin','61','55','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1950','1235335066','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('1951','1235335116','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1952','1235335125','1','admin','61','56','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1953','1235335160','1','admin','3','55','Untitled document','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1954','1235335163','1','admin','27','55','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1955','1235335182','1','admin','5','55','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1956','1235335185','1','admin','3','55','February','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1957','1235335205','1','admin','94','15','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1958','1235335205','1','admin','3','57','Duplicate of item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1959','1235335212','1','admin','51','57','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1960','1235335217','1','admin','52','57','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1961','1235335224','1','admin','61','57','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1962','1235335236','1','admin','94','11','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1963','1235335236','1','admin','3','58','Duplicate of Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1964','1235335241','1','admin','51','58','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1965','1235335243','1','admin','52','58','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1966','1235335252','1','admin','94','13','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1967','1235335252','1','admin','3','59','Duplicate of Item 03','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1968','1235335257','1','admin','51','59','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1969','1235335259','1','admin','52','59','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1970','1235335269','1','admin','94','14','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1971','1235335269','1','admin','3','60','Duplicate of Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1972','1235335274','1','admin','51','60','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('1973','1235335277','1','admin','52','60','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('1974','1235335283','1','admin','61','58','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1975','1235335290','1','admin','61','59','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1976','1235335294','1','admin','61','60','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('1977','1235335376','1','admin','94','56','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1978','1235335376','1','admin','3','61','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1979','1235335378','1','admin','27','61','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1980','1235335389','1','admin','5','61','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1981','1235335392','1','admin','3','61','February Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1982','1235335394','1','admin','94','61','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1983','1235335394','1','admin','3','62','Duplicate of February Item 02','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1984','1235335396','1','admin','27','62','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1985','1235335404','1','admin','5','62','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1986','1235335404','1','admin','5','62','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1987','1235335407','1','admin','3','62','February Item 03','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1988','1235335414','1','admin','94','62','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1989','1235335414','1','admin','3','63','Duplicate of February Item 03','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1990','1235335420','1','admin','27','63','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1991','1235335427','1','admin','5','63','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1992','1235335430','1','admin','3','63','February Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1993','1235335442','1','admin','94','63','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1994','1235335443','1','admin','3','64','Duplicate of February Item 04','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1995','1235335444','1','admin','27','64','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('1996','1235335453','1','admin','5','64','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('1997','1235335456','1','admin','3','64','February Item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('1998','1235335463','1','admin','94','64','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('1999','1235335463','1','admin','3','65','Duplicate of February Item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2000','1235335464','1','admin','27','65','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2001','1235335469','1','admin','5','65','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2002','1235335472','1','admin','3','65','Duplicate of February Item 06','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2003','1235335477','1','admin','27','65','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2004','1235335483','1','admin','5','65','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2005','1235335486','1','admin','3','65','February Item 06','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2006','1235335507','1','admin','94','65','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('2007','1235335507','1','admin','3','66','Duplicate of February Item 06','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2008','1235335509','1','admin','27','66','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2009','1235335517','1','admin','5','66','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2010','1235335521','1','admin','3','66','February Item 07','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2011','1235335530','1','admin','94','66','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('2012','1235335531','1','admin','3','67','Duplicate of February Item 07','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2013','1235335532','1','admin','27','67','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2014','1235335545','1','admin','5','67','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2015','1235335548','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2016','1235335570','1','admin','61','66','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2017','1235335582','1','admin','61','65','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2018','1235335593','1','admin','61','64','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2019','1235335598','1','admin','61','63','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2020','1235335603','1','admin','61','62','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2021','1235335609','1','admin','61','61','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2022','1235335618','1','admin','61','67','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2023','1235336523','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2024','1235336524','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2025','1235336535','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2026','1235336539','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2027','1235336570','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2028','1235336581','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2029','1235336584','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2030','1235338404','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2031','1235338406','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2032','1235338410','1','admin','3','15','item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2033','1235338413','1','admin','3','11','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2034','1235338414','1','admin','27','11','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2035','1235338419','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2036','1235338422','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2037','1235338423','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2038','1235338433','1','admin','5','56','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2039','1235338436','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2040','1235338487','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2041','1235338489','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2042','1235338491','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2043','1235338514','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2044','1235338517','1','admin','27','67','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2045','1235338521','1','admin','5','67','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2046','1235338524','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2047','1235338586','1','admin','3','55','February','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2048','1235338622','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2049','1235338624','1','admin','27','67','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2050','1235338657','1','admin','5','67','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2051','1235338659','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2052','1235338704','1','admin','27','67','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2053','1235338715','1','admin','5','67','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2054','1235338718','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2055','1235338748','1','admin','27','67','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2056','1235338756','1','admin','5','67','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2057','1235338758','1','admin','3','67','February Item 08','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2058','1235524794','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2059','1235524819','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2060','1235524837','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2061','1235525051','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2062','1235525054','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2063','1235525062','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2064','1235525084','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2065','1235525094','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2066','1235525099','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2067','1235525253','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2068','1235525255','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2069','1235525274','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2070','1235525278','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('2071','1235525287','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('2072','1235525296','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2073','1235525296','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2074','1235525307','1','admin','3','66','February Item 07','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2075','1235525308','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2076','1235525309','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2077','1235525310','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2078','1235525312','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2079','1235525313','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2080','1235525316','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2081','1235525317','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2082','1235525318','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2083','1235525354','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2084','1235525359','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2085','1235525360','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2086','1235526585','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2087','1235526589','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2088','1235526590','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2089','1235526595','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2090','1235526600','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2091','1235526706','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2092','1235526709','1','admin','77','-','New Chunk','Creating a new Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2093','1235526775','1','admin','79','-','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2094','1235526776','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2095','1235526783','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2096','1235526786','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2097','1235526787','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2098','1235526800','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2099','1235526803','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2100','1235526814','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2101','1235526815','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2102','1235526854','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2103','1235526865','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2104','1235526883','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2105','1235526912','1','admin','302','-','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2106','1235526913','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2107','1235526925','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2108','1235526926','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2109','1235526941','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('2110','1235526973','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('2111','1235526979','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2112','1235526981','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2113','1235527102','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('2114','1235527120','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('2115','1235527125','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2116','1235527125','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2117','1235527178','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('2118','1235527201','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('2119','1235527207','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2120','1235527208','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2121','1235527263','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('2122','1235527307','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('2123','1235527318','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2124','1235527319','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2125','1235527474','1','admin','3','27','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2126','1235527478','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2127','1235527479','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2128','1235527497','1','admin','5','27','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2129','1235527500','1','admin','3','27','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2130','1235527501','1','admin','3','27','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2131','1235527505','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2132','1235527508','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2133','1235527509','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2134','1235527573','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2135','1235527574','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2136','1235608016','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2137','1235608024','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2138','1235608041','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2139','1235608043','1','admin','27','27','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2140','1235611460','1','admin','6','27','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('2141','1235611467','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('2142','1235611472','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2143','1235611473','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2144','1235613357','1','admin','5','28','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2145','1235613361','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2146','1235613365','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2147','1235613368','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2148','1235613402','1','admin','5','29','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2149','1235613406','1','admin','3','29','Stop Motion','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2150','1235613408','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2151','1235613411','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2152','1235613439','1','admin','5','30','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2153','1235613442','1','admin','3','30','International Students','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2154','1235613455','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2155','1235613456','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2156','1235613477','1','admin','5','31','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2157','1235613479','1','admin','3','31','Item 05','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2158','1235613482','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2159','1235613483','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2160','1235613503','1','admin','5','31','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2161','1235613505','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2162','1235613529','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2163','1235613530','1','admin','27','5','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2164','1235613540','1','admin','5','5','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2165','1235613543','1','admin','3','5','Diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2166','1235613546','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2167','1235613547','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2168','1235613557','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2169','1235613558','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2170','1235613636','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2171','1235613642','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2172','1235613642','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2173','1235613659','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2174','1235613660','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2175','1235613668','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2176','1235613672','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2177','1235613673','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2178','1235613679','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2179','1235613690','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2180','1235613690','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2181','1235613697','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2182','1235613699','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2183','1235613760','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2184','1235613761','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2185','1235613766','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2186','1235613770','1','admin','301','2','video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2187','1235613773','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2188','1235613779','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2189','1235613838','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2190','1235613839','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2191','1235614650','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2192','1235614652','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2193','1235614670','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2194','1235614673','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2195','1235614681','1','admin','61','68','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2196','1235614686','1','admin','61','69','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2197','1235614691','1','admin','61','70','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2198','1235614889','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2199','1235614890','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2200','1235614951','1','admin','5','28','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2201','1235614954','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2202','1235616478','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2203','1235694654','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2204','1235864741','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2205','1235913319','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2206','1235931003','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2207','1235931009','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2208','1235931040','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2209','1235931041','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2210','1235956979','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2211','1235956986','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2212','1235966199','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2213','1235966201','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2214','1235966272','1','admin','5','28','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2215','1235966275','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2216','1235966276','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2217','1235966277','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2218','1235966293','1','admin','5','29','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2219','1235966296','1','admin','3','29','Stop Motion','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2220','1235966300','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2221','1235966301','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2222','1235966322','1','admin','5','30','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2223','1235966325','1','admin','3','30','International Students','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2224','1235966329','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2225','1235966329','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2226','1235966347','1','admin','5','31','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2227','1235966349','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2228','1235966350','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2229','1235966352','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2230','1235966367','1','admin','5','31','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2231','1235966370','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2232','1235966374','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2233','1235966398','1','admin','5','31','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2234','1235966401','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2235','1235966403','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2236','1235966442','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2237','1235966455','1','admin','5','31','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2238','1235966457','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2239','1235984439','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2240','1236041617','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2241','1236043947','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2242','1236123625','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2243','1236213071','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2244','1236223343','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2245','1236223345','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2246','1236298138','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2247','1236306274','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2248','1236444501','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2249','1236455921','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2250','1236456035','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2251','1236456101','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2252','1236456196','1','admin','3','41','January','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2253','1236456197','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2254','1236456199','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2255','1236456217','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2256','1236456217','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2257','1236456221','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2258','1236456259','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2259','1236456259','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2260','1236456263','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2261','1236456271','1','admin','27','41','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2262','1236456272','1','admin','27','41','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2263','1236456298','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2264','1236456301','1','admin','27','41','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2265','1236456302','1','admin','27','41','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2266','1236456304','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2267','1236456305','1','admin','27','55','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2268','1236456306','1','admin','27','55','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2269','1236456319','1','admin','3','55','February','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2270','1236456323','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2271','1236456353','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2272','1236456353','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2273','1236456358','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2274','1236456428','1','admin','27','41','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2275','1236456428','1','admin','27','41','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2276','1236456435','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2277','1236456437','1','admin','27','55','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2278','1236456438','1','admin','27','55','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2279','1236456512','1','admin','5','55','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2280','1236456515','1','admin','3','55','February','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2281','1236456518','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2282','1236456612','1','admin','27','41','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2283','1236456613','1','admin','27','41','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2284','1236456617','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2285','1236456619','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2286','1236456619','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2287','1236456644','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2288','1236456652','1','admin','3','55','February','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2289','1236456663','1','admin','6','71','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('2290','1236456666','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2291','1236456669','1','admin','3','71','March','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2292','1236456678','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2293','1236456689','1','admin','27','71','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2294','1236456689','1','admin','27','71','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2295','1236456700','1','admin','6','71','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('2296','1236456704','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2297','1236456726','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2298','1236456805','1','admin','3','71','March','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2299','1236456817','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2300','1236456820','1','admin','3','55','February','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2301','1236456824','1','admin','3','71','March','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2302','1236456845','1','admin','61','71','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2303','1236456868','1','admin','3','71','March','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2304','1236456876','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2305','1236456890','1','admin','3','71','March','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2306','1236456909','1','admin','6','19','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('2307','1236456921','1','admin','94','2','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('2308','1236456922','1','admin','3','72','Duplicate of Features','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2309','1236456932','1','admin','6','2','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('2310','1236456945','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('2311','1236456990','1','admin','6','72','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('2312','1236456995','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('2313','1236457320','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2314','1236457341','1','admin','3','40','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2315','1236457357','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2316','1236457357','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2317','1236457360','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2318','1236457361','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2319','1236457368','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2320','1236457372','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2321','1236457373','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2322','1236457426','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2323','1236457436','1','admin','3','80','March','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2324','1236457439','1','admin','5','10','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2325','1236457442','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2326','1236457470','1','admin','61','80','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2327','1236457471','1','admin','61','80','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2328','1236457492','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2329','1236457492','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2330','1236457493','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2331','1236457494','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2332','1236457705','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2333','1236457708','1','admin','3','46','January','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2334','1236457714','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2335','1236457999','1','admin','3','22','Org Information','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2336','1236457999','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2337','1236458000','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2338','1236458006','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2339','1236458006','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2340','1236458040','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('2341','1236458046','1','admin','87','-','New web user','Create new web user');
INSERT INTO `modx_manager_log` VALUES ('2342','1236458049','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('2343','1236458057','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2344','1236458061','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('2345','1236458093','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2346','1236458095','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('2347','1236458123','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2348','1236458124','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2349','1236458128','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2350','1236458133','1','admin','11','-','New user','Creating a user');
INSERT INTO `modx_manager_log` VALUES ('2351','1236458176','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2352','1236458188','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2353','1236458200','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2354','1236458203','1','admin','11','-','New user','Creating a user');
INSERT INTO `modx_manager_log` VALUES ('2355','1236458220','1','admin','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2356','1236458227','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('2357','1236458229','1','admin','38','-','New role','Creating new role');
INSERT INTO `modx_manager_log` VALUES ('2358','1236458293','1','admin','36','-','-','Saving role');
INSERT INTO `modx_manager_log` VALUES ('2359','1236458294','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('2360','1236458297','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2361','1236458302','1','admin','11','-','New user','Creating a user');
INSERT INTO `modx_manager_log` VALUES ('2362','1236458381','1','admin','32','-','-','Saving user');
INSERT INTO `modx_manager_log` VALUES ('2363','1236458381','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2364','1236458386','1','admin','12','3','Diary Test','Editing user');
INSERT INTO `modx_manager_log` VALUES ('2365','1236458602','1','admin','32','3','-','Saving user');
INSERT INTO `modx_manager_log` VALUES ('2366','1236458602','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2367','1236458607','1','admin','12','3','Diary Test','Editing user');
INSERT INTO `modx_manager_log` VALUES ('2368','1236458713','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2369','1236458715','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('2370','1236458717','1','admin','35','3','Diary Participant','Editing role');
INSERT INTO `modx_manager_log` VALUES ('2371','1236458799','1','admin','36','3','-','Saving role');
INSERT INTO `modx_manager_log` VALUES ('2372','1236458799','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('2373','1236576070','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2374','1236576075','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2375','1236576233','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2376','1236576234','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2377','1236576416','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2378','1236576464','1','admin','61','81','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2379','1236576500','1','admin','27','81','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2380','1236576525','1','admin','5','81','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2381','1236576527','1','admin','3','81','HIP HOP PROJECT','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2382','1236576590','1','admin','27','64','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2383','1236576590','1','admin','27','64','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2384','1236900107','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2385','1236900143','1','admin','3','68','Category 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2386','1236900150','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2387','1236900152','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2388','1236900155','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2389','1236900156','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2390','1236900208','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2391','1236900219','1','admin','61','82','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2392','1236900239','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2393','1236900239','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2394','1236900406','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2395','1236900411','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2396','1236900421','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2397','1236900423','1','admin','77','-','New Chunk','Creating a new Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2398','1236900435','1','admin','79','-','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2399','1236900435','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2400','1236900447','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2401','1236900451','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2402','1236900452','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2403','1236900459','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2404','1236900462','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2405','1236900467','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2406','1236900470','1','admin','78','4','LinksFeed','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2407','1236900476','1','admin','79','4','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2408','1236900477','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2409','1236900507','1','admin','78','4','linksFeed','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2410','1236900646','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2411','1236900646','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2412','1236900722','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2413','1236900726','1','admin','23','-','New snippet','Creating a new Snippet');
INSERT INTO `modx_manager_log` VALUES ('2414','1236900743','1','admin','24','-','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2415','1236900743','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2416','1236900747','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2417','1236900758','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2418','1236900765','1','admin','61','83','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2419','1236900780','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2420','1236900786','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2421','1236900866','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2422','1236900867','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2423','1236900876','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2424','1236900885','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2425','1236900885','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2426','1236900940','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2427','1236900940','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2428','1236901084','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2429','1236901084','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2430','1236901094','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2431','1236901095','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2432','1236901129','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2433','1236901129','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2434','1236901152','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2435','1236901152','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2436','1236901174','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2437','1236901174','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2438','1236901195','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2439','1236901196','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2440','1236901285','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2441','1236901285','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2442','1236901300','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2443','1236901300','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2444','1236901330','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2445','1236901331','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2446','1236901417','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2447','1236901417','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2448','1236901446','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2449','1236901447','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2450','1236901482','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2451','1236901485','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2452','1236901488','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2453','1236901492','1','admin','78','4','linksFeed','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2454','1236901502','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2455','1236901516','1','admin','78','4','linksFeed','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2456','1236901536','1','admin','79','4','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2457','1236901536','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2458','1236901598','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2459','1236901599','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2460','1236901619','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2461','1236901622','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2462','1236901649','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2463','1236901650','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2464','1236901669','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2465','1236901672','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2466','1236901685','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2467','1236901685','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2468','1236901717','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2469','1236901726','1','admin','61','84','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2470','1236901734','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2471','1236901734','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2472','1236901766','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2473','1236901774','1','admin','61','85','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2474','1236901786','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2475','1236901787','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2476','1236901880','1','admin','27','83','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2477','1236901880','1','admin','27','83','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2478','1236901886','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2479','1236901893','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2480','1236901916','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2481','1236901917','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2482','1236901960','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2483','1236902017','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2484','1236902017','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2485','1236902856','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2486','1236902856','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2487','1236902897','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2488','1236902897','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2489','1236902952','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2490','1236902953','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2491','1236903030','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2492','1236903033','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2493','1236903041','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2494','1236903042','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2495','1236903367','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2496','1236903409','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2497','1236903409','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2498','1236904014','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2499','1236904015','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2500','1236904159','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2501','1236904159','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2502','1236904191','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2503','1236904191','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2504','1236904265','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2505','1236904266','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2506','1236904312','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2507','1236904313','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2508','1236904337','1','admin','5','68','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2509','1236904339','1','admin','3','68','Title','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2510','1236904414','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2511','1236904418','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2512','1236904427','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2513','1236904427','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2514','1236904445','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2515','1236904445','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2516','1236905436','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2517','1236905436','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2518','1236905500','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2519','1236905500','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2520','1236905720','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2521','1236905720','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2522','1236906166','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2523','1236906166','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2524','1236906266','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2525','1236906266','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2526','1236906310','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2527','1236906310','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2528','1236907179','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2529','1236907179','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2530','1236907420','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2531','1236907420','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2532','1236907594','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2533','1236907594','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2534','1236907609','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2535','1236907609','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2536','1236907689','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2537','1236907689','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2538','1236907757','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2539','1236907757','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2540','1236907792','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2541','1236907792','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2542','1236907831','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2543','1236907831','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2544','1236907882','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2545','1236907883','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2546','1236907931','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2547','1236907931','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2548','1236907984','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2549','1236907984','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2550','1236908110','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2551','1236908111','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2552','1236908144','1','admin','5','68','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2553','1236908147','1','admin','3','68','Category 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2554','1236908151','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2555','1236908151','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2556','1236908154','1','admin','5','68','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2557','1236908158','1','admin','3','68','Category 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2558','1236944392','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2559','1236944392','1','admin','3','68','Category 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2560','1236944398','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2561','1236945168','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2562','1236945169','1','admin','3','68','Category 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2563','1236945174','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2564','1236983802','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2565','1236983817','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2566','1236983817','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2567','1236983861','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2568','1236983864','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2569','1236983869','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2570','1236983875','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2571','1236983881','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2572','1236983905','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2573','1236983906','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2574','1236983909','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2575','1236983910','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2576','1236983933','1','admin','5','82','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2577','1236983936','1','admin','3','82','Ghostmonk Home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2578','1236983950','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2579','1236983951','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2580','1236983954','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2581','1236983955','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2582','1236983985','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2583','1236983991','1','admin','61','86','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2584','1236983994','1','admin','61','87','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2585','1236984003','1','admin','61','87','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2586','1236984006','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2587','1236984007','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2588','1236984013','1','admin','5','68','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2589','1236984016','1','admin','3','68','Ghostmonk Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2590','1236984019','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2591','1236984019','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2592','1236984029','1','admin','5','69','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2593','1236984032','1','admin','3','69','Not So Simpleton Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2594','1236984045','1','admin','27','84','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2595','1236984047','1','admin','27','84','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2596','1236984080','1','admin','5','84','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2597','1236984083','1','admin','3','84','No So Simpleton Home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2598','1236984093','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2599','1236984094','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2600','1236984097','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2601','1236984098','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2602','1236984125','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2603','1236984131','1','admin','61','88','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2604','1236984142','1','admin','27','70','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2605','1236984143','1','admin','27','70','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2606','1236984149','1','admin','5','70','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2607','1236984151','1','admin','3','70','Popular Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2608','1236984152','1','admin','27','85','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2609','1236984153','1','admin','27','85','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2610','1236984175','1','admin','5','85','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2611','1236984177','1','admin','3','85','MSN','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2612','1236984352','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2613','1236984352','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2614','1236984379','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2615','1236984384','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2616','1236984385','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2617','1236984467','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2618','1236984474','1','admin','61','89','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2619','1236984478','1','admin','61','90','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2620','1236984485','1','admin','61','90','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2621','1236988207','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('2622','1236988233','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('2623','1236988234','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2624','1236988268','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2625','1236988287','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2626','1236988287','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2627','1236988293','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2628','1236988294','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2629','1236988320','1','admin','27','83','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2630','1236988321','1','admin','27','83','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2631','1236988325','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2632','1236988326','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2633','1236988335','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2634','1236988338','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2635','1236988411','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2636','1236988413','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('2637','1236988463','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2638','1236988463','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2639','1236988467','1','admin','23','-','New snippet','Creating a new Snippet');
INSERT INTO `modx_manager_log` VALUES ('2640','1236988526','1','admin','24','-','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('2641','1236988527','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2642','1236988531','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2643','1236988534','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2644','1236988535','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2645','1236988555','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2646','1236988558','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2647','1236994380','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2648','1237030566','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2649','1237047098','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2650','1237049073','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2651','1237049082','1','admin','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2652','1237049089','1','admin','3','68','Ghostmonk Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2653','1237049093','1','admin','3','82','Ghostmonk Home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2654','1237050980','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2655','1237050984','1','admin','101','-','New Plugin','Create new plugin');
INSERT INTO `modx_manager_log` VALUES ('2656','1237051382','1','admin','103','-','-','Saving plugin');
INSERT INTO `modx_manager_log` VALUES ('2657','1237051382','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2658','1237051388','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2659','1237051402','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('2660','1237051405','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2661','1237051408','1','admin','102','12','ManagerManager','Edit plugin');
INSERT INTO `modx_manager_log` VALUES ('2662','1237051539','1','admin','103','12','-','Saving plugin');
INSERT INTO `modx_manager_log` VALUES ('2663','1237051539','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2664','1237051543','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2665','1237051555','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2666','1237051559','1','admin','102','12','ManagerManager','Edit plugin');
INSERT INTO `modx_manager_log` VALUES ('2667','1237051644','1','admin','103','12','-','Saving plugin');
INSERT INTO `modx_manager_log` VALUES ('2668','1237051645','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2669','1237051650','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2670','1237051770','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2671','1237051774','1','admin','16','3','Minimal Template','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2672','1237051794','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2673','1237051994','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2674','1237052430','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2675','1237052522','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2676','1237052530','1','admin','5','10','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2677','1237052532','1','admin','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2678','1237052540','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2679','1237052609','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2680','1237052635','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2681','1237052646','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2682','1237052662','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2683','1237052777','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2684','1237052919','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2685','1237052954','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2686','1237052983','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2687','1237053041','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2688','1237054083','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2689','1237054110','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2690','1237054306','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2691','1237054318','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2692','1237054320','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('2693','1237054324','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2694','1237054331','1','admin','12','1','admin','Editing user');
INSERT INTO `modx_manager_log` VALUES ('2695','1237054349','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2696','1237054351','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('2697','1237054355','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2698','1237054356','1','admin','12','3','Diary Test','Editing user');
INSERT INTO `modx_manager_log` VALUES ('2699','1237054359','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('2700','1237054361','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('2701','1237054385','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('2702','1237054388','1','admin','35','2','Content Manger','Editing role');
INSERT INTO `modx_manager_log` VALUES ('2703','1237054450','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2704','1237054465','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('2705','1237054469','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('2706','1237054502','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('2707','1237054508','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2708','1237054571','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2709','1237054576','1','admin','16','4','basic','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2710','1237054591','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2711','1237054592','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2712','1237054594','1','admin','16','4','Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2713','1237054627','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2714','1237054627','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2715','1237054629','1','admin','16','3','Minimal Template','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2716','1237054636','1','admin','21','3','-','Deleting template');
INSERT INTO `modx_manager_log` VALUES ('2717','1237054636','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2718','1237054648','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2719','1237054655','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2720','1237054670','1','admin','301','2','video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2721','1237054679','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2722','1237054682','1','admin','19','-','New template','Creating a new template');
INSERT INTO `modx_manager_log` VALUES ('2723','1237054726','1','admin','20','-','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2724','1237054726','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2725','1237054738','1','admin','301','2','video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2726','1237054743','1','admin','302','2','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2727','1237054744','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2728','1237054864','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2729','1237054887','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2730','1237054899','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('2731','1237054909','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2732','1237054915','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2733','1237054918','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2734','1237054931','1','admin','3','55','February','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2735','1237054943','1','admin','302','1','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2736','1237054943','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2737','1237054945','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2738','1237054951','1','admin','301','2','video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2739','1237054954','1','admin','302','2','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2740','1237054954','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2741','1237054994','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2742','1237055007','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2743','1237055026','1','admin','27','81','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2744','1237055030','1','admin','3','80','March','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2745','1237055035','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2746','1237055035','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2747','1237055170','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2748','1237055178','1','admin','61','91','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('2749','1237055343','1','admin','27','55','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2750','1237055346','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2751','1237055353','1','admin','3','91','Updates at CCAP!','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2752','1237055357','1','admin','27','91','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2753','1237055374','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2754','1237055386','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2755','1237055445','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2756','1237055568','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2757','1237055571','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2758','1237055610','1','admin','302','-','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2759','1237055610','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2760','1237055613','1','admin','301','3','thumbnail used for video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2761','1237055617','1','admin','302','3','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2762','1237055618','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2763','1237055623','1','admin','16','5','Video Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2764','1237055640','1','admin','20','5','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2765','1237055640','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2766','1237055648','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2767','1237055653','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2768','1237055654','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2769','1237055672','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2770','1237055923','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2771','1237055927','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2772','1237055944','1','admin','5','28','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2773','1237055947','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2774','1237055960','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2775','1237055968','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2776','1237056025','1','admin','5','29','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2777','1237056028','1','admin','3','29','Stop Motion','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2778','1237056030','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2779','1237056044','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2780','1237056062','1','admin','5','30','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2781','1237056064','1','admin','3','30','International Students','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2782','1237056067','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2783','1237056075','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2784','1237056091','1','admin','5','31','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2785','1237056094','1','admin','3','31','Pop Culture','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2786','1237056127','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2787','1237056174','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2788','1237056176','1','admin','19','-','New template','Creating a new template');
INSERT INTO `modx_manager_log` VALUES ('2789','1237056210','1','admin','20','-','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2790','1237056210','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2791','1237056217','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2792','1237056222','1','admin','302','1','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2793','1237056223','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2794','1237056229','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2795','1237056240','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2796','1237056252','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2797','1237056255','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2798','1237056257','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2799','1237056262','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2800','1237056266','1','admin','5','38','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2801','1237056269','1','admin','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2802','1237056270','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2803','1237056274','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2804','1237056277','1','admin','5','39','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2805','1237056281','1','admin','3','39','Privacy Policy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2806','1237056281','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2807','1237056289','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2808','1237056292','1','admin','5','54','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2809','1237056295','1','admin','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2810','1237056329','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2811','1237056364','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('2812','1237056504','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('2813','1237056512','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2814','1237056523','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2815','1237056671','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2816','1237056781','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2817','1237056784','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2818','1237056787','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2819','1237056795','1','admin','302','1','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2820','1237056796','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2821','1237056796','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2822','1237056803','1','admin','16','5','Video Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2823','1237056805','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2824','1237056807','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2825','1237056817','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2826','1237056817','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2827','1237056821','1','admin','16','5','Video Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2828','1237056824','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2829','1237056830','1','admin','301','3','thumbnail used for video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2830','1237056835','1','admin','302','3','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('2831','1237056835','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2832','1237056840','1','admin','16','5','Video Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2833','1237056844','1','admin','20','5','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2834','1237056844','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2835','1237056851','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2836','1237056965','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2837','1237056972','1','admin','102','11','Image TV Preview','Edit plugin');
INSERT INTO `modx_manager_log` VALUES ('2838','1237056984','1','admin','103','11','-','Saving plugin');
INSERT INTO `modx_manager_log` VALUES ('2839','1237056984','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2840','1237057005','1','admin','102','11','Image TV Preview','Edit plugin');
INSERT INTO `modx_manager_log` VALUES ('2841','1237057012','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2842','1237057043','1','admin','102','3','Inherit Parent Template','Edit plugin');
INSERT INTO `modx_manager_log` VALUES ('2843','1237057085','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2844','1237057089','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2845','1237057107','1','admin','5','56','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2846','1237057111','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2847','1237057123','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2848','1237057128','1','admin','5','28','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2849','1237057131','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2850','1237057132','1','admin','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2851','1237057137','1','admin','3','30','International Students','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2852','1237057148','1','admin','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2853','1237057153','1','admin','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2854','1237057306','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2855','1237057315','1','admin','78','3','videoRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2856','1237057372','1','admin','79','3','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2857','1237057373','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2858','1237057377','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2859','1237057410','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2860','1237057410','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2861','1237057416','1','admin','78','3','videoRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2862','1237057436','1','admin','79','3','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2863','1237057436','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2864','1237057447','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2865','1237057455','1','admin','20','4','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2866','1237057455','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2867','1237057458','1','admin','16','5','Video Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2868','1237057463','1','admin','20','5','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2869','1237057463','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2870','1237057464','1','admin','16','6','Straight Text','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2871','1237057466','1','admin','20','6','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('2872','1237057467','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2873','1237057513','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('2874','1237057517','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('2875','1237057518','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2876','1237057521','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2877','1237057533','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2878','1237057534','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2879','1237057537','1','admin','78','3','videoRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2880','1237057547','1','admin','79','3','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('2881','1237057547','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2882','1237062879','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2883','1237064413','1','admin','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('2884','1237064424','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2885','1237064436','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2886','1237064442','1','admin','5','3','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2887','1237064446','1','admin','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2888','1237064452','1','admin','27','45','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2889','1237064460','1','admin','27','45','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2890','1237064466','1','admin','5','45','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2891','1237064470','1','admin','3','45','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2892','1237064476','1','admin','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2893','1237064482','1','admin','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2894','1237064487','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2895','1237064503','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2896','1237064509','1','admin','102','3','Inherit Parent Template','Edit plugin');
INSERT INTO `modx_manager_log` VALUES ('2897','1237064559','1','admin','103','3','-','Saving plugin');
INSERT INTO `modx_manager_log` VALUES ('2898','1237064560','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2899','1237064643','1','admin','102','3','Inherit Parent Template','Edit plugin');
INSERT INTO `modx_manager_log` VALUES ('2900','1237064724','1','admin','103','3','-','Saving plugin');
INSERT INTO `modx_manager_log` VALUES ('2901','1237064725','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('2902','1237064728','1','admin','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2903','1237064735','1','admin','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2904','1237064739','1','admin','5','46','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2905','1237064743','1','admin','3','46','January','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2906','1237064748','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('2907','1237064766','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2908','1237064771','1','admin','27','40','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2909','1237064778','1','admin','27','55','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2910','1237064783','1','admin','27','80','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2911','1237064798','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2912','1237064805','1','admin','27','49','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2913','1237064812','1','admin','27','48','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2914','1237064813','1','admin','27','48','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2915','1237064834','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2916','1237064847','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2917','1237064850','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2918','1237064855','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2919','1237064862','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2920','1237064874','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2921','1237064889','1','admin','27','86','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2922','1237064893','1','admin','27','86','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2923','1237064898','1','admin','5','86','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2924','1237064901','1','admin','3','86','Ghostmonk Work','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2925','1237064902','1','admin','27','87','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2926','1237064908','1','admin','27','87','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2927','1237064910','1','admin','5','87','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2928','1237064913','1','admin','3','87','Ghostmonk Blog','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2929','1237064920','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2930','1237064927','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2931','1237064932','1','admin','27','84','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2932','1237064937','1','admin','27','84','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2933','1237064940','1','admin','27','88','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2934','1237064946','1','admin','27','88','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2935','1237064955','1','admin','27','70','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2936','1237064959','1','admin','27','84','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2937','1237064963','1','admin','27','84','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2938','1237064966','1','admin','5','84','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2939','1237064970','1','admin','27','88','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2940','1237064970','1','admin','3','84','No So Simpleton Home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2941','1237064972','1','admin','27','88','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2942','1237064978','1','admin','27','88','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2943','1237064980','1','admin','5','88','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2944','1237064982','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2945','1237064988','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2946','1237064990','1','admin','5','69','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2947','1237064994','1','admin','3','69','Not So Simpleton Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2948','1237065020','1','admin','27','70','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2949','1237065026','1','admin','27','70','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2950','1237065029','1','admin','5','70','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2951','1237065032','1','admin','3','70','Popular Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2952','1237065037','1','admin','27','85','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2953','1237065041','1','admin','27','85','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2954','1237065047','1','admin','5','85','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2955','1237065051','1','admin','3','85','MSN','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2956','1237065053','1','admin','27','89','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2957','1237065058','1','admin','27','89','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2958','1237065060','1','admin','5','89','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2959','1237065064','1','admin','3','89','Google','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2960','1237065065','1','admin','27','90','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2961','1237065071','1','admin','27','90','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2962','1237065073','1','admin','5','90','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2963','1237065080','1','admin','3','90','Yahoo','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2964','1237065085','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2965','1237065091','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2966','1237065096','1','admin','5','22','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2967','1237065100','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2968','1237065100','1','admin','3','22','Org Information','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2969','1237065102','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2970','1237065108','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2971','1237065114','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2972','1237065120','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2973','1237065124','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2974','1237065132','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2975','1237065138','1','admin','27','22','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2976','1237065149','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2977','1237065155','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2978','1237065160','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2979','1237065172','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2980','1237065178','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2981','1237065180','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2982','1237065185','1','admin','27','16','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2983','1237065189','1','admin','5','16','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2984','1237065191','1','admin','3','16','Feeds','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2985','1237065193','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2986','1237065198','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2987','1237065200','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2988','1237065203','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2989','1237065206','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2990','1237065212','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2991','1237065217','1','admin','5','18','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2992','1237065220','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2993','1237065222','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2994','1237065227','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2995','1237065230','1','admin','5','20','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('2996','1237065234','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('2997','1237065376','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2998','1237074954','1','admin','27','10','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('2999','1237123453','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3000','1237124225','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3001','1237124251','1','admin','5','82','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3002','1237124254','1','admin','3','82','Ghostmonk Home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3003','1237144711','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3004','1237149643','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3005','1237149659','1','admin','27','91','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3006','1237149717','1','admin','5','91','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3007','1237149719','1','admin','3','91','Updates at CCAP!','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3008','1237149839','1','admin','27','81','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3009','1237149875','1','admin','5','81','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3010','1237149878','1','admin','3','81','HIP HOP PROJECT','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3011','1237149880','1','admin','27','91','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3012','1237149990','1','admin','5','91','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3013','1237149993','1','admin','3','91','Updates at CCAP!','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3014','1237150031','1','admin','27','81','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3015','1237150092','1','admin','5','81','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3016','1237150095','1','admin','3','81','HIP HOP PROJECT','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3017','1237257417','1','admin','3','81','HIP HOP PROJECT','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3018','1237338080','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3019','1237338091','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3020','1237338116','1','admin','5','18','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3021','1237338119','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3022','1237338127','1','admin','6','48','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3023','1237338133','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('3024','1237338139','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3025','1237338778','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3026','1237338983','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3027','1237339051','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3028','1237339060','1','admin','61','92','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3029','1237339065','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3030','1237339076','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3031','1237339085','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3032','1237339089','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3033','1237339101','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3034','1237339107','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3035','1237339110','1','admin','27','94','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3036','1237339114','1','admin','5','94','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3037','1237339117','1','admin','3','94','essays','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3038','1237339118','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3039','1237339126','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3040','1237339132','1','admin','61','96','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3041','1237339136','1','admin','61','95','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3042','1237339143','1','admin','61','95','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3043','1237339147','1','admin','61','94','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3044','1237339151','1','admin','61','93','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3045','1237339170','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3046','1237339545','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3047','1237339554','1','admin','51','56','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('3048','1237339562','1','admin','52','56','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('3049','1237339570','1','admin','51','66','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('3050','1237339577','1','admin','51','56','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('3051','1237339583','1','admin','52','56','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('3052','1237339595','1','admin','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3053','1237339603','1','admin','5','92','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3054','1237339605','1','admin','3','92','essays','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3055','1237339737','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3056','1237339931','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3057','1237339942','1','admin','22','18','linksFeed','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3058','1237340330','1','admin','24','18','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3059','1237340331','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3060','1237340342','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3061','1237340350','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3062','1237340353','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3063','1237340356','1','admin','23','-','New snippet','Creating a new Snippet');
INSERT INTO `modx_manager_log` VALUES ('3064','1237340379','1','admin','24','-','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3065','1237340380','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3066','1237340386','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3067','1237340394','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3068','1237340422','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3069','1237340447','1','admin','5','18','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3070','1237340450','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3071','1237340502','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3072','1237340504','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3073','1237340611','1','admin','24','19','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3074','1237340612','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3075','1237340625','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3076','1237340636','1','admin','24','19','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3077','1237340637','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3078','1237375314','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3079','1237456994','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3080','1237521153','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3081','1237667022','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('3082','1237667026','1','admin','95','-','-','Importing documents from HTML');
INSERT INTO `modx_manager_log` VALUES ('3083','1237667027','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('3084','1237667032','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('3085','1237667081','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('3086','1237667092','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3087','1237667099','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3088','1237667781','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3089','1237668375','2','contentManager','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3090','1237669658','2','contentManager','6','94','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3091','1237669704','2','contentManager','62','94','-','Un-publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3092','1237669727','2','contentManager','3','94','essays','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3093','1237669732','2','contentManager','6','94','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3094','1237669736','2','contentManager','3','4','Resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3095','1237669767','2','contentManager','61','94','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3096','1237669778','2','contentManager','3','94','essays','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3097','1237669901','2','contentManager','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('3098','1237671422','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3099','1237671459','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3100','1237671475','2','contentManager','6','97','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3101','1237671757','2','contentManager','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('3102','1237671893','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3103','1237672328','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3104','1237672336','2','contentManager','61','98','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3105','1237672392','2','contentManager','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3106','1237672395','2','contentManager','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3107','1237672413','2','contentManager','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3108','1237672438','2','contentManager','5','98','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3109','1237672440','2','contentManager','3','98','Essay on Abstract Art','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3110','1237672481','2','contentManager','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3111','1237672486','2','contentManager','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3112','1237672504','2','contentManager','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3113','1237672584','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3114','1237672634','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3115','1237672675','2','contentManager','61','99','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3116','1237672745','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3117','1237672809','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3118','1237672821','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3119','1237672867','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3120','1237672874','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3121','1237672917','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3122','1237672929','2','contentManager','61','100','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3123','1237672936','2','contentManager','61','101','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3124','1237672941','2','contentManager','61','102','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3125','1237672950','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3126','1237673118','2','contentManager','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3127','1237673166','2','contentManager','27','101','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3128','1237673173','2','contentManager','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3129','1237673336','2','contentManager','6','102','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3130','1237673340','2','contentManager','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('3131','1237673349','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3132','1237673365','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3133','1237673369','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3134','1237673372','2','contentManager','61','103','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3135','1237673399','2','contentManager','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3136','1237673406','2','contentManager','27','99','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3137','1237673413','2','contentManager','27','100','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3138','1237673421','2','contentManager','27','101','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3139','1237674200','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3140','1237674250','2','contentManager','3','104','Tenderflake is for losers','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3141','1237674253','2','contentManager','27','104','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3142','1237674269','2','contentManager','6','104','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3143','1237674731','2','contentManager','6','103','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3144','1237674733','2','contentManager','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('3145','1237674736','2','contentManager','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3146','1237674750','2','contentManager','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3147','1237674757','2','contentManager','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('3148','1237674761','2','contentManager','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('3149','1237674767','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3150','1237674770','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3151','1237674775','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3152','1237677329','1','admin','24','19','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3153','1237677330','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3154','1237677474','1','admin','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('3155','1237677479','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3156','1237677481','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3157','1237677495','1','admin','24','19','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3158','1237677495','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3159','1237677577','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3160','1237684115','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3161','1237694030','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3162','1237694066','2','contentManager','27','100','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3163','1237694079','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3164','1237694583','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3165','1237694587','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3166','1237694619','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3167','1237694625','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3168','1237694652','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3169','1237694655','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3170','1237697500','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3171','1237697508','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3172','1237697567','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3173','1237697576','2','contentManager','61','93','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3174','1237697586','2','contentManager','61','105','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3175','1237697590','2','contentManager','61','106','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3176','1237697594','2','contentManager','61','107','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3177','1237697599','2','contentManager','61','108','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3178','1237697603','2','contentManager','61','109','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3179','1237697824','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3180','1237697892','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3181','1237697899','2','contentManager','61','110','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3182','1237697904','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3183','1237697953','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3184','1237704427','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3185','1237704487','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3186','1237704492','2','contentManager','61','111','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3187','1237704496','2','contentManager','61','112','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3188','1237704501','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3189','1237704600','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3190','1237704606','2','contentManager','61','113','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3191','1237704618','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3192','1237704717','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3193','1237704723','2','contentManager','61','114','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3194','1237704726','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3195','1237704768','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3196','1237704776','2','contentManager','61','115','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3197','1237704781','2','contentManager','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('3198','1237704784','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3199','1237704817','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3200','1237704829','2','contentManager','61','116','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3201','1237724808','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3202','1237739067','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3203','1237739089','1','admin','27','114','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3204','1237759870','1','admin','27','114','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3205','1237760618','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3206','1237762468','2','contentManager','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3207','1237762484','2','contentManager','27','99','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3208','1237762511','2','contentManager','5','99','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3209','1237762513','2','contentManager','3','99','essay about the moon','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3210','1237762514','2','contentManager','27','100','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3211','1237762537','2','contentManager','5','100','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3212','1237762539','2','contentManager','27','101','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3213','1237762560','2','contentManager','5','101','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3214','1237762563','2','contentManager','3','101','ESSAy no image','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3215','1237762565','2','contentManager','27','105','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3216','1237762579','2','contentManager','5','105','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3217','1237762581','2','contentManager','3','105','Children having children','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3218','1237762582','2','contentManager','27','106','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3219','1237762603','2','contentManager','5','106','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3220','1237762605','2','contentManager','3','106','Underage drinking','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3221','1237762606','2','contentManager','27','107','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3222','1237762636','2','contentManager','5','107','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3223','1237762639','2','contentManager','27','108','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3224','1237762639','2','contentManager','3','107','teen suicide','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3225','1237762647','2','contentManager','27','108','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3226','1237762664','2','contentManager','5','108','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3227','1237762667','2','contentManager','3','108','prescription drugs: good vs bad','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3228','1237762667','2','contentManager','27','109','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3229','1237762692','2','contentManager','5','109','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3230','1237762695','2','contentManager','3','109','the real facts: teens and gangs','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3231','1237762702','2','contentManager','27','110','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3232','1237762726','2','contentManager','5','110','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3233','1237762728','2','contentManager','3','110','interview with Einstein','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3234','1237762729','2','contentManager','27','111','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3235','1237762756','2','contentManager','5','111','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3236','1237762759','2','contentManager','3','111','interview with Tonya','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3237','1237762760','2','contentManager','27','112','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3238','1237762807','2','contentManager','5','112','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3239','1237762809','2','contentManager','27','113','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3240','1237762828','2','contentManager','5','113','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3241','1237762830','2','contentManager','3','113','Interview with Van Gogh','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3242','1237762840','2','contentManager','3','113','Interview with Van Gogh','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3243','1237762841','2','contentManager','27','115','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3244','1237762874','2','contentManager','5','115','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3245','1237762880','2','contentManager','27','116','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3246','1237762891','2','contentManager','5','116','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3247','1237762893','2','contentManager','3','116','Assignment # 3 - write someone a letter','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3248','1237763747','1','admin','27','110','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3249','1237763807','1','admin','27','114','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3250','1237763839','1','admin','5','114','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3251','1237763842','1','admin','3','114','Assignment # 1 - Visit Someone in a Hospital','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3252','1237766276','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('3253','1237766280','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('3254','1237766281','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3255','1237766288','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3256','1237766567','1','admin','24','19','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3257','1237766568','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3258','1237767901','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3259','1237767923','1','admin','24','19','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3260','1237767924','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3261','1237767951','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3262','1237767984','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3263','1237767985','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3264','1237767988','1','admin','27','83','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3265','1237768033','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3266','1237768035','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3267','1237768108','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3268','1237768108','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3269','1237768135','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3270','1237768143','1','admin','24','19','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3271','1237768144','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3272','1237768168','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3273','1237769401','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3274','1237769547','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3275','1237769553','1','admin','61','117','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3276','1237850473','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('3277','1237850497','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3278','1237850499','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3279','1237850502','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3280','1237850510','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3281','1237850564','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3282','1237850565','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3283','1237850580','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3284','1237850631','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3285','1237850631','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3286','1237850653','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3287','1237850653','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3288','1237850670','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3289','1237850670','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3290','1237850720','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3291','1237850720','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3292','1237852423','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3293','1237852423','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3294','1237852446','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3295','1237852446','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3296','1237852465','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3297','1237852465','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3298','1237852542','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3299','1237852542','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3300','1237852585','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3301','1237852586','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3302','1237852609','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3303','1237852609','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3304','1237852642','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3305','1237852643','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3306','1237852655','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3307','1237852655','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3308','1237852820','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3309','1237852820','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3310','1237852917','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3311','1237852917','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3312','1237852917','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3313','1237852920','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3314','1237852949','1','admin','24','19','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3315','1237852949','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3316','1237934496','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3317','1237946590','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3318','1237946605','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3319','1237946694','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3320','1237946763','1','admin','27','99','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3321','1237946767','1','admin','27','100','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3322','1237946770','1','admin','27','101','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3323','1237946773','1','admin','27','117','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3324','1237980936','1','admin','27','117','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3325','1238034874','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3326','1238034890','1','admin','27','101','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3327','1238036203','1','admin','5','101','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3328','1238036205','1','admin','3','101','ESSAy no image','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3329','1238081177','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3330','1238081346','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3331','1238081584','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3332','1238082737','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3333','1238082740','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3334','1238082742','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3335','1238082742','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3336','1238114954','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3337','1238193831','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3338','1238251101','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3339','1238251169','1','admin','5','98','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3340','1238251170','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('3341','1238251337','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('3342','1238251343','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3343','1238251437','1','admin','5','98','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3344','1238251439','1','admin','3','98','Essay on Abstract Art','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3345','1238251602','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3346','1238251636','1','admin','5','98','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3347','1238251639','1','admin','3','98','Essay on Abstract Art','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3348','1238251660','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3349','1238251796','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3350','1238251819','1','admin','5','98','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3351','1238251822','1','admin','3','98','Essay on Abstract Art','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3352','1238251832','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3353','1238251859','1','admin','5','98','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3354','1238251863','1','admin','3','98','Essay on Abstract Art','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3355','1238253377','1','admin','17','-','-','Editing settings');
INSERT INTO `modx_manager_log` VALUES ('3356','1238253389','1','admin','30','-','-','Saving settings');
INSERT INTO `modx_manager_log` VALUES ('3357','1238253397','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3358','1238253398','1','admin','3','92','essays','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3359','1238253401','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3360','1238284692','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3361','1238290673','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3362','1238294071','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3363','1238302033','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3364','1238331720','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3365','1238354376','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3366','1238356983','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3367','1238365817','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3368','1238413884','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3369','1238414235','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3370','1238452767','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3371','1238468659','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3372','1238472664','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3373','1238499602','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3374','1238627144','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3375','1238728533','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3376','1238771676','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3377','1238781978','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3378','1238781993','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3379','1238845776','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3380','1238861341','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3381','1238861348','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3382','1238871999','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3383','1238874927','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3384','1238875612','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3385','1238877392','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3386','1238935496','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3387','1239139274','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3388','1239399667','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3389','1239399698','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('3390','1239399700','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3391','1239399722','1','admin','22','18','linksFeed','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3392','1242905236','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3393','1242905258','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3394','1242905297','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3395','1242905304','1','admin','27','118','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3396','1242905308','1','admin','5','118','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3397','1242905311','1','admin','3','118','May','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3398','1242905314','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3399','1242905325','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3400','1242906890','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3401','1242906895','1','admin','27','119','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3402','1242906905','1','admin','5','119','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3403','1242906908','1','admin','3','119','Empty','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3404','1242906918','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3405','1242907002','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3406','1242907009','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3407','1242907040','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3408','1242907045','1','admin','27','121','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3409','1242907049','1','admin','5','121','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3410','1242907051','1','admin','3','121','Bonbons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3411','1242907057','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3412','1242907084','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3413','1242934666','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3414','1242934680','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3415','1242934684','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3416','1244386902','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3417','1244386913','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3418','1244386915','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3419','1244394005','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3420','1244411900','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3421','1244412532','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3422','1244412534','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3423','1244412689','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3424','1244412691','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3425','1244412708','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3426','1244412711','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3427','1244412748','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3428','1244412766','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3429','1244412766','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3430','1244412926','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3431','1244412926','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3432','1244412990','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3433','1244412990','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3434','1244412994','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3435','1244413000','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3436','1244413008','1','admin','51','1','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('3437','1244413015','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3438','1244413028','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3439','1244413030','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3440','1244413039','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('3441','1244413042','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('3442','1244413044','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3443','1244413059','1','admin','22','3','Ditto','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3444','1244413076','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3445','1244413092','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('3446','1244413094','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3447','1244413099','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3448','1244413208','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3449','1244413211','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3450','1244413522','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3451','1244413525','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3452','1244413586','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3453','1244413789','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3454','1244413792','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3455','1244413798','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3456','1244413818','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3457','1244413821','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3458','1244413879','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3459','1244413881','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3460','1244413906','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3461','1244413985','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3462','1244413988','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3463','1244413991','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3464','1244414034','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3465','1244414034','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3466','1244414085','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3467','1244414085','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3468','1244414092','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3469','1244414100','1','admin','22','3','Ditto','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3470','1244414134','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3471','1244414143','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3472','1244414146','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3473','1244414150','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3474','1244414204','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3475','1244414206','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3476','1244414210','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3477','1244414263','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3478','1244414267','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3479','1244414296','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3480','1244414296','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3481','1244414315','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3482','1244414340','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3483','1244414341','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3484','1244414349','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3485','1244414425','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3486','1244414427','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3487','1244414584','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3488','1244414592','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3489','1244414595','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3490','1244414609','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3491','1244414680','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3492','1244414683','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3493','1244414712','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3494','1244414745','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3495','1244414748','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3496','1244414768','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3497','1244414785','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3498','1244414788','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3499','1244414812','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3500','1244414863','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3501','1244414865','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3502','1244414918','1','admin','79','2','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3503','1244414918','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3504','1244414932','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3505','1244414944','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3506','1244414946','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3507','1244415120','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3508','1244415135','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3509','1244415135','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3510','1244419571','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3511','1244419571','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3512','1244419807','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3513','1244419808','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3514','1244419859','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3515','1244419859','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3516','1244420078','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3517','1244420137','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3518','1244420140','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('3519','1244420140','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3520','1244420144','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('3521','1244420145','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3522','1244420149','1','admin','78','3','videoRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3523','1244420164','1','admin','79','3','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3524','1244420164','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3525','1244420169','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3526','1244420297','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3527','1244420311','1','admin','3','20','Links Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3528','1244420313','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3529','1244420592','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3530','1244420609','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3531','1244420612','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3532','1244420613','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3533','1244420622','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3534','1244420625','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3535','1244420695','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3536','1244420716','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3537','1244420719','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3538','1244420721','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3539','1244420727','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3540','1244420730','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3541','1244420768','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3542','1244420771','1','admin','78','3','videoRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3543','1244420778','1','admin','79','3','-','Saving Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3544','1244420779','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3545','1244593055','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3546','1244593078','1','admin','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3547','1247413162','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3548','1247421343','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3549','1247889563','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3550','1247890035','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3551','1247890218','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3552','1247921113','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3553','1248010768','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3554','1248051345','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3555','1248051350','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3556','1248051384','1','admin','94','41','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('3557','1248051384','1','admin','3','123','Duplicate of January','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3558','1248051393','1','admin','61','123','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3559','1248051403','1','admin','61','124','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3560','1248051406','1','admin','61','125','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3561','1248051410','1','admin','61','126','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3562','1248051413','1','admin','61','127','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3563','1248051417','1','admin','61','128','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3564','1248051428','1','admin','61','128','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3565','1248051728','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3566','1248051858','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3567','1248051861','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3568','1248051871','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3569','1248052356','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3570','1248052359','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3571','1248052385','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3572','1248092939','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3573','1248147184','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3574','1248180050','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3575','1248180069','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3576','1248180072','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3577','1249140229','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3578','1249140242','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('3579','1249140244','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('3580','1249140249','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('3581','1249140274','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('3582','1249140281','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('3583','1249140308','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('3584','1249140310','1','admin','12','1','admin','Editing user');
INSERT INTO `modx_manager_log` VALUES ('3585','1249140316','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('3586','1249140317','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('3587','1249140344','1','admin','32','2','-','Saving user');
INSERT INTO `modx_manager_log` VALUES ('3588','1249140367','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('3589','1249140409','1','admin','3','5','Diaries','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3590','1249140450','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3591','1249160108','1','admin','93','-','-','Backup Manager');
INSERT INTO `modx_manager_log` VALUES ('3592','1249160156','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3593','1249160184','1','admin','22','16','WebSignup','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3594','1249185525','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3595','1249185544','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3596','1249185549','2','contentManager','3','45','2009','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3597','1249185561','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3598','1249185577','2','contentManager','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3599','1249185585','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3600','1249185643','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3601','1249185652','2','contentManager','6','129','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3602','1249185665','2','contentManager','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3603','1249185675','2','contentManager','27','31','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3604','1249185693','2','contentManager','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3605','1249185714','2','contentManager','51','129','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('3606','1249185735','2','contentManager','51','129','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('3607','1249185747','2','contentManager','52','129','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('3608','1249185820','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3609','1249185841','2','contentManager','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('3610','1249185851','2','contentManager','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3611','1249185892','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3612','1249185900','2','contentManager','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3613','1249185912','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3614','1249185932','2','contentManager','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3615','1249188718','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3616','1249188726','2','contentManager','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3617','1249188745','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3618','1249188775','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3619','1249188814','2','contentManager','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3620','1249188852','2','contentManager','27','45','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3621','1249188864','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3622','1249189044','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3623','1249189060','2','contentManager','51','129','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('3624','1249189066','2','contentManager','52','129','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('3625','1249189190','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3626','1249189493','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3627','1249189508','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3628','1249189550','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3629','1249189597','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3630','1249189634','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3631','1249189644','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3632','1249189726','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3633','1249189840','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3634','1249189887','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3635','1249189895','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3636','1249189920','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3637','1249189935','2','contentManager','61','131','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3638','1249189966','2','contentManager','61','129','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3639','1249189989','2','contentManager','61','129','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3640','1249190000','2','contentManager','61','132','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3641','1249190010','2','contentManager','61','129','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3642','1249190025','2','contentManager','61','133','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3643','1249190034','2','contentManager','61','134','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3644','1249190041','2','contentManager','61','135','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3645','1249190050','2','contentManager','61','136','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3646','1249190059','2','contentManager','61','137','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3647','1249190104','2','contentManager','27','129','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3648','1249190124','2','contentManager','5','129','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3649','1249190129','2','contentManager','3','129','VIDEO TEST 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3650','1249190133','2','contentManager','61','129','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3651','1249190366','2','contentManager','62','129','-','Un-publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3652','1249233978','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3653','1249233994','1','admin','27','129','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3654','1249234065','1','admin','3','129','VIDEO TEST 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3655','1249234070','1','admin','27','129','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3656','1249234118','1','admin','3','129','VIDEO TEST 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3657','1249234126','1','admin','27','129','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3658','1249234409','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3659','1249235184','1','admin','3','129','VIDEO TEST 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3660','1249235188','1','admin','27','129','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3661','1249235188','1','admin','27','129','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3662','1249235194','1','admin','5','129','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3663','1249235197','1','admin','3','129','VIDEO TEST 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3664','1249235201','1','admin','3','129','VIDEO TEST 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3665','1249235210','1','admin','27','129','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3666','1249235227','1','admin','61','129','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3667','1249235234','1','admin','3','132','VIDEO TEST 3','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3668','1249235235','1','admin','3','129','VIDEO TEST 1','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3669','1249235237','1','admin','27','129','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3670','1249235258','1','admin','6','129','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3671','1249235274','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('3672','1249235279','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3673','1249235300','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('3674','1249235423','1','admin','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3675','1249235461','1','admin','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3676','1249235471','1','admin','61','138','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3677','1249235481','1','admin','6','138','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3678','1249235490','1','admin','61','138','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3679','1249235502','1','admin','63','138','-','Un-deleting a document');
INSERT INTO `modx_manager_log` VALUES ('3680','1249235508','1','admin','62','138','-','Un-publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3681','1249235515','1','admin','6','138','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('3682','1249235523','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('3683','1249235566','1','admin','62','137','-','Un-publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3684','1249235575','1','admin','61','137','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3685','1249237278','2','contentManager','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('3686','1249237385','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3687','1249237594','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3688','1249237603','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3689','1249237894','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3690','1249237954','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3691','1249239676','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3692','1249239713','2','contentManager','27','131','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3693','1249239727','2','contentManager','5','131','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3694','1249239734','2','contentManager','3','131','VIDEO TEST 2','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3695','1249239754','2','contentManager','51','141','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('3696','1249239760','2','contentManager','52','141','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('3697','1249239771','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3698','1249239897','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3699','1249239928','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3700','1249240053','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3701','1249240067','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3702','1249240093','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3703','1249240227','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3704','1249240245','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3705','1249240341','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3706','1249240496','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3707','1249240608','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3708','1249240623','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3709','1249240662','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3710','1249240674','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3711','1249240715','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3712','1249240740','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3713','1249240867','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3714','1249240893','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3715','1249240956','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3716','1249240968','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3717','1249241010','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3718','1249241029','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3719','1249241069','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3720','1249241077','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3721','1249241566','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3722','1249241582','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3723','1249241669','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3724','1249241679','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3725','1249241723','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3726','1249241737','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3727','1249241861','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3728','1249241885','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3729','1249241996','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3730','1249242008','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3731','1249242068','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3732','1249242085','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3733','1249242139','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3734','1249242151','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3735','1249242199','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3736','1249242203','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3737','1249242608','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3738','1249242621','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3739','1249242667','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3740','1249242679','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3741','1249242736','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3742','1249242757','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3743','1249242826','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3744','1249242857','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3745','1249242899','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3746','1249242912','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3747','1249242964','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3748','1249242987','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3749','1249243039','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3750','1249243058','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3751','1249243110','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3752','1249243134','2','contentManager','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('3753','1249243170','2','contentManager','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3754','1249243189','2','contentManager','61','141','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3755','1249243210','2','contentManager','61','139','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3756','1249243219','2','contentManager','61','140','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3757','1249243231','2','contentManager','61','142','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3758','1249243245','2','contentManager','61','143','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3759','1249243273','2','contentManager','61','144','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3760','1249243285','2','contentManager','61','145','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3761','1249243305','2','contentManager','61','146','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3762','1249243318','2','contentManager','61','147','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3763','1249243413','2','contentManager','61','148','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3764','1249243426','2','contentManager','61','149','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3765','1249243437','2','contentManager','61','150','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3766','1249243451','2','contentManager','61','151','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3767','1249243464','2','contentManager','61','152','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3768','1249243479','2','contentManager','61','153','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3769','1249243491','2','contentManager','61','154','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3770','1249243500','2','contentManager','61','155','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3771','1249243514','2','contentManager','61','156','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3772','1249243526','2','contentManager','61','157','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3773','1249243573','2','contentManager','61','158','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3774','1249243582','2','contentManager','61','159','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3775','1249243595','2','contentManager','61','160','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3776','1249243608','2','contentManager','61','161','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3777','1249243620','2','contentManager','61','162','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3778','1249243635','2','contentManager','61','163','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3779','1249243647','2','contentManager','61','164','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3780','1249243658','2','contentManager','61','165','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3781','1249243673','2','contentManager','61','166','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3782','1249243684','2','contentManager','61','167','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3783','1249243695','2','contentManager','62','168','-','Un-publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3784','1249243708','2','contentManager','61','168','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3785','1249243719','2','contentManager','61','169','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3786','1249245548','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3787','1249412345','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3788','1249743980','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3789','1249744004','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3790','1249744005','1','admin','3','66','February Item 07','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3791','1249744019','1','admin','94','56','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('3792','1249744020','1','admin','3','170','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3793','1249744022','1','admin','3','170','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3794','1249744027','1','admin','61','170','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3795','1249744062','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3796','1249744068','1','admin','94','56','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('3797','1249744069','1','admin','3','171','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3798','1249744076','1','admin','61','171','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3799','1249744275','1','admin','94','56','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('3800','1249744276','1','admin','3','172','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3801','1249744284','1','admin','61','172','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3802','1249744338','1','admin','94','56','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('3803','1249744338','1','admin','3','173','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3804','1249744345','1','admin','61','173','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3805','1249744579','1','admin','3','56','February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3806','1249744582','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3807','1249744622','1','admin','94','56','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('3808','1249744622','1','admin','3','174','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3809','1249744625','1','admin','94','56','-','Duplicate Document');
INSERT INTO `modx_manager_log` VALUES ('3810','1249744625','1','admin','3','175','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3811','1249744631','1','admin','61','175','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3812','1249744632','1','admin','3','174','Duplicate of February Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3813','1249744635','1','admin','61','174','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('3814','1249756826','1','admin','3','18','Resources Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3815','1249756834','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3816','1249756955','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3817','1249757032','1','admin','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3818','1249757044','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('3819','1249757048','1','admin','112','1','Doc Manager','Execute module');
INSERT INTO `modx_manager_log` VALUES ('3820','1249757061','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3821','1249757066','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3822','1249757072','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3823','1249757076','1','admin','19','-','New template','Creating a new template');
INSERT INTO `modx_manager_log` VALUES ('3824','1249757132','1','admin','20','-','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3825','1249757132','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3826','1249757141','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3827','1249757146','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3828','1249757150','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3829','1249757151','1','admin','5','4','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3830','1249757154','1','admin','3','4','Resources','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3831','1249757156','1','admin','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3832','1249757160','1','admin','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3833','1249757162','1','admin','5','92','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3834','1249757164','1','admin','3','92','essays','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3835','1249757167','1','admin','27','93','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3836','1249757171','1','admin','27','93','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3837','1249757173','1','admin','5','93','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3838','1249757176','1','admin','27','95','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3839','1249757176','1','admin','3','93','facts','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3840','1249757178','1','admin','27','93','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3841','1249757182','1','admin','5','93','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3842','1249757184','1','admin','27','96','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3843','1249757189','1','admin','27','96','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3844','1249757192','1','admin','5','96','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3845','1249757195','1','admin','3','96','assignments','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3846','1249757902','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3847','1249757907','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3848','1249757911','1','admin','5','98','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3849','1249757914','1','admin','3','98','Essay on Abstract Art','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3850','1249757916','1','admin','27','99','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3851','1249757922','1','admin','27','99','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3852','1249757923','1','admin','5','99','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3853','1249757926','1','admin','3','99','essay about the moon','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3854','1249757928','1','admin','27','100','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3855','1249757933','1','admin','27','100','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3856','1249757934','1','admin','5','100','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3857','1249757937','1','admin','3','100','stop motion animation at its best','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3858','1249757939','1','admin','27','101','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3859','1249757949','1','admin','27','101','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3860','1249757951','1','admin','5','101','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3861','1249757954','1','admin','3','101','ESSAy no image','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3862','1249757956','1','admin','27','117','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3863','1249757963','1','admin','27','117','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3864','1249757964','1','admin','5','117','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('3865','1249757967','1','admin','3','117','Incidence of  substance abuse in the sex trade','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3866','1249758162','1','admin','27','114','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3867','1249758175','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3868','1249758178','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3869','1249758180','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3870','1249758195','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3871','1249759009','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3872','1249759012','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3873','1249759014','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3874','1249759015','1','admin','16','6','Straight Text','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3875','1249759017','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3876','1249759019','1','admin','16','5','Video Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3877','1249759021','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3878','1249759025','1','admin','301','1','Post an Image','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('3879','1249759031','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3880','1249759033','1','admin','301','2','video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('3881','1249759034','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3882','1249759037','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('3883','1249759038','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3884','1249759041','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('3885','1249759060','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3886','1249759085','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3887','1249759090','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3888','1249759116','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3889','1249759120','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3890','1249759124','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3891','1249759126','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3892','1249759127','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3893','1249759133','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3894','1249759133','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3895','1249759142','1','admin','300','-','New Template Variable','Create Template Variable');
INSERT INTO `modx_manager_log` VALUES ('3896','1249759143','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3897','1249759151','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3898','1249759164','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3899','1249759164','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3900','1249759203','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3901','1249759225','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3902','1249759226','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3903','1249759265','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3904','1249759293','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3905','1249759294','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3906','1249759391','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3907','1249759400','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3908','1249759400','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3909','1249759408','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3910','1249759507','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3911','1249759507','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3912','1249759522','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3913','1249759527','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3914','1249759527','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3915','1249759546','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3916','1249759605','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3917','1249759611','1','admin','22','9','NewsPublisher','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3918','1249759625','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3919','1249759658','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3920','1249759673','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3921','1249759673','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3922','1249759686','1','admin','23','-','New snippet','Creating a new Snippet');
INSERT INTO `modx_manager_log` VALUES ('3923','1249759687','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3924','1249759692','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3925','1249759763','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3926','1249759764','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3927','1249759768','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3928','1249759808','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3929','1249759808','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3930','1249759812','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3931','1249759814','1','admin','23','-','New snippet','Creating a new Snippet');
INSERT INTO `modx_manager_log` VALUES ('3932','1249759825','1','admin','24','-','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3933','1249759826','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3934','1249759831','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3935','1249759851','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3936','1249759861','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3937','1249759883','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3938','1249759883','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3939','1249759896','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3940','1249759924','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3941','1249759925','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3942','1249759930','1','admin','22','18','linksFeed','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3943','1249759967','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3944','1249759972','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3945','1249759993','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3946','1249759993','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3947','1249760015','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3948','1249760016','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3949','1249760017','1','admin','22','16','WebSignup','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3950','1249760019','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3951','1249760020','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3952','1249760028','1','admin','24','17','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3953','1249760029','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3954','1249760046','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3955','1249760059','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3956','1249760068','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3957','1249760069','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3958','1249760071','1','admin','22','20','authorDate','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3959','1249760094','1','admin','24','20','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3960','1249760094','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3961','1249760106','1','admin','22','20','authorDate','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3962','1249760116','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3963','1249760119','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3964','1249760120','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3965','1249760122','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3966','1249760135','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3967','1249760136','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3968','1249760228','1','admin','22','19','xmlTreeParser','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3969','1249760259','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3970','1249760263','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3971','1249760265','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3972','1249760267','1','admin','22','20','authorDate','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3973','1249760312','1','admin','24','20','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3974','1249760312','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3975','1249760324','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3976','1249760328','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3977','1249760335','1','admin','22','20','authorDate','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3978','1249760360','1','admin','24','20','-','Saving Snippet');
INSERT INTO `modx_manager_log` VALUES ('3979','1249760360','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3980','1249760394','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3981','1249760398','1','admin','22','17','testing','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3982','1249760399','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3983','1249760401','1','admin','22','20','authorDate','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('3984','1249760404','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3985','1249760407','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('3986','1249760420','1','admin','20','7','-','Saving template');
INSERT INTO `modx_manager_log` VALUES ('3987','1249760420','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3988','1251059293','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3989','1251059300','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('3990','1251059310','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3991','1251059314','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3992','1251059465','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('3993','1251059620','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('3994','1251059627','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3995','1251059634','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3996','1251060065','1','admin','3','137','Video Test 8','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3997','1251060067','1','admin','27','137','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('3998','1251060071','1','admin','3','139','Video Test 9','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('3999','1251060073','1','admin','27','139','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4000','1251060149','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4001','1251060162','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4002','1251060170','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4003','1251060173','1','admin','301','3','thumbnail used for video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('4004','1251060185','1','admin','3','131','VIDEO TEST 2','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4005','1251060187','1','admin','27','131','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4006','1251060191','1','admin','3','141','Video Test 11','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4007','1251060193','1','admin','27','141','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4008','1251060206','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4009','1251060208','1','admin','301','3','thumbnail used for video','Edit Template Variable');
INSERT INTO `modx_manager_log` VALUES ('4010','1251060218','1','admin','302','3','-','Save Template Variable');
INSERT INTO `modx_manager_log` VALUES ('4011','1251060218','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4012','1251163213','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4013','1251169107','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4014','1251858393','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4015','1251858421','1','admin','27','124','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4016','1251858858','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4017','1251858859','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4018','1251858863','1','admin','16','7','Resource Format','Editing template');
INSERT INTO `modx_manager_log` VALUES ('4019','1251859284','2','contentManager','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4020','1251859882','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4021','1251859886','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('4022','1251859895','1','admin','31','-','-','Using file manager');
INSERT INTO `modx_manager_log` VALUES ('4023','1251859900','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4024','1251859911','1','admin','78','2','ImageRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('4025','1251859930','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4026','1251859933','1','admin','78','4','linksFeed','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('4027','1251859947','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4028','1251859950','1','admin','78','3','videoRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('4029','1251859958','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4030','1251859967','1','admin','78','1','WebLoginSideBar','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('4031','1251859969','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4032','1251859975','1','admin','16','5','Video Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('4033','1251859977','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4034','1251859979','1','admin','16','6','Straight Text','Editing template');
INSERT INTO `modx_manager_log` VALUES ('4035','1251859981','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4036','1251860161','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('4037','1251860166','1','admin','12','2','contentManager','Editing user');
INSERT INTO `modx_manager_log` VALUES ('4038','1251860226','1','admin','32','2','-','Saving user');
INSERT INTO `modx_manager_log` VALUES ('4039','1251860227','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('4040','1251860231','1','admin','12','2','aDavis','Editing user');
INSERT INTO `modx_manager_log` VALUES ('4041','1251860268','1','admin','32','2','-','Saving user');
INSERT INTO `modx_manager_log` VALUES ('4042','1251860319','1','admin','99','-','-','Manage Web Users');
INSERT INTO `modx_manager_log` VALUES ('4043','1251860322','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4044','1251860354','1','admin','27','81','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4045','1251860360','2','aDavis','28','-','-','Changing password');
INSERT INTO `modx_manager_log` VALUES ('4046','1251860360','2','aDavis','28','-','-','Changing password');
INSERT INTO `modx_manager_log` VALUES ('4047','1251860364','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4048','1251860388','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4049','1251860411','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4050','1251860488','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4051','1251860568','1','admin','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4052','1251860591','1','admin','27','101','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4053','1251860654','1','admin','27','100','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4054','1251860676','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4055','1251860741','1','admin','27','85','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4056','1251860874','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4057','1251860899','1','admin','27','84','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4058','1251861454','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4059','1251861475','1','admin','27','124','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4060','1251861493','1','admin','27','125','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4061','1251861499','2','aDavis','27','124','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4062','1251861541','1','admin','27','126','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4063','1251861770','1','admin','5','126','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4064','1251861775','1','admin','3','126','Whatever','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4065','1251861803','1','admin','27','126','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4066','1251861851','1','admin','5','126','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4067','1251861854','1','admin','3','126','Whatever','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4068','1251861937','1','admin','27','126','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4069','1251862014','1','admin','3','124','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4070','1251862019','1','admin','3','124','Item 01','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4071','1251862071','1','admin','27','131','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4072','1251862082','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4073','1251862087','2','aDavis','27','124','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4074','1251862094','2','aDavis','27','30','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4075','1251862116','1','admin','27','46','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4076','1251862131','1','admin','27','3','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4077','1251862146','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4078','1251862185','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4079','1251862201','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4080','1251862210','1','admin','27','130','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4081','1251862264','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4082','1251862303','1','admin','5','130','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4083','1251862306','1','admin','3','130','August','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4084','1251862310','1','admin','27','130','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4085','1251862351','1','admin','27','130','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4086','1251862383','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4087','1251862402','2','aDavis','27','29','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4088','1251862615','1','admin','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4089','1251862627','1','admin','27','174','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4090','1251862724','1','admin','27','4','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4091','1251862741','2','aDavis','3','98','Essay on Abstract Art','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4092','1251862746','2','aDavis','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4093','1251862750','2','aDavis','3','99','essay about the moon','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4094','1251862755','2','aDavis','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4095','1251862771','1','admin','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4096','1251862796','1','admin','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4097','1251862805','1','admin','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4098','1251862807','1','admin','27','99','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4099','1251862917','2','aDavis','5','98','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4100','1251862920','2','aDavis','3','98','Essay on Abstract Art','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4101','1251862966','2','aDavis','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4102','1251863097','2','aDavis','27','105','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4103','1251863111','2','aDavis','5','105','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4104','1251863114','2','aDavis','3','105','Children having children','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4105','1251863126','2','aDavis','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4106','1251863139','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4107','1251863154','2','aDavis','5','82','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4108','1251863157','2','aDavis','3','82','Ghostmonk Home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4109','1251863168','2','aDavis','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4110','1251863237','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4111','1251863242','2','aDavis','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4112','1251863251','2','aDavis','5','6','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4113','1251863254','2','aDavis','3','6','Link','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4114','1251863258','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4115','1251863263','2','aDavis','3','6','Link','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4116','1251863264','1','admin','5','6','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4117','1251863267','1','admin','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4118','1251863270','2','aDavis','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4119','1251863276','2','aDavis','5','6','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4120','1251863279','2','aDavis','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4121','1251863364','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4122','1251863393','2','aDavis','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4123','1251863396','2','aDavis','27','87','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4124','1251863397','1','admin','5','82','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4125','1251863399','1','admin','3','82','Ghostmonk Home','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4126','1251863402','1','admin','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4127','1251863411','2','aDavis','5','87','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4128','1251863413','2','aDavis','3','87','Ghostmonk Blog','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4129','1251863414','2','aDavis','27','87','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4130','1251863456','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4131','1251863472','2','aDavis','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4132','1251863506','1','admin','27','68','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4133','1251863514','1','admin','27','69','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4134','1251863522','1','admin','27','6','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4135','1251863528','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4136','1251863544','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4137','1251863559','1','admin','3','22','Org Information','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4138','1251863679','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4139','1251863684','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4140','1251863709','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4141','1251863717','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4142','1251863723','1','admin','106','-','-','Viewing Modules');
INSERT INTO `modx_manager_log` VALUES ('4143','1251863729','1','admin','75','-','-','User/ role management');
INSERT INTO `modx_manager_log` VALUES ('4144','1251863779','1','admin','86','-','-','Role management');
INSERT INTO `modx_manager_log` VALUES ('4145','1251863786','1','admin','35','2','Content Manger','Editing role');
INSERT INTO `modx_manager_log` VALUES ('4146','1251863855','1','admin','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4147','1251939128','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4148','1251939139','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4149','1251939161','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4150','1251939201','2','aDavis','27','81','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4151','1251939252','2','aDavis','27','91','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4152','1251939278','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4153','1251939311','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4154','1251939330','2','aDavis','27','177','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4155','1251939362','2','aDavis','5','177','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4156','1251939365','2','aDavis','3','177','News Test Alison','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4157','1251940868','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4158','1251940949','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4159','1251940958','2','aDavis','28','-','-','Changing password');
INSERT INTO `modx_manager_log` VALUES ('4160','1251940963','2','aDavis','28','-','-','Changing password');
INSERT INTO `modx_manager_log` VALUES ('4161','1251940967','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4162','1251940984','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4163','1251941047','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4164','1251941132','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4165','1251941156','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4166','1251941301','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4167','1251941350','2','aDavis','61','179','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4168','1251941414','2','aDavis','6','177','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4169','1251941432','2','aDavis','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('4170','1251941452','2','aDavis','6','179','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4171','1251941486','2','aDavis','3','179','News Test Alison on the phone','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4172','1251941488','2','aDavis','27','179','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4173','1251941511','2','aDavis','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('4174','1251941545','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4175','1251941677','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4176','1251941686','2','aDavis','61','180','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4177','1251941736','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4178','1251941873','2','aDavis','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4179','1251941882','2','aDavis','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4180','1251941895','2','aDavis','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4181','1251941908','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4182','1251942153','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4183','1251942176','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4184','1251942184','2','aDavis','61','181','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4185','1251942553','2','aDavis','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4186','1251942625','2','aDavis','27','92','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4187','1251942633','2','aDavis','5','92','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4188','1251942636','2','aDavis','3','92','articles','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4189','1251942659','2','aDavis','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4190','1251942711','2','aDavis','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4191','1251942777','2','aDavis','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4192','1251942808','2','aDavis','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4193','1251942816','2','aDavis','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4194','1251942865','2','aDavis','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4195','1251942966','2','aDavis','27','119','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4196','1251947493','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4197','1251947815','2','aDavis','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4198','1251951836','2','aDavis','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4199','1251954545','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4200','1251954779','2','aDavis','27','98','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4201','1251955206','2','aDavis','27','82','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4202','1251956143','2','aDavis','27','55','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4203','1251956150','2','aDavis','27','178','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4204','1251956486','2','aDavis','6','64','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4205','1251956523','2','aDavis','61','64','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4206','1251956596','2','aDavis','63','64','-','Un-deleting a document');
INSERT INTO `modx_manager_log` VALUES ('4207','1251956606','2','aDavis','6','81','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4208','1251956612','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4209','1251956672','2','aDavis','63','81','-','Un-deleting a document');
INSERT INTO `modx_manager_log` VALUES ('4210','1251994038','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4211','1251994068','1','admin','51','28','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4212','1251994089','1','admin','51','131','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4213','1252039153','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4214','1252039165','2','aDavis','51','119','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4215','1252039189','2','aDavis','52','119','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('4216','1252039195','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4217','1252039226','2','aDavis','51','119','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4218','1252468432','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4219','1252468499','2','aDavis','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4220','1252468531','2','aDavis','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4221','1252468632','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4222','1252510362','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4223','1252510427','2','aDavis','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4224','1252510443','2','aDavis','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4225','1252510751','2','aDavis','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4226','1252510753','2','aDavis','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4227','1252510804','2','aDavis','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4228','1252510889','2','aDavis','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4229','1252510891','2','aDavis','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4230','1252510933','2','aDavis','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4231','1252511120','2','aDavis','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4232','1252511270','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4233','1252511351','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4234','1252511363','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4235','1252511480','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4236','1252511534','2','aDavis','61','182','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4237','1252511542','2','aDavis','61','183','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4238','1252511604','2','aDavis','3','182','CCAP Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4239','1252511609','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4240','1252511724','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4241','1252511738','2','aDavis','61','184','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4242','1252511872','2','aDavis','3','182','CCAP Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4243','1252511875','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4244','1252511937','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4245','1252511973','2','aDavis','61','185','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4246','1252513105','2','aDavis','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4247','1252513120','2','aDavis','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4248','1252513237','2','aDavis','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4249','1252513240','2','aDavis','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4250','1252618302','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4251','1252618312','2','aDavis','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4252','1252618318','2','aDavis','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4253','1252618349','2','aDavis','5','54','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4254','1252618352','2','aDavis','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4255','1252618383','2','aDavis','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4256','1252618388','2','aDavis','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4257','1252618467','2','aDavis','5','54','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4258','1252618470','2','aDavis','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4259','1252618494','2','aDavis','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4260','1252618505','2','aDavis','5','54','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4261','1252618508','2','aDavis','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4262','1252618520','2','aDavis','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4263','1252618531','2','aDavis','61','54','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4264','1252618567','2','aDavis','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4265','1252618641','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4266','1253028014','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4267','1253028037','2','aDavis','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4268','1253028040','2','aDavis','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4269','1253028097','2','aDavis','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4270','1253028100','2','aDavis','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4271','1253028136','2','aDavis','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4272','1253028293','2','aDavis','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4273','1253028304','2','aDavis','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4274','1253034497','2','aDavis','3','169','Video Test 40','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4275','1253034519','2','aDavis','27','169','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4276','1253034573','2','aDavis','5','169','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4277','1253034576','2','aDavis','3','169','Empty','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4278','1253034599','2','aDavis','27','169','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4279','1253034658','2','aDavis','5','169','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4280','1253034661','2','aDavis','3','169','Empty','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4281','1253034692','2','aDavis','3','165','Video Test 36','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4282','1253034694','2','aDavis','27','165','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4283','1253034713','2','aDavis','3','167','Video Test 38','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4284','1253034715','2','aDavis','27','167','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4285','1253034783','2','aDavis','5','167','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4286','1253034786','2','aDavis','3','167','Gimme My Fix','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4287','1253034870','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4288','1253034987','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4289','1253034998','2','aDavis','61','186','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4290','1253035022','2','aDavis','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4291','1253035024','2','aDavis','27','186','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4292','1253035043','2','aDavis','5','186','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4293','1253035046','2','aDavis','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4294','1253035120','2','aDavis','27','186','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4295','1253035142','2','aDavis','5','186','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4296','1253035146','2','aDavis','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4297','1253039886','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4298','1253050084','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4299','1253050102','2','aDavis','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4300','1253050105','2','aDavis','27','186','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4301','1253051173','2','aDavis','3','185','The Women\'s Foundation of Nepal','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4302','1253051180','2','aDavis','6','185','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4303','1253051746','2','aDavis','51','184','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4304','1253051807','2','aDavis','52','184','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('4305','1253051812','2','aDavis','51','184','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4306','1253051828','2','aDavis','52','184','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('4307','1253051832','2','aDavis','3','184','National Film Board of Canada','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4308','1253051835','2','aDavis','51','184','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4309','1253051840','2','aDavis','52','184','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('4310','1253052676','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4311','1253056643','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4312','1253056658','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4313','1253056712','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4314','1253056720','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4315','1253057225','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4316','1253057236','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4317','1253057259','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4318','1253057274','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4319','1253057309','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4320','1253057346','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4321','1253057360','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4322','1253057600','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4323','1253057608','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4324','1253057647','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4325','1253066588','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4326','1253066601','1','admin','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4327','1253066604','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4328','1253066616','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4329','1253066649','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4330','1253066673','1','admin','5','54','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4331','1253066675','1','admin','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4332','1253068314','1','admin','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4333','1253068317','1','admin','27','54','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4334','1253068331','1','admin','5','54','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4335','1253068334','1','admin','3','54','Contact','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4336','1253069386','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4337','1253069397','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4338','1253069401','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4339','1253069443','1','admin','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4340','1253069445','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4341','1253069473','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4342','1253069486','1','admin','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4343','1253069489','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4344','1253069491','1','admin','27','186','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4345','1253069517','1','admin','5','186','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4346','1253069520','1','admin','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4347','1253069531','1','admin','27','169','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4348','1253069566','1','admin','5','169','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4349','1253069569','1','admin','3','169','Empty','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4350','1253069643','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4351','1253069652','1','admin','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4352','1253069655','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4353','1253069655','1','admin','27','186','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4354','1253069665','1','admin','27','169','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4355','1253069672','1','admin','5','169','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4356','1253069675','1','admin','3','169','Empty','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4357','1253069753','1','admin','27','169','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4358','1253069763','1','admin','5','169','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4359','1253069766','1','admin','3','169','Empty','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4360','1253069801','1','admin','27','122','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4361','1253069814','1','admin','27','167','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4362','1253069833','1','admin','5','167','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4363','1253069836','1','admin','3','167','Gimme My Fix','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4364','1253069870','1','admin','27','167','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4365','1253069876','1','admin','27','167','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4366','1253069889','1','admin','27','167','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4367','1253069905','1','admin','5','167','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4368','1253069908','1','admin','3','167','Gimme My Fix','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4369','1253070133','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4370','1253070146','1','admin','5','28','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4371','1253070149','1','admin','3','28','Butterfly','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4372','1253070168','1','admin','61','28','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4373','1253070208','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4374','1253070293','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4375','1253070313','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4376','1253070370','1','admin','62','28','-','Un-publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4377','1253070384','1','admin','61','28','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4378','1253070388','1','admin','27','28','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4379','1253070426','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4380','1253070457','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4381','1253070496','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4382','1253070511','1','admin','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4383','1253070514','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4384','1253070546','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4385','1253070559','1','admin','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4386','1253070562','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4387','1253070567','1','admin','61','181','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4388','1253070572','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4389','1253070575','1','admin','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4390','1253070578','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4391','1253070616','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4392','1253070636','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4393','1253070650','1','admin','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4394','1253070653','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4395','1253070689','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4396','1253070696','1','admin','16','5','Video Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('4397','1253070707','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4398','1253070718','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4399','1253070721','1','admin','16','4','RSS Post','Editing template');
INSERT INTO `modx_manager_log` VALUES ('4400','1253070731','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4401','1253070737','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4402','1253070743','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4403','1253070754','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4404','1253070760','1','admin','78','3','videoRSS','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('4405','1253070973','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4406','1253071009','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4407','1253071012','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4408','1253071054','1','admin','27','181','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4409','1253071063','1','admin','5','181','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4410','1253071066','1','admin','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4411','1253071083','1','admin','27','186','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4412','1253071092','1','admin','5','186','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4413','1253071094','1','admin','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4414','1253071228','1','admin','27','186','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4415','1253071233','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4416','1253071256','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4417','1253071259','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4418','1253071267','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4419','1253071275','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4420','1253071277','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4421','1253071491','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4422','1253071513','1','admin','5','17','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4423','1253071516','1','admin','3','17','Videos Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4424','1253071536','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4425','1253071542','1','admin','27','17','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4426','1253071555','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4427','1253071564','1','admin','5','1','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4428','1253071567','1','admin','3','1','News Feed','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4429','1253071569','1','admin','27','18','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4430','1253071586','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4431','1253071595','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4432','1253071598','1','admin','78','4','linksFeed','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('4433','1253071606','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4434','1253071615','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4435','1253071625','1','admin','78','4','linksFeed','Editing Chunk (HTML Snippet)');
INSERT INTO `modx_manager_log` VALUES ('4436','1253071635','1','admin','27','20','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4437','1253071643','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4438','1253071652','1','admin','22','18','linksFeed','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('4439','1253100897','1','admin','22','18','linksFeed','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('4440','1253101378','1','admin','22','18','linksFeed','Editing Snippet');
INSERT INTO `modx_manager_log` VALUES ('4441','1253101488','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4442','1253103476','1','admin','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4443','1253476775','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4444','1253476781','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4445','1253476795','2','aDavis','3','10','News','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4446','1253476802','2','aDavis','27','56','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4447','1253912238','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4448','1253912265','2','aDavis','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4449','1253912270','2','aDavis','27','186','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4450','1253912337','2','aDavis','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4451','1253912357','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4452','1253912525','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4453','1253912543','2','aDavis','3','187','Gimme My Fix','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4454','1253912550','2','aDavis','61','187','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4455','1253912671','2','aDavis','3','187','Gimme My Fix','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4456','1253912673','2','aDavis','3','186','Excerpts from \"Pictures of Self-Harm\"','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4457','1253912685','2','aDavis','3','181','Bon Bons','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4458','1253912688','2','aDavis','3','187','Gimme My Fix','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4459','1253912692','2','aDavis','27','187','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4460','1253912773','2','aDavis','27','187','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4461','1253913550','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4462','1254517072','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4463','1254851983','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4464','1254852025','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4465','1254852041','2','aDavis','76','-','-','Resource management');
INSERT INTO `modx_manager_log` VALUES ('4466','1254852074','2','aDavis','81','-','-','Managing keywords');
INSERT INTO `modx_manager_log` VALUES ('4467','1254852084','2','aDavis','3','187','Gimme My Fix','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4468','1254852095','2','aDavis','27','187','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4469','1254852175','2','aDavis','5','187','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4470','1254852254','2','aDavis','3','187','Gimme My Fix','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4471','1254854244','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4472','1254854286','2','aDavis','3','3','Videos','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4473','1254854454','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4474','1254854466','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4475','1254854470','2','aDavis','3','188','Untitled document','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4476','1254854474','2','aDavis','3','188','Untitled document','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4477','1254854478','2','aDavis','27','188','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4478','1254856003','2','aDavis','5','188','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4479','1254856005','2','aDavis','3','188','October','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4480','1254856013','2','aDavis','61','188','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4481','1254856060','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4482','1254856289','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4483','1254856296','2','aDavis','61','189','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4484','1254856378','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4485','1254856407','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4486','1254856419','2','aDavis','61','190','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4487','1254856425','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4488','1254858701','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4489','1254858711','2','aDavis','61','191','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4490','1254858827','2','aDavis','3','191','White to Red','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4491','1254858830','2','aDavis','27','191','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4492','1254859031','2','aDavis','5','191','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4493','1254859034','2','aDavis','3','191','White to Red by Rita Shrestha','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4494','1254859145','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4495','1254859171','2','aDavis','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4496','1254859240','2','aDavis','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4497','1254859244','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4498','1254859290','2','aDavis','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4499','1254859383','2','aDavis','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4500','1254859386','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4501','1254859420','2','aDavis','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4502','1254859423','2','aDavis','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4503','1254859478','2','aDavis','5','38','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4504','1254859480','2','aDavis','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4505','1254859503','2','aDavis','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4506','1254859554','2','aDavis','5','38','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4507','1254859557','2','aDavis','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4508','1254859594','2','aDavis','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4509','1254859625','2','aDavis','5','38','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4510','1254859628','2','aDavis','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4511','1254859660','2','aDavis','27','38','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4512','1254859848','2','aDavis','5','38','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4513','1254859851','2','aDavis','3','38','About Us','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4514','1254859880','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4515','1254859883','2','aDavis','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4516','1254859952','2','aDavis','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4517','1254859955','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4518','1254860051','2','aDavis','3','39','Privacy Policy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4519','1254860054','2','aDavis','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4520','1254860070','2','aDavis','5','39','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4521','1254860072','2','aDavis','3','39','Privacy Policy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4522','1254860108','2','aDavis','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4523','1254860142','2','aDavis','5','39','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4524','1254860145','2','aDavis','3','39','Privacy Policy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4525','1254860172','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4526','1254860177','2','aDavis','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4527','1254860245','2','aDavis','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4528','1254860248','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4529','1254860352','2','aDavis','3','191','White to Red by Rita Shrestha','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4530','1254860355','2','aDavis','3','190','October','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4531','1254860359','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4532','1254860469','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4533','1254860476','2','aDavis','27','192','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4534','1254860500','2','aDavis','5','192','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4535','1254860503','2','aDavis','3','192','Empty','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4536','1254860530','2','aDavis','61','192','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4537','1254861278','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4538','1254861354','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4539','1254861366','2','aDavis','61','193','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4540','1254861512','2','aDavis','3','192','Empty','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4541','1254861515','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4542','1254861610','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4543','1254861618','2','aDavis','61','194','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4544','1254862295','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4545','1254862355','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4546','1254862363','2','aDavis','61','195','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4547','1254862660','2','aDavis','61','70','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4548','1254862672','2','aDavis','6','70','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4549','1254862685','2','aDavis','6','69','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4550','1254862695','2','aDavis','6','68','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4551','1254862706','2','aDavis','6','182','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4552','1254862741','2','aDavis','61','182','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4553','1254862758','2','aDavis','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('4554','1254862778','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4555','1254862807','2','aDavis','3','6','Links','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4556','1254862810','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4557','1254862852','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4558','1254862887','2','aDavis','61','196','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4559','1254862921','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4560','1254862949','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4561','1254862958','2','aDavis','61','197','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4562','1254862967','2','aDavis','51','196','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4563','1254862986','2','aDavis','52','196','-','Moved document');
INSERT INTO `modx_manager_log` VALUES ('4564','1254863030','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4565','1254863037','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4566','1254863083','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4567','1254863091','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4568','1254863157','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4569','1254863165','2','aDavis','61','198','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4570','1254863176','2','aDavis','61','199','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4571','1254863202','2','aDavis','3','199','UNIFEM Canadian Committee','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4572','1254863205','2','aDavis','27','199','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4573','1254863215','2','aDavis','5','199','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4574','1254863217','2','aDavis','3','199','UNIFEM Canadian Committee','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4575','1254863282','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4576','1254863360','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4577','1254863372','2','aDavis','61','200','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4578','1254863450','2','aDavis','72','-','-','Adding a weblink');
INSERT INTO `modx_manager_log` VALUES ('4579','1254863491','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4580','1254863501','2','aDavis','61','201','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4581','1254863602','2','aDavis','6','123','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4582','1254863613','2','aDavis','6','41','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4583','1254863623','2','aDavis','6','178','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4584','1254863631','2','aDavis','6','80','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4585','1254863640','2','aDavis','6','55','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4586','1254864646','2','aDavis','6','130','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4587','1254864653','2','aDavis','6','118','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4588','1254864664','2','aDavis','6','46','-','Deleting document');
INSERT INTO `modx_manager_log` VALUES ('4589','1254865991','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4590','1254878288','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4591','1254878317','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4592','1254878332','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4593','1254878354','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4594','1254943921','2','aDavis','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4595','1254943966','2','aDavis','3','190','October','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4596','1254943974','2','aDavis','27','190','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4597','1254944087','2','aDavis','27','190','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4598','1254944346','2','aDavis','3','190','October','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4599','1254944355','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4600','1254944371','2','aDavis','4','-','-','Creating a document');
INSERT INTO `modx_manager_log` VALUES ('4601','1254944575','2','aDavis','5','-','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4602','1254944593','2','aDavis','61','202','-','Publishing a document');
INSERT INTO `modx_manager_log` VALUES ('4603','1254944775','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4604','1254944787','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4605','1254946132','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4606','1254948575','2','aDavis','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4607','1254948576','2','aDavis','8','-','-','Logged out');
INSERT INTO `modx_manager_log` VALUES ('4608','1254954794','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4609','1254954804','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4610','1254954807','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4611','1254954814','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4612','1254954824','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4613','1254954866','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4614','1254954897','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4615','1254954900','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4616','1254955349','1','admin','27','1','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4617','1254955369','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4618','1254955444','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4619','1254955922','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4620','1254955926','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4621','1254956088','1','admin','27','202','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4622','1254956137','1','admin','5','202','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4623','1254956141','1','admin','3','202','Nature Vs. Industrial','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4624','1254957138','1','admin','27','202','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4625','1254957144','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4626','1254957155','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4627','1254957178','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4628','1254957181','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4629','1254957707','1','admin','51','37','-','Moving document');
INSERT INTO `modx_manager_log` VALUES ('4630','1254957713','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4631','1254957984','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4632','1254957988','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4633','1254958200','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4634','1254958200','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4635','1254958217','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4636','1254958677','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4637','1254958749','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4638','1254958752','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4639','1254958798','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4640','1254958829','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4641','1254958939','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4642','1254959043','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4643','1254959120','1','admin','5','39','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4644','1254959124','1','admin','3','39','Privacy Policy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4645','1254959161','1','admin','27','39','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4646','1254959172','1','admin','5','39','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4647','1254959176','1','admin','3','39','Privacy Policy','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4648','1254959180','1','admin','27','37','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4649','1254959192','1','admin','5','37','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4650','1254959196','1','admin','3','37','Terms of Use','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4651','1254960160','1','admin','27','189','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4652','1254960185','1','admin','5','189','-','Saving document');
INSERT INTO `modx_manager_log` VALUES ('4653','1254960188','1','admin','3','189','LISO Videos at imagineNATIVE','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4654','1254967850','1','admin','3','189','LISO Videos at imagineNATIVE','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4655','1254967931','1','admin','3','189','LISO Videos at imagineNATIVE','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4656','1254973558','1','admin','3','189','LISO Videos at imagineNATIVE','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4657','1255001535','1','admin','3','189','LISO Videos at imagineNATIVE','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4658','1255017236','1','admin','3','189','LISO Videos at imagineNATIVE','Viewing data for document');
INSERT INTO `modx_manager_log` VALUES ('4659','1255139067','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4660','1255139091','1','admin','27','187','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4661','1255139099','1','admin','27','187','-','Editing document');
INSERT INTO `modx_manager_log` VALUES ('4662','1255370094','1','admin','58','-','MODx','Logged in');
INSERT INTO `modx_manager_log` VALUES ('4663','1255370117','1','admin','64','-','-','Removing deleted content');
INSERT INTO `modx_manager_log` VALUES ('4664','1255370158','1','admin','93','-','-','Backup Manager');

# --------------------------------------------------------

#
# Table structure for table `modx_manager_users`
#

CREATE TABLE `modx_manager_users` (
  `id` int(10) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL default '',
  `password` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Contains login information for backend users.';

#
# Dumping data for table `modx_manager_users`
#

INSERT INTO `modx_manager_users` VALUES ('1','admin','98c2dd6f34cdec6aceb1011009c52463');
INSERT INTO `modx_manager_users` VALUES ('2','aDavis','aff8179216b25a0f68f9f80e8fbc24d5');
INSERT INTO `modx_manager_users` VALUES ('3','Diary Test','98c2dd6f34cdec6aceb1011009c52463');

# --------------------------------------------------------

#
# Table structure for table `modx_member_groups`
#

CREATE TABLE `modx_member_groups` (
  `id` int(10) NOT NULL auto_increment,
  `user_group` int(10) NOT NULL default '0',
  `member` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_member_groups`
#


# --------------------------------------------------------

#
# Table structure for table `modx_membergroup_access`
#

CREATE TABLE `modx_membergroup_access` (
  `id` int(10) NOT NULL auto_increment,
  `membergroup` int(10) NOT NULL default '0',
  `documentgroup` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_membergroup_access`
#


# --------------------------------------------------------

#
# Table structure for table `modx_membergroup_names`
#

CREATE TABLE `modx_membergroup_names` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for access permissions.';

#
# Dumping data for table `modx_membergroup_names`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_content`
#

CREATE TABLE `modx_site_content` (
  `id` int(10) NOT NULL auto_increment,
  `type` varchar(20) NOT NULL default 'document',
  `contentType` varchar(50) NOT NULL default 'text/html',
  `pagetitle` varchar(255) NOT NULL default '',
  `longtitle` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `alias` varchar(255) default '',
  `link_attributes` varchar(255) NOT NULL default '',
  `published` int(1) NOT NULL default '0',
  `pub_date` int(20) NOT NULL default '0',
  `unpub_date` int(20) NOT NULL default '0',
  `parent` int(10) NOT NULL default '0',
  `isfolder` int(1) NOT NULL default '0',
  `introtext` text COMMENT 'Used to provide quick summary of the document',
  `content` mediumtext,
  `richtext` tinyint(1) NOT NULL default '1',
  `template` int(10) NOT NULL default '1',
  `menuindex` int(10) NOT NULL default '0',
  `searchable` int(1) NOT NULL default '1',
  `cacheable` int(1) NOT NULL default '1',
  `createdby` int(10) NOT NULL default '0',
  `createdon` int(20) NOT NULL default '0',
  `editedby` int(10) NOT NULL default '0',
  `editedon` int(20) NOT NULL default '0',
  `deleted` int(1) NOT NULL default '0',
  `deletedon` int(20) NOT NULL default '0',
  `deletedby` int(10) NOT NULL default '0',
  `publishedon` int(20) NOT NULL default '0',
  `publishedby` int(10) NOT NULL default '0',
  `menutitle` varchar(255) NOT NULL default '' COMMENT 'Menu title',
  `donthit` tinyint(1) NOT NULL default '0' COMMENT 'Disable page hit count',
  `haskeywords` tinyint(1) NOT NULL default '0' COMMENT 'has links to keywords',
  `hasmetatags` tinyint(1) NOT NULL default '0' COMMENT 'has links to meta tags',
  `privateweb` tinyint(1) NOT NULL default '0' COMMENT 'Private web document',
  `privatemgr` tinyint(1) NOT NULL default '0' COMMENT 'Private manager document',
  `content_dispo` tinyint(1) NOT NULL default '0' COMMENT '0-inline, 1-attachment',
  `hidemenu` tinyint(1) NOT NULL default '0' COMMENT 'Hide document from menu',
  PRIMARY KEY  (`id`),
  KEY `id` (`id`),
  KEY `parent` (`parent`),
  KEY `aliasidx` (`alias`),
  FULLTEXT KEY `content_ft_idx` (`pagetitle`,`description`,`content`)
) ENGINE=MyISAM AUTO_INCREMENT=203 DEFAULT CHARSET=latin1 COMMENT='Contains the site document tree.';

#
# Dumping data for table `modx_site_content`
#

INSERT INTO `modx_site_content` VALUES ('1','document','text/xml','News Feed','','','news_feed','','1','0','0','16','0','','<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n\r\n<rss version=\"2.0\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\">\r\n    <channel>\r\n\r\n        <title>News Feed</title>\r\n        <link>http://www.lookinginspeakingout.com/news_feed</link>\r\n        <description></description>\r\n        <language>en</language>\r\n        <copyright>Looking In Speaking Out 2006</copyright>\r\n        <ttl>120</ttl>\r\n\r\n        [!Ditto? &startID=`10` &display=`10` &summarize=`10` &extenders=`summary` &paginate=`1` &dateSource=`pub_date` &sortBy=`pub_date` &paginateAlwaysShowLinks=`1` &depth=`0` &hideFolders=`1` &tpl=`ImageRSS` !]\r\n\r\n    </channel>\r\n    <totalPosts>[+total+]</totalPosts>\r\n    <totalPages>[+totalPages+]</totalPages>\r\n</rss>','0','0','0','0','0','1','1130304721','1','1253071564','0','0','0','1130304721','1','','1','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('3','document','text/html','Videos','','','videos','','1','0','0','0','1','','<p>videos</p>','1','5','3','1','1','1','1231465113','1','1237064442','0','0','0','1231465832','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('4','document','text/html','Resources','','','resources','','1','0','0','0','1','','<p>resources</p>','1','7','5','1','1','1','1231465164','1','1249757151','0','0','0','1231465964','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('5','document','text/html','Diaries','','','diaries','','1','0','0','0','0','','','1','4','4','1','1','1','1231465203','1','1235613540','0','0','0','1231465970','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('6','document','text/html','Links','','','links','','1','0','0','0','1','','<p>links</p>','1','4','6','1','1','1','1231465405','2','1251863276','0','0','0','1231465992','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('18','document','text/xml','Resources Feed','','','resources_feed','','1','0','0','16','0','','[[xmlTreeParser?parent=`4` &parentNode=`resources`]]','1','0','2','0','0','1','1231607232','1','1237340447','0','0','0','1231607232','1','','1','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('17','document','text/xml','Videos Feed','','','videos_feed','','1','0','0','16','0','','<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n\r\n<rss version=\"2.0\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\">\r\n    <channel>\r\n\r\n        <title>News Feed</title>\r\n        <link>http://www.lookinginspeakingout.com/news_feed</link>\r\n        <description></description>\r\n        <language>en</language>\r\n        <copyright>Looking In Speaking Out 2006</copyright>\r\n        <ttl>120</ttl>\r\n\r\n        [!Ditto? &startID=`3` &display=`10` &summarize=`10` &extenders=`summary` &paginate=`1` &dateSource=`pub_date` &sortBy=`pub_date` &paginateAlwaysShowLinks=`1` &depth=`0` &hideFolders=`1` &tpl=`videoRSS` !]\r\n\r\n    </channel>\r\n    <totalPosts>[+total+]</totalPosts>\r\n    <totalPages>[+totalPages+]</totalPages>\r\n</rss>','0','0','1','0','0','1','1231606917','1','1253071513','0','0','0','1231606917','1','','1','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('16','document','text/html','Feeds','','','feeds','','1','0','0','0','1','','','1','0','8','1','1','1','1231606759','1','1237065188','0','0','0','1231606767','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('10','document','text/html','News','','','news','','1','0','0','0','1','','','0','4','1','0','0','1','1231603232','1','1237052529','0','0','0','1231603258','1','','1','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('20','document','text/xml','Links Feed','','','links_feed','','1','0','0','16','0','','[[linksFeed?parent=`6`]]','0','0','4','0','0','1','1231607688','1','1237065230','0','0','0','1231607688','1','','1','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('106','document','text/html','Underage drinking','Underage drinking','i think this happens to everyone','','','1','0','0','93','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','1','1','1','2','1237694618','2','1237762603','0','0','0','1237697590','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('22','document','text/html','Org Information','','','info','','1','0','0','0','1','','','1','0','7','1','1','1','1231607878','1','1237065096','0','0','0','1231607884','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('115','document','text/html','Assignment #2 - Draw someone you love','draw someone you love','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','','','1','0','0','96','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','1','1','1','2','1237704767','2','1237762874','0','0','0','1237704776','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('114','document','text/html','Assignment # 1 - Visit Someone in a Hospital','visit someone in a hospital','Helping people in need feels good','','','1','0','0','96','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','0','1','1','2','1237704717','1','1237763839','0','0','0','1237704722','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('113','document','text/html','Interview with Van Gogh','Interview with Van Gogh','the last known interview found in a corn field','','','1','0','0','95','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','3','1','1','2','1237704600','2','1237762828','0','0','0','1237704606','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('112','document','text/html','Interview with Jeanne Randolph','Interview with Jeanne Randolph','in this interview jeanne talks about art and children','','','1','0','0','95','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','2','1','1','2','1237704487','2','1237762806','0','0','0','1237704496','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('110','document','text/html','interview with Einstein','interview with Einstein','this is the only know interview of albert einstein','','','1','0','0','95','0','','<div id=\"lipsum\">\r\n<p>\r\n<strong>Lorem</strong> ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula <strong>sollicitudin</strong> enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','0','1','1','2','1237697892','2','1237762726','0','0','0','1237697899','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('109','document','text/html','the real facts: teens and gangs','the real facts: teens and gangs','there have been over 30 gang related shootings in vancouver','','','1','0','0','93','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','4','1','1','2','1237697567','2','1237762692','0','0','0','1237697603','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('107','document','text/html','teen suicide','teen suicide','the amount of teen suicides is frightening','','','1','0','0','93','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','2','1','1','2','1237694652','2','1237762636','0','0','0','1237697594','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('202','document','text/html','Nature Vs. Industrial','','','','','1','0','0','190','0','','<p>Alexus was 15 when she moved from a remote community in Manitoba to the city of Winnipeg.  The cultural shock was unexpected and intense.  Now having lived in the city for a number of years Alexus reflects on her changed relationship to the natural and urban worlds.</p>','1','5','4','1','1','2','1254944574','1','1254956136','0','0','0','1254944593','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('108','document','text/html','prescription drugs: good vs bad','prescription drugs: good vs bad','sometimes these are good but most of the time bad','','','1','0','0','93','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','3','1','1','2','1237697500','2','1237762664','0','0','0','1237697599','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('105','document','text/html','Children having children','','the idea of this terrifies me','','','1','0','0','93','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit <strong>amet</strong>, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales <strong>iaculis</strong>. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','0','1','1','2','1237694582','2','1251863111','0','0','0','1237697586','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('101','document','text/html','ESSAy no image','ESSAy no image','this doesn\'t have an image but it sure is a great essay','','','1','0','0','92','0','','<div id=\"lipsum\">\r\n<p><strong>\r\n</strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec\r\npulvinar, metus id porttitor dignissim, justo purus sollicitudin\r\nturpis, bibendum mattis enim ante tincidunt eros. Phasellus magna.\r\nNullam quam. Nullam magna felis, sollicitudin ac, condimentum id,\r\ndictum nec, lorem. Sed tortor sapien, porttitor ac, viverra eget,\r\nrhoncus id, purus. Integer ullamcorper molestie massa. Suspendisse\r\nconsectetur leo vel elit. Duis ornare, augue quis egestas consectetur,\r\nelit lectus dignissim lectus, vitae aliquam nunc augue quis turpis.\r\nNullam ullamcorper. Fusce lacinia ornare purus. Sed a metus. Quisque\r\nrhoncus. Aliquam lobortis lectus nec ligula. Nunc nec dolor. Donec sed\r\nnunc. In tempor sem vitae ipsum. Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit.\r\n</p>\r\n<p>Curabitur viverra quam. In hac habitasse platea dictumst. Quisque\r\ngravida gravida tortor. Pellentesque arcu. Fusce faucibus dolor ut\r\nnibh. Donec nisl. Quisque ante diam, dignissim ut, bibendum vitae,\r\neuismod nec, est. In tincidunt laoreet velit. Etiam enim lacus, viverra\r\nin, convallis auctor, viverra eu, neque. Vivamus sed nulla eget elit\r\nmollis congue. Vivamus non turpis. Phasellus convallis. Fusce tempus\r\ndictum metus.\r\n</p>\r\n<p>Fusce semper rhoncus ante. Donec sodales enim suscipit lacus. Fusce\r\ntempor sollicitudin metus. Nullam porta. Suspendisse egestas imperdiet\r\nturpis. Sed urna. Aenean auctor lacus vel ligula egestas venenatis.\r\nNulla et tortor eget eros dapibus congue. Mauris vitae eros ut lectus\r\nauctor lobortis. In hac habitasse platea dictumst. Proin condimentum\r\nnisl laoreet justo. Integer pulvinar sollicitudin dolor. Ut magna\r\ntellus, blandit quis, rutrum nec, varius at, sem. Vivamus vestibulum\r\nluctus tortor. Ut nec lorem ut ipsum rhoncus lobortis. Maecenas sit\r\namet eros.\r\n</p>\r\n<p>Praesent fringilla ornare justo. Sed at erat. Aliquam eu odio quis\r\neros pretium bibendum. Donec laoreet orci sed lacus. Phasellus non\r\ntellus in sapien fringilla lobortis. Pellentesque hendrerit ipsum eu\r\nnunc. Sed sed nulla. Maecenas rutrum feugiat mauris. Mauris vehicula mi\r\nvitae risus. Morbi augue augue, rutrum sed, luctus eget, fringilla sit\r\namet, justo. Cras placerat tempor nisi. Proin vitae orci ac ipsum\r\nullamcorper adipiscing. Curabitur velit. Fusce odio velit, tempus nec,\r\nmalesuada nec, sodales sit amet, felis. Praesent lorem. Pellentesque\r\nipsum enim, consectetur hendrerit, eleifend id, ullamcorper nec, lorem.\r\nSuspendisse vel odio eget metus lacinia malesuada. Cras est.\r\n</p>\r\n<p>Morbi nulla elit, lobortis ac, tempor ut, scelerisque ut, odio.\r\nVivamus sollicitudin enim sit amet mi. Aliquam purus. Fusce ligula dui,\r\nornare eu, pellentesque a, convallis quis, tellus. Sed id lorem\r\nelementum ligula ullamcorper mattis. Vivamus gravida tincidunt leo.\r\nCurabitur feugiat erat nec tortor. Nulla et orci sed odio lobortis\r\ngravida. Nunc eleifend fermentum felis. Suspendisse pretium, urna quis\r\ntempor lobortis, ante tortor aliquet eros, a aliquam mi magna nec\r\nsapien. Curabitur euismod malesuada leo. Mauris dignissim ipsum vel\r\nmetus. Aenean semper, lorem sit amet pharetra pharetra, orci sapien\r\ndictum mi, sagittis laoreet erat ante a metus. Fusce tristique. Aenean\r\nvel orci. Morbi lectus.\r\n</p>\r\n</div>','1','7','3','1','1','2','1237672867','1','1249757951','0','0','0','1237672936','2','res04','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('100','document','text/html','stop motion animation at its best','stop motion animation at its best','an essay on stop motion by crossing communities','','','1','0','0','92','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec\r\npulvinar, metus id porttitor dignissim, justo purus sollicitudin\r\nturpis, bibendum mattis enim ante tincidunt eros. Phasellus magna.\r\nNullam quam. Nullam magna felis, sollicitudin ac, condimentum id,\r\ndictum nec, lorem. Sed tortor sapien, porttitor ac, viverra eget,\r\nrhoncus id, purus. Integer ullamcorper molestie massa. Suspendisse\r\nconsectetur leo vel elit. Duis ornare, augue quis egestas consectetur,\r\nelit lectus dignissim lectus, vitae aliquam nunc augue quis turpis.\r\nNullam ullamcorper. Fusce lacinia ornare purus. Sed a metus. Quisque\r\nrhoncus. Aliquam lobortis lectus nec ligula. Nunc nec dolor. Donec sed\r\nnunc. In tempor sem vitae ipsum. Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit.\r\n</p>\r\n<p>Curabitur viverra quam. In hac habitasse platea dictumst. Quisque\r\ngravida gravida tortor. Pellentesque arcu. Fusce faucibus dolor ut\r\nnibh. Donec nisl. Quisque ante diam, dignissim ut, bibendum vitae,\r\neuismod nec, est. In tincidunt laoreet velit. Etiam enim lacus, viverra\r\nin, convallis auctor, viverra eu, neque. Vivamus sed nulla eget elit\r\nmollis congue. Vivamus non turpis. Phasellus convallis. Fusce tempus\r\ndictum metus.\r\n</p>\r\n<p>Fusce semper rhoncus ante. Donec sodales enim suscipit lacus. Fusce\r\ntempor sollicitudin metus. Nullam porta. Suspendisse egestas imperdiet\r\nturpis. Sed urna. Aenean auctor lacus vel ligula egestas venenatis.\r\nNulla et tortor eget eros dapibus congue. Mauris vitae eros ut lectus\r\nauctor lobortis. In hac habitasse platea dictumst. Proin condimentum\r\nnisl laoreet justo. Integer pulvinar sollicitudin dolor. Ut magna\r\ntellus, blandit quis, rutrum nec, varius at, sem. Vivamus vestibulum\r\nluctus tortor. Ut nec lorem ut ipsum rhoncus lobortis. Maecenas sit\r\namet eros.\r\n</p>\r\n<p>Praesent fringilla ornare justo. Sed at erat. Aliquam eu odio quis\r\neros pretium bibendum. Donec laoreet orci sed lacus. Phasellus non\r\ntellus in sapien fringilla lobortis. Pellentesque hendrerit ipsum eu\r\nnunc. Sed sed nulla. Maecenas rutrum feugiat mauris. Mauris vehicula mi\r\nvitae risus. Morbi augue augue, rutrum sed, luctus eget, fringilla sit\r\namet, justo. Cras placerat tempor nisi. Proin vitae orci ac ipsum\r\nullamcorper adipiscing. Curabitur velit. Fusce odio velit, tempus nec,\r\nmalesuada nec, sodales sit amet, felis. Praesent lorem. Pellentesque\r\nipsum enim, consectetur hendrerit, eleifend id, ullamcorper nec, lorem.\r\nSuspendisse vel odio eget metus lacinia malesuada. Cras est.\r\n</p>\r\n<p>Morbi nulla elit, lobortis ac, tempor ut, scelerisque ut, odio.\r\nVivamus sollicitudin enim sit amet mi. Aliquam purus. Fusce ligula dui,\r\nornare eu, pellentesque a, convallis quis, tellus. Sed id lorem\r\nelementum ligula ullamcorper mattis. Vivamus gravida tincidunt leo.\r\nCurabitur feugiat erat nec tortor. Nulla et orci sed odio lobortis\r\ngravida. Nunc eleifend fermentum felis. Suspendisse pretium, urna quis\r\ntempor lobortis, ante tortor aliquet eros, a aliquam mi magna nec\r\nsapien. Curabitur euismod malesuada leo. Mauris dignissim ipsum vel\r\nmetus. Aenean semper, lorem sit amet pharetra pharetra, orci sapien\r\ndictum mi, sagittis laoreet erat ante a metus. Fusce tristique. Aenean\r\nvel orci. Morbi lectus.\r\n</p>\r\n</div>','1','7','2','1','1','2','1237672809','1','1249757934','0','0','0','1237672929','2','res03','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('99','document','text/html','essay about the moon','essay about the moon','the moon is full tonight....','','','1','0','0','92','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Donec\r\npulvinar, metus id porttitor dignissim, justo purus sollicitudin\r\nturpis, bibendum mattis enim ante tincidunt eros. Phasellus magna.\r\nNullam quam. Nullam magna felis, sollicitudin ac, condimentum id,\r\ndictum nec, lorem. Sed tortor sapien, porttitor ac, viverra eget,\r\nrhoncus id, purus. Integer ullamcorper molestie massa. Suspendisse\r\nconsectetur leo vel elit. Duis ornare, augue quis egestas consectetur,\r\nelit lectus dignissim lectus, vitae aliquam nunc augue quis turpis.\r\nNullam ullamcorper. Fusce lacinia ornare purus. Sed a metus. Quisque\r\nrhoncus. Aliquam lobortis lectus nec ligula. Nunc nec dolor. Donec sed\r\nnunc. In tempor sem vitae ipsum. Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit.\r\n</p>\r\n<p>Curabitur viverra quam. In hac habitasse platea dictumst. Quisque\r\ngravida gravida tortor. Pellentesque arcu. Fusce faucibus dolor ut\r\nnibh. Donec nisl. Quisque ante diam, dignissim ut, bibendum vitae,\r\neuismod nec, est. In tincidunt laoreet velit. Etiam enim lacus, viverra\r\nin, convallis auctor, viverra eu, neque. Vivamus sed nulla eget elit\r\nmollis congue. Vivamus non turpis. Phasellus convallis. Fusce tempus\r\ndictum metus.\r\n</p>\r\n<p>Fusce semper rhoncus ante. Donec sodales enim suscipit lacus. Fusce\r\ntempor sollicitudin metus. Nullam porta. Suspendisse egestas imperdiet\r\nturpis. Sed urna. Aenean auctor lacus vel ligula egestas venenatis.\r\nNulla et tortor eget eros dapibus congue. Mauris vitae eros ut lectus\r\nauctor lobortis. In hac habitasse platea dictumst. Proin condimentum\r\nnisl laoreet justo. Integer pulvinar sollicitudin dolor. Ut magna\r\ntellus, blandit quis, rutrum nec, varius at, sem. Vivamus vestibulum\r\nluctus tortor. Ut nec lorem ut ipsum rhoncus lobortis. Maecenas sit\r\namet eros.\r\n</p>\r\n<p>Praesent fringilla ornare justo. Sed at erat. Aliquam eu odio quis\r\neros pretium bibendum. Donec laoreet orci sed lacus. Phasellus non\r\ntellus in sapien fringilla lobortis. Pellentesque hendrerit ipsum eu\r\nnunc. Sed sed nulla. Maecenas rutrum feugiat mauris. Mauris vehicula mi\r\nvitae risus. Morbi augue augue, rutrum sed, luctus eget, fringilla sit\r\namet, justo. Cras placerat tempor nisi. Proin vitae orci ac ipsum\r\nullamcorper adipiscing. Curabitur velit. Fusce odio velit, tempus nec,\r\nmalesuada nec, sodales sit amet, felis. Praesent lorem. Pellentesque\r\nipsum enim, consectetur hendrerit, eleifend id, ullamcorper nec, lorem.\r\nSuspendisse vel odio eget metus lacinia malesuada. Cras est.\r\n</p>\r\n<p>Morbi nulla elit, lobortis ac, tempor ut, scelerisque ut, odio.\r\nVivamus sollicitudin enim sit amet mi. Aliquam purus. Fusce ligula dui,\r\nornare eu, pellentesque a, convallis quis, tellus. Sed id lorem\r\nelementum ligula ullamcorper mattis. Vivamus gravida tincidunt leo.\r\nCurabitur feugiat erat nec tortor. Nulla et orci sed odio lobortis\r\ngravida. Nunc eleifend fermentum felis. Suspendisse pretium, urna quis\r\ntempor lobortis, ante tortor aliquet eros, a aliquam mi magna nec\r\nsapien. Curabitur euismod malesuada leo. Mauris dignissim ipsum vel\r\nmetus. Aenean semper, lorem sit amet pharetra pharetra, orci sapien\r\ndictum mi, sagittis laoreet erat ante a metus. Fusce tristique. Aenean\r\nvel orci. Morbi lectus.\r\n</p>\r\n</div>','1','7','1','1','1','2','1237672634','1','1249757923','0','0','0','1237672675','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('95','document','text/html','interviews','','','','','1','0','0','4','1','','','1','4','3','1','1','1','1237339107','1','1237339143','0','0','0','1237339143','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('96','document','text/html','assignments','','','','','1','0','0','4','1','','','1','7','4','1','1','1','1237339126','1','1249757192','0','0','0','1237339132','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('37','document','text/html','Terms of Use','','','termsofuse','','1','0','0','22','0','','<p>LISO </p>\r\n<p>*Read Privacy Policy\r\nWelcome to Looking In Speaking Out (LISO), the collaborative art website, owned and operated by the Crossing Communities Art Project (\"CCAP\") and located at the URL http://lookinginspeakingout.com.</p>\r\n<p> \r\nLooking In Speaking Out is an online virtual space that exists as a forum for members of Canada (\"LISO Members\") to share their personal stories and experiences represented by original literary, artistic, video and photographic works (\"Original Works\") for viewing by the public and to allow other LISO Members to dialogue with the artists about the Original Works. LISO Members (hereinafter known as \"Visitors\") may view the Original and Derivative Works, but may not engage in recombining, modifying or remixing of any works. All uses of the Website are free of charge. \r\nSections1.0 Membership Agreement and Terms of Use\r\n2.0 Privacy Policy\r\nPlease Read This Document Carefully! This Agreement (the \"Agreement\") governs your relationship with LISO respecting LISO and describes the legally binding terms for your access to and use of the Website.\r\n</p>\r\n<p>SECTION 1.0 - MEMBERSHIP AGREEMENT AND TERMS OF USE\r\n</p>\r\n<p>1.1 By using the Website, you agree to be bound by this Agreement and all applicable laws governing this Agreement. If you do not agree with any\r\nof it, you should leave the Website and discontinue your use of it\r\nimmediately.</p>\r\n<p>1.2&nbsp;&nbsp;&nbsp; LISO reserves the right to modify any or all\r\nterms of this Agreement from time to time and such modification shall\r\nbe effective immediately upon being posted on the Website. You agree to\r\nbe bound to any changes to this Agreement when you use the Website\r\nafter any such modification is posted.</p>\r\n<p>1.3&nbsp;&nbsp;&nbsp; When you become a LISO\r\nMember, you will have an opportunity to create a profile containing\r\nyour personal information (a \"Profile\"), receive a membership and to\r\npost comments or dialogue about Original or Derivative Works. All\r\ninformation provided by you as part of your Profile shall be governed\r\nby the Privacy Policy attached in Section 2.0 hereto. All Original\r\nWorks and Derivative Works shall be available for Visitors to view and\r\nfor other LISO Members.</p>\r\n<p>1.4 Your Profile and dialogue may not\r\ninclude nor link to, any content or information that is perceived by\r\nLISO as being racist, defamatory, pornographic, offensive, obscene,\r\nlewd, excessively violent, harassing, sexually explicit, discriminatory\r\nor otherwise illegal or objectionable in nature, nor shall it include\r\npasswords, viruses, spyware, malware, illegal software or warez or any\r\nhyperlinks to same. If you become aware of misuse of the Website by any\r\nperson, please Contact Us.</p>\r\n<p>1.5&nbsp;&nbsp;&nbsp; You acknowledge that LISO has the\r\nright but not the obligation to monitor the Website, and LISO does not\r\nassume any responsibility for your dialogue that you create, for which\r\nyou retain complete responsibility. LISO does not endorse any LISO\r\nMember\'s content nor does any LISO Member\'s content posted on the\r\nWebsite necessarily reflect the views and opinions of LISO. LISO makes\r\nno warranties, express or implied, as to any LISO Member\'s content, or\r\nto the accuracy or reliability of any LISO Member\'s content or to any\r\nmaterial or information transmitted from one LISO Member to another.&nbsp;&nbsp;&nbsp;\r\n</p>\r\n<p>1.6&nbsp;&nbsp;&nbsp; LISO shall have the right to use (or not use), modify, edit,\r\nalter, remix, juxtapose, compile, publish, display, reproduce, publicly\r\nperform, broadcast, exhibit, promote, advertise, market, and content\r\n(collectively known as the \"Authorized Uses\") without your prior\r\nwritten consent, so long as such Authorized Uses are connected directly\r\nto the promotion of LISO.</p>\r\n<p>1.8&nbsp;&nbsp;&nbsp; You represent and warrant that your\r\nOriginal Works are all original to you and are owned or licensed in\r\ntheir entirety by you at the time of the upload and that you have the\r\nunencumbered right to make the grant of license set forth in the\r\npreceding paragraph </p>\r\n<p>1.7.1.9&nbsp;&nbsp;&nbsp; LISO is not obliged to pay you any\r\nconsideration, royalties, fees or buy-outs of any kind or nature for\r\nyour content contribution. All claims for monetary or non-monetary\r\nconsideration, compensation, royalties, fees or buy-outs for your\r\ncontent contribution, along with any claims for droit moral, are\r\nexpressly and forever waived by you.&nbsp;&nbsp; </p>\r\n<p>1.10 Third party commercial\r\nadvertisements, affiliate links, and other forms of solicitation shall\r\nbe removed without notice and may result in suspension of your LISO\r\nMembership.</p>\r\n<p>1.11&nbsp;&nbsp;&nbsp; Except as permitted by the LISO you are not\r\npermitted to use LISO identification, as a reference or means of\r\npromotion or publicity. Furthermore, you will not create an\r\nassociation, express or implied, between you and LISO.</p>\r\n<p>1.12&nbsp;&nbsp;&nbsp; You\r\nmay cancel your LISO Membership at any time by emailing\r\nnewmedia@crossingcommunities.org. Cancellation of LISO Membership does\r\nnot terminate LISO\'s ongoing rights above (1.7).</p>\r\n<p>1.13&nbsp;&nbsp;&nbsp; LISO\r\nreserves the right, but not the obligation, to delete any information\r\nabout any LISO Member and further reserves the right to unilaterally\r\nremove from the Website any material that it deems to be in violation\r\nof this Agreement. However, given the \"always on\" nature of the\r\nworld-wide web, it is realistic to expect that, at times, the Website\r\nmay contain material that you are offended by. If you encounter such\r\ncontent, please immediately notify us at Contact Us.</p>\r\n<p>1.14&nbsp;&nbsp;&nbsp; LISO\r\nowns all right, title and interest, including without limitation all\r\nintellectual property rights, and all derivative works or enhancements\r\nthereof, relating to the Website (and any derivative works or\r\nenhancements thereof), including but not limited to all information,\r\ncontent, materials, products, services, URLs, software, technology,\r\neditorial or user guidelines, documentation and music, and shall be\r\npermitted unfettered use of this material. You shall not acquire any\r\nright, title or interest therein, except for the limited use rights to\r\nmodify, recombine and remix Original Content and Derivative content\r\nexpressly set forth in this Agreement. Any rights not expressly granted\r\nherein are deemed withheld.</p>\r\n<p>1.16&nbsp;&nbsp;&nbsp; You agree to defend, indemnify\r\nand hold LISO and its respective affiliates, officers, directors,\r\nemployees, shareholders, heirs and assigns, agents, funders and\r\nlicensees harmless from and against any and all costs, claims, fines,\r\npenalties, demands, investigations, liabilities, losses, damages,\r\njudgments, settlements, costs and expenses, including reasonable legal\r\nand accounting fees arising out of or in connection with your use of\r\nthe Website (including anyone acting under your password or LISO\r\nMembership username), or any alleged violation by you of any of the\r\nterms contained in this Agreement. Notwithstanding the foregoing, LISO\r\nretains the exclusive right to defend, settle, and pay any and all\r\nclaims, demands, proceedings, suits, actions or causes of actions which\r\nare brought against LISO and in no event shall you settle any such\r\nclaim without our prior written approval.</p>\r\n<p>ACCORDINGLY, THIS\r\nWEBSITE, INCLUDING ALL CONTENT MADE AVAILABLE ON OR ACCESSED THROUGH OR\r\nSENT FROM THE WEBSITE, IS PROVIDED ON AN \"AS IS\" AND \"AS AVAILABLE\"\r\nBASIS.LISO DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING,\r\nBUT NOT LIMITED TO, IMPLIED WARRANTIES AND CONDITIONS OF\r\nMERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND NON-INFRINGEMENT\r\nOF ANY RIGHTS OF ANY THIRD PARTIES OR ANY LAWS. (remove?)&nbsp; LISO\r\nDOES NOT WARRANT THAT THE FUNCTIONS CONTAINED IN THE WEBSITE OR ANY\r\nCONTENT CONTAINED THEREIN WILL BE UNINTERRUPTED OR ERROR-FREE; THAT\r\nDEFECTS WILL BE CORRECTED; OR THAT THE WEBSITE OR THE SERVERS THAT MAKE\r\nIT AVAILABLE ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS.LISO\r\nSHALL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL OR\r\nCONSEQUENTIAL DAMAGES THAT MAY RESULT FROM OR IN CONNECTION WITH THE\r\nUSE OF, OR THE INABILITY TO USE, OR FROM ANY ERRORS OR OMISSIONS\r\nCONTAINED ON, THE WEBSITE OR ANY CONTENT OR SERVICE THEREIN, EVEN IF\r\nLISO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.SECTION </p>\r\n<p>2.0 - PRIVACY POLICYRespecting\r\nthe privacy and confidentiality of your personal information is\r\nimportant to us. This website Privacy Policy explains how LISO\r\ncollects, uses and discloses your personal information on the Website.The\r\npurpose of this Policy is to make Visitors and LISO Members aware of\r\nhow we protect the privacy and confidentiality of your personal\r\ninformation and the circumstances under which we use your personal\r\ninformation.</p>\r\n<p>2.1&nbsp;&nbsp; What is personal information? Personal\r\ninformation is information about an identifiable individual, not\r\nincluding business contact information (i.e. information that would\r\nallow an individual to be contacted at their place of business, or work\r\nproduct information). For example, personal information includes such\r\nthings as your first and last names, date of birth, e-mail address,\r\npreferred language, place of residence, including city, Province, State\r\nand Country.</p>\r\n<p>2.2&nbsp;&nbsp;&nbsp; Username and Password. Your username and\r\npassword are personal to you and you may not allow any others to use\r\nyour username or password under any circumstances. You can edit or\r\nchange your password at any time.&nbsp; We are not liable for any harm\r\ncaused or related to the theft or misappropriation of your username or\r\npassword, disclosure of your username or password, or your\r\nauthorization of anyone else to use your username or password.&nbsp; You\r\nagree to immediately notify us if you become aware of or believe there\r\nis or may have been any unauthorized use of (or activity using) your\r\nusername or password or any other need to deactivate your username or\r\npassword due to security concerns.</p>\r\n<p>2.3&nbsp;&nbsp;&nbsp; What are the purposes for\r\ncollection, use and disclosure of personal information? LISO collects,\r\nuses and discloses your personal information with your knowledge and\r\nconsent and only for purposes that we have identified prior to or at\r\nthe time we collect the information. Some of the purposes for which we\r\nmay collect, use and disclose your personal information on this Website\r\nare:\r\n</p>\r\n<ul class=\"unIndentedList\">\r\n<li> To facilitate communication with you;</li>\r\n<li> To process any of your requests for information;</li>\r\n</ul>\r\n<p>\r\n\r\nTo grant/enable you to access certain special features or areas of the\r\nWebsite., including the ability to upload your Original Content to\r\nmodify, recombine and remix all Original Content to create Derivative\r\nContent;\r\n</p>\r\n<ul class=\"unIndentedList\">\r\n<li> To make certain other services available to you through the Website;</li>\r\n<li> To permit you to subscribe to an e-mail list to receive updates from the Website; and</li>\r\n<li> To announce special events and advise you of changes to the Website.</li>\r\n</ul>\r\n<p>\r\n\r\nLISO\r\nwill not sell, trade, lend or otherwise voluntarily disclose to any\r\nthird parties any personal information that you have provided to us for\r\nany purpose not identified under this Privacy Policy. We may share your\r\npersonal information with third parties to assist us to administer\r\nactivities on this Website, such as for website administration and\r\nmaintenance, administration of contests and data processing, to our\r\nprofessional advisors or otherwise for legitimate and reasonable\r\npurposes for which we have obtained your prior consent.In certain\r\ncircumstances, we may disclose your personal information to a\r\ngovernment institution that has asserted its lawful authority to obtain\r\nthe information or where we have reasonable grounds to believe the\r\ninformation could be used in the investigation of an unlawful activity,\r\nor to comply with a subpoena or warrant or an order made by a court,\r\nperson or body with jurisdiction to compel production of information or\r\nto comply with court rules regarding a production of records and\r\ninformation, or to our own legal counsel.When you provide us with\r\nyour personal information, such as to sign up to receive e-mails about\r\nour activities, you are consenting to the collection, use and\r\ndisclosure of your personal information for these purposes in\r\naccordance with the principles that are outlined in the Privacy Policy.If\r\nyou have given us your consent, you may at any time withdraw your\r\nconsent upon giving us reasonable notice. However, withdrawal of\r\nconsent does not terminate LISO\'s ongoing rights and licenses in and to\r\nyour Original Content and Derivative Content.We specifically\r\nconfirm that from time-to-time, we will publically display your first\r\nname, your City and Country alongside of your Original Works and\r\nDerivative Works as a means to afford your credit, as well as to\r\npromote LISO, and that by becoming a LISO Member, you are consenting to\r\nthe public use of your first name, City and Country information in this\r\nmanner.&nbsp; </p>\r\n<p>2.4&nbsp;&nbsp;&nbsp; Use of technology on the website. The Website uses\r\ncookie technology to make your use of the Website more convenient. A\r\ncookie is a text file which enables the Website to store any\r\ninformation about your activities on the Website or the length of your\r\nstay. Personal information about your visits to the Website is used on\r\nan aggregate basis, which means it is used for statistical purposes to\r\nassist us in enhancing your online experience, or to evaluate the\r\nmaterials that we may carry on the Website.LISO may also use the\r\ncookies to monitor polling results. The cookies will monitor results to\r\ndiscourage voting twice in one poll. This will give us the ability to\r\nproperly count activity reports and polling results.In addition,\r\nthe cookie will store your e-mail address, which is your unique\r\nidentifier, so that on return visits the Website will be able to match\r\nyou to your identity in the database. This is an opt-in feature which\r\nwill only be made operational if you choose to do so.If you choose\r\nto disable cookies associated with the Website, this may affect your\r\nuse of the Website and your ability to access certain features of the\r\nWebsite. </p>\r\n<p>2.5&nbsp;&nbsp;&nbsp; E-mail list. The Website allows you to provide us\r\nwith your e-mail address in order that we may send you information and\r\nupdates about LISO activities. In order to administer the e-mail list\r\nwe will collect your name, e-mail address, geographical location. We\r\nmay also collect other information as may be required from time to time\r\nin connection with the services offered on the Website.</p>\r\n<p>2.6&nbsp;&nbsp;&nbsp; Links\r\nto other sites. This Website may contain links to other websites. When\r\nyou click on of those links, you are contacting another internet\r\nresource. LISO has no responsibility or liability for, or control over\r\nthose websites, or internet resources or their collection, use and\r\ndisclosure of your personal information. Please refer to the privacy\r\npolicy and terms of use contained in any linked site you choose to go\r\nto, and familiarize yourself with that website\'s terms and conditions\r\nof use.</p>\r\n<p>2.7&nbsp;&nbsp;&nbsp; How do we protect your personal information. The\r\nWebsite endeavours to maintain appropriate procedural and technological\r\nmeasures and storage facilities to prevent any unauthorized use or\r\ndisclosure of your personal information. We exercise care in the secure\r\ntransmission of your information, however no transmission of\r\ninformation over the internet is one hundred percent secure. We cannot\r\nguarantee that information disclosed through the internet cannot be\r\nintercepted by third parties. LISO is taking the steps that are\r\nreasonably available to it to protect any personal information that is\r\nprovided to it through this Website including, in its disposal or\r\ndestruction.</p>\r\n<p>2.8&nbsp;&nbsp;&nbsp; Contacting us about your privacy. You may\r\nrequest access to your personal information and information about our\r\ncollection, use and disclosure of information by Contacting Us. LISO\r\nattempts to keep records as accurate and complete as possible. You can\r\nhelp us maintain the accuracy of your information by notifying us of\r\nany changes to your personal information.This Privacy Policy\r\nmay be changed from time to time at our sole discretion and without any\r\nprior notice. New versions of this Privacy Policy will be posted here.\r\nYour continued use of this Website, subsequent to any changes to the\r\nPrivacy Policy, will signify that you consent to our collection, use\r\nand disclosure of your personal information in accordance with the\r\nrevised Privacy Policy.BINDING CONSENTThis Agreement and\r\nany amendments thereto which are posted on the Website from time to\r\ntime constitutes the entire agreement between you and LISO with respect\r\nto the use of the Website. This Agreement is to be governed by and\r\nconstrued in accordance with the laws of the Province of Manitoba,\r\nCanada and no other jurisdiction and any litigation relating to this\r\nAgreement shall be brought solely Manitoba, and the courts of Manitoba\r\nshall have exclusive jurisdiction over all matters herein. If any\r\nprovision shall be unlawful, void, or for any reason unenforceable,\r\nthen such provision shall be severable and shall not affect the\r\nvalidity and enforceability of any remaining provisions.If you have any questions, comments or concerns about the Website or this Agreement, you may reach us at Contact Us.I HAVE READ THIS AGREEMENT AND AGREE TO ALL OF THE PROVISIONS CONTAINED HEREIN.</p>','1','6','0','1','1','1','1231610151','1','1254959192','0','0','0','1231610231','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('38','document','text/html','About Us','','','aboutus','','1','0','0','22','0','','<p>Looking In Speaking Out is a project created by Crossing Communities Art Project.&nbsp; The Crossing Communities Art Project advocates the use of the imagination and culture as a means to share stories, histories, experiences, idenitities and values. We bring people together from diverse backgrounds to exchange and create networks of friendship to work towards social and economic transformation.</p>','1','6','1','1','1','1','1231610169','2','1254859848','0','0','0','1231610239','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('39','document','text/html','Privacy Policy','','','privacypolicy','','1','0','0','22','0','','<p>Respecting the privacy and confidentiality of your personal information is important to us. This website Privacy Policy explains how LISO collects, uses and discloses your personal information on the Website.</p>\r\n<p>The purpose of this Policy is to make Visitors and LISO Members aware of how we protect the privacy and confidentiality of your personal information and the circumstances under which we use your personal information.</p>\r\n<p>2.1&nbsp; What is personal information? Personal information is information about an identifiable individual, not including business contact information (i.e. information that would allow an individual to be contacted at their place of business, or work product information). For example, personal information includes such things as your first and last names, date of birth, e-mail address, preferred language, place of residence, including city, Province, State and Country.</p>\r\n<p>2.2&nbsp;&nbsp;&nbsp; Username and Password. Your username and password are personal to you and you may not allow any others to use your username or password under any circumstances. You can edit or change your password at any time.&nbsp; We are not liable for any harm caused or related to the theft or misappropriation of your username or password, disclosure of your username or password, or your authorization of anyone else to use your username or password.&nbsp; You agree to immediately notify us if you become aware of or believe there is or may have been any unauthorized use of (or activity using) your username or password or any other need to deactivate your username or password due to security concerns.</p>\r\n<p>2.3&nbsp;&nbsp;&nbsp; What are the purposes for collection, use and disclosure of personal information? LISO collects, uses and discloses your personal information with your knowledge and consent and only for purposes that we have identified prior to or at the time we collect the information. Some of the purposes for which we may collect, use and disclose your personal information on this Website are:</p>\r\n<ul>\r\n<li>To facilitate communication with you;</li>\r\n<li>To process any of your requests for information;</li>\r\n<li>To grant/enable you to access certain special features or areas of the Website., including the ability to upload your Original Content to modify, recombine and remix all Original Content to create Derivative Content;</li>\r\n<li>To make certain other services available to you through the Website;</li>\r\n<li>To permit you to subscribe to an e-mail list to receive updates from the Website; and</li>\r\n<li>To announce special events and advise you of changes to the Website.</li>\r\n</ul>\r\n<p>LISO will not sell, trade, lend or otherwise voluntarily disclose to any third parties any personal information that you have provided to us for any purpose not identified under this Privacy Policy. We may share your personal information with third parties to assist us to administer activities on this Website, such as for website administration and maintenance, administration of contests and data processing, to our professional advisors or otherwise for legitimate and reasonable purposes for which we have obtained your prior consent.<br />In certain circumstances, we may disclose your personal information to a government institution that has asserted its lawful authority to obtain the information or where we have reasonable grounds to believe the information could be used in the investigation of an unlawful activity, or to comply with a subpoena or warrant or an order made by a court, person or body with jurisdiction to compel production of information or to comply with court rules regarding a production of records and information, or to our own legal counsel.<br />When you provide us with your personal information, such as to sign up to receive e-mails about our activities, you are consenting to the collection, use and disclosure of your personal information for these purposes in accordance with the principles that are outlined in the Privacy Policy.<br />If you have given us your consent, you may at any time withdraw your consent upon giving us reasonable notice. However, withdrawal of consent does not terminate LISO&rsquo;s ongoing rights and licenses in and to your Original Content and Derivative Content.<br />We specifically confirm that from time-to-time, we will publically display your first name, your City and Country alongside of your Original Works and Derivative Works as a means to afford your credit, as well as to promote LISO, and that by becoming a LISO Member, you are consenting to the public use of your first name, City and Country information in this manner.&nbsp; </p>\r\n<p>2.4&nbsp;&nbsp;&nbsp; Use of technology on the website. The Website uses cookie technology to make your use of the Website more convenient. A cookie is a text file which enables the Website to store any information about your activities on the Website or the length of your stay. Personal information about your visits to the Website is used on an aggregate basis, which means it is used for statistical purposes to assist us in enhancing your online experience, or to evaluate the materials that we may carry on the Website. LISO may also use the cookies to monitor polling results. The cookies will monitor results to discourage voting twice in one poll. This will give us the ability to properly count activity reports and polling results. In addition, the cookie will store your e-mail address, which is your unique identifier, so that on return visits the Website will be able to match you to your identity in the database. This is an opt-in feature which will only be made operational if you choose to do so. If you choose to disable cookies associated with the Website, this may affect your use of the Website and your ability to access certain features of the Website. </p>\r\n<p>2.5&nbsp;&nbsp;&nbsp; E-mail list. The Website allows you to provide us with your e-mail address in order that we may send you information and updates about LISO activities. In order to administer the e-mail list we will collect your name, e-mail address, geographical location. We may also collect other information as may be required from time to time in connection with the services offered on the Website.</p>\r\n<p>2.6&nbsp;&nbsp;&nbsp; Links to other sites. This Website may contain links to other websites. When you click on of those links, you are contacting another internet resource. LISO has no responsibility or liability for, or control over those websites, or internet resources or their collection, use and disclosure of your personal information. Please refer to the privacy policy and terms of use contained in any linked site you choose to go to, and familiarize yourself with that website&rsquo;s terms and conditions of use.</p>\r\n<p>2.7&nbsp;&nbsp;&nbsp; How do we protect your personal information. The Website endeavours to maintain appropriate procedural and technological measures and storage facilities to prevent any unauthorized use or disclosure of your personal information. We exercise care in the secure transmission of your information, however no transmission of information over the internet is one hundred percent secure. We cannot guarantee that information disclosed through the internet cannot be intercepted by third parties. LISO is taking the steps that are reasonably available to it to protect any personal information that is provided to it through this Website including, in its disposal or destruction.</p>\r\n<p>2.8&nbsp;&nbsp;&nbsp; Contacting us about your privacy. You may request access to your personal information and information about our collection, use and disclosure of information by Contacting Us. LISO attempts to keep records as accurate and complete as possible. You can help us maintain the accuracy of your information by notifying us of any changes to your personal information.<br /><br />This Privacy Policy may be changed from time to time at our sole discretion and without any prior notice. New versions of this Privacy Policy will be posted here. Your continued use of this Website, subsequent to any changes to the Privacy Policy, will signify that you consent to our collection, use and disclosure of your personal information in accordance with the revised Privacy Policy.</p>','1','6','2','1','1','1','1231610199','1','1254959172','0','0','0','1231610243','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('40','document','text/html','2009','','','news_2009','','1','1231552718','0','10','1','','','1','4','5','1','1','1','1231627821','1','1231723653','0','0','0','1231627838','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('45','document','text/html','2009','','','videos_2009','','1','0','0','3','1','','','1','5','5','1','1','1','1231633776','1','1237064466','0','0','0','1231633784','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('98','document','text/html','Essay on Abstract Art','',' check out this amazing essay check out this amazing essay check out this amazing essay check out this amazing essay','','','1','0','0','92','0','','<p>\r\n<strong>Lorem ipsum dolor </strong><em>sit</em> <span style=\"text-decoration: underline;\">amet</span>, <a href=\"http://www.google.com\">consectetur </a>adipiscing elit. Donec\r\npulvinar, metus id porttitor dignissim, justo purus sollicitudin\r\nturpis, bibendum mattis enim ante tincidunt eros. Phasellus magna.\r\nNullam quam. Nullam magna felis, sollicitudin ac, condimentum id,\r\ndictum nec, lorem. Sed tortor sapien, porttitor ac, viverra eget,\r\nrhoncus id, purus. Integer ullamcorper molestie massa. Suspendisse\r\nconsectetur leo vel elit. Duis ornare, augue quis egestas consectetur,\r\nelit lectus dignissim lectus, vitae aliquam nunc augue quis turpis.\r\nNullam ullamcorper. Fusce lacinia ornare purus. Sed a metus. Quisque\r\nrhoncus. Aliquam lobortis lectus nec ligula. Nunc nec dolor. Donec sed\r\nnunc. In tempor sem vitae ipsum. Lorem ipsum dolor sit amet,\r\nconsectetur adipiscing elit.\r\n</p>\r\n<p>Curabitur viverra quam. In hac habitasse platea <span style=\"text-decoration: underline;\">dictumst</span>. Quisque\r\ngravida gravida tortor. Pellentesque arcu. Fusce faucibus dolor ut\r\nnibh. Donec nisl. Quisque ante diam, dignissim ut, bibendum vitae,\r\neuismod nec, est. In tincidunt laoreet velit. Etiam enim lacus, viverra\r\nin, convallis auctor, vivera eu, neque. Vivamus sed nulla eget elit\r\nmollis congue. Vivamus non turpis. Phasellus convallis. Fusce tempus\r\ndictum metus.\r\n</p>\r\n<p>Fusce semper rhoncus ante. Donec sodales enim suscipit lacus. Fusce\r\ntempor sollicitudin metus. Nullam porta. Suspendisse egestas imperdiet\r\nturpis. Sed urna. Aenean auctor lacus vel ligula egestas venenatis.\r\nNulla et tortor eget eros dapibus congue. Mauris vitae eros ut lectus\r\nauctor lobortis. In hac habitasse platea dictumst. Proin condimentum\r\nnisl laoreet justo. Integer pulvinar sollicitudin dolor. Ut magna\r\ntellus, blandit quis, rutrum nec, varius at, sem. Vivamus vestibulum\r\nluctus tortor. Ut nec lorem ut ipsum rhoncus lobortis. Maecenas sit\r\namet eros.\r\n</p>\r\n<p>Praesent fringilla ornare justo. Sed at erat. Aliquam eu odio quis\r\neros <em>pretium</em> bibendum. Donec laoreet orci sed lacus. Phasellus non\r\ntellus in sapien fringilla lobortis. Pellentesque hendrerit ipsum eu\r\nnunc. Sed sed nulla. Maecenas rutrum feugiat mauris. Mauris vehicula mi\r\nvitae risus. Morbi augue augue, rutrum sed, luctus eget, fringilla sit\r\namet, justo. Cras placerat tempor nisi. Proin vitae orci ac ipsum\r\nullamcorper adipiscing. Curabitur velit. Fusce odio velit, tempus nec,\r\nmalesuada nec, sodales sit amet, felis. Praesent lorem. Pellentesque\r\nipsum enim, consectetur hendrerit, eleifend id, ullamcorper nec, lorem.\r\nSuspendisse vel odio eget metus lacinia malesuada. Cras est.\r\n</p>\r\n<p>Morbi nulla elit, lobortis ac, tempor ut, scelerisque ut, odio.\r\nVivamus sollicitudin enim sit amet mi. Aliquam purus. Fusce ligula dui,\r\nornare eu, pellentesque a, convallis quis, tellus. Sed id lorem\r\nelementum ligula ullamcorper mattis. Vivamus gravida tincidunt leo.\r\nCurabitur feugiat erat nec tortor. Nulla et orci sed odio lobortis\r\ngravida. Nunc eleifend fermentum felis. Suspendisse pretium, urna quis\r\ntempor lobortis, ante tortor aliquet eros, a aliquam mi magna nec\r\nsapien. Curabitur euismod malesuada leo. Mauris dignissim ipsum vel\r\nmetus. Aenean semper, lorem sit amet pharetra pharetra, orci sapien\r\ndictum mi, sagittis laoreet erat ante a metus. Fusce tristique. Aenean\r\nvel orci. Morbi lectus.\r\n</p>','1','7','0','1','1','2','1237672328','2','1251862917','0','0','0','1237672336','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('93','document','text/html','facts','','','','','1','0','0','4','1','','','1','7','1','1','1','1','1237339076','1','1249757182','0','0','0','1237697576','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('92','document','text/html','articles','','','','','1','0','0','4','1','','','1','7','0','1','1','1','1237339051','2','1251942633','0','0','0','1237339060','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('54','document','text/html','Contact','','','contact','','1','0','0','22','0','','<p>CONTACT US:\r\n</p>\r\n<p>Have a question? </p>\r\n<p>If you have a question or comment about Looking In Speaking Out please do not hesitate to connect with us.\r\n</p>\r\n<p>\r\nPlease contact:\r\nStephanie Scott\r\n</p>\r\n<p>Project Coordinator\r\n</p>\r\n<p>Email:  sscott@crossingcommunities.org\r\n</p>\r\n<p>Phone: 1-204-947-5430</p>','1','6','3','1','1','2','1234623277','1','1253068331','0','0','0','1252618531','2','contact','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('83','document','text/html','Testing','','','testing','','1','0','0','0','0','','[[testing]]','1','4','7','1','1','1','1236900758','1','1236900765','0','0','0','1236900765','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('111','document','text/html','interview with Tonya','Interview with Tonya','tonya tells all in this fascinating interview','','','1','0','0','95','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','1','1','1','2','1237697953','2','1237762756','0','0','0','1237704492','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('116','document','text/html','Assignment # 3 - write someone a letter','write someone a letter','Lorem ipsum dolor sit amet, consectetur adipiscing elit.','','','1','0','0','96','0','','<div id=\"lipsum\">\r\n<p>\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam tellus\r\nfelis, vulputate id, hendrerit vitae, scelerisque eu, massa.\r\nSuspendisse id neque et augue vehicula sodales. In ultricies libero sit\r\namet leo. Mauris egestas. Nullam mollis volutpat mauris. Fusce sodales\r\ntellus at lorem. Nunc vitae nisl in odio sodales iaculis. Ut eget diam.\r\nCras tempor, ligula suscipit imperdiet porttitor, sapien elit auctor\r\nmi, ac ornare orci purus vitae diam. Quisque erat diam, malesuada sed,\r\nsodales et, fringilla id, odio. In dictum lacinia nisi. Aliquam sed\r\nmassa. Suspendisse id dui.\r\n</p>\r\n<p>Donec eget leo. Donec quis nisl. Mauris dignissim nisl eget enim.\r\nCras vehicula sollicitudin enim. Maecenas in massa. Donec tortor mi,\r\ntempor sed, lacinia a, ultrices ac, nunc. Ut at tortor ut neque auctor\r\npretium. Nunc eu massa. Maecenas fringilla metus sit amet eros\r\nadipiscing blandit. Nulla id mi.\r\n</p>\r\n<p>Integer augue. Vivamus nisi. Duis nisi. Donec scelerisque nisi non\r\norci. Pellentesque interdum interdum lorem. In massa arcu, consequat\r\nin, gravida a, faucibus vel, diam. Fusce nunc ante, dapibus eu,\r\nvestibulum a, vulputate sed, sem. Nullam adipiscing nisl id lorem.\r\nCurabitur nec metus. Praesent sodales. Aenean nulla lectus, tincidunt\r\nvitae, consectetur id, rutrum consequat, augue. Vestibulum ante ipsum\r\nprimis in faucibus orci luctus et ultrices posuere cubilia Curae;\r\nMauris risus orci, laoreet ut, pellentesque id, tristique sit amet,\r\nligula. Pellentesque at orci. Vivamus eleifend tellus nec dolor. Fusce\r\nin ipsum. Donec suscipit. Quisque id erat. Maecenas rhoncus dolor id\r\nturpis. Donec volutpat.\r\n</p>\r\n<p>Integer erat orci, ultrices nec, volutpat quis, pellentesque ut,\r\nmauris. Curabitur faucibus turpis. Aenean justo. Curabitur pulvinar\r\naliquet lacus. Praesent quis nisi ac quam faucibus blandit. Vestibulum\r\npharetra, ipsum eget consectetur hendrerit, enim enim rhoncus enim, a\r\nporttitor mauris turpis nec lacus. Aenean venenatis blandit lacus. Sed\r\nelementum massa vel ligula. Nulla hendrerit nunc eu diam. Suspendisse\r\nsit amet elit. Aliquam placerat.\r\n</p>\r\n<p>Sed ac tortor non ante suscipit elementum. Suspendisse potenti.\r\nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per\r\ninceptos himenaeos. Maecenas non purus. Cras aliquet odio at purus.\r\nMaecenas augue tortor, rhoncus vel, consectetur id, pretium sed, orci.\r\nUt non eros a odio tincidunt malesuada. Phasellus non sapien id felis\r\nrhoncus luctus. Phasellus id odio. Ut quis sem. Pellentesque orci\r\ntellus, sodales at, viverra a, semper ornare, neque. Etiam rutrum leo\r\nvitae massa. Nam pharetra, dui et ultricies congue, nunc nisl gravida\r\nvelit, non venenatis elit orci vitae odio. Aenean euismod. Pellentesque\r\na dui. Suspendisse mauris. Nunc a dolor eu purus tincidunt laoreet.\r\nDonec suscipit ullamcorper purus. Sed dui velit, convallis congue,\r\nvehicula vitae, luctus ac, sapien. Proin enim lorem, posuere at, tempor\r\na, rhoncus et, orci.\r\n</p>\r\n</div>','1','7','2','1','1','2','1237704817','2','1237762891','0','0','0','1237704829','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('117','document','text/html','Incidence of  substance abuse in the sex trade','Incidence of  substance abuse in the sex trade','A study to quantify substance abuse in sex trade workers in Vancouver','','','1','0','0','92','0','','<p>Lots of lorem ipsum</p>','1','7','4','1','1','1','1237769546','1','1249757964','0','0','0','1237769553','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('176','document','text/html','September','','','','','1','0','0','45','1','','','1','5','3','1','1','2','1251862263','2','1251862263','0','0','0','1251862263','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('181','document','text/html','Bon Bons','','','','','1','1252908637','0','176','0','','<p><strong><em><strong>Chocolates are so yummy I try to resist but they can be so persistent.</strong></em><br /></strong>Video by Heather Traverse&nbsp; (2009)&nbsp; 1 min 20 sec</p>','1','5','0','1','1','2','1251942176','1','1253071063','0','0','0','1253070567','1','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('197','document','text/html','Links','','','','','1','0','0','6','1','','','1','4','1','1','1','2','1254862949','2','1254862958','0','0','0','1254862958','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('190','document','text/html','October','','','','','1','0','0','45','1','','','1','5','4','1','1','2','1254856407','2','1254856419','0','0','0','1254856419','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('191','document','text/html','White to Red by Rita Shrestha','','','','','1','0','0','190','0','','<p>Within majority Hindu societies, including Nepali society, if a woman becomes a widow she is expected to wear white in mourning and to shun colourful apparel, potentially for the rest of her life.&nbsp; Being marked as widows the women endure discrimination, secual harassment and societal ostracism.&nbsp; Some widows are fighting against this and defy the dominant culture by wearing red, a colour usually reserved for married women.</p>\r\n<p>&nbsp;</p>','1','5','0','1','1','2','1254858701','2','1254859031','0','0','0','1254858711','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('187','document','text/html','Gimme My Fix','','','','','1','1251835316','0','176','0','','<p>By Alexus Young</p>\r\n<p>The world of drugs can be\r\nseductive, fun, dangerous, escapist, enslaving, alluring, and nearly impossible\r\nto turn away from.&nbsp; <em>Gimme My Fix</em> is an experimental short that touches on these\r\nemotions with music by Rae Spoon.\r\n</p>\r\n<p>1 min 15 sec</p>','1','5','2','1','1','2','1253912524','2','1254852159','0','0','0','1253912550','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('188','document','text/html','October','Looking In Speaking Out Videos at imagineNATIVE','','','','1','0','0','40','1','','<p>Three videos produced for Looking In Speaking Out played at imagineNATIVE film and media arts festival in Toronto, in mid-October.&nbsp; <em>Empty, Gimme My Fix, </em>and <em>White to Red.</em></p>','1','4','5','1','1','2','1254854466','2','1254856013','0','0','0','1254856013','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('189','document','text/html','LISO Videos at imagineNATIVE','','Looking In Speaking Out Videos at imagineNATIVE film and media arts festival','','','1','0','0','188','0','','<p>Empty, Gimme My Fix and White to Red,&nbsp; three videos produced for Looking In Speaking Out were presented at imagineNATIVE in Toronto in mid-October.</p>','1','4','0','1','1','2','1254856289','1','1254960184','0','0','0','1254856296','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('186','document','text/html','Excerpts from \"Pictures of Self-Harm\"','','','','','1','1252736284','0','176','0','','<p>Women who self-harm are the main protagonists, of <em>Pictures\r\nof Self-Harm</em>, they are both interviewees&nbsp;\r\nand documentary artists in search of an understanding of their own self-harming\r\nbehaviour and society\'s response. This is a 4 minute preview of the final 20\r\nminute video. <em>Pictures of Self-Harm</em>\r\nhas been presented and provoked discussion across Canada and internationally.</p>\r\n<p>4 min Excerpt (2008)</p>','1','5','1','1','1','2','1253034987','1','1253071092','0','0','0','1253034998','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('196','reference','text/html','Crossing Communities Art Project','','Visit Crossing Communities Website','','','1','0','0','197','0','','http://www.crossingcommunities.org','1','4','0','1','0','2','1254862851','2','1254862985','0','0','0','1254862886','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('192','document','text/html','Empty','','','','','1','0','0','190','0','','<p><em>Empty </em>is a touching and poignant animated portrayal of a daughter experiencing the destruction that alcoholism inlicted on her mother.&nbsp; With music by Little Hawk.</p>','1','5','1','1','1','2','1254860469','2','1254860530','0','0','0','1254860530','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('193','document','text/html','Pictures of Self-Harm DVD\'s','','','','','1','0','0','188','0','','<p>Pictures of Self-Harm DVD is now available for purchase.</p>','1','4','1','1','1','2','1254861354','2','1254861366','0','0','0','1254861366','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('194','document','text/html','The Meaning of Addiction','','','','','1','0','0','190','0','','<p>Complete recovery to me is being able to look in the mirror and like what I see most of the time.&nbsp; The hindrances to this goal are many but slowly the numbers of positive influences in my life are growing.</p>','1','5','2','1','1','2','1254861610','2','1254861618','0','0','0','1254861618','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('195','document','text/html','Bon Bons','','','','','1','0','0','190','0','','<p>Chocolates are so yummy.&nbsp; I try to resist but they can be so persistent.</p>','1','5','3','1','1','2','1254862355','2','1254862363','0','0','0','1254862363','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('198','reference','text/html','National Film Board of Canada','','The NFB is a public agency that produces and distributes films and other audiovisual works which reflect Canada to Canadians and the rest of the world.','','','1','0','0','197','0','','http://www.nfb.ca','1','4','1','1','0','2','1254863083','2','1254863165','0','0','0','1254863165','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('199','reference','text/html','UNIFEM Canadian Committee','','United Nations Development Fund for Women','','','1','0','0','197','0','','http://www.unifem.ca/','1','4','2','1','0','2','1254863157','2','1254863215','0','0','0','1254863176','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('200','reference','text/html','Stop Rape Now','Stop Rape Now -- U.N. Action Against Sexual Violence in Conflict','U.N. Action Against Sexual Violence in Conflict','','','1','0','0','197','0','','http://www.stoprapenow.org/','1','4','3','1','0','2','1254863360','2','1254863372','0','0','0','1254863372','2','','0','0','0','0','0','0','0');
INSERT INTO `modx_site_content` VALUES ('201','reference','text/html','Status of Women Canada','','','','','1','0','0','197','0','','http://www.swc-cfc.gc.ca/index-eng.html','1','4','4','1','0','2','1254863491','2','1254863501','0','0','0','1254863501','2','','0','0','0','0','0','0','0');

# --------------------------------------------------------

#
# Table structure for table `modx_site_content_metatags`
#

CREATE TABLE `modx_site_content_metatags` (
  `content_id` int(11) NOT NULL default '0',
  `metatag_id` int(11) NOT NULL default '0',
  KEY `content_id` (`content_id`),
  KEY `metatag_id` (`metatag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Reference table between meta tags and content';

#
# Dumping data for table `modx_site_content_metatags`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_htmlsnippets`
#

CREATE TABLE `modx_site_htmlsnippets` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default 'Chunk',
  `editor_type` int(11) NOT NULL default '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL default '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL default '0' COMMENT 'Cache option',
  `snippet` mediumtext,
  `locked` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='Contains the site chunks.';

#
# Dumping data for table `modx_site_htmlsnippets`
#

INSERT INTO `modx_site_htmlsnippets` VALUES ('1','WebLoginSideBar','WebLogin Sidebar Template','0','0','0','<!-- #declare:separator <hr> --> \r\n<!-- login form section-->\r\n<form method=\"post\" name=\"loginfrm\" action=\"[+action+]\" style=\"margin: 0px; padding: 0px;\"> \r\n<input type=\"hidden\" value=\"[+rememberme+]\" name=\"rememberme\"> \r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tr>\r\n<td>\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n  <tr>\r\n	<td><b>User:</b></td>\r\n	<td><input type=\"text\" name=\"username\" tabindex=\"1\" onkeypress=\"return webLoginEnter(document.loginfrm.password);\" size=\"5\" style=\"width: 100px;\" value=\"[+username+]\" /></td>\r\n  </tr>\r\n  <tr>\r\n	<td><b>Password:</b></td>\r\n	<td><input type=\"password\" name=\"password\" tabindex=\"2\" onkeypress=\"return webLoginEnter(document.loginfrm.cmdweblogin);\" size=\"5\" style=\"width: 100px;\" value=\"\" /></td>\r\n  </tr>\r\n  <tr>\r\n	<td><label for=\"chkbox\" style=\"cursor:pointer\">Remember me:&nbsp; </label></td>\r\n	<td>\r\n	<table width=\"100%\"  border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n	  <tr>\r\n		<td valign=\"top\"><input type=\"checkbox\" id=\"chkbox\" name=\"chkbox\" tabindex=\"4\" size=\"1\" value=\"\" [+checkbox+] onClick=\"webLoginCheckRemember()\" /></td>\r\n		<td align=\"right\">									\r\n		<input type=\"submit\" value=\"[+logintext+]\" name=\"cmdweblogin\" /></td>\r\n	  </tr>\r\n	</table>\r\n	</td>\r\n  </tr>\r\n  <tr>\r\n	<td colspan=\"2\"><a href=\"#\" onclick=\"webLoginShowForm(2);return false;\">Forget Password?</a></td>\r\n  </tr>\r\n</table>\r\n</td>\r\n</tr>\r\n</table>\r\n</form>\r\n<hr>\r\n<!-- log out hyperlink section -->\r\n<a href=\'[+action+]\'>[+logouttext+]</a>\r\n<hr>\r\n<!-- Password reminder form section -->\r\n<form name=\"loginreminder\" method=\"post\" action=\"[+action+]\" style=\"margin: 0px; padding: 0px;\">\r\n<input type=\"hidden\" name=\"txtpwdrem\" value=\"0\" />\r\n<table border=\"0\">\r\n	<tr>\r\n	  <td>Enter the email address of your account <br />below to receive your password:</td>\r\n	</tr>\r\n	<tr>\r\n	  <td><input type=\"text\" name=\"txtwebemail\" size=\"24\" /></td>\r\n	</tr>\r\n	<tr>\r\n	  <td align=\"right\"><input type=\"submit\" value=\"Submit\" name=\"cmdweblogin\" />\r\n	  <input type=\"reset\" value=\"Cancel\" name=\"cmdcancel\" onclick=\"webLoginShowForm(1);\" /></td>\r\n	</tr>\r\n  </table>\r\n</form>\r\n\r\n','0');
INSERT INTO `modx_site_htmlsnippets` VALUES ('2','ImageRSS','','0','0','0','<item>  \r\n   <title>[+pagetitle+]</title>  \r\n   <link>[(site_url)][~[+id+]~]</link>  \r\n   [+newsImage+]\r\n   <description><![CDATA[ [+newsImage+] [+summary+] ]]></description>  \r\n   <pubDate>[+date+]</pubDate>  \r\n   <guid isPermaLink=\"false\">[(site_url)][~[+id+]~]</guid>  \r\n   <dc:creator>[+author+]</dc:creator> \r\n    [+tagLinks+] \r\n</item> ','0');
INSERT INTO `modx_site_htmlsnippets` VALUES ('3','videoRSS','','0','0','0','<item>  \r\n   <title>[+pagetitle+]</title>  \r\n   <link>[+video+]</link> \r\n   [+videoThumb+]\r\n   <description><![CDATA[ [+videoThumb+] [+summary+] ]]></description>  \r\n   <pubDate>[+date+]</pubDate>  \r\n   <guid isPermaLink=\"false\">[(site_url)][~[+id+]~]</guid>  \r\n   <dc:creator>[+author+]</dc:creator> \r\n    [+tagLinks+] \r\n</item> ','0');
INSERT INTO `modx_site_htmlsnippets` VALUES ('4','linksFeed','','0','0','0','<item>\r\n   <category id=\"\">\r\n   <link>[+content+]</link>\r\n</item> ','0');

# --------------------------------------------------------

#
# Table structure for table `modx_site_keywords`
#

CREATE TABLE `modx_site_keywords` (
  `id` int(11) NOT NULL auto_increment,
  `keyword` varchar(40) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `keyword` (`keyword`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Site keyword list';

#
# Dumping data for table `modx_site_keywords`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_metatags`
#

CREATE TABLE `modx_site_metatags` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `tag` varchar(50) NOT NULL default '' COMMENT 'tag name',
  `tagvalue` varchar(255) NOT NULL default '',
  `http_equiv` tinyint(4) NOT NULL default '0' COMMENT '1 - use http_equiv tag style, 0 - use name',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Site meta tags';

#
# Dumping data for table `modx_site_metatags`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_module_access`
#

CREATE TABLE `modx_site_module_access` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `module` int(11) NOT NULL default '0',
  `usergroup` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Module users group access permission';

#
# Dumping data for table `modx_site_module_access`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_module_depobj`
#

CREATE TABLE `modx_site_module_depobj` (
  `id` int(11) NOT NULL auto_increment,
  `module` int(11) NOT NULL default '0',
  `resource` int(11) NOT NULL default '0',
  `type` int(2) NOT NULL default '0' COMMENT '10-chunks, 20-docs, 30-plugins, 40-snips, 50-tpls, 60-tvs',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Module Dependencies';

#
# Dumping data for table `modx_site_module_depobj`
#

INSERT INTO `modx_site_module_depobj` VALUES ('1','2','4','30');

# --------------------------------------------------------

#
# Table structure for table `modx_site_modules`
#

CREATE TABLE `modx_site_modules` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '0',
  `editor_type` int(11) NOT NULL default '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `disabled` tinyint(4) NOT NULL default '0',
  `category` int(11) NOT NULL default '0' COMMENT 'category id',
  `wrap` tinyint(4) NOT NULL default '0',
  `locked` tinyint(4) NOT NULL default '0',
  `icon` varchar(255) NOT NULL default '' COMMENT 'url to module icon',
  `enable_resource` tinyint(4) NOT NULL default '0' COMMENT 'enables the resource file feature',
  `resourcefile` varchar(255) NOT NULL default '' COMMENT 'a physical link to a resource file',
  `createdon` int(11) NOT NULL default '0',
  `editedon` int(11) NOT NULL default '0',
  `guid` varchar(32) NOT NULL default '' COMMENT 'globally unique identifier',
  `enable_sharedparams` tinyint(4) NOT NULL default '0',
  `properties` text,
  `modulecode` mediumtext COMMENT 'module boot up code',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Site Modules';

#
# Dumping data for table `modx_site_modules`
#

INSERT INTO `modx_site_modules` VALUES ('1','Doc Manager','Quickly perform bulk updates to the Documents in your site including templates, publishing details, and permissions.','0','0','0','0','0','','0','','0','0','','1','','/**\r\n * Document Manager Module\r\n * \r\n * Purpose: Allows for the bulk management of key document settings.\r\n * Author: Garry Nutting (Mark Kaplan - Menu Index functionalty, Luke Stokes - Document Permissions concept)\r\n * For: MODx CMS (www.modxcms.com)\r\n * Date:29/09/2006 Version: 1.6\r\n *\r\n */\r\n\r\nglobal $theme;\r\nglobal $table;\r\nglobal $_lang;\r\nglobal $siteURL;\r\n\r\n$basePath = $modx->config[\'base_path\'];\r\n$siteURL = $modx->config[\'site_url\'];\r\n\r\n/** CONFIGURATION SETTINGS **/\r\n\r\n//-- set to false to hide the \'Select Tree\' option\r\n$showTree = false;\r\n\r\n/** END CONFIGURATION SETTINGS **/\r\n\r\n//-- include language file\r\n$manager_language = $modx->config[\'manager_language\'];\r\n$sql = \"SELECT setting_name, setting_value FROM \".$modx->getFullTableName(\'user_settings\').\" WHERE setting_name=\'manager_language\' AND user=\" . $modx->getLoginUserID();\r\n$rs = $modx->db->query($sql);\r\nif ($modx->db->getRecordCount($rs) > 0) {\r\n    $row = $modx->db->getRow($rs);\r\n    $manager_language = $row[\'setting_value\'];\r\n}\r\ninclude_once $basePath.\'assets/modules/docmanager/lang/english.inc.php\';\r\nif($manager_language!=\"english\") {\r\nif (file_exists($basePath.\'assets/modules/docmanager/lang/\'.$manager_language.\'.inc.php\')) {\r\n     include_once $basePath.\'assets/modules/docmanager/lang/\'.$manager_language.\'.inc.php\';\r\n}\r\n}\r\n\r\n//-- get theme\r\n$tb_prefix = $modx->db->config[\'table_prefix\'];\r\n$theme = $modx->db->select(\'setting_value\', \'`\' . $tb_prefix . \'system_settings`\', \'setting_name=\\\'manager_theme\\\'\', \'\');\r\n$theme = $modx->db->getRow($theme);\r\n$theme = ($theme[\'setting_value\'] <> \'\') ? \'/\' . $theme[\'setting_value\'] : \'\';\r\n\r\n//-- setup initial vars\r\n$table = $modx->getFullTableName(\'site_content\');\r\n$output = \'\';\r\n$error = \'\';\r\n\r\n//-- include php files\r\ninclude_once $basePath.\'manager/includes/controls/datagrid.class.php\';\r\ninclude_once $basePath.\'assets/modules/docmanager/includes/interaction.inc.php\';\r\ninclude_once $basePath.\'assets/modules/docmanager/includes/process.inc.php\';\r\n\r\n//-- get POST vars\r\n$tabAction = (isset ($_POST[\'tabAction\'])) ? $_POST[\'tabAction\'] : \'\'; // get action for active tab\r\n$intType = (isset($_POST[\'opcode\']) && $_POST[\'opcode\'] == \'range\') ? \'range\' : \'tree\'; // get interaction type\r\n\r\n//-- Menu Index\r\nif ($tabAction == \'sortMenu\' || isset($_POST[\'sortableListsSubmitted\'])) {\r\n$id= isset($_POST[\'new_parent\'])? $_POST[\'new_parent\']: 0;\r\n$actionkey = isset($_POST[\'actionkey\'])? $_POST[\'actionkey\']: 0;\r\nif(isset($_POST[\'sortableListsSubmitted\'])) {$actionkey =1;}\r\n\r\ninclude_once $basePath.\'assets/modules/docmanager/includes/SLLists.class.php\';\r\n\r\n}\r\n\r\n//-- process POST actions if required\r\nif ($tabAction == \'change_template\') {\r\n    $output .= changeTemplate($intType, $_POST[\'pids\'], $_POST[\'newvalue\']);\r\n    return $output;\r\n} elseif($tabAction == \'change_tv\') {\r\n        $output .= changeTemplateVariables($intType, $_POST[\'pids\']);\r\n        return $output;\r\n} elseif ($tabAction == \'pushDocGroup\' || $tabAction == \'pullDocGroup\' ) {\r\n    $output.=changeDocGroups($intType, $_POST[\'pids\'],$_POST[\'newvalue\'],$tabAction);\r\n    return $output;\r\n} elseif ((isset($_POST[\'actionkey\'])) && $tabAction == \'sortMenu\' || isset($_POST[\'sortableListsSubmitted\']) ) {\r\n        $output .= \' <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html\'.($modx->config[\'manager_direction\'] == \'rtl\' ? \'dir=\"rtl\"\' : \'\').\' lang=\"\'.$modx->config[\'manager_lang_attribute\'].\'\" xml:lang=\"\'.$modx->config[\'manager_lang_attribute\'].\'\">\r\n<head>\r\n                                        <script type=\"text/javascript\">\r\n                                        function save() \r\n                                        { \r\n                                        populateHiddenVars(); \r\n                                        if (document.getElementById(\"updated\")) {new Effect.Fade(\\\'updated\\\', {duration:0});} \r\n                                        new Effect.Appear(\\\'updating\\\',{duration:0.5}); \r\n                                        setTimeout(\"document.sortableListForm.submit()\",1000); \r\n                                        }\r\n                                        </script>\r\n                                        <style type=\"text/css\">\r\n                                        input {display:none;}\r\n                                        </style>\r\n                                        \';\r\n\r\n    $output.= sortMenu($id);\r\n    return $output;\r\n} elseif ($tabAction == \'changeOther\') {\r\n    $output.= changeOther($intType, $_POST[\'pids\']);\r\n    return $output;\r\n}\r\n\r\n//-- render tabbed output\r\n//--- HEAD\r\n$output .= \' <!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\r\n<html \'.($modx->config[\'manager_direction\'] == \'rtl\' ? \'dir=\"rtl\"\' : \'\').\' lang=\"\'.$modx->config[\'manager_lang_attribute\'].\'\" xml:lang=\"\'.$modx->config[\'manager_lang_attribute\'].\'\"> \r\n        <head>\r\n                <title>\'.$_lang[\'DM_module_title\'].\'</title> \r\n<script type=\"text/javascript\">var MODX_MEDIA_PATH = \"media\";</script>\r\n        <link rel=\"stylesheet\" type=\"text/css\" href=\"media/style\' . $theme . \'/style.css\" /> \r\n        <script type=\"text/javascript\" src=\"media/script/scriptaculous/prototype.js\"></script> \r\n        <script type=\"text/javascript\" src=\"media/script/scriptaculous/scriptaculous.js\"></script> \r\n        <script type=\"text/javascript\" src=\"media/script/modx.js\"></script> \r\n        <script type=\"text/javascript\" src=\"media/script/cb2.js\"></script> \r\n        <script type=\"text/javascript\" src=\"media/script/tabpane.js\"></script>  \r\n        <script type=\"text/javascript\" src=\"../assets/modules/docmanager/js/functions.js\"></script>\r\n        <script type=\"text/javascript\" src=\"media/script/datefunctions.js\"></script>\r\n        <script type=\"text/javascript\">\r\n        function save()\r\n        {\r\n            document.newdocumentparent.submit();\r\n        }   \r\n\r\nfunction setMoveValue(pId, pName) {\r\n    if (pId==0 || checkParentChildRelation(pId, pName)) {\r\n        document.newdocumentparent.new_parent.value=pId;\r\n        document.getElementById(\\\'parentName\\\').innerHTML = \"Parent: <strong>\" + pId + \"<\\/strong> \'.($modx->config[\'manager_direction\']==\'rtl\' ? \'&rlm;\' : \'\') .\'(\" + pName + \")\";\r\n    }\r\n}\r\n\r\n// check if the selected parent is a child of this document\r\nfunction checkParentChildRelation(pId, pName) {\r\n    var sp;\r\n    var id = document.newdocumentparent.id.value;\r\n    var tdoc = parent.tree.document;\r\n    var pn = (tdoc.getElementById) ? tdoc.getElementById(\"node\"+pId) : tdoc.all[\"node\"+pId];\r\n    if (!pn) return;\r\n        while (pn.p>0) {\r\n            pn = (tdoc.getElementById) ? tdoc.getElementById(\"node\"+pn.p) : tdoc.all[\"node\"+pn.p];\r\n            if (pn.id.substr(4)==id) {\r\n                alert(\"Illegal Parent\");\r\n                return;\r\n            }\r\n        }\r\n    \r\n    return true;\r\n}\r\n\';\r\n\r\nif (isset($_POST[\'selectedTV\']) && $_POST[\'selectedTV\'] <> \'\') {\r\n $output.= \'new Ajax.Updater(\\\'results\\\',\\\'\'.$modx->getConfig(\'site_url\').\'assets/modules/docmanager/includes/tv.ajax.php\\\', {method:\\\'post\\\',evalScripts:true, postBody:\\\'theme=\'.$theme.\'&langIgnoreTV=\'.addslashes($_lang[\'DM_tv_ignore_tv\']).\'&langNoTV=\'.addslashes($_lang[\'DM_tv_no_tv\']).\'&tplID=\'.$_POST[\'selectedTV\'].\'&langInsert=\'.$_lang[\'DM_tv_ajax_insertbutton\'].\'\\\',onSuccess: function(t) { $(\\\'results\\\').innerHTML = t.responseText; $(\\\'selectedTV\\\').value=\\\'\'.$_POST[\'selectedTV\'].\'\\\';}});\';\r\n}\r\n\r\n$output.=\'</script>\';\r\n$output.= buttonCSS();\r\n$output.=\'\r\n        </head>\r\n        <body>\r\n        <div class=\"subTitle\" id=\"bttn\"> \r\n                <span class=\"right\"><img src=\"media/style\' . $theme . \'/images/_tx_.gif\" width=\"1\" height=\"5\" alt=\"\" /><br />\' . $_lang[\'DM_module_title\'] . \'</span> \r\n                <div class=\"bttnheight\"><a id=\"Button5\" onclick=\"document.location.href=\\\'index.php?a=106\\\';\">\r\n                    <img src=\"media/style\' . $theme . \'/images/icons/close.gif\" alt=\"\" /> \'.$_lang[\'DM_close\'].\'</a>\r\n                </div> \r\n                <div class=\"stay\"></div> \r\n        </div> \r\n    \';\r\n        \r\n//--- TABS\r\n$output.= \'<div class=\"sectionHeader\">&nbsp;\' . $_lang[\'DM_action_title\'] . \'</div>\r\n           <div class=\"sectionBody\"> \r\n           <div class=\"tab-pane\" id=\"docManagerPane\"> \r\n           <script type=\"text/javascript\"> \r\n                tpResources = new WebFXTabPane( document.getElementById( \"docManagerPane\" ) ); \r\n           </script>\';\r\n\r\n//--- template         \r\n$output.= \'<div class=\"tab-page\" id=\"tabTemplates\">  \r\n        <h2 class=\"tab\">\' . $_lang[\'DM_change_template\'] . \'</h2>  \r\n        <script type=\"text/javascript\">tpResources.addTabPage( document.getElementById( \"tabTemplates\" ) );</script> \r\n        \';\r\n$output.=showTemplate();\r\n$output.=\'</div>\';\r\n\r\n//--- template variables       \r\n$output.= \'<div class=\"tab-page\" id=\"tabTemplateVariables\">  \r\n        <h2 class=\"tab\">\' . $_lang[\'DM_template_variables\']. \'</h2>  \r\n        <script type=\"text/javascript\">tpResources.addTabPage( document.getElementById( \"tabTemplateVariables\" ) );</script> \r\n        \';\r\n$output.=showTemplateVariables();   \r\n$output.=\'</div>\';\r\n\r\n//--- document permissions         \r\n$output.= \'<div class=\"tab-page\" id=\"tabDocPermissions\">  \r\n        <h2 class=\"tab\">\' . $_lang[\'DM_doc_permissions\']. \'</h2>  \r\n        <script type=\"text/javascript\">tpResources.addTabPage( document.getElementById( \"tabDocPermissions\" ) );</script> \r\n        \';\r\n$output.=showDocGroups();   \r\n$output.=\'</div>\';\r\n\r\n//--- sort menu        \r\n$output.= \'<div class=\"tab-page\" id=\"tabSortMenu\">  \r\n        <h2 class=\"tab\">\' . $_lang[\'DM_sort_menu\'] . \'</h2>  \r\n        <script type=\"text/javascript\">tpResources.addTabPage( document.getElementById( \"tabSortMenu\" ) );</script> \r\n        \';\r\n$output.= showSortMenu();\r\n$output.=\'</div>\';\r\n\r\n//--- show Other    \r\n$output.= \'<div class=\"tab-page\" id=\"tabOther\">  \r\n        <h2 class=\"tab\">\' . $_lang[\'DM_other\'] . \'</h2>  \r\n        <script type=\"text/javascript\">tpResources.addTabPage( document.getElementById( \"tabOther\" ) );</script> \r\n        \';\r\n$output.= showOther();\r\n$output.= showAdjustDates();\r\n$output.= showAdjustAuthors();\r\n    \r\n$output.=\'</div></div></div>\';\r\n\r\n$output.= showInteraction($showTree);\r\n\r\n//-- send output\r\n$output.=\'</body></html>\';\r\nreturn $output;');
INSERT INTO `modx_site_modules` VALUES ('2','QuickEdit','Renders QuickEdit links in the frontend','0','0','0','0','0','','0','','0','0','f888bac76e1537ca8e0cbec772b4624a','1','&mod_path=Module Path (from site root);string;assets/modules/quick_edit &show_manager_link=Show Manager Link;int;1 &show_help_link=Show Help Link;int;1 &editable=Editable Fields;string;pagetitle,longtitle,description,content,alias,introtext,menutitle,published,hidemenu,menuindex,searchable,cacheable,template ','/*\r\n *  Written by: Adam Crownoble\r\n *  Contact: adam@obledesign.com\r\n *  Created: 8/14/2005\r\n *  Updated: 8/20/2005 - Added documentation page support\r\n *  Updated: 11/27/2005 - Added show Manager & Help link options and apply (AJAX saving)\r\n *  Updated: 12/05/2005 - Added editable fields as configuration option\r\n *  For: MODx cms (modxcms.com)\r\n *  Name: QuickEdit\r\n *  Description: Edit pages from the frontend of the site\r\n *  Parameters: &mod_path=Module Path (from site root);string;assets/modules/quick_edit &show_manager_link=Show Manager Link;int;1 &show_help_link=Show Help Link;int;1 &editable=Editable Fields;string;pagetitle,longtitle,description,content,alias,introtext,menutitle,published,hidemenu,menuindex,searchable,cacheable\r\n *  Parameter sharing: enabled\r\n *  Dependencies: QuickEdit plugin\r\n */\r\n\r\n/*\r\n                             License\r\n\r\nQuickEdit - A MODx module which allows the editing of content via\r\n            the frontent of the site\r\nCopyright (C) 2005  Adam Crownoble\r\n\r\nThis program is free software; you can redistribute it and/or modify\r\nit under the terms of the GNU General Public License as published by\r\nthe Free Software Foundation; either version 2 of the License, or\r\n(at your option) any later version.\r\n\r\nThis program is distributed in the hope that it will be useful,\r\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\r\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\nGNU General Public License for more details.\r\n\r\nYou should have received a copy of the GNU General Public License\r\nalong with this program; if not, write to the Free Software\r\nFoundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA\r\n\r\n*/\r\n\r\n// Set configuration variables if not already set\r\nif(!isset($mod_path)) { $mod_path = \'assets/modules/quick_edit\'; }\r\nif(!isset($show_manager_link)) { $show_manager_link = 1; }\r\nif(!isset($show_help_link)) { $show_help_link = 1; }\r\nif(!isset($editable)) { $editable = \'pagetitle,longtitle,description,content,alias,introtext,menutitle,published,hidemenu,menuindex,searchable,cacheable,template\'; }\r\n\r\n$basePath = $modx->config[\'base_path\'];\r\n\r\n// If we cant\'t find the module files...\r\nif(!file_exists($basePath.$mod_path)) {\r\n\r\n // Log an error\r\n $error_message = \'<strong>QuickEdit module not found!</strong></p><p>Edit the QuickEdit module, click the Configuration tab and change the Module Path to point to the module.</p>\';\r\n $modx->Event->alert($error_message);\r\n $modx->logEvent(0, 3, $error_message, \'QuickEditor\');\r\n\r\n} else {\r\n\r\n $GLOBALS[\'qe_editable\'] = $editable;\r\n $GLOBALS[\'quick_edit_path\'] = $mod_path;\r\n include($basePath.$mod_path.\'/editor.class.inc.php\');\r\n\r\n $qe = new QuickEditor;\r\n $html = \'\';\r\n $doc_id = 0;\r\n $var_id = 0;\r\n $mod_id = 0;\r\n $save = 0;\r\n $ajax = 0;\r\n $apply = 0;\r\n\r\n if(isset($_REQUEST[\'doc\'])) $doc_id = $_REQUEST[\'doc\'];\r\n if(isset($_REQUEST[\'var\'])) $var_id = $_REQUEST[\'var\'];\r\n if(isset($_REQUEST[\'id\'])) $mod_id = $_REQUEST[\'id\'];\r\n if(isset($_REQUEST[\'save\'])) $save = $_REQUEST[\'save\'];\r\n if(isset($_REQUEST[\'ajax\'])) $ajax = $_REQUEST[\'ajax\'];\r\n\r\n if($doc_id && $var_id && $save && $ajax) {\r\n\r\n  $qe->save($doc_id, $var_id);\r\n\r\n } elseif($doc_id && $var_id && $save) {\r\n\r\n  $qe->save($doc_id, $var_id);\r\n  $qe->renderSaveAndCloseHTML();\r\n\r\n } elseif($doc_id && $var_id) {\r\n\r\n  $qe->renderEditorHTML($doc_id, $var_id, $mod_id);\r\n\r\n } else {\r\n\r\n  include($basePath.$mod_path.\'/documentation.php\');\r\n\r\n }\r\n\r\n}\r\n');

# --------------------------------------------------------

#
# Table structure for table `modx_site_plugin_events`
#

CREATE TABLE `modx_site_plugin_events` (
  `pluginid` int(10) NOT NULL default '0',
  `evtid` int(10) NOT NULL default '0',
  `priority` int(10) NOT NULL default '0' COMMENT 'determines plugin run order'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Links to system events';

#
# Dumping data for table `modx_site_plugin_events`
#

INSERT INTO `modx_site_plugin_events` VALUES ('1','73','0');
INSERT INTO `modx_site_plugin_events` VALUES ('1','65','0');
INSERT INTO `modx_site_plugin_events` VALUES ('1','59','0');
INSERT INTO `modx_site_plugin_events` VALUES ('1','53','0');
INSERT INTO `modx_site_plugin_events` VALUES ('1','47','0');
INSERT INTO `modx_site_plugin_events` VALUES ('1','41','0');
INSERT INTO `modx_site_plugin_events` VALUES ('1','35','0');
INSERT INTO `modx_site_plugin_events` VALUES ('1','29','0');
INSERT INTO `modx_site_plugin_events` VALUES ('1','23','0');
INSERT INTO `modx_site_plugin_events` VALUES ('2','93','0');
INSERT INTO `modx_site_plugin_events` VALUES ('2','81','0');
INSERT INTO `modx_site_plugin_events` VALUES ('2','80','0');
INSERT INTO `modx_site_plugin_events` VALUES ('3','28','0');
INSERT INTO `modx_site_plugin_events` VALUES ('4','92','0');
INSERT INTO `modx_site_plugin_events` VALUES ('4','3','0');
INSERT INTO `modx_site_plugin_events` VALUES ('5','85','0');
INSERT INTO `modx_site_plugin_events` VALUES ('5','87','0');
INSERT INTO `modx_site_plugin_events` VALUES ('5','88','0');
INSERT INTO `modx_site_plugin_events` VALUES ('6','3','0');
INSERT INTO `modx_site_plugin_events` VALUES ('7','29','0');
INSERT INTO `modx_site_plugin_events` VALUES ('9','85','0');
INSERT INTO `modx_site_plugin_events` VALUES ('9','87','0');
INSERT INTO `modx_site_plugin_events` VALUES ('9','88','0');
INSERT INTO `modx_site_plugin_events` VALUES ('10','3','0');
INSERT INTO `modx_site_plugin_events` VALUES ('11','29','0');
INSERT INTO `modx_site_plugin_events` VALUES ('12','35','1');
INSERT INTO `modx_site_plugin_events` VALUES ('12','29','1');
INSERT INTO `modx_site_plugin_events` VALUES ('12','28','1');

# --------------------------------------------------------

#
# Table structure for table `modx_site_plugins`
#

CREATE TABLE `modx_site_plugins` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default 'Plugin',
  `editor_type` int(11) NOT NULL default '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL default '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL default '0' COMMENT 'Cache option',
  `plugincode` mediumtext,
  `locked` tinyint(4) NOT NULL default '0',
  `properties` text,
  `disabled` tinyint(4) NOT NULL default '0' COMMENT 'Disables the plugin',
  `moduleguid` varchar(32) NOT NULL default '' COMMENT 'GUID of module from which to import shared parameters',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COMMENT='Contains the site plugins.';

#
# Dumping data for table `modx_site_plugins`
#

INSERT INTO `modx_site_plugins` VALUES ('1','Bottom Button Bar','Adds a set of buttons to the bottom of all manager pages','0','0','0','/* BottomButtonBar v1.0 (by Mitch)\r\n *\r\n * I just got tired of scrolling up after editing a document/snippet/etc... \r\n * to save it. The GoUp plugin improved it a bit, but I was not completely \r\n * satisfied. So I wrote this plugin that will add the complete buttonbar \r\n * at the bottom of the edit screen. Also there is a GoUp link in the bottombar.\r\n *\r\n * To use the plugin you have to enable the following events:\r\n *\r\n *   OnChunkFormRender\r\n *   OnDocFormRender\r\n *   OnModFormRender\r\n *   OnPluginFormRender\r\n *   OnSnipFormRender\r\n *   OnTVFormRender\r\n *   OnTempFormRender\r\n *   OnUserFormRender\r\n *   OnWUsrFormRender\r\n *\r\n * Hope you like it as much as I do.\r\n * \r\n */\r\n\r\n// I know the code looks messy, but that is mainly because of the copy/pasting.\r\n\r\nglobal $_lang;\r\n\r\n//Get Manager Theme - added by garryn\r\n$manager_theme = $modx->config[\'manager_theme\'] ? $modx->config[\'manager_theme\'] : \'\';\r\n\r\n// Get a reference to the event\r\n$e = & $modx->Event;\r\n\r\n// For every form basicially the code is just copied from the appropriate\r\n// mutate_XXXXXX.dynamic.action.php file. Then the CSS id\'s are updated so they\r\n// are unique. I just added __ after it.\r\n\r\nswitch ($e->name) {\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnDocFormRender\":\r\n\r\n// From mutate_content.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\" /><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<tr>\r\n			<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.mutate.save.click();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'save\']; ?></a></td>\r\n			<td id=\"Button2__\"><a href=\"#\" onclick=\"deletedocument();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'delete\']; ?></span></a></td>\r\n				<?php if($_REQUEST[\'a\']==\'4\' || $_REQUEST[\'a\']==72) { ?><script>document.getElementById(\"Button2__\").className=\'disabled\';</script><?php } ?>\r\n			<td id=\"Button5__\"><a href=\"<?php echo $id==0 ? \"index.php?a=2\" : \"index.php?a=3&id=$id\"; ?>\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n		</tr>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnSnipFormRender\":\r\n\r\n// From mutate_snippet.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\" /><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.mutate.save.click(); saveWait(\'mutate\');\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'save\']; ?></a></td>\r\n<?php if($_GET[\'a\']==\'22\') { ?>\r\n		<td id=\"Button2__\"><a href=\"#\" onclick=\"duplicaterecord();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/copy.gif\" align=\"absmiddle\" /> <?php echo $_lang[\"duplicate\"]; ?></a></td>\r\n		<td id=\"Button3__\"><a href=\"#\" onclick=\"deletedocument();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'delete\']; ?></a></td>\r\n<?php } ?>\r\n		<td id=\"Button4__\"><a href=\"index.php?a=76\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnChunkFormRender\":\r\n\r\n// From mutate_htmlsnippet.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\" /><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.mutate.save.click(); saveWait(\'mutate\');\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'save\']; ?></a></td>\r\n<?php if($_REQUEST[\'a\']==\'78\') { ?>\r\n		<td id=\"Button2__\"><a href=\"#\" onclick=\"duplicaterecord();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/copy.gif\" align=\"absmiddle\" /> <?php echo $_lang[\"duplicate\"]; ?></a></td>\r\n		<td id=\"Button3__\"><a href=\"#\" onclick=\"deletedocument();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'delete\']; ?></a></td>\r\n<?php } ?>\r\n		<td id=\"Button4__\"><a href=\"index.php?a=76\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnModFormRender\":\r\n\r\n// From mutate_module.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\"><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<tr>\r\n		<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.mutate.save.click(); saveWait(\'mutate\');\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\"> <?php echo $_lang[\'save\']; ?></a></td>\r\n<?php if($_GET[\'a\']==\'108\') { ?>\r\n		<td id=\"Button2__\"><a href=\"#\" onclick=\"duplicaterecord();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/copy.gif\" align=\"absmiddle\"> <?php echo $_lang[\"duplicate\"]; ?></a></td>\r\n		<td id=\"Button3__\"><a href=\"#\" onclick=\"deletedocument();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\"> <?php echo $_lang[\'delete\']; ?></a></td>\r\n<?php } ?>\r\n		<td id=\"Button4__\"><a href=\"#\" onclick=\"documentDirty=false;document.location.href=\'index.php?a=106\';\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\"> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n		</tr>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnPluginFormRender\":\r\n\r\n// From mutate_plugin.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\" /><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.mutate.save.click(); saveWait(\'mutate\');\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'save\']; ?></a></td>\r\n<?php if($_GET[\'a\']==\'102\') { ?>\r\n		<td id=\"Button2__\"><a href=\"#\" onclick=\"duplicaterecord();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/copy.gif\" align=\"absmiddle\" /> <?php echo $_lang[\"duplicate\"]; ?></a></td>\r\n		<td id=\"Button3__\"><a href=\"#\" onclick=\"deletedocument();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'delete\']; ?></a></td>\r\n<?php } ?>\r\n		<td id=\"Button4__\"><a href=\"index.php?a=76\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\"> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnTVFormRender\":\r\n\r\n// From mutate_tmplvars.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\" /><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.mutate.save.click(); saveWait(\'mutate\');\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'save\']; ?></a></td>\r\n<?php if($_GET[\'a\']==\'301\') { ?>\r\n		<td id=\"Button2__\"><a href=\"#\" onclick=\"duplicaterecord();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/copy.gif\" align=\"absmiddle\" /> <?php echo $_lang[\"duplicate\"]; ?></a></td>\r\n		<td id=\"Button3__\"><a href=\"#\" onclick=\"deletedocument();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'delete\']; ?></a></td>\r\n<?php } ?>\r\n		<td id=\"Button4__\"><a href=\"index.php?a=76\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnTempFormRender\":\r\n\r\n// From mutate_templates.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\" /><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.mutate.save.click(); saveWait(\'mutate\');\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'save\']; ?></a></td>\r\n<?php if($_REQUEST[\'a\']==\'16\') { ?>\r\n		<td id=\"Button2__\"><a href=\"#\" onclick=\"duplicaterecord();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/copy.gif\" align=\"absmiddle\" /> <?php echo $_lang[\"duplicate\"]; ?></a></td>\r\n		<td id=\"Button3__\"><a href=\"#\" onclick=\"deletedocument();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'delete\']; ?></a></td>\r\n<?php } ?>\r\n		<td id=\"Button4__\"><a href=\"index.php?a=76\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnWUsrFormRender\":\r\n\r\n// From mutate_web_user.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\" /><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<tr>\r\n			<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.userform.save.click();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'save\']; ?></a></td>\r\n			<td id=\"Button2__\"><a href=\"#\" onclick=\"deleteuser();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'delete\']; ?></a></td>\r\n				<?php if($_GET[\'a\']!=\'88\') { ?>\r\n					<script>document.getElementById(\"Button2__\").className=\'disabled\';</script>\r\n				<?php } ?>\r\n			<td id=\"Button3__\"><a href=\"index.php?a=99\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n		</tr>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n//-------------------------------------------------------------------\r\n   case \"OnUserFormRender\":\r\n\r\n// From mutate_user.dynamic.action.php\r\nob_start();\r\n?>\r\n<div class=\"subTitle\" style=\"width:100%\">\r\n	<span class=\"right\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/_tx_.gif\" width=\"1\" height=\"5\" /><br />\"<a href=\'javascript:scroll(0,0);\'><?php echo $_lang[\'scroll_up\']; ?></a>\"</span>\r\n\r\n	<table cellpadding=\"0\" cellspacing=\"0\" class=\"actionButtons\">\r\n		<tr>\r\n			<td id=\"Button1__\"><a href=\"#\" onclick=\"documentDirty=false; document.userform.save.click();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/save.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'save\']; ?></a></td>\r\n			<td id=\"Button2__\"><a href=\"#\" onclick=\"deleteuser();\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/delete.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'delete\']; ?></a></td>\r\n				<?php if($_GET[\'a\']!=\'12\') { ?>	\r\n					<script>document.getElementById(\"Button2__\").className=\'disabled\';</script>\r\n				<?php } ?>\r\n			<td id=\"Button3__\"><a href=\"index.php?a=75\"><img src=\"media/style/<?php echo $manager_theme ? \"$manager_theme/\":\"\"; ?>images/icons/cancel.gif\" align=\"absmiddle\" /> <?php echo $_lang[\'cancel\']; ?></a></td>\r\n		</tr>\r\n	</table>\r\n</div>\r\n<?php\r\n$output = ob_get_clean();\r\n    break;\r\n\r\n}\r\n\r\n// Add the new bar to the output\r\n$e->output($output);\r\n','0','','0','');
INSERT INTO `modx_site_plugins` VALUES ('2','Forgot Manager Login','Resets your manager login when you forget your password.','0','0','0','\r\nif(!class_exists(\'ForgotManagerPassword\')) {\r\nclass ForgotManagerPassword{\r\n\r\n function ForgotManagerPassword(){\r\n \r\n  $this->errors = array();\r\n \r\n  $this->checkLang();\r\n\r\n }\r\n\r\n function getLink() {\r\n \r\n  global $_lang;\r\n \r\n$link = <<<EOD\r\n<a id=\"ForgotManagerPassword-show_form\" href=\"index.php?action=show_form\">{$_lang[\'forgot_your_password\']}</a>\r\nEOD;\r\n\r\n  return $link;\r\n\r\n }\r\n\r\n function getForm() {\r\n\r\n  global $_lang;\r\n\r\n$form = <<<EOD\r\n<label id=\"FMP-email_label\" for=\"FMP_email\">{$_lang[\'account_email\']}:</label>\r\n<input id=\"FMP-email\" type=\"text\" />\r\n<button id=\"FMP-email_button\" type=\"button\" onclick=\"window.location = \'index.php?action=send_email&email=\'+document.getElementById(\'FMP-email\').value;\">{$_lang[\'send\']}</button>\r\nEOD;\r\n\r\n  return $form;\r\n\r\n }\r\n\r\n // Get user info including a hash unique to this user, password, and day\r\n function getUser($user_id=0, $username=\'\', $email=\'\', $hash=\'\') {\r\n\r\n  global $modx, $_lang;\r\n \r\n  $user_id = $modx->db->escape($user_id);\r\n  $username = $modx->db->escape($username);\r\n  $email = $modx->db->escape($email);\r\n  $emaail = $modx->db->escape($hash);\r\n  \r\n  $pre = $modx->db->config[\'table_prefix\'];\r\n  $site_id = $modx->config[\'site_id\'];\r\n  $today = date(\'Yz\'); // Year and day of the year\r\n  $wheres = array();\r\n  $where = \'\';\r\n  $user = array(\'id\'=>0, \'username\'=>\'\', \'email\'=>\'\', \'hash\'=>\'\');\r\n \r\n  if(!empty($user_id)) { $wheres[] = \"id = \'{$user_id}\'\"; }\r\n  if(!empty($username)) { $wheres[] = \"username = \'{$username}\'\"; }\r\n  if(!empty($email)) { $wheres[] = \"email = \'{$email}\'\"; }\r\n  if(!empty($hash)) { $wheres[] = \"MD5(CONCAT(usr.username,usr.password,\'{$site_id}\',\'{$today}\')) = \'{$hash}\'\"; }\r\n \r\n  if($wheres) {\r\n  \r\n   $where = \' WHERE \'.implode(\' AND \',$wheres);\r\n   $sql = \"SELECT usr.id, usr.username, attr.email, MD5(CONCAT(usr.username,usr.password,\'{$site_id}\',\'{$today}\')) AS hash\r\n           FROM `{$pre}manager_users` usr\r\n           INNER JOIN `{$pre}user_attributes` attr ON usr.id = attr.internalKey\r\n           {$where};\";\r\n   \r\n   if($result = $modx->db->query($sql)){\r\n    if($modx->db->getRecordCount($result)==1) {\r\n     $user = $modx->db->getRow($result);\r\n    }\r\n   }\r\n   \r\n  }\r\n  \r\n  if(!$user[\'id\']) { $this->errors[] = $_lang[\'could_not_find_user\']; }\r\n\r\n  return $user;\r\n\r\n }\r\n\r\n // Send an email with a link to login\r\n function sendEmail($to) {\r\n\r\n  global $modx, $_lang;\r\n\r\n  $subject = $_lang[\'password_change_request\'];\r\n  $headers  = \"MIME-Version: 1.0\\n\".\r\n              \"Content-type: text/html; charset=iso-8859-1\\n\".\r\n              \"From: MODx\\n\".\r\n              \"Reply-To: no-reply@{$_SERVER[\'HTTP_HOST\']}\\n\".\r\n              \"X-Mailer: PHP/\".phpversion();\r\n  \r\n  $user = $this->getUser(0, \'\', $to);\r\n\r\n  if($user[\'username\']) {\r\n\r\n$body = <<<EOD\r\n<p>{$_lang[\'forgot_password_email_intro\']} <a href=\"{$modx->config[\'site_url\']}manager/processors/login.processor.php?username={$user[\'username\']}&hash={$user[\'hash\']}\">{$_lang[\'forgot_password_email_link\']}</a></p>\r\n\r\n<p>{$_lang[\'forgot_password_email_instructions\']}</p>\r\n\r\n<p><small>{$_lang[\'forgot_password_email_fine_print\']}</small></p>\r\nEOD;\r\n\r\n   $mail = mail($to, $subject, $body, $headers);\r\n \r\n   if(!$mail) { $this->errors[] = $_lang[\'error_sending_email\']; }\r\n \r\n   return $mail;\r\n   \r\n  }\r\n\r\n }\r\n \r\n function unblockUser($user_id) {\r\n  \r\n  global $modx, $_lang;\r\n  $pre = $modx->db->config[\'table_prefix\'];\r\n  \r\n  $modx->db->update(array(\'blocked\'=>\'\', \'blockeduntil\'=>\'\'), \"`{$pre}user_attributes`\", \"internalKey = \'{$user_id}\'\");\r\n  \r\n  if(!$modx->db->getAffectedRows()) { $this->errors[] = $_lang[\'user_doesnt_exist\']; return; }\r\n  \r\n  return true;\r\n  \r\n }\r\n \r\n function checkLang() {\r\n  \r\n  global $_lang;\r\n  $eng = array();\r\n  \r\n  $eng[\'forgot_your_password\'] = \'Forgot your password?\';\r\n  $eng[\'account_email\'] = \'Account email\';\r\n  $eng[\'send\'] = \'Send\';\r\n  $eng[\'password_change_request\'] = \'Password change request\';\r\n  $eng[\'forgot_password_email_intro\'] = \'A request has been made to change the password on your account.\';\r\n  $eng[\'forgot_password_email_link\'] = \'Click here to complete the process.\';\r\n  $eng[\'forgot_password_email_instructions\'] = \'From there you will be able to change your password from the My Account menu.\';\r\n  $eng[\'forgot_password_email_fine_print\'] = \'* The URL above will expire once you change your password or after today.\';\r\n  $eng[\'error_sending_email\'] = \'Error sending email\';\r\n  $eng[\'could_not_find_user\'] = \'Could not find user\';\r\n  $eng[\'user_doesnt_exist\'] = \'User does not exist\';\r\n  $eng[\'email_sent\'] = \'Email sent\';\r\n  \r\n  foreach($eng as $key=>$value) {\r\n   if(empty($_lang[$key])) { $_lang[$key] = $value; }\r\n  }\r\n  \r\n }\r\n \r\n function getErrorOutput() {\r\n \r\n  $outptut = \'\';\r\n \r\n  if($this->errors) {\r\n   $output = \'<span class=\"error\">\'.implode(\'</span><span class=\"errors\">\', $this->errors).\'</span>\';\r\n  }\r\n \r\n  return $output;\r\n \r\n }\r\n \r\n}\r\n}\r\n\r\nglobal $_lang;\r\n\r\n$output = \'\';\r\n$event_name = $modx->Event->name;\r\n$action = (empty($_GET[\'action\']) ? \'\' : $_GET[\'action\']);\r\n$username = (empty($_GET[\'username\']) ? \'\' : $_GET[\'username\']);\r\n$to = (empty($_GET[\'email\']) ? \'\' : $_GET[\'email\']);\r\n$hash = (empty($_GET[\'hash\']) ? \'\' : $_GET[\'hash\']);\r\n$forgot = new ForgotManagerPassword();\r\n\r\nif($event_name == \'OnManagerLoginFormRender\') {\r\n\r\n switch($action) {\r\n\r\n  case \'show_form\':\r\n   $output = $forgot->getForm();\r\n   break;\r\n  \r\n  case \'send_email\':\r\n   if($forgot->sendEmail($to)) { $output = $_lang[\'email_sent\']; }\r\n   break;\r\n  \r\n  default:\r\n   $output = $forgot->getLink();\r\n\r\n }\r\n \r\n if($forgot->errors) { $output = $forgot->getErrorOutput() . $forgot->getLink(); }\r\n \r\n}\r\n\r\nif($event_name == \'OnBeforeManagerLogin\') {\r\n $user = $forgot->getUser(0, $username, \'\', $hash);\r\n if($user[\'id\'] && !$forgot->errors) {\r\n  $forgot->unblockUser($user[\'id\']);\r\n }\r\n}\r\n\r\nif($event_name == \'OnManagerAuthentication\' && $hash) {\r\n $user = $forgot->getUser(0, \'\', \'\', $hash);\r\n $output = ($user[\'id\'] > 0 && !$forgot->errors);\r\n}\r\n\r\n$modx->Event->output($output);\r\n','0','','1','');
INSERT INTO `modx_site_plugins` VALUES ('3','Inherit Parent Template','New docs automatically select template of parent folder','0','0','0','/*\r\n * Inherit Template from Parent\r\n * Written By Raymond Irving - 12 Oct 2006\r\n *\r\n * Simply results in new documents inherriting the template \r\n * of their parent folder upon creating a new document\r\n *\r\n * Configuration:\r\n * check the OnDocFormPrerender event\r\n *\r\n * Version 1.0\r\n *\r\n */\r\n\r\nglobal $content;\r\n$e = &$modx->Event;\r\n\r\nswitch($e->name) {\r\n  case \'OnDocFormPrerender\':\r\n    if(($_REQUEST[\'pid\'] > 0) && ($id == 0)) {\r\n      if($parent = $modx->getPageInfo($_REQUEST[\'pid\'],0,\'template\')) {\r\n        $content[\'template\'] = $parent[\'template\'];\r\n      }\r\n    }\r\n    break;\r\n\r\n  default:\r\n    return;\r\n    break;\r\n}\r\n','0','','0',' ');
INSERT INTO `modx_site_plugins` VALUES ('4','QuickEdit','Front-end Content Editor.','0','0','0','/*\r\n *  Written by: Adam Crownoble\r\n *  Contact: adam@obledesign.com\r\n *  Created: 8/14/2005\r\n *  Updated: 11/27/2005 - Added support for show Manager & Help links option\r\n *  Updated: 12/05/2005 - Added support for editable fields as a module configuration option\r\n *  For: MODx cms (modxcms.com)\r\n *  Name: QuickEdit\r\n *  Description: Renders QuickEdit links in the frontend\r\n *  Shared parameters from: QuickEdit module\r\n *  Events: OnParseDocument, OnWebPagePrerender\r\n */\r\n\r\n/*\r\n                             License\r\n\r\nQuickEdit - A MODx module which allows the editing of content via\r\n            the frontent of the site\r\nCopyright (C) 2005  Adam Crownoble\r\n\r\nThis program is free software; you can redistribute it and/or modify\r\nit under the terms of the GNU General Public License as published by\r\nthe Free Software Foundation; either version 2 of the License, or\r\n(at your option) any later version.\r\n\r\nThis program is distributed in the hope that it will be useful,\r\nbut WITHOUT ANY WARRANTY; without even the implied warranty of\r\nMERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\r\nGNU General Public License for more details.\r\n\r\nYou should have received a copy of the GNU General Public License\r\nalong with this program; if not, write to the Free Software\r\nFoundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA\r\n\r\n*/\r\n\r\n// Don\'t do anything if we aren\'t logged in\r\nif(isset($_SESSION[\'mgrValidated\']) && $_SESSION[\'mgrValidated\']) {\r\n\r\n // Set configuration variables if not already set\r\n if(!isset($mod_path)) { $mod_path = $modx->config[\'base_path\'].\'assets/modules/quick_edit\'; }\r\n if(!isset($show_manager_link)) { $show_manager_link = 1; }\r\n if(!isset($show_help_link)) { $show_help_link = 1; }\r\n if(!isset($editable)) { $editable = \'pagetitle,longtitle,description,content,alias,introtext,menutitle,published,hidemenu,menuindex,searchable,cacheable\'; }\r\n\r\n // If we can\'t find the module files...\r\n if(!file_exists($mod_path)) {\r\n\r\n  // Only log the error if we haven\'t already logged it...\r\n  if(!isset($GLOBALS[\'quick_edit_not_found_sent\'])) {\r\n\r\n   // Set a global variable so that we can only log this once\r\n   $GLOBALS[\'quick_edit_not_found_sent\'] = true;\r\n\r\n   // Log an error\r\n   $error_message = \'<strong>QuickEdit module not found!</strong></p><p>Edit the QuickEdit module, click the Configuration tab and change the Module Path to point to the module.</p>\';\r\n   $modx->logEvent(0, 3, $error_message, \'QuickEditor\');\r\n\r\n  }\r\n\r\n } else {\r\n\r\n  // Set globals from QE Module\'s shared paramaters so we can get them from the frontend\r\n  $GLOBALS[\'qe_show_manager_link\'] = $show_manager_link;\r\n  $GLOBALS[\'qe_show_help_link\'] = $show_help_link;\r\n  $GLOBALS[\'qe_editable\'] = $editable;\r\n\r\n  // Set the mod_path as a global variable\r\n  $GLOBALS[\'quick_edit_path\'] = $mod_path;\r\n  if (!class_exists(\'Output\')) include_once($mod_path.\'/output.class.inc.php\');\r\n\r\n  $outputObject = new Output;\r\n\r\n  switch($modx->Event->name) {\r\n\r\n   case \'OnParseDocument\' :\r\n\r\n    $outputObject->output = $modx->documentOutput;\r\n\r\n    // Merge QuickEdit comment into the output\r\n    $outputObject->mergeTags();\r\n\r\n    break;\r\n\r\n   case \'OnWebPagePrerender\' :\r\n\r\n    $outputObject->output = &$modx->documentOutput;\r\n\r\n    include_once($mod_path.\'/module.class.inc.php\');\r\n    $module = new Module;\r\n    $module->getIdFromDependentPluginName($modx->Event->activePlugin);\r\n\r\n    // Replace QuickEdit comments with QuickEdit links\r\n    $outputObject->mergeLinks($module->id);\r\n\r\n    break;\r\n\r\n  }\r\n\r\n  // Set the event output\r\n  $modx->documentOutput = $outputObject->output;\r\n\r\n  // Logout ?\r\n  $qe_logout= (isset($_GET[\'QuickEdit_logout\'])? $_GET[\'QuickEdit_logout\']: \'\');\r\n  if($qe_logout == \'logout\') {\r\n   $_SESSION = array();\r\n  }\r\n\r\n }\r\n\r\n}\r\n','0','','0','f888bac76e1537ca8e0cbec772b4624a');
INSERT INTO `modx_site_plugins` VALUES ('5','TinyMCE','<strong>3.1.0.1a:</strong> TinyMCE RichText Editor Plugin','0','0','0','/*\r\n * TinyMCE RichText Editor Plugin \r\n * Written By Jeff Whitfield - September 9, 2005\r\n * Modified On - June 23, 2008\r\n *\r\n * Version 3.1.0.1a\r\n *\r\n * Events: OnRichTextEditorInit, OnRichTextEditorRegister, OnInterfaceSettingsRender\r\n *\r\n */\r\n\r\n// Set the name of the plugin folder\r\n$pluginfolder = \"tinymce3101\";\r\n\r\ninclude_once $modx->config[\'base_path\'].\'assets/plugins/\'.$pluginfolder.\'/tinymce.lang.php\';\r\ninclude_once $modx->config[\'base_path\'].\'assets/plugins/\'.$pluginfolder.\'/tinymce.functions.php\';\r\n\r\n// Set path and base setting variables\r\nif(!isset($tinyPath)) { \r\n	global $tinyPath, $tinyURL;\r\n	$tinyPath = $modx->config[\'base_path\'].\'assets/plugins/\'.$pluginfolder; \r\n	$tinyURL = $modx->config[\'base_url\'].\'assets/plugins/\'.$pluginfolder; \r\n}\r\n$base_url = $modx->config[\'base_url\'];\r\n$displayStyle = ( ($_SESSION[\'browser\']==\'mz\') || ($_SESSION[\'browser\']==\'op\') ) ? \"table-row\" : \"block\" ;\r\n\r\n// Handle event\r\n$e = &$modx->Event; \r\nswitch ($e->name) { \r\n	case \"OnRichTextEditorRegister\": // register only for backend\r\n		$e->output(\"TinyMCE\");\r\n		break;\r\n\r\n	case \"OnRichTextEditorInit\": \r\n		if($editor==\"TinyMCE\") {\r\n			$elementList = implode(\",\", $elements);\r\n			if(isset($forfrontend)||$modx->isFrontend()){\r\n				$frontend = \'true\';\r\n				$frontend_language = isset($modx->config[\'fe_editor_lang\']) ? $modx->config[\'fe_editor_lang\']:\"\";\r\n				$tinymce_language = getTinyMCELang($frontend_language);\r\n				$webuser = (isset($modx->config[\'rb_webuser\']) ? $modx->config[\'rb_webuser\'] : null);\r\n				$html = getTinyMCEScript($elementList,$webtheme,$width,$height,$tinymce_language,$frontend,$base_url, $webPlugins, $webButtons1, $webButtons2, $webButtons3, $webButtons4, $disabledButtons, $tinyFormats, $entity_encoding, $entities, $tinyPathOptions, $tinyCleanup, $tinyResizing, $modx->config[\'editor_css_path\'], $modx->config[\'tinymce_css_selectors\'], $modx->config[\'use_browser\'], $webAlign, null, null, $tinyLinkList, $customparams, $tinyURL, $webuser);\r\n			} else {\r\n				$frontend = \'false\';\r\n				$manager_language = $modx->config[\'manager_language\'];\r\n				$tinymce_language = getTinyMCELang($manager_language);\r\n				$html = getTinyMCEScript($elementList, $modx->config[\'tinymce_editor_theme\'], $width=\'100%\', $height=\'400px\', $tinymce_language, $frontend, $modx->config[\'base_url\'], $modx->config[\'tinymce_custom_plugins\'], $modx->config[\'tinymce_custom_buttons1\'], $modx->config[\'tinymce_custom_buttons2\'], $modx->config[\'tinymce_custom_buttons3\'], $modx->config[\'tinymce_custom_buttons4\'], $disabledButtons, $tinyFormats, $entity_encoding, $entities, $tinyPathOptions, $tinyCleanup, $tinyResizing, $modx->config[\'editor_css_path\'], $modx->config[\'tinymce_css_selectors\'], $modx->config[\'use_browser\'], $modx->config[\'manager_direction\'], $advimage_styles, $advlink_styles, $tinyLinkList, $customparams, $tinyURL, null);\r\n			}\r\n			$e->output($html);\r\n		}		\r\n		break;\r\n\r\n	case \"OnInterfaceSettingsRender\":\r\n		global $usersettings,$settings;\r\n		$action = $modx->manager->action;\r\n		switch ($action) {\r\n    		case 11:\r\n        		$tinysettings = \"\";\r\n        		break;\r\n    		case 12:\r\n        		$tinysettings = $usersettings;\r\n        		break;\r\n    		default:\r\n        		$tinysettings = $settings;\r\n        		break;\r\n    	}\r\n		$tinymce_editor_theme = $tinysettings[\'tinymce_editor_theme\'];\r\n		$tinymce_css_selectors = $tinysettings[\'tinymce_css_selectors\'];\r\n		$tinymce_custom_plugins = $tinysettings[\'tinymce_custom_plugins\'];\r\n		$tinymce_custom_buttons1 = $tinysettings[\'tinymce_custom_buttons1\'];\r\n		$tinymce_custom_buttons2 = $tinysettings[\'tinymce_custom_buttons2\'];\r\n		$tinymce_custom_buttons3 = $tinysettings[\'tinymce_custom_buttons3\'];\r\n		$tinymce_custom_buttons4 = $tinysettings[\'tinymce_custom_buttons4\'];\r\n		$manager_language = $modx->config[\'manager_language\'];\r\n		$html = getTinyMCESettings($_lang, $tinyPath, $modx->config[\'manager_language\'], $modx->config[\'use_editor\'], $tinymce_editor_theme, $tinymce_css_selectors, $tinymce_custom_plugins, $tinymce_custom_buttons1, $tinymce_custom_buttons2, $tinymce_custom_buttons3, $tinymce_custom_buttons4, $displayStyle, $action);\r\n		$e->output($html);\r\n		break;\r\n\r\n   default :    \r\n      return; // stop here - this is very important. \r\n      break; \r\n}\r\n','0','&customparams=Custom Parameters;textarea; &tinyFormats=Block Formats;text;p,h1,h2,h3,h4,h5,h6,div,blockquote,code,pre,address &entity_encoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &tinyPathOptions=Path Options;list;rootrelative,docrelative,fullpathurl;docrelative &tinyCleanup=Cleanup;list;enabled,disabled;enabled &tinyResizing=Advanced Resizing;list;true,false;false &advimage_styles=Advanced Image Styles;text; &advlink_styles=Advanced Link Styles;text; &disabledButtons=Disabled Buttons;text; &tinyLinkList=Link List;list;enabled,disabled;enabled &webtheme=Web Theme;list;simple,advanced,editor,custom;simple &webPlugins=Web Plugins;text;style,advimage,advlink,searchreplace,print,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras,visualchars,media &webButtons1=Web Buttons 1;text;undo,redo,selectall,separator,pastetext,pasteword,separator,search,replace,separator,nonbreaking,hr,charmap,separator,image,link,unlink,anchor,media,separator,cleanup,removeformat,separator,fullscreen,print,code,help &webButtons2=Web Buttons 2;text;bold,italic,underline,strikethrough,sub,sup,separator,separator,blockquote,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,separator,styleprops &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr','1','');
INSERT INTO `modx_site_plugins` VALUES ('6','Search Highlighting','<strong>1.2.0.2</strong> - Show search terms highlighted on page linked from search results. (Requires AjaxSearch snippet)','0','0','0','/*\r\n  ------------------------------------------------------------------------\r\n  Plugin: Search_Highlight v1.2.0.2\r\n  ------------------------------------------------------------------------\r\n  Changes:\r\n  10/02/08 - Strip_tags added to avoid sql injection and XSS. Use of $_REQUEST \r\n  01/03/07 - Added fies/updates from forum from users mikkelwe/identity\r\n  (better highlight replacement, additional div around term/removal message)\r\n  ------------------------------------------------------------------------\r\n  Description: When a user clicks on the link from the AjaxSearch results\r\n    the target page will have the terms highlighted.\r\n  ------------------------------------------------------------------------\r\n  Created By:  Susan Ottwell (sottwell@sottwell.com)\r\n               Kyle Jaebker (kjaebker@muddydogpaws.com)\r\n  ------------------------------------------------------------------------\r\n  Based off the the code by Susan Ottwell (www.sottwell.com)\r\n    http://modxcms.com/forums/index.php/topic,1237.0.html\r\n  ------------------------------------------------------------------------\r\n  CSS:\r\n    The classes used for the highlighting are the same as the AjaxSearch\r\n  ------------------------------------------------------------------------\r\n  Notes:\r\n    To add a link to remove the highlighting and to show the searchterms\r\n    put the following on your page where you would like this to appear:\r\n    \r\n      <!--search_terms-->\r\n    \r\n    Example output for this:\r\n    \r\n      Search Terms: the, template\r\n      Remove Highlighting\r\n      \r\n    Set the following variables to change the text:\r\n    \r\n      $termText - the text before the search terms\r\n      $removeText - the text for the remove link\r\n  ------------------------------------------------------------------------\r\n*/\r\n\r\nif(isset($_REQUEST[\'searched\']) && isset($_REQUEST[\'highlight\'])) {\r\n\r\n  // Set these to customize the text for the highlighting key\r\n  // --------------------------------------------------------\r\n     $termText = \'<div class=\"searchTerms\">Search Terms: \';\r\n     $removeText = \'Remove Highlighting\';\r\n  // --------------------------------------------------------\r\n\r\n  $highlightText = $termText;\r\n\r\n  $searched = strip_tags(urldecode($_REQUEST[\'searched\']));\r\n  $highlight = strip_tags(urldecode($_REQUEST[\'highlight\']));\r\n  $output = $modx->documentOutput; // get the parsed document\r\n\r\n  $body = explode(\"<body>\", $output); // break out the head\r\n\r\n  $searchArray = explode(\' \', $searched); // break apart the search terms\r\n\r\n  $highlightClass = explode(\' \',$highlight); // break out the highlight classes\r\n\r\n  $i = 0; // for individual class names\r\n\r\n  foreach($searchArray as $word) {\r\n    $i++;\r\n    $class = $highlightClass[0].\' \'.$highlightClass[$i];\r\n\r\n    $highlightText .= ($i > 1) ? \', \' : \'\';\r\n    $highlightText .= \'<span class=\"\'.$class.\'\">\'.$word.\'</span>\';\r\n\r\n    $pattern = \'/\' . preg_quote($word) . \'(?=[^>]*<)/i\';\r\n    $replacement = \'<span class=\"\' . $class . \'\">${0}</span>\';\r\n    $body[1] = preg_replace($pattern, $replacement, $body[1]);\r\n  }\r\n\r\n  $output = implode(\"<body>\", $body);\r\n\r\n  $removeUrl = $modx->makeUrl($modx->documentIdentifier);\r\n  $highlightText .= \'<br /><a href=\"\'.$removeUrl.\'\" class=\"ajaxSearch_removeHighlight\">\'.$removeText.\'</a></div>\';\r\n\r\n  $output = str_replace(\'<!--search_terms-->\',$highlightText,$output);\r\n\r\n  $modx->documentOutput = $output;\r\n}','0','','1','');
INSERT INTO `modx_site_plugins` VALUES ('7','Image TV Preview','<strong>1.2.0.2</strong> - Show preview of any images loaded into image Tempalte Variables','0','0','0','// <?php\r\n//    @name       ShowImageTVs\r\n//    @version    0.2, 5 Feb 2008\r\n//\r\n//\r\n//    @author     Brett @ The Man Can!\r\n//                rewritten by Rachael Black\r\n//                now works with MooTools and finds the image tvs itself\r\n//\r\n\r\n/* ---------------------------------------------------------------\r\nInstructions:\r\n     Create a new Plugin and tick Documents > OnDocFormRender event.\r\n     Make sure it is set to execute after any other plugin that\r\n     could effect the template, like InheritParentTemplate. To edit\r\n     the plugin execution order, from the manager go to Resources >\r\n     Manage Resources > Plugins > Edit Plugin Execution Order by Event\r\n     link. That\'s it. It should now show images of all image TVs.\r\n\r\n     To configure image size, copy the following text (no leading spaces):\r\n       &w=Max width;int;300 &h=Max height;int;100\r\n     into the plugin configuration and change values to suit\r\n     This sets style=\"max-width: ; max-height: \" for the image\r\n     If you don\'t configure w or h, the image will be fullsize but\r\n     you can add a css rule rule div.tvimage img {...} to the Manager Theme\r\n------------------------------------------------------------------- */\r\n\r\nglobal $content;\r\n$template = $content[\'template\'];\r\n$e = &$modx->Event;\r\n\r\nif ($e->name == \'OnDocFormRender\' && ($template > 0)) {\r\n	$site = $modx->config[\'site_url\'];\r\n\r\n	if (isset($w) || isset($h)) {\r\n		$w = isset($w) ? $w : 300;\r\n		$h = isset($h) ? $h : 100;\r\n		$style = \"\'max-width:{$w}px; max-height:{$h}px\'\";\r\n	}\r\n	else\r\n		$style = \'\';\r\n\r\n		// get list of all image template vars\r\n	$table = $modx->getFullTableName(\'site_tmplvars\');\r\n	$result = $modx->db->select(\'name\', $table, \"type=\'image\'\");\r\n	$tvs = \'\';\r\n	while ($row = $modx->db->getRow($result))\r\n		$tvs .= \",\'\" . $row[\'name\'] . \"\'\";\r\n	$tvs = substr($tvs, 1);		// remove leading \',\'\r\n\r\n	$output = <<< EOT\r\n<!-- ShowImageTVs Plugin :: Start -->\r\n\r\n<script type=\"text/javascript\" charset=\"utf-8\">\r\n  var imageNames = [$tvs];\r\n  var pageImages = [];\r\n\r\n  function full_url(url)\r\n  {\r\n	 	return (url != \'\' && url.search(/http:\\/\\//i) == -1) ? (\'$site\' + url) : url;\r\n  }\r\n\r\n  function checkImages()\r\n  {\r\n    for (var i = 0; i < pageImages.length; i++) {\r\n    	var elem = pageImages[i];\r\n      var url = elem.value;\r\n      if (url != elem.oldUrl) {\r\n     	  elem.thumbnail.setProperty(\'src\', full_url(url));\r\n     	  elem.thumbnail.setStyle(\'display\', url==\'\' ? \'none\': \'inline\');\r\n        elem.oldUrl = url;\r\n      }\r\n    }\r\n  }\r\n\r\n	window.onDomReady(function() {\r\n    for (var i = 0; i < imageNames.length; i++) {\r\n    	var elem = $(\'tv\' + imageNames[i]);\r\n      if (elem) {\r\n        var url = elem.value;\r\n\r\n        	// create div and img to show thumbnail\r\n        var div = new Element(\'div\').addClass(\'tvimage\');\r\n        var img = new Element(\'img\').setProperty(\'src\', full_url(url)).setStyles($style);\r\n        elem.getParent().adopt(div.adopt(img));\r\n\r\n        elem.thumbnail = img;    // direct access for when need to update\r\n        elem.oldUrl = url;   		 // oldUrl so change HTML only when necessary\r\n        pageImages.push(elem);   // save so don\'t have to search each time\r\n      }\r\n    }\r\n    setInterval(checkImages, 1000);\r\n  })\r\n</script>\r\n\r\n<!-- ShowImageTVs Plugin :: End -->\r\nEOT;\r\n\r\n	$e->output($output);\r\n}\r\n\r\n// ?>\r\n','0','','1','');
INSERT INTO `modx_site_plugins` VALUES ('8','Forgot Manager Login','Resets your manager login when you forget your password.','0','0','0','\r\nif(!class_exists(\'ForgotManagerPassword\')) {\r\nclass ForgotManagerPassword{\r\n\r\n function ForgotManagerPassword(){\r\n \r\n  $this->errors = array();\r\n \r\n  $this->checkLang();\r\n\r\n }\r\n\r\n function getLink() {\r\n \r\n  global $_lang;\r\n \r\n$link = <<<EOD\r\n<a id=\"ForgotManagerPassword-show_form\" href=\"index.php?action=show_form\">{$_lang[\'forgot_your_password\']}</a>\r\nEOD;\r\n\r\n  return $link;\r\n\r\n }\r\n\r\n function getForm() {\r\n\r\n  global $_lang;\r\n\r\n$form = <<<EOD\r\n<label id=\"FMP-email_label\" for=\"FMP_email\">{$_lang[\'account_email\']}:</label>\r\n<input id=\"FMP-email\" type=\"text\" />\r\n<button id=\"FMP-email_button\" type=\"button\" onclick=\"window.location = \'index.php?action=send_email&email=\'+document.getElementById(\'FMP-email\').value;\">{$_lang[\'send\']}</button>\r\nEOD;\r\n\r\n  return $form;\r\n\r\n }\r\n\r\n // Get user info including a hash unique to this user, password, and day\r\n function getUser($user_id=0, $username=\'\', $email=\'\', $hash=\'\') {\r\n\r\n  global $modx, $_lang;\r\n \r\n  $user_id = $modx->db->escape($user_id);\r\n  $username = $modx->db->escape($username);\r\n  $email = $modx->db->escape($email);\r\n  $emaail = $modx->db->escape($hash);\r\n  \r\n  $pre = $modx->db->config[\'table_prefix\'];\r\n  $site_id = $modx->config[\'site_id\'];\r\n  $today = date(\'Yz\'); // Year and day of the year\r\n  $wheres = array();\r\n  $where = \'\';\r\n  $user = array(\'id\'=>0, \'username\'=>\'\', \'email\'=>\'\', \'hash\'=>\'\');\r\n \r\n  if(!empty($user_id)) { $wheres[] = \"id = \'{$user_id}\'\"; }\r\n  if(!empty($username)) { $wheres[] = \"username = \'{$username}\'\"; }\r\n  if(!empty($email)) { $wheres[] = \"email = \'{$email}\'\"; }\r\n  if(!empty($hash)) { $wheres[] = \"MD5(CONCAT(usr.username,usr.password,\'{$site_id}\',\'{$today}\')) = \'{$hash}\'\"; }\r\n \r\n  if($wheres) {\r\n  \r\n   $where = \' WHERE \'.implode(\' AND \',$wheres);\r\n   $sql = \"SELECT usr.id, usr.username, attr.email, MD5(CONCAT(usr.username,usr.password,\'{$site_id}\',\'{$today}\')) AS hash\r\n           FROM `{$pre}manager_users` usr\r\n           INNER JOIN `{$pre}user_attributes` attr ON usr.id = attr.internalKey\r\n           {$where};\";\r\n   \r\n   if($result = $modx->db->query($sql)){\r\n    if($modx->db->getRecordCount($result)==1) {\r\n     $user = $modx->db->getRow($result);\r\n    }\r\n   }\r\n   \r\n  }\r\n  \r\n  if(!$user[\'id\']) { $this->errors[] = $_lang[\'could_not_find_user\']; }\r\n\r\n  return $user;\r\n\r\n }\r\n\r\n // Send an email with a link to login\r\n function sendEmail($to) {\r\n\r\n  global $modx, $_lang;\r\n\r\n  $subject = $_lang[\'password_change_request\'];\r\n  $headers  = \"MIME-Version: 1.0\\n\".\r\n              \"Content-type: text/html; charset=iso-8859-1\\n\".\r\n              \"From: MODx\\n\".\r\n              \"Reply-To: no-reply@{$_SERVER[\'HTTP_HOST\']}\\n\".\r\n              \"X-Mailer: PHP/\".phpversion();\r\n  \r\n  $user = $this->getUser(0, \'\', $to);\r\n\r\n  if($user[\'username\']) {\r\n\r\n$body = <<<EOD\r\n<p>{$_lang[\'forgot_password_email_intro\']} <a href=\"{$modx->config[\'site_url\']}manager/processors/login.processor.php?username={$user[\'username\']}&hash={$user[\'hash\']}\">{$_lang[\'forgot_password_email_link\']}</a></p>\r\n\r\n<p>{$_lang[\'forgot_password_email_instructions\']}</p>\r\n\r\n<p><small>{$_lang[\'forgot_password_email_fine_print\']}</small></p>\r\nEOD;\r\n\r\n   $mail = mail($to, $subject, $body, $headers);\r\n \r\n   if(!$mail) { $this->errors[] = $_lang[\'error_sending_email\']; }\r\n \r\n   return $mail;\r\n   \r\n  }\r\n\r\n }\r\n \r\n function unblockUser($user_id) {\r\n  \r\n  global $modx, $_lang;\r\n  $pre = $modx->db->config[\'table_prefix\'];\r\n  \r\n  $modx->db->update(array(\'blocked\'=>\'\', \'blockeduntil\'=>\'\'), \"`{$pre}user_attributes`\", \"internalKey = \'{$user_id}\'\");\r\n  \r\n  if(!$modx->db->getAffectedRows()) { $this->errors[] = $_lang[\'user_doesnt_exist\']; return; }\r\n  \r\n  return true;\r\n  \r\n }\r\n \r\n function checkLang() {\r\n  \r\n  global $_lang;\r\n  $eng = array();\r\n  \r\n  $eng[\'forgot_your_password\'] = \'Forgot your password?\';\r\n  $eng[\'account_email\'] = \'Account email\';\r\n  $eng[\'send\'] = \'Send\';\r\n  $eng[\'password_change_request\'] = \'Password change request\';\r\n  $eng[\'forgot_password_email_intro\'] = \'A request has been made to change the password on your account.\';\r\n  $eng[\'forgot_password_email_link\'] = \'Click here to complete the process.\';\r\n  $eng[\'forgot_password_email_instructions\'] = \'From there you will be able to change your password from the My Account menu.\';\r\n  $eng[\'forgot_password_email_fine_print\'] = \'* The URL above will expire once you change your password or after today.\';\r\n  $eng[\'error_sending_email\'] = \'Error sending email\';\r\n  $eng[\'could_not_find_user\'] = \'Could not find user\';\r\n  $eng[\'user_doesnt_exist\'] = \'User does not exist\';\r\n  $eng[\'email_sent\'] = \'Email sent\';\r\n  \r\n  foreach($eng as $key=>$value) {\r\n   if(empty($_lang[$key])) { $_lang[$key] = $value; }\r\n  }\r\n  \r\n }\r\n \r\n function getErrorOutput() {\r\n \r\n  $outptut = \'\';\r\n \r\n  if($this->errors) {\r\n   $output = \'<span class=\"error\">\'.implode(\'</span><span class=\"errors\">\', $this->errors).\'</span>\';\r\n  }\r\n \r\n  return $output;\r\n \r\n }\r\n \r\n}\r\n}\r\n\r\nglobal $_lang;\r\n\r\n$output = \'\';\r\n$event_name = $modx->Event->name;\r\n$action = (empty($_GET[\'action\']) ? \'\' : $_GET[\'action\']);\r\n$username = (empty($_GET[\'username\']) ? \'\' : $_GET[\'username\']);\r\n$to = (empty($_GET[\'email\']) ? \'\' : $_GET[\'email\']);\r\n$hash = (empty($_GET[\'hash\']) ? \'\' : $_GET[\'hash\']);\r\n$forgot = new ForgotManagerPassword();\r\n\r\nif($event_name == \'OnManagerLoginFormRender\') {\r\n\r\n switch($action) {\r\n\r\n  case \'show_form\':\r\n   $output = $forgot->getForm();\r\n   break;\r\n  \r\n  case \'send_email\':\r\n   if($forgot->sendEmail($to)) { $output = $_lang[\'email_sent\']; }\r\n   break;\r\n  \r\n  default:\r\n   $output = $forgot->getLink();\r\n\r\n }\r\n \r\n if($forgot->errors) { $output = $forgot->getErrorOutput() . $forgot->getLink(); }\r\n \r\n}\r\n\r\nif($event_name == \'OnBeforeManagerLogin\') {\r\n $user = $forgot->getUser(0, $username, \'\', $hash);\r\n if($user[\'id\'] && !$forgot->errors) {\r\n  $forgot->unblockUser($user[\'id\']);\r\n }\r\n}\r\n\r\nif($event_name == \'OnManagerAuthentication\' && $hash) {\r\n $user = $forgot->getUser(0, \'\', \'\', $hash);\r\n $output = ($user[\'id\'] > 0 && !$forgot->errors);\r\n}\r\n\r\n$modx->Event->output($output);\r\n','0','','0','');
INSERT INTO `modx_site_plugins` VALUES ('9','TinyMCE','<strong>3.2.0.1:</strong> TinyMCE RichText Editor Plugin','0','0','0','/*\r\n * TinyMCE RichText Editor Plugin \r\n * Written By Jeff Whitfield - September 9, 2005\r\n * Modified On - September 24, 2008\r\n *\r\n * Version 3.2.0.1\r\n *\r\n * Events: OnRichTextEditorInit, OnRichTextEditorRegister, OnInterfaceSettingsRender\r\n *\r\n */\r\n\r\n// Set the name of the plugin folder\r\n$pluginfolder = \"tinymce3201\";\r\n\r\ninclude_once $modx->config[\'base_path\'].\'assets/plugins/\'.$pluginfolder.\'/tinymce.lang.php\';\r\ninclude_once $modx->config[\'base_path\'].\'assets/plugins/\'.$pluginfolder.\'/tinymce.functions.php\';\r\n\r\n// Set path and base setting variables\r\nif(!isset($tinyPath)) { \r\n	global $tinyPath, $tinyURL;\r\n	$tinyPath = $modx->config[\'base_path\'].\'assets/plugins/\'.$pluginfolder; \r\n	$tinyURL = $modx->config[\'base_url\'].\'assets/plugins/\'.$pluginfolder; \r\n}\r\n$base_url = $modx->config[\'base_url\'];\r\n$site_url = $modx->config[\'site_url\'];\r\n$displayStyle = ( ($_SESSION[\'browser\']==\'mz\') || ($_SESSION[\'browser\']==\'op\') ) ? \"table-row\" : \"block\" ;\r\n\r\n// Handle event\r\n$e = &$modx->Event; \r\nswitch ($e->name) { \r\n	case \"OnRichTextEditorRegister\": // register only for backend\r\n		$e->output(\"TinyMCE\");\r\n		break;\r\n\r\n	case \"OnRichTextEditorInit\": \r\n		if($editor==\"TinyMCE\") {\r\n			$elementList = implode(\",\", $elements);\r\n			if(isset($forfrontend)||$modx->isFrontend()){\r\n				$frontend = \'true\';\r\n				$frontend_language = isset($modx->config[\'fe_editor_lang\']) ? $modx->config[\'fe_editor_lang\']:\"\";\r\n				$tinymce_language = getTinyMCELang($frontend_language);\r\n				$webuser = (isset($modx->config[\'rb_webuser\']) ? $modx->config[\'rb_webuser\'] : null);\r\n				$html = getTinyMCEScript($elementList,$webtheme,$width,$height,$tinymce_language,$frontend,$base_url, $site_url, $webPlugins, $webButtons1, $webButtons2, $webButtons3, $webButtons4, $disabledButtons, $tinyFormats, $entity_encoding, $entities, $tinyPathOptions, $tinyCleanup, $tinyResizing, $modx->config[\'editor_css_path\'], $modx->config[\'tinymce_css_selectors\'], $modx->config[\'use_browser\'], $webAlign, null, null, $tinyLinkList, $customparams, $tinyURL, $webuser);\r\n			} else {\r\n				$frontend = \'false\';\r\n				$manager_language = $modx->config[\'manager_language\'];\r\n				$tinymce_language = getTinyMCELang($manager_language);\r\n				$html = getTinyMCEScript($elementList, $modx->config[\'tinymce_editor_theme\'], $width=\'100%\', $height=\'400px\', $tinymce_language, $frontend, $modx->config[\'base_url\'], $modx->config[\'site_url\'], $modx->config[\'tinymce_custom_plugins\'], $modx->config[\'tinymce_custom_buttons1\'], $modx->config[\'tinymce_custom_buttons2\'], $modx->config[\'tinymce_custom_buttons3\'], $modx->config[\'tinymce_custom_buttons4\'], $disabledButtons, $tinyFormats, $entity_encoding, $entities, $tinyPathOptions, $tinyCleanup, $tinyResizing, $modx->config[\'editor_css_path\'], $modx->config[\'tinymce_css_selectors\'], $modx->config[\'use_browser\'], $modx->config[\'manager_direction\'], $advimage_styles, $advlink_styles, $tinyLinkList, $customparams, $tinyURL, null);\r\n			}\r\n			$e->output($html);\r\n		}		\r\n		break;\r\n\r\n	case \"OnInterfaceSettingsRender\":\r\n		global $usersettings,$settings;\r\n		$action = $modx->manager->action;\r\n		switch ($action) {\r\n    		case 11:\r\n        		$tinysettings = \"\";\r\n        		break;\r\n    		case 12:\r\n        		$tinysettings = $usersettings;\r\n        		break;\r\n    		default:\r\n        		$tinysettings = $settings;\r\n        		break;\r\n    	}\r\n		$tinymce_editor_theme = $tinysettings[\'tinymce_editor_theme\'];\r\n		$tinymce_css_selectors = $tinysettings[\'tinymce_css_selectors\'];\r\n		$tinymce_custom_plugins = $tinysettings[\'tinymce_custom_plugins\'];\r\n		$tinymce_custom_buttons1 = $tinysettings[\'tinymce_custom_buttons1\'];\r\n		$tinymce_custom_buttons2 = $tinysettings[\'tinymce_custom_buttons2\'];\r\n		$tinymce_custom_buttons3 = $tinysettings[\'tinymce_custom_buttons3\'];\r\n		$tinymce_custom_buttons4 = $tinysettings[\'tinymce_custom_buttons4\'];\r\n		$manager_language = $modx->config[\'manager_language\'];\r\n		$html = getTinyMCESettings($_lang, $tinyPath, $modx->config[\'manager_language\'], $modx->config[\'use_editor\'], $tinymce_editor_theme, $tinymce_css_selectors, $tinymce_custom_plugins, $tinymce_custom_buttons1, $tinymce_custom_buttons2, $tinymce_custom_buttons3, $tinymce_custom_buttons4, $displayStyle, $action);\r\n		$e->output($html);\r\n		break;\r\n\r\n   default :    \r\n      return; // stop here - this is very important. \r\n      break; \r\n}\r\n','0','&customparams=Custom Parameters;textarea; &tinyFormats=Block Formats;text;p,h1,h2,h3,h4,h5,h6,div,blockquote,code,pre,address &entity_encoding=Entity Encoding;list;named,numeric,raw;named &entities=Entities;text; &tinyPathOptions=Path Options;list;rootrelative,docrelative,fullpathurl;docrelative &tinyCleanup=Cleanup;list;enabled,disabled;enabled &tinyResizing=Advanced Resizing;list;true,false;false &advimage_styles=Advanced Image Styles;text; &advlink_styles=Advanced Link Styles;text; &disabledButtons=Disabled Buttons;text; &tinyLinkList=Link List;list;enabled,disabled;enabled &webtheme=Web Theme;list;simple,advanced,editor,custom;simple &webPlugins=Web Plugins;text;style,advimage,advlink,searchreplace,print,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras,visualchars,media &webButtons1=Web Buttons 1;text;undo,redo,selectall,separator,pastetext,pasteword,separator,search,replace,separator,nonbreaking,hr,charmap,separator,image,link,unlink,anchor,media,separator,cleanup,removeformat,separator,fullscreen,print,code,help &webButtons2=Web Buttons 2;text;bold,italic,underline,strikethrough,sub,sup,separator,separator,blockquote,bullist,numlist,outdent,indent,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,styleselect,formatselect,separator,styleprops &webButtons3=Web Buttons 3;text; &webButtons4=Web Buttons 4;text; &webAlign=Web Toolbar Alignment;list;ltr,rtl;ltr','0','');
INSERT INTO `modx_site_plugins` VALUES ('10','Search Highlighting','<strong>1.3</strong> - Show search terms highlighted on page linked from search results. (Requires AjaxSearch snippet)','0','0','0','/*\r\n  ------------------------------------------------------------------------\r\n  Plugin: Search_Highlight v1.3\r\n  ------------------------------------------------------------------------\r\n  Changes:\r\n  18/07/08 - advSearch parameter and pcre modifier added\r\n  10/02/08 - Strip_tags added to avoid sql injection and XSS. Use of $_REQUEST \r\n  01/03/07 - Added fies/updates from forum from users mikkelwe/identity\r\n  (better highlight replacement, additional div around term/removal message)\r\n  ------------------------------------------------------------------------\r\n  Description: When a user clicks on the link from the AjaxSearch results\r\n    the target page will have the terms highlighted.\r\n  ------------------------------------------------------------------------\r\n  Created By:  Susan Ottwell (sottwell@sottwell.com)\r\n               Kyle Jaebker (kjaebker@muddydogpaws.com)\r\n  ------------------------------------------------------------------------\r\n  Based off the the code by Susan Ottwell (www.sottwell.com)\r\n    http://modxcms.com/forums/index.php/topic,1237.0.html\r\n  ------------------------------------------------------------------------\r\n  CSS:\r\n    The classes used for the highlighting are the same as the AjaxSearch\r\n  ------------------------------------------------------------------------\r\n  Notes:\r\n    To add a link to remove the highlighting and to show the searchterms\r\n    put the following on your page where you would like this to appear:\r\n    \r\n      <!--search_terms-->\r\n    \r\n    Example output for this:\r\n    \r\n      Search Terms: the, template\r\n      Remove Highlighting\r\n      \r\n    Set the following variables to change the text:\r\n    \r\n      $termText - the text before the search terms\r\n      $removeText - the text for the remove link\r\n  ------------------------------------------------------------------------\r\n*/\r\n\r\nif (isset($_REQUEST[\'searched\']) && isset($_REQUEST[\'highlight\'])) {\r\n\r\n  // Set these to customize the text for the highlighting key\r\n  // --------------------------------------------------------\r\n     $termText = \'<div class=\"searchTerms\">Search Terms: \';\r\n     $removeText = \'Remove Highlighting\';\r\n  // --------------------------------------------------------\r\n\r\n  $highlightText = $termText;\r\n\r\n  $searched = strip_tags(urldecode($_REQUEST[\'searched\']));\r\n  $highlight = strip_tags(urldecode($_REQUEST[\'highlight\']));\r\n\r\n  if (isset($_REQUEST[\'advsearch\'])) $advsearch = strip_tags(urldecode($_REQUEST[\'advsearch\']));\r\n  else $advsearch = \'oneword\'; \r\n\r\n  if ($advsearch != \'nowords\') {\r\n  \r\n      $output = $modx->documentOutput; // get the parsed document\r\n   \r\n      $body = explode(\"<body>\", $output); // break out the head\r\n    \r\n      $searchArray = array();      \r\n      if ($advsearch == \'exactphrase\') $searchArray[0] = $searched;\r\n      else $searchArray = explode(\' \', $searched);\r\n    \r\n      $highlightClass = explode(\' \',$highlight); // break out the highlight classes\r\n    \r\n      $i = 0; // for individual class names\r\n      $pcreModifier = ($database_connection_charset == \'utf8\') ? \'iu\' : \'i\';\r\n       \r\n      foreach($searchArray as $word) {\r\n        $i++;\r\n        $class = $highlightClass[0].\' \'.$highlightClass[$i];\r\n    \r\n        $highlightText .= ($i > 1) ? \', \' : \'\';\r\n        $highlightText .= \'<span class=\"\'.$class.\'\">\'.$word.\'</span>\';\r\n    \r\n        $pattern = \'/\' . preg_quote($word, \'/\') . \'(?=[^>]*<)/\' . $pcreModifier;\r\n        $replacement = \'<span class=\"\' . $class . \'\">${0}</span>\';\r\n        $body[1] = preg_replace($pattern, $replacement, $body[1]);\r\n      }\r\n    \r\n      $output = implode(\"<body>\", $body);\r\n    \r\n      $removeUrl = $modx->makeUrl($modx->documentIdentifier);\r\n      $highlightText .= \'<br /><a href=\"\'.$removeUrl.\'\" class=\"ajaxSearch_removeHighlight\">\'.$removeText.\'</a></div>\';\r\n    \r\n      $output = str_replace(\'<!--search_terms-->\',$highlightText,$output);\r\n      $modx->documentOutput = $output;\r\n  }\r\n}','0','','0','');
INSERT INTO `modx_site_plugins` VALUES ('11','Image TV Preview','<strong>1.2.0.4</strong> - Show preview of any images loaded into image Template Variables','0','0','0','// <?php\r\n//    @name       ShowImageTVs\r\n//    @version    0.2.2, 24 Nov 2008\r\n//\r\n//\r\n//    @author     Brett @ The Man Can!\r\n//                rewritten by Rachael Black, update by pixelchutes\r\n//                now works with MooTools and finds the image tvs itself\r\n//\r\n\r\n/* ---------------------------------------------------------------\r\nInstructions:\r\n     Create a new Plugin and tick Documents > OnDocFormRender event.\r\n     Make sure it is set to execute after any other plugin that\r\n     could effect the template, like InheritParentTemplate. To edit\r\n     the plugin execution order, from the manager go to Resources >\r\n     Manage Resources > Plugins > Edit Plugin Execution Order by Event\r\n     link. That\'s it. It should now show images of all image TVs.\r\n\r\n     To configure image size, copy the following text (no leading spaces):\r\n       &w=Max width;int;300 &h=Max height;int;100\r\n     into the plugin configuration and change values to suit\r\n     This sets style=\"max-width: ; max-height: \" for the image\r\n     If you don\'t configure w or h, the image will be fullsize but\r\n     you can add a css rule rule div.tvimage img {...} to the Manager Theme\r\n------------------------------------------------------------------- */\r\n\r\nglobal $content;\r\n$template = $content[\'template\'];\r\n$e = &$modx->Event;\r\n\r\nif ($e->name == \'OnDocFormRender\' && ($template > 0)) {\r\n	$site = $modx->config[\'site_url\'];\r\n\r\n	if (isset($w) || isset($h)) {\r\n		$w = isset($w) ? $w : 300;\r\n		$h = isset($h) ? $h : 100;\r\n		$style = \"\'max-width:{$w}px; max-height:{$h}px\'\";\r\n	}\r\n	else\r\n		$style = \'\';\r\n\r\n		// get list of all image template vars\r\n	$table = $modx->getFullTableName(\'site_tmplvars\');\r\n	$result = $modx->db->select(\'name\', $table, \"type=\'image\'\");\r\n	$tvs = \'\';\r\n	while ($row = $modx->db->getRow($result))\r\n		$tvs .= \",\'\" . $row[\'name\'] . \"\'\";\r\n	$tvs = substr($tvs, 1);		// remove leading \',\'\r\n\r\n	$output = <<< EOT\r\n<!-- ShowImageTVs Plugin :: Start -->\r\n\r\n<script type=\"text/javascript\" charset=\"utf-8\">\r\n  var imageNames = [$tvs];\r\n  var pageImages = [];\r\n\r\n  function full_url(url)\r\n  {\r\n	new_url = (url != \'\' && url.search(/http:\\/\\//i) == -1) ? (\'$site\' + url) : url;\r\n	return ( ( new_url.search(\'@INHERIT\') == -1 ) ? new_url : new_url.replace( new RegExp(/@INHERIT/ig), \'\' ) ); // Update by pixelchutes\r\n  }\r\n\r\n  function checkImages()\r\n  {\r\n    for (var i = 0; i < pageImages.length; i++) {\r\n    	var elem = pageImages[i];\r\n      var url = elem.value;\r\n      if (url != elem.oldUrl) {\r\n     	  elem.thumbnail.setProperty(\'src\', full_url(url));\r\n     	  elem.thumbnail.setStyle(\'display\', url==\'\' ? \'none\': \'inline\');\r\n        elem.oldUrl = url;\r\n      }\r\n    }\r\n  }\r\n\r\n	window.onDomReady(function() {\r\n    for (var i = 0; i < imageNames.length; i++) {\r\n    	var elem = $(\'tv\' + imageNames[i]);\r\n\r\n  	// Account for TVs with \"underscores\" in their name (MODx escapes them to %5F)\r\n        // Update by pixelchutes\r\n        if (!elem) {\r\n            newname = imageNames[i].replace(new RegExp(/_/ig),\'%5F\');\r\n            var elem = $(\'tv\' + newname);\r\n        }\r\n\r\n      if (elem) {\r\n        var url = elem.value;\r\n\r\n        	// create div and img to show thumbnail\r\n        var div = new Element(\'div\').addClass(\'tvimage\');\r\n        var img = new Element(\'img\').setProperty(\'src\', full_url(url)).setStyles($style);\r\n        elem.getParent().adopt(div.adopt(img));\r\n\r\n        elem.thumbnail = img;    // direct access for when need to update\r\n        elem.oldUrl = url;   		 // oldUrl so change HTML only when necessary\r\n        pageImages.push(elem);   // save so don\'t have to search each time\r\n      }\r\n    }\r\n    setInterval(checkImages, 1000);\r\n  })\r\n</script>\r\n\r\n<!-- ShowImageTVs Plugin :: End -->\r\nEOT;\r\n\r\n	$e->output($output);\r\n}\r\n\r\n// ?>\r\n','0','','1',' ');
INSERT INTO `modx_site_plugins` VALUES ('12','ManagerManager','','0','0','0','// ManagerManager plugin v0.3.2\r\n// Description: <strong>0.3.2</strong> Manage the ModX Manager\r\n\r\n\r\n// See readme.htm for licensing and installation instructions\r\n// The following System Events should be checked:\r\n// [X] OnDocFormPrerender\r\n// [X] OnDocFormRender\r\n// [X] OnPluginFormRender\r\n// [X] OnTVFormRender\r\n\r\n// You should put your ManagerManager rules in a chunk OR in an external file. \r\n// To use an external file, put your rules in /assets/plugins/managermanager/mm_rules.inc.php (you can rename default.mm_rules.inc.php as use it as an example)\r\n// If you want to put your rules in a chunk (so you can edit them through the Manager)\r\n// enter a chunk name here (suggested default is \'mm_rules\').\r\n// A valid chunk name / content will take preference over a file.\r\n// The file contains some example rules (which you can copy to the chunk - although the chunk should not have php tags at the beginning  or end\r\n$config_chunk = \'\';\r\n\r\n// The URL to the Jquery library. Leave blank and it will use the one available from jquery.com\r\n// or if you want more control, specify your own local copy (e.g. http://www.yoursite.com/assets/js/jquery.js)\r\n$js_url = \'\';	\r\n\r\n// Should we remove deprecated Template variable types from the TV creation list?\r\n//$remove_deprecated_tv_types = true;\r\n\r\n\r\n// You don\'t need to change anything else from here onwards\r\n//-------------------------------------------------------\r\n// Run the main code\r\n$asset_path = $modx->config[\'base_path\'] . \'assets/plugins/managermanager/mm.inc.php\';\r\ninclude($asset_path);\r\n','0','','0',' ');

# --------------------------------------------------------

#
# Table structure for table `modx_site_snippets`
#

CREATE TABLE `modx_site_snippets` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default 'Snippet',
  `editor_type` int(11) NOT NULL default '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL default '0' COMMENT 'category id',
  `cache_type` tinyint(1) NOT NULL default '0' COMMENT 'Cache option',
  `snippet` mediumtext,
  `locked` tinyint(4) NOT NULL default '0',
  `properties` text,
  `moduleguid` varchar(32) NOT NULL default '' COMMENT 'GUID of module from which to import shared parameters',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=latin1 COMMENT='Contains the site snippets.';

#
# Dumping data for table `modx_site_snippets`
#

INSERT INTO `modx_site_snippets` VALUES ('1','AjaxSearch','<strong>1.8.1</strong> Ajax enabled search form with results highlighting.','0','0','0','/* -----------------------------------------------------------------------------\r\n:: Snippet: AjaxSearch\r\n--------------------------------------------------------------------------------\r\n  Short Description: \r\n        Ajax-driven & Flexible Search form\r\n\r\n  Version:\r\n        1.8.1\r\n\r\n  Date: 02/10/2008\r\n  \r\n  Created by:\r\n	    Coroico (coroico@wangba.fr)\r\n	    Jason Coward (opengeek - jason@opengeek.com)\r\n	    Kyle Jaebker (kylej - kjaebker@muddydogpaws.com)\r\n	    Ryan Thrash  (rthrash - ryan@vertexworks.com)\r\n	    \r\n	    Live Search by Thomas (Shadock)\r\n	    Fixes & Additions by identity/Perrine/mikkelwe\r\n	    Document selection from Ditto by Mark Kaplan\r\n	    \r\n  Copyright & Licencing:\r\n  ----------------------\r\n  GNU General Public License (GPL) (http://www.gnu.org/copyleft/gpl.html)\r\n\r\n  Originally based on the FlexSearchForm snippet created by jaredc (jaredc@honeydewdesign.com)\r\n\r\n--------------------------------------------------------------------------------\r\n:: Description\r\n--------------------------------------------------------------------------------\r\n\r\n    The AjaxSearch snippet is an enhanced version of the original FlexSearchForm\r\n    snippet for MODx. This snippet adds AJAX functionality on top of the robust \r\n    content searching.\r\n    \r\n    - search in title, description, content and TVs of documents\r\n    - search in a subset of documents\r\n    - highlighting of searchword in the results returned\r\n\r\n    It could works in two modes:\r\n\r\n    ajaxSearch mode : \r\n    - Search results displayed in current page through AJAX request\r\n    - Multiple search options including live search and non-AJAX option\r\n    - Available link to view all results in a new page (FSF) when only a subset is retuned\r\n    - Customize the number of results returned\r\n    - Uses the MooTools js library for AJAX and visual effects\r\n\r\n    non-ajaxSearch mode (FSF) :\r\n    - Search results displayed in a new page\r\n    - customize the paginating of results\r\n    - works without JS enabled as FlexSearchForm\r\n    - designed to load only the required FSF code\r\n\r\n\r\nMORE : See the ajaxSearch.readme.txt file for more informations\r\n\r\n----------------------------------------------------------------------------- */\r\nglobal $modx;\r\n\r\n// ajaxSearch version being executed\r\ndefine(\'AS_VERSION\', \'1.8.1\');\r\n\r\n// Path where ajaxSearch is installed\r\ndefine(\'AS_SPATH\', \'assets/snippets/ajaxSearch/\');\r\n\r\n//include snippet file\r\ndefine (\'AS_PATH\', $modx->config[\'base_path\'].AS_SPATH);\r\n\r\n//------------------------------------------------------------------------------\r\n// Configure - general AjaxSearch snippet setup options\r\n//------------------------------------------------------------------------------\r\n$cfg = array();\r\n\r\n// &config [config_name | \"default\"] (optional)\r\n// Load a custom configuration\r\n// config_name - Other configs installed in the configs folder or in any folder within the MODx base path via @FILE\r\n// Configuration files are named in the form: <config_name>.config.php\r\n$config = (isset($config)) ? $config : \"default\";\r\n$cfg[\'config\'] = $config;\r\n$as_config = (substr($config, 0, 5) != \"@FILE\") ? AS_PATH.\"configs/$config.config.php\" : $modx->config[\'base_path\'].trim(substr($config, 5));\r\nif (file_exists($as_config)) include $as_config;\r\nelse return  \"<h3>\" .$as_config . \" not found !<br />Check your config parameter or your config file name!</h3>\";\r\n\r\n// ajax Search version - Don\'t change!\r\n$cfg[\'version\'] = AS_VERSION;\r\n\r\n// &debug = [ 0 | 1 | 2 | 3 | -1 | -2 | -3 ]\r\n// by default: 0 - no logs\r\n// 1,2,3 : File mode - Output logged into a file named ajaxSearch_log.txt in the ajaxSearch folder\r\n// -1,-2,-3 : FireBug mode. The trace is logged into the Firebug console of Mozilla.\r\n$cfg[\'debug\'] = isset($debug)? $debug : (isset($__debug)? $__debug : 0);\r\n\r\n// &language [ language_name | manager_language ] (optional)\r\n// with manager_language = $modx->config[\'manager_language\'] by default \r\n$cfg[\'language\'] = isset($language) ? $language : (isset($__language) ? $__language : $modx->config[\'manager_language\']);\r\n\r\n// &ajaxSearch [1 | 0] (as passed in snippet variable ONLY)\r\n// Use this to display the search results using ajax You must include the Mootools library in your template\r\n$cfg[\'ajaxSearch\'] = isset($ajaxSearch) ? $ajaxSearch : (isset($__ajaxSearch) ? $__ajaxSearch : 1);\r\n\r\n// &advSearch [ \'exactphrase\' | \'allwords\' | \'nowords\' | \'oneword\' ]\r\n// Advanced search    \r\n// - exactphrase : provides the documents which contain the exact phrase \r\n// - allwords : provides the documents which contain all the words\r\n// - nowords : provides the documents which do not contain the words\r\n// - oneword : provides the document which contain at least one word [default]\r\n$cfg[\'advSearch\'] = isset($advSearch) ? $advSearch : (isset($__advSearch) ? $__advSearch : \'oneword\');\r\n\r\n// &whereSearch     \r\n// Define where should occur the search\r\n// a separated list of keywords describing the tables where to search\r\n// keywords allowed : \r\n// \"content\" for site_content, \"tv\" for site_tmplvar_contentvalues, \"jot\" for jot_content, \"maxigallery\" for maxigallery\r\n// you could add your own keywords. But the keyword should be a user function which describes the tables to use\r\n// by default all the text fields are searchable but you could specify the fields like this:\r\n// whereSearch=`content:pagetitle,introtext,content|tv:tv_value|maxigallery:gal_title`\r\n$cfg[\'whereSearch\'] = isset($whereSearch) ? $whereSearch : (isset($__whereSearch) ? $__whereSearch : \'content|tv\');\r\n\r\n// &subSearch  [ int , int ]\r\n// Define the maximum number of choice and the default choice selected\r\n// by default 5 choices and default choice 1 selected\r\n$cfg[\'subSearch\'] = isset($subSearch) ? $subSearch : (isset($__subSearch) ? $__subSearch : \'5,1\');\r\n\r\n// &withTvs - Define which Tvs are used for the search in Tvs\r\n// a comma separated list of TV names\r\n// by default all TVs are used (empty list)\r\n$cfg[\'withTvs\'] = isset($withTvs) ? $withTvs : (isset($__withTvs) ? $__withTvs : \'\');\r\n\r\n// &order - Define the sort order of results\r\n// Comma separated list of fields defined as searchable in the table definition\r\n// by default : \'pub_date,pagetitle\'\r\n// to suppress the sorting, use &order=``\r\n$cfg[\'order\'] = isset($order) ? $order : (isset($__order) ? $__order : \'publishedon,pagetitle\');\r\n\r\n// &rank - Define the rank of search results. Results are sorted by rank value\r\n// Comma separated list of fields with optionally user defined weight\r\n// by default : \'pagetitle:100,extract\'\r\n// to suppress the rank sorting, use &rank=``; \r\n// &rank sort occurs after the &order sort\r\n$cfg[\'rank\'] = isset($rank) ? $rank : (isset($__rank) ? $__rank : \'pagetitle:100,extract\');\r\n\r\n// &minChars [ int ]\r\n// Minimum number of characters to require for a word to be valid for searching.\r\n// MySQL will typically NOT search for words with less than 4 characters (relevance mode). \r\n// If you have $advSearch = \'allwords\', \'oneword\' or \'nowords\' and a three or \r\n// fewer letter words appears in the search string, the results will always be 0. \r\n// Setting this drops those words from the search in THAT CIRCUMSTANCE ONLY \r\n// (relevance mode, advsearch = \'allwords\', \'oneword\' or \'nowords\')\r\n$cfg[\'minChars\'] = isset($minChars) ? intval($minChars) : (isset($__minChars) ? intval($__minChars) : 3);\r\n\r\n// &AS_showForm [0 | 1]\r\n// If you would like to turn off the search form when showing results you can set this to false.(1=true, 0=false)\r\n$cfg[\'AS_showForm\'] = isset($AS_showForm ) ? $AS_showForm : (isset($__AS_showForm ) ? $__AS_showForm : 1);\r\n\r\n// &resultsPage [int]\r\n// The default behavior is to show the results on the current page, but you may define the results page any way you like. The priority is:\r\n// 1- snippet variable - set in page template like this: [[AjaxSearch? AS_landing=int]]\r\n//    where int is the page id number of the page you want your results on\r\n// 2- querystring variable AS_form\r\n// 3- variable set here\r\n// 4- use current page\r\n// This is VERY handy when you want to put the search form in a discrete and/or small place on your page- like a side column, but don\'t want all your results to show up there!\r\n// Set to results page or leave 0 as default\r\n$cfg[\'resultsPage\'] = 0;\r\n\r\n// &grabMax [ int ]\r\n// Set to the max number of records you would like on each page. Set to 0 if unlimited.\r\n$cfg[\'grabMax\'] = isset($grabMax)? intval($grabMax) : (isset($__grabMax)? intval($__grabMax) : 10);\r\n\r\n// &extract [ n:searchable fields list | 1:content,description,introtext,tv_content]\r\n// show the search terms highlighted in a little extract\r\n// n : maximum number of extracts displayed\r\n// ordered searchable fields list : separated list of fields define as searchable in the table definition\r\n// by default : 1:content,description,introtext,tv_content - One extract from content then description,introtext,tv_content \r\n$cfg[\'extract\'] = isset($extract) ? $extract : (isset($__extract) ? $__extract : \'1:content,description,introtext,tv_content\');\r\n\r\n// &extractLength [int]\r\n// Length of extract around the search words found - between 50 and 800 characters\r\n$cfg[\'extractLength\'] = isset($extractLength) ? intval($extractLength) : (isset($__extractLength) ? intval($__extractLength) : 200);\r\n\r\n// &extractEllips [ string ]\r\n// Ellipside to mark the star and the end of  an extract when the sentence is cutting\r\n// by default : \'...\'\r\n$cfg[\'extractEllips\'] = isset($extractEllips) ? $extractEllips : (isset($__extractEllips) ? $__extractEllips : \'...\');\r\n\r\n// &extractSeparator [ string ]\r\n// Any html tag to mark the separation between extracts\r\n// by default : \'<br />\' but you could also choose for instance \'<hr />\'\r\n$cfg[\'extractSeparator\'] = isset($extractSeparator) ? $extractSeparator : (isset($__extractSeparator) ? $__extractSeparator : \'<br />\');\r\n\r\n// &formatDate [ string ]\r\n// The format of outputted dates. See http://www.php.net/manual/en/function.date.php\r\n// by default : \"d/m/y : H:i:s\" e.g: 21/01/08 : 23:09:22\r\n$cfg[\'formatDate\'] = isset($formatDate) ? $formatDate : (isset($__formatDate) ? $__formatDate : \"d/m/y : H:i:s\");\r\n\r\n// &highlightResult [1 | 0]\r\n// create links so that search terms will be highlighted when linked page clicked\r\n$cfg[\'highlightResult\'] = isset($highlightResult) ? $highlightResult : (isset($__highlightResult) ? $__highlightResult : 1);\r\n\r\n// &pageLinkSeparator [ string ]\r\n// What you want, if anything, between your page link numbers\r\n$cfg[\'pageLinkSeparator\'] = isset($pageLinkSeparator) ? $pageLinkSeparator : (isset($__pageLinkSeparator) ? $__pageLinkSeparator : \" | \");\r\n\r\n// &AS_landing  [int] set the page to show the results page (non Ajax search)\r\n$cfg[\'AS_landing\'] = isset($AS_landing) ? $AS_landing : (isset($__AS_landing) ? $__AS_landing : false);\r\n\r\n// &AS_showResults  [1 | 0]  establish whether to show the results or not\r\n$cfg[\'AS_showResults\'] = isset($AS_showResults) ? $AS_showResults : (isset($__AS_showResults) ? $__AS_showResults : true);\r\n\r\n// type of IDs - (INTERNAL USE)\r\n$cfg[\'idType\'] = isset($documents) ? \"documents\" : \"parents\";\r\n\r\n// &parents [ comma separated list of IDs | \'\' ]  \r\n// IDs of documents to retrieve their children to &depth depth  where to do the search - - empty list by default\r\n$cfg[\'parents\'] = isset($parents) ? $parents : (isset($__parents) ? $__parents : \'\');\r\n\r\n// &documents [ comma separated list of IDs | \'\' ]  \r\n// IDs of documents where to do the search - empty list by default\r\n$cfg[\'documents\'] = isset($documents) ? $documents : (isset($__documents) ? $__documents : \'\');\r\n\r\n// &depth [ int | 10 ] Number of levels deep to retrieve documents\r\n$cfg[\'depth\'] = isset($depth) ? intval($depth): (isset($__depth) ? intval($__depth) : 10);\r\n\r\n// &hideMenu [0 | 1| 2]  Search in hidden documents from menu.\r\n// 0 - search only in documents visible from menu\r\n// 1 - search only in documents hidden from menu\r\n// 2 - search in hidden or visible documents from menu [default]\r\n$cfg[\'hideMenu\'] = isset($hideMenu) ? $hideMenu : (isset($__hideMenu) ? $__hideMenu : 2);\r\n\r\n// &hideLink [0 | 1 ]   Search in content of type reference (link) \r\n// 0 - search only in content of type document\r\n// 1 - search in content of type document AND reference (default)\r\n$cfg[\'hideLink\'] = isset($hideLink) ? $hideLink : (isset($__hideLink) ? $__hideLink : 1);\r\n\r\n// &filter - Basic filtering : remove unwanted documents that meets the criteria of the filter\r\n// See Ditto 2 Basic filtering for more information : http://ditto.modxcms.com/tutorials/basic_filtering.html\r\n$cfg[\'filter\'] = isset($filter) ? $filter : (isset($__filter) ? $__filter : \'\');\r\n\r\n// &tplLayout - Chunk to style the ajaxSearch input form and layout\r\n$cfg[\'tplLayout\'] = isset($tplLayout) ? $tplLayout : (isset($__tplLayout) ? $__tplLayout : \"@FILE:\".AS_SPATH.\'templates/layout.tpl.html\');\r\n\r\n// &tplResults - Chunk to style the non-ajax output results outer\r\n$cfg[\'tplResults\'] = isset($tplResults) ? $tplResults : (isset($__tplResults) ? $__tplResults : \"@FILE:\".AS_SPATH.\'templates/results.tpl.html\');\r\n\r\n// &tplResult - Chunk to style each output result\r\n$cfg[\'tplResult\'] = isset($tplResult) ? $tplResult : (isset($__tplResult) ? $__tplResult : \"@FILE:\".AS_SPATH.\'templates/result.tpl.html\');\r\n\r\n// &tplPaging - Chunk to style the paging links\r\n$cfg[\'tplPaging\'] = isset($tplPaging) ? $tplPaging : (isset($__tplPaging) ? $__tplPaging : \"@FILE:\".AS_SPATH.\'templates/paging.tpl.html\');\r\n\r\n// &stripInput - stripInput user function name\r\n$cfg[\'stripInput\'] = isset($stripInput) ? $stripInput : (isset($__stripInput) ? $__stripInput : \'defaultStripInput\');\r\n\r\n// &stripOutput - stripOutput user function name\r\n$cfg[\'stripOutput\'] = isset($stripOutput) ? $stripOutput : (isset($__stripOutput) ? $__stripOutput : \'defaultStripOutput\');\r\n\r\n// &searchWordList - searchWordList user function name\r\n// [user_function_name,params] where params is an optional array of parameters\r\n$cfg[\'searchWordList\'] = isset($searchWordList) ? $searchWordList : (isset($__searchWordList) ? $__searchWordList : \'\');\r\n\r\n// &breadcrumbs\r\n// 0 : disallow the breadcrumbs link\r\n// Name of the breadcrumbs function : allow the breadcrumbs link\r\n// The function name could be followed by some parameter initialization\r\n// e.g: &breadcrumbs=`Breadcrumbs,showHomeCrumb:0,showCrumbsAtHome:1`\r\n$cfg[\'breadcrumbs\'] = isset($breadcrumbs) ? $breadcrumbs : (isset($__breadcrumbs) ? $__breadcrumbs : 0);\r\n\r\n// &tvPhx - Set placeHolders for TV (template variables)\r\n// 0 : disallow the feature (default)\r\n// \'tv:displayTV\' : set up a placeholder named [+as.tvName.+] for each TV (named tvName) linked to the documents found\r\n// displayTV is a provided ajaxSearch function which render the TV output\r\n// tvPhx could also be used with custom tables (see examples on www.modx.wangba.fr)\r\n$cfg[\'tvPhx\'] = isset($tvPhx) ? $tvPhx : (isset($__tvPhx) ? $__tvPhx : 0);\r\n\r\n// &jsClearDefault - Clearing default text\r\n// Set this to 1 if you would like to include the clear default js function\r\n// add the class \"cleardefault\" to your input text form and set this parameter\r\n$cfg[\'clearDefault\'] = isset($clearDefault) ? $clearDefault : (isset($__clearDefault) ? $__clearDefault : 0);\r\n\r\n// &jsSearchInput - Location of the js library\r\n// mandatory to protect the site against JS cross scripting attacks\r\n$cfg[\'jsClearDefault\'] = AS_SPATH . \'js/clearDefault.js\';\r\n\r\n\r\n//------------------------------------------------------------------------------\r\n// Configure - Ajax mode snippet setup options\r\n//------------------------------------------------------------------------------\r\n\r\nif ($cfg[\'ajaxSearch\']){  // ajax mode\r\n    // $ajaxSearchType [1 | 0] (as passed in snippet variable ONLY)\r\n    // Use this to display the search results using ajax\r\n    // Set this to 1 if you would like to use the live search (i.e. results as you type)\r\n    $cfg[\'ajaxSearchType\'] = isset($ajaxSearchType) ? $ajaxSearchType : (isset($__ajaxSearchType) ? $__ajaxSearchType : 0);\r\n    \r\n    // &ajaxMax [int] - The maximum number of results to show for the ajaxsearch\r\n    $cfg[\'ajaxMax\'] = isset($ajaxMax) ? $ajaxMax : (isset($__ajaxMax) ? $__ajaxMax : 6);\r\n    \r\n    // &showMoreResults [1 | 0]\r\n    // Set this to 1 if you would like a link to show all of the search results\r\n    $cfg[\'showMoreResults\'] = isset($showMoreResults) ? $showMoreResults : (isset($__showMoreResults) ? $__showMoreResults : 0);\r\n    \r\n    // &moreResultsPage [int]\r\n    // The document id of the page you want the more results link to point to\r\n    $cfg[\'moreResultsPage\'] = isset($moreResultsPage ) ? $moreResultsPage : (isset($__moreResultsPage ) ? $__moreResultsPage : 0);\r\n    \r\n    // &opacity - set the opacity of the div ajaxSearch_output \r\n    $cfg[\'opacity\'] = isset($opacity) ? $opacity : (isset($__opacity) ? $__opacity : 1.);\r\n    \r\n    // &tplAjaxResults - Chunk to style the ajax output results outer\r\n    $cfg[\'tplAjaxResults\'] = isset($tplAjaxResults) ? $tplAjaxResults : (isset($__tplAjaxResults) ? $__tplAjaxResults : \'\');\r\n    \r\n    // &tplAjaxResult - Chunk to style each output result\r\n    $cfg[\'tplAjaxResult\'] = isset($tplAjaxResult) ? $tplAjaxResult : (isset($__tplAjaxResult) ? $__tplAjaxResult : \'\');\r\n\r\n    // &jScript [\'jquery\'|\'mootools\']\r\n    // Set this to jquery if you would like use the jquery library\r\n    // Default: mootools\r\n    $cfg[\'jscript\'] = isset($jscript ) ? $jscript : (isset($__jscript ) ? $__jscript : \'mootools\');\r\n    \r\n    // &addJscript [1 | 0]\r\n    // Set this to 1 if you would like to include or not the mootool/jquery library\r\n    // in the header of your pages automatically.\r\n    $cfg[\'addJscript\'] = isset($addJscript ) ? $addJscript : (isset($__addJscript ) ? $__addJscript : 1);\r\n    \r\n    // &jsMootools - Location of the mootools javascript library\r\n    $cfg[\'jsMooTools\'] = \'manager/media/script/mootools/mootools.js\';\r\n        \r\n    // &jsQuery - Location of the jquery javascript library\r\n    $cfg[\'jsJquery\'] = AS_SPATH . \'js/jQuery/jquery.js\';\r\n}\r\n\r\ninclude_once AS_PATH.\"classes/ajaxSearch.class.inc.php\";\r\n  \r\nif (class_exists(\'AjaxSearch\')) {\r\n  $as = new ajaxSearch($cfg);\r\n  //Process ajaxSearch\r\n  $output = $as->run();\r\n} else {\r\n  $output = \"<h3>error: AjaxSearch class not found</h3>\";\r\n}\r\nreturn $output;','0','','');
INSERT INTO `modx_site_snippets` VALUES ('2','Breadcrumbs','<strong>1.0.1</strong> Configurable breadcrumb page trail navigation.','0','0','0','/*\r\n * Breadcrumbs\r\n *\r\n * This snippet shows the path through the various levels of site structure. It\r\n * is NOT necessarily the path the user took to arrive at a given page.\r\n * Version: 1.0.1\r\n */\r\n\r\n/* -----------------------------------------------------------------------------\r\n * CONFIGURATION\r\n * -----------------------------------------------------------------------------\r\n * This section contains brief explanations of the available parameters.\r\n */\r\n\r\n/* General setup\r\n * -----------------------------------------------------------------------------\r\n */\r\n\r\n/* $maxCrumbs [ integer ]\r\n * Max number of elemetns to have in a breadcrumb path. The default 100 is an\r\n * arbitrarily high number that will essentially include everything. If you were\r\n * to set it to 2, and you were 5 levels deep, it would appear like:\r\n * HOME > ... > Level 3 > Level 4 > CURRENT PAGE\r\n * It should be noted that the \"home\" link, and the current page do not count as\r\n * they are managed by their own configuration settings.\r\n */\r\n( isset($maxCrumbs) ) ? $maxCrumbs : $maxCrumbs = 100;\r\n\r\n/* $pathThruUnPub [ 1 | 0 ]\r\n * When your path includes an unpublished folder, setting this to 1 (true) will\r\n * show all documents in path EXCEPT the unpublished. When set to 0 (false), the\r\n * path will not go \"through\" that unpublished folder and will stop there.\r\n */\r\n( isset($pathThruUnPub) ) ? $pathThruUnPub : $pathThruUnPub = 1;\r\n\r\n/* $respectHidemenu [ 0 | 1 ]\r\n * Setting this to 1 (true) will respect the hidemenu setting of the document\r\n * and not include it in trail.\r\n */\r\n( isset($respectHidemenu) ) ? (int)$respectHidemenu : $respectHidemenu = 1;\r\n\r\n/* $showCurrentCrumb [ 1 | 0 ]\r\n * Include the current page at the end of the trail. On by default.\r\n */\r\n( isset($showCurrentCrumb) ) ? $showCurrentCrumb : $showCurrentCrumb = 1;\r\n\r\n/* $currentAsLink [ 1 | 0 ]\r\n * If the current page is included, this parameter will show it as a link (1) or\r\n * just plain text (0).\r\n */\r\n( $currentAsLink ) ? $currentAsLink : $currentAsLink = 0;\r\n\r\n/* $linkTextField [ string ]\r\n * Prioritized list of fields to use as link text. Options are: pagetitle,\r\n * longtitle, description, menutitle. The first of these fields that has a value\r\n * will be the title.\r\n */\r\n( isset($linkTextField) ) ? $linkTextField : $linkTextField = \'menutitle,pagetitle,longtitle\';\r\n\r\n/* $linkDescField [ string ]\r\n * Prioritized list of fields to use as link title text. Options are: pagetitle,\r\n * longtitle, description, menutitle. The first of these fields that has a value\r\n * will be the title.\r\n */\r\n( isset($linkDescField) ) ? $linkDescField : $linkDescField = \'description,longtitle,pagetitle,menutitle\';\r\n\r\n/* $showCrumbsAsLinks [ 1 | 0 ]\r\n * If for some reason you want breadcrumbs to be text and not links, set to 0\r\n * (false).\r\n */\r\n( isset($showCrumbsAsLinks) ) ? $showCrumbsAsLinks : $showCrumbsAsLinks = 1;\r\n\r\n/* $templateSet [ string ]\r\n * The set of templates you\'d like to use. (Templates are defined below.) It\r\n * will default to defaultString which replicates the output of previous\r\n * versions.\r\n */\r\n( isset($templateSet) ) ? $templateSet : $templateSet = \'defaultString\';\r\n\r\n/* $crumbGap [ string ]\r\n * String to be shown to represent gap if there are more crumbs in trail than\r\n * can be shown. Note: if you would like to use an image, the entire image tag\r\n * must be provided. When making a snippet call, you cannot use \"=\", so use \"||\"\r\n * instead and it will be converted for you.\r\n */\r\n( isset($crumbGap) ) ? $crumbGap : $crumbGap = \'...\';\r\n\r\n/* $stylePrefix [ string ]\r\n * Breadcrumbs will add style classes to various parts of the trail. To avoid\r\n * class name conflicts, you can determine your own prefix. The following\r\n * classes will be attached:\r\n * crumbBox: Span that surrounds all crumb output\r\n * hideCrumb: Span that surrounds the \"...\" if there are more crumbs than will\r\n * be shown\r\n * currentCrumb: Span or A tag surrounding the current crumb\r\n * firstCrumb: Span that will be applied to first crumb, whether it is \"home\" or\r\n * not\r\n * lastCrumb: Span surrounding last crumb, whether it is the current page or\r\n * not\r\n * crumb: Class given to each A tag surrounding the intermediate crumbs (not\r\n * \"home\", \"current\", or \"hide\")\r\n * homeCrumb: Class given to the home crumb\r\n */\r\n( isset($stylePrefix) ) ? $stylePrefix : $stylePrefix = \'B_\';\r\n\r\n\r\n\r\n/* Home link parameters\r\n * -----------------------------------------------------------------------------\r\n * The home link is unique. It is a link that can be placed at the head of the\r\n * breadcrumb trail, even if it is not truly in the hierarchy.\r\n */\r\n\r\n/* $showHomeCrumb [ 1 | 0 ]\r\n * This toggles the \"home\" crumb to be added to the beginning of your trail.\r\n */\r\n( isset($showHomeCrumb) ) ? $showHomeCrumb : $showHomeCrumb = 1;\r\n\r\n/* $homeId [ integer ]\r\n * Usually the page designated as \"site start\" in MODx configuration is\r\n * considered the home page. But if you would like to use some other document,\r\n * you may explicitly define it.\r\n */\r\n( isset($homeId) ) ? (int)$homeId : $homeId = $modx->config[\'site_start\'];\r\n\r\n/* $homeCrumbTitle [ string ]\r\n * If you\'d like to use something other than the menutitle (or pagetitle) for\r\n * the home link.\r\n */\r\n( isset($homeCrumbTitle) ) ? $homeCrumbTitle : $homeCrumbTitle = \'\';\r\n\r\n/* $homeCrumbDescription [ string ]\r\n * If you\'d like to use a custom description (link title) on the home link. If\r\n * left blank, the title will follow the title order set in $titleField.\r\n */\r\n( isset($homeCrumbDescription) ) ? $homeCrumbDescription : $homeCrumbDescription = \'\';\r\n\r\n\r\n/* Custom behaviors\r\n * -----------------------------------------------------------------------------\r\n * The following parameters will alter the behavior of the Breadcrumbs based on\r\n * the page it is on.\r\n */\r\n\r\n/* $showCrumbsAtHome [ 1 | 0 ]\r\n * You can turn off Breadcrumbs all together on the home page by setting this to\r\n * 1 (true);\r\n */\r\n( isset($showCrumbsAtHome) ) ? $showCrumbsAtHome : $showCrumbsAtHome = 0;\r\n\r\n/* $hideOn [ string ]\r\n * Comma separated list of documents you don\'t want Breadcrumbs on at all. If\r\n * you have a LOT of pages like this, you might try $hideUnder or use another\r\n * template. This parameter is best for those rare odd balls - otherwise it will\r\n * become a pain to manage.\r\n */\r\n( isset($hideOn) ) ? $hideOn : $hideOn = \'\';\r\n\r\n/* $hideUnder [ string ]\r\n * Comma separated list of parent documents, whose CHILDREN you don\'t want\r\n * Breadcrumbs to appear on at all. This enables you to hide Breadcrumbs on a\r\n * whole folders worth of documents by specifying the parent only. The PARENT\r\n * will not have Breadcrumbs hidden however. If you wanted to hide the parent\r\n * and the children, put the parent ID in hideUnder AND hideOn.\r\n */\r\n( isset($hideUnder) ) ? $hideUnder : $hideUnder = \'\';\r\n\r\n/* $stopIds [ string ]\r\n * Comma separated list of document IDs that when reached, stops Breadcrumbs\r\n * from going any further. This is useful in situations like where you have\r\n * language branches, and you don\'t want the Breadcrumbs going past the \"home\"\r\n * of the language you\'re in.\r\n */\r\n( isset($stopIds) ) ? $stopIds : $stopIds = \'\';\r\n\r\n/* $ignoreIds [ string ]\r\n * Comma separated list of document IDs to explicitly ignore.\r\n */\r\n( isset($ignoreIds) ) ? $ignoreids : \'\';\r\n\r\n/* Templates\r\n * -----------------------------------------------------------------------------\r\n * In an effort to keep the MODx chunks manager from getting mired down in lots\r\n * of templates, Breadcrumbs templates are included here. Two sets are provided\r\n * prefixed with defaultString, and defaultList. You can create as many more as\r\n * you like, each set with it\'s own prefix\r\n */\r\n$templates = array(\r\n    \'defaultString\' => array(\r\n        \'crumb\' => \'[+crumb+]\',\r\n        \'separator\' => \' &raquo; \',\r\n        \'crumbContainer\' => \'<span class=\"[+crumbBoxClass+]\">[+crumbs+]</span>\',\r\n        \'lastCrumbWrapper\' => \'<span class=\"[+lastCrumbClass+]\">[+lastCrumbSpanA+]</span>\',\r\n        \'firstCrumbWrapper\' => \'<span class=\"[+firstCrumbClass+]\">[+firstCrumbSpanA+]</span>\'\r\n    ),\r\n    \'defaultList\' => array(\r\n        \'crumb\' => \'<li>[+crumb+]</li>\',\r\n        \'separator\' => \'\',\r\n        \'crumbContainer\' => \'<ul class=\"[+crumbBoxClass+]\">[+crumbs+]</ul>\',\r\n        \'lastCrumbWrapper\' => \'<span class=\"[+lastCrumbClass+]\">[+lastCrumbSpanA+]</span>\',\r\n        \'firstCrumbWrapper\' => \'<span class=\"[+firstCrumbClass+]\">[+firstCrumbSpanA+]</span>\'\r\n    ),\r\n);\r\n\r\n\r\n/* -----------------------------------------------------------------------------\r\n * END CONFIGURATION\r\n * -----------------------------------------------------------------------------\r\n */\r\n\r\n// Return blank if necessary: on home page\r\nif ( !$showCrumbsAtHome && $homeId == $modx->documentObject[\'id\'] )\r\n{\r\n    return \'\';\r\n}\r\n// Return blank if necessary: specified pages\r\nif ( $hideOn || $hideUnder )\r\n{\r\n    // Create array of hide pages\r\n    $hideOn = str_replace(\' \',\'\',$hideOn);\r\n    $hideOn = explode(\',\',$hideOn);\r\n\r\n    // Get more hide pages based on parents if needed\r\n    if ( $hideUnder )\r\n    {\r\n        $hiddenKids = array();\r\n        // Get child pages to hide\r\n        $hideKidsQuery = $modx->db->select(\'id\',$modx->getFullTableName(\"site_content\"),\"parent IN ($hideUnder)\");\r\n        while ( $hideKid = $modx->db->getRow($hideKidsQuery) )\r\n        {\r\n            $hiddenKids[] = $hideKid[\'id\'];\r\n        }\r\n        // Merge with hideOn pages\r\n        $hideOn = array_merge($hideOn,$hiddenKids);\r\n    }\r\n\r\n    if ( in_array($modx->documentObject[\'id\'],$hideOn) )\r\n    {\r\n        return \'\';\r\n    }\r\n\r\n}\r\n\r\n\r\n// Initialize ------------------------------------------------------------------\r\n\r\n// Put certain parameters in arrays\r\n$stopIds = str_replace(\' \',\'\',$stopIds);\r\n$stopIds = explode(\',\',$stopIds);\r\n$linkTextField = str_replace(\' \',\'\',$linkTextField);\r\n$linkTextField = explode(\',\',$linkTextField);\r\n$linkDescField = str_replace(\' \',\'\',$linkDescField);\r\n$linkDescField = explode(\',\',$linkDescField);\r\n$ignoreIds = str_replace(\' \',\'\',$ignoreIds);\r\n$ignoreIds = explode(\',\',$ignoreIds);\r\n\r\n/* $crumbs\r\n * Crumb elements are: id, parent, pagetitle, longtitle, menutitle, description,\r\n * published, hidemenu\r\n */\r\n$crumbs = array();\r\n$parent = $modx->documentObject[\'parent\'];\r\n$output = \'\';\r\n$maxCrumbs += ($showCurrentCrumb) ? 1 : 0;\r\n\r\n// Replace || in snippet parameters that accept them with =\r\n$crumbGap = str_replace(\'||\',\'=\',$crumbGap);\r\n\r\n// Curent crumb ----------------------------------------------------------------\r\n\r\n// Decide if current page is to be a crumb\r\nif ( $showCurrentCrumb )\r\n{\r\n    $crumbs[] = array(\r\n        \'id\' => $modx->documentObject[\'id\'],\r\n        \'parent\' => $modx->documentObject[\'parent\'],\r\n        \'pagetitle\' => $modx->documentObject[\'pagetitle\'],\r\n        \'longtitle\' => $modx->documentObject[\'longtitle\'],\r\n        \'menutitle\' => $modx->documentObject[\'menutitle\'],\r\n        \'description\' => $modx->documentObject[\'description\']);\r\n}\r\n\r\n// Intermediate crumbs ---------------------------------------------------------\r\n\r\n\r\n// Iterate through parents till we hit root or a reason to stop\r\n$loopSafety = 0;\r\nwhile ( $parent && $loopSafety < 1000 )\r\n{\r\n    // Get next crumb\r\n    $tempCrumb = $modx->getPageInfo($parent,0,\"id,parent,pagetitle,longtitle,menutitle,description,published,hidemenu\");\r\n\r\n    // Check for include conditions & add to crumbs\r\n    if (\r\n        $tempCrumb[\'published\'] &&\r\n        ( !$tempCrumb[\'hidemenu\'] || !$respectHidemenu ) &&\r\n        !in_array($tempCrumb[\'id\'],$ignoreIds)\r\n    )\r\n    {\r\n        // Add crumb\r\n        $crumbs[] = array(\r\n        \'id\' => $tempCrumb[\'id\'],\r\n        \'parent\' => $tempCrumb[\'parent\'],\r\n        \'pagetitle\' => $tempCrumb[\'pagetitle\'],\r\n        \'longtitle\' => $tempCrumb[\'longtitle\'],\r\n        \'menutitle\' => $tempCrumb[\'menutitle\'],\r\n        \'description\' => $tempCrumb[\'description\']);\r\n    }\r\n\r\n    // Check stop conditions\r\n    if (\r\n        in_array($tempCrumb[\'id\'],$stopIds) ||  // Is one of the stop IDs\r\n        !$tempCrumb[\'parent\'] || // At root\r\n        ( !$tempCrumb[\'published\'] && !$pathThruUnPub ) // Unpublished\r\n    )\r\n    {\r\n        // Halt making crumbs\r\n        break;\r\n    }\r\n\r\n    // Reset parent\r\n    $parent = $tempCrumb[\'parent\'];\r\n\r\n    // Increment loop safety\r\n    $loopSafety++;\r\n}\r\n\r\n// Home crumb ------------------------------------------------------------------\r\n\r\nif ( $showHomeCrumb && $homeCrumb = $modx->getPageInfo($homeId,0,\"id,parent,pagetitle,longtitle,menutitle,description,published,hidemenu\") )\r\n{\r\n    $crumbs[] = array(\r\n    \'id\' => $homeCrumb[\'id\'],\r\n    \'parent\' => $homeCrumb[\'parent\'],\r\n    \'pagetitle\' => $homeCrumb[\'pagetitle\'],\r\n    \'longtitle\' => $homeCrumb[\'longtitle\'],\r\n    \'menutitle\' => $homeCrumb[\'menutitle\'],\r\n    \'description\' => $homeCrumb[\'description\']);\r\n}\r\n\r\n\r\n// Process each crumb ----------------------------------------------------------\r\n$pretemplateCrumbs = array();\r\n\r\nforeach ( $crumbs as $c )\r\n{\r\n\r\n    // Skip if we\'ve exceeded our crumb limit but we\'re waiting to get to home\r\n    if ( count($pretemplateCrumbs) > $maxCrumbs && $c[\'id\'] != $homeId )\r\n    {\r\n        continue;\r\n    }\r\n\r\n    $text = \'\';\r\n    $title = \'\';\r\n    $pretemplateCrumb = \'\';\r\n\r\n    // Determine appropriate span/link text: home link specified\r\n    if ( $c[\'id\'] == $homeId && $homeCrumbTitle )\r\n    {\r\n        $text = $homeCrumbTitle;\r\n    }\r\n    else\r\n    // Determine appropriate span/link text: home link not specified\r\n    {\r\n        for ($i = 0; !$text && $i < count($linkTextField); $i++)\r\n        {\r\n            if ( $c[$linkTextField[$i]] )\r\n            {\r\n                $text = $c[$linkTextField[$i]];\r\n            }\r\n        }\r\n    }\r\n\r\n    // Determine link/span class(es)\r\n    if ( $c[\'id\'] == $homeId )\r\n    {\r\n        $crumbClass = $stylePrefix.\'homeCrumb\';\r\n    }\r\n    else if ( $modx->documentObject[\'id\'] == $c[\'id\'] )\r\n    {\r\n        $crumbClass = $stylePrefix.\'currentCrumb\';\r\n    }\r\n    else\r\n    {\r\n        $crumbClass = $stylePrefix.\'crumb\';\r\n    }\r\n\r\n    // Make link\r\n    if (\r\n        ( $c[\'id\'] != $modx->documentObject[\'id\'] && $showCrumbsAsLinks ) ||\r\n        ( $c[\'id\'] == $modx->documentObject[\'id\'] && $currentAsLink )\r\n    )\r\n    {\r\n        // Determine appropriate title for link: home link specified\r\n        if ( $c[\'id\'] == $homeId && $homeCrumbDescription )\r\n        {\r\n            $title = htmlspecialchars($homeCrumbDescription);\r\n        }\r\n        else\r\n        // Determine appropriate title for link: home link not specified\r\n        {\r\n            for ($i = 0; !$title && $i < count($linkDescField); $i++)\r\n            {\r\n                if ( $c[$linkDescField[$i]] )\r\n                {\r\n                    $title = htmlspecialchars($c[$linkDescField[$i]]);\r\n                }\r\n            }\r\n        }\r\n\r\n\r\n        $pretemplateCrumb .= \'<a class=\"\'.$crumbClass.\'\" href=\"\'.$modx->makeUrl($c[\'id\']).\'\" title=\"\'.$title.\'\">\'.$text.\'</a>\';\r\n    }\r\n    else\r\n    // Make a span instead of a link\r\n    {\r\n       $pretemplateCrumb .= \'<span class=\"\'.$crumbClass.\'\">\'.$text.\'</span>\';\r\n    }\r\n\r\n    // Add crumb to pretemplate crumb array\r\n    $pretemplateCrumbs[] = $pretemplateCrumb;\r\n\r\n    // If we have hit the crumb limit\r\n    if ( count($pretemplateCrumbs) == $maxCrumbs )\r\n    {\r\n        if ( count($crumbs) > ($maxCrumbs + (($showHomeCrumb) ? 1 : 0)) )\r\n        {\r\n            // Add gap\r\n            $pretemplateCrumbs[] = \'<span class=\"\'.$stylePrefix.\'hideCrumb\'.\'\">\'.$crumbGap.\'</span>\';\r\n        }\r\n\r\n        // Stop here if we\'re not looking for the home crumb\r\n        if ( !$showHomeCrumb )\r\n        {\r\n            break;\r\n        }\r\n    }\r\n}\r\n\r\n// Put in correct order for output\r\n$pretemplateCrumbs = array_reverse($pretemplateCrumbs);\r\n\r\n// Wrap first/last spans\r\n$pretemplateCrumbs[0] = str_replace(\r\n    array(\'[+firstCrumbClass+]\',\'[+firstCrumbSpanA+]\'),\r\n    array($stylePrefix.\'firstCrumb\',$pretemplateCrumbs[0]),\r\n    $templates[$templateSet][\'firstCrumbWrapper\']\r\n);\r\n$pretemplateCrumbs[(count($pretemplateCrumbs)-1)] = str_replace(\r\n    array(\'[+lastCrumbClass+]\',\'[+lastCrumbSpanA+]\'),\r\n    array($stylePrefix.\'lastCrumb\',$pretemplateCrumbs[(count($pretemplateCrumbs)-1)]),\r\n    $templates[$templateSet][\'lastCrumbWrapper\']\r\n);\r\n\r\n// Insert crumbs into crumb template\r\n$processedCrumbs = array();\r\nforeach ( $pretemplateCrumbs as $pc )\r\n{\r\n    $processedCrumbs[] = str_replace(\'[+crumb+]\',$pc,$templates[$templateSet][\'crumb\']);\r\n}\r\n\r\n// Combine crumbs together into one string with separator\r\n$processedCrumbs = implode($templates[$templateSet][\'separator\'],$processedCrumbs);\r\n\r\n// Put crumbs into crumb container template\r\n$container = str_replace(\r\n    array(\'[+crumbBoxClass+]\',\'[+crumbs+]\'),\r\n    array($stylePrefix.\'crumbBox\',$processedCrumbs),\r\n    $templates[$templateSet][\'crumbContainer\']\r\n    );\r\n\r\n// Return crumbs\r\nreturn $container;','0','','');
INSERT INTO `modx_site_snippets` VALUES ('17','testing','','0','0','0','\r\n$child = $modx->getDocument(98);\r\necho $modx->documentObject[\'id\'];\r\necho date(\"D, d-m-Y\", $child[publishedon]);\r\n','0','',' ');
INSERT INTO `modx_site_snippets` VALUES ('3','Ditto','<strong>2.1</strong>+ Summarizes and lists pages to create blogs, catalogs, PR archives, bio listings and more. Includes patches post-2.1 release to fix sorting bug and default display behavior.','0','0','0','/*\r\n * Title: Ditto Snippet\r\n * \r\n * Description:\r\n *  	Aggregates documents to create blogs, article/news\r\n *  	collections, and more,with full support for templating.\r\n * \r\n * Author: \r\n * 		Mark Kaplan for MODx CMF\r\n * \r\n * Version: \r\n * 		2.1.0\r\n*/\r\n\r\n//---Core Settings---------------------------------------------------- //\r\n\r\n$ditto_version = \"2.1.0\";\r\n	// Ditto version being executed\r\n\r\n$ditto_base = isset($ditto_base) ? $modx->config[\'base_path\'].$ditto_base : $modx->config[\'base_path\'].\"assets/snippets/ditto/\";\r\n/*\r\n	Param: ditto_base\r\n	\r\n	Purpose:\r\n	Location of Ditto files\r\n\r\n	Options:\r\n	Any valid folder location containing the Ditto source code with a trailing slash\r\n\r\n	Default:\r\n	[(base_path)]assets/snippets/ditto/\r\n*/\r\n$dittoID = (!isset($id)) ? \"\" : $id.\"_\";\r\n$GLOBALS[\"dittoID\"] = $dittoID;\r\n/*\r\n	Param: id\r\n\r\n	Purpose:\r\n	Unique ID for this Ditto instance for connection with other scripts (like Reflect) and unique URL parameters\r\n\r\n	Options:\r\n	Any combination of characters a-z, underscores, and numbers 0-9\r\n	\r\n	Note:\r\n	This is case sensitive\r\n\r\n	Default:\r\n	\"\" - blank\r\n*/		\r\n$language = (isset($language))? $language : \"english\";\r\n/*\r\n	Param: language\r\n\r\n	Purpose:\r\n	language for defaults, debug, and error messages\r\n\r\n	Options:\r\n	Any language name with a corresponding file in the &ditto_base/lang folder\r\n\r\n	Default:\r\n	\"english\"\r\n*/\r\n$format = (isset($format)) ? strtolower($format) : \"html\" ;\r\n/*\r\n	Param: format\r\n\r\n	Purpose:\r\n 	Output format to use\r\n\r\n	Options:\r\n	- \"html\"\r\n	- \"json\"\r\n	- \"xml\"\r\n	- \"atom\"\r\n	- \"rss\"\r\n\r\n	Default:\r\n	\"html\"\r\n*/\r\n$config = (isset($config)) ? $config : \"default\";\r\n/*\r\n	Param: config\r\n\r\n	Purpose:\r\n 	Load a custom configuration\r\n\r\n	Options:\r\n	\"default\" - default blank config file\r\n	CONFIG_NAME - Other configs installed in the configs folder or in any folder within the MODx base path via @FILE\r\n\r\n	Default:\r\n	\"default\"\r\n	\r\n	Related:\r\n	- <extenders>\r\n*/\r\n$debug = isset($debug)? $debug : 0;\r\n/*\r\n	Param: debug\r\n\r\n	Purpose:\r\n 	Output debugging information\r\n\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	0 - off\r\n	\r\n	Related:\r\n	- <debug>\r\n*/\r\n$phx = (isset($phx))? $phx : 1;\r\n/*\r\n	Param: phx\r\n\r\n	Purpose:\r\n 	Use PHx formatting\r\n\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	1 - on\r\n*/		\r\n$extenders = isset($extenders) ? explode(\",\",$extenders) : array();\r\n/*\r\n	Param: extenders\r\n\r\n	Purpose:\r\n 	Load an extender which adds functionality to Ditto\r\n\r\n	Options:\r\n	Any extender in the extenders folder or in any folder within the MODx base path via @FILE\r\n\r\n	Default:\r\n	[NULL]\r\n\r\n	Related:\r\n	- <config>\r\n*/\r\n	// Variable: extenders\r\n	// Array that can be added to by configs or formats to load that extender\r\n	\r\n$placeholders = array();\r\n	// Variable: placeholders\r\n	// Initialize custom placeholders array for configs or extenders to add to\r\n\r\n$filters = array(\"custom\"=>array(),\"parsed\"=>array());\r\n	// Variable: filters\r\n	// Holds both the custom filters array for configs or extenders to add to \r\n	// and the parsed filters array. To add to this array, use the following format\r\n	// (code)\r\n	// $filters[\"parsed\"][] = array(\"name\" => array(\"source\"=>$source,\"value\"=>$value,\"mode\"=>$mode));\r\n	// $filters[\"custom\"][] = array(\"source\",\"callback_function\");\r\n\r\n$orderBy = array(\'parsed\'=>array(),\'custom\'=>array(),\'unparsed\'=>$orderBy);\r\n	// Variable: orderBy\r\n	// An array that holds all criteria to sort the result set by. \r\n	// Note that using a custom sort will disable all other sorting.\r\n	// (code)\r\n	// $orderBy[\"parsed\"][] = array(\"sortBy\",\"sortDir\");\r\n	// $orderBy[\"custom\"][] = array(\"sortBy\",\"callback_function\");\r\n		\r\n//---Includes-------------------------------------------------------- //\r\n\r\n$files = array (\r\n	\"base_language\" => $ditto_base.\"lang/english.inc.php\",\r\n	\"language\" => $ditto_base.\"lang/$language.inc.php\",\r\n	\"main_class\" => $ditto_base.\"classes/ditto.class.inc.php\",\r\n	\"template_class\" => $ditto_base.\"classes/template.class.inc.php\",\r\n	\"filter_class\" => $ditto_base.\"classes/filter.class.inc.php\",\r\n	\"format\" => $ditto_base.\"formats/$format.format.inc.php\",\r\n	\"config\" => $ditto_base.\"configs/default.config.php\",\r\n	\"user_config\" => (substr($config, 0, 5) != \"@FILE\") ? $ditto_base.\"configs/$config.config.php\" : $modx->config[\'base_path\'].trim(substr($config, 5))\r\n);\r\n\r\nif ($phx == 1) {\r\n	$files[\"prePHx_class\"] = $ditto_base.\"classes/phx.pre.class.inc.php\";\r\n}\r\nif (isset($randomize)) {\r\n	$files[\"randomize_class\"] = $ditto_base.\"classes/random.class.inc.php\";\r\n}\r\nif ($debug == 1) {\r\n	$files[\"modx_debug_class\"] = $ditto_base.\"debug/modxDebugConsole.class.php\";\r\n	$files[\"debug_class\"] = $ditto_base.\"classes/debug.class.inc.php\";\r\n	$files[\"debug_templates\"] = $ditto_base.\"debug/debug.templates.php\";\r\n}\r\n\r\n$files = array_unique($files);\r\nforeach ($files as $filename => $filevalue) {\r\n	if (file_exists($filevalue) && strpos($filename,\"class\")) {\r\n		include_once($filevalue);\r\n	} else if (file_exists($filevalue)) {\r\n		include($filevalue);\r\n	} else if ($filename == \"language\") {\r\n		$modx->logEvent(1, 3, \"Language file does not exist Please check: \" . $filevalue, \"Ditto \" . $ditto_version);\r\n		return \"Language file does not exist Please check: \" . $filevalue;\r\n	} else {\r\n		$modx->logEvent(1, 3, $filevalue . \" \" . $_lang[\'file_does_not_exist\'], \"Ditto \" . $ditto_version);\r\n		return $filevalue . \" \" . $_lang[\'file_does_not_exist\'];\r\n	}\r\n}\r\n\r\n//---Initiate Class-------------------------------------------------- //\r\nif (class_exists(\'ditto\')) {\r\n	$ditto = new ditto($dittoID,$format,$_lang,$dbg_templates);\r\n		// create a new Ditto instance in the specified format and language with the requested debug level\r\n} else {\r\n	$modx->logEvent(1,3,$_lang[\'invalid_class\'],\"Ditto \".$ditto_version);\r\n	return $_lang[\'invalid_class\'];\r\n}\r\n\r\n//---Initiate Extenders---------------------------------------------- //\r\nif (isset($tagData)) {\r\n	$extenders[] = \"tagging\";\r\n}\r\nif(count($extenders) > 0) {\r\n	$extenders = array_unique($extenders);\r\n	foreach ($extenders as $extender) {\r\n			if(substr($extender, 0, 5) != \"@FILE\") {\r\n				$extender_path = $ditto_base.\"extenders/\".$extender.\".extender.inc.php\";				\r\n			} else {\r\n				$extender_path = $modx->config[\'base_path\'].trim(substr($extender, 5));\r\n			}\r\n			\r\n			if (file_exists($extender_path)){\r\n				include($extender_path);\r\n			} else {\r\n				$modx->logEvent(1, 3, $extender . \" \" . $_lang[\'extender_does_not_exist\'], \"Ditto \".$ditto_version);\r\n				return $extender . \" \" . $_lang[\'extender_does_not_exist\'];\r\n			}		\r\n	}	\r\n}\r\n\r\n//---Parameters------------------------------------------------------- /*\r\nif (isset($startID)) {$parents = $startID;}\r\nif (isset($summarize)) {$display = $summarize;}\r\nif (isset($limit)) {$queryLimit = $limit;}\r\nif (isset($sortBy) || isset($sortDir) || is_null($orderBy[\'unparsed\'])) {\r\n	$sortDir = isset($sortDir) ? strtoupper($sortDir) : \'DESC\';\r\n	$sortBy = isset($sortBy) ? $sortBy : \"createdon\";\r\n	$orderBy[\'parsed\'][]=array($sortBy,$sortDir);\r\n}\r\n	// Allow backwards compatibility\r\n\r\n$idType = isset($documents) ? \"documents\" : \"parents\";\r\n	// Variable: idType\r\n	// type of IDs provided; can be either parents or documents\r\n\r\n$parents = isset($parents) ? $ditto->cleanIDs($parents) : $modx->documentIdentifier;\r\n\r\n/*\r\n	Param: parents\r\n\r\n	Purpose:\r\n	IDs of containers for Ditto to retrieve their children to &depth depth\r\n\r\n	Options:\r\n	Any valid MODx document marked as a container\r\n\r\n	Default:\r\n	Current MODx Document\r\n\r\n	Related:\r\n	- <documents>\r\n	- <depth>\r\n*/\r\n$documents = isset($documents) ? $ditto->cleanIDs($documents) : false;\r\n/*\r\n	Param: documents\r\n\r\n	Purpose:\r\n	IDs of documents for Ditto to retrieve\r\n\r\n	Options:\r\n	Any valid MODx document marked as a container\r\n\r\n	Default:\r\n	None\r\n\r\n	Related:\r\n	- <parents>\r\n*/\r\n\r\n$IDs = ($idType == \"parents\") ? $parents : $documents;\r\n	// Variable: IDs\r\n	// Internal variable which holds the set of IDs for Ditto to fetch\r\n\r\n$depth = isset($depth) ? $depth : 1;\r\n/*\r\n	Param: depth\r\n\r\n	Purpose:\r\n	Number of levels deep to retrieve documents\r\n\r\n	Options:\r\n	Any number greater than or equal to 1\r\n	0 - infinite depth\r\n\r\n	Default:\r\n	1\r\n\r\n	Related:\r\n	- <seeThruUnpub>\r\n*/\r\n$paginate = isset($paginate)? $paginate : 0;\r\n/*\r\n	Param: paginate\r\n\r\n	Purpose:\r\n	Paginate the results set into pages of &display length.\r\n	Use &total to limit the number of documents retreived.\r\n\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	0 - off\r\n	\r\n	Related:\r\n	- <paginateAlwaysShowLinks>\r\n	- <paginateSplitterCharacter>\r\n	- <display>\r\n*/\r\n$dateSource = isset($dateSource) ? $dateSource : \"createdon\";\r\n/*\r\n	Param: dateSource\r\n\r\n	Purpose:\r\n	Source of the [+date+] placeholder\r\n\r\n	Options:\r\n	# - Any UNIX timestamp from MODx fields or TVs such as createdon, pub_date, or editedon\r\n	\r\n	Default:\r\n	\"createdon\"\r\n	\r\n	Related:\r\n	- <dateFormat>\r\n*/\r\n$dateFormat = isset($dateFormat)? $dateFormat : $_lang[\"dateFormat\"];\r\n/*\r\n	Param: dateFormat\r\n\r\n	Purpose:\r\n	Format the [+date+] placeholder in human readable form\r\n\r\n	Options:\r\n	Any PHP valid strftime option\r\n\r\n	Default:\r\n	[LANG]\r\n	\r\n	Related:\r\n	- <dateSource>\r\n*/\r\n$display = isset($display) ? $display : \"all\";\r\n/*\r\n	Param: display\r\n\r\n	Purpose:\r\n	Number of documents to display in the results\r\n\r\n	Options:\r\n	# - Any number\r\n	\"all\" - All documents found\r\n\r\n	Default:\r\n	\"all\"\r\n	\r\n	Related:\r\n	- <queryLimit>\r\n	- <total>\r\n*/\r\n$total = isset($total) ? $total : \"all\";\r\n/*\r\n	Param: total\r\n\r\n	Purpose:\r\n	Number of documents to retrieve\r\n	\r\n	Options:\r\n	# - Any number\r\n	\"all\" - All documents found\r\n\r\n	Default:\r\n	\"all\" - All documents found\r\n	\r\n	Related:\r\n	- <display>\r\n	- <queryLimit>\r\n*/\r\n$showPublishedOnly = isset($showPublishedOnly) ? $showPublishedOnly : 1;\r\n/*\r\n	Param: showPublishedOnly\r\n\r\n	Purpose:\r\n	Show only published documents\r\n\r\n	Options:\r\n	0 - show only unpublished documents\r\n	1 - show both published and unpublished documents\r\n	\r\n	Default:\r\n	1 - show both published and unpublished documents\r\n	\r\n	Related:\r\n	- <seeThruUnpub>\r\n	- <hideFolders>\r\n	- <showPublishedOnly>\r\n	- <where>\r\n*/\r\n$showInMenuOnly = isset($showInMenuOnly) ? $showInMenuOnly : 0;\r\n/*\r\n	Param: showInMenuOnly\r\n\r\n	Purpose:\r\n	Show only documents visible in the menu\r\n\r\n	Options:\r\n	0 - show all documents\r\n	1 - show only documents with the show in menu flag checked\r\n	\r\n	Default:\r\n	0 - show all documents\r\n	\r\n	Related:\r\n	- <seeThruUnpub>\r\n	- <hideFolders>\r\n	- <where>\r\n*/\r\n$hideFolders = isset($hideFolders)? $hideFolders : 0;\r\n/*\r\n	Param: hideFolders\r\n\r\n	Purpose:\r\n	Don\'t show folders in the returned results\r\n\r\n	Options:\r\n	0 - keep folders\r\n	1 - remove folders\r\n	\r\n	Default:\r\n	0 - keep folders\r\n	\r\n	Related:\r\n	- <seeThruUnpub>\r\n	- <showInMenuOnly>\r\n	- <where>\r\n*/\r\n$hidePrivate = isset($hidePrivate)? $hidePrivate : 1;\r\n/*\r\n	Param: hidePrivate\r\n\r\n	Purpose:\r\n	Don\'t show documents the guest or user does not have permission to see\r\n\r\n	Options:\r\n	0 - show private documents\r\n	1 - hide private documents\r\n	\r\n	Default:\r\n	1 - hide private documents\r\n	\r\n	Related:\r\n	- <seeThruUnpub>\r\n	- <showInMenuOnly>\r\n	- <where>\r\n*/\r\n$seeThruUnpub = (isset($seeThruUnpub))? $seeThruUnpub : 1 ;\r\n/*\r\n	Param: seeThruUnpub\r\n\r\n	Purpose:\r\n	See through unpublished folders to retrive their children\r\n	Used when depth is greater than 1\r\n\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	0 - off\r\n	\r\n	Related:\r\n	- <hideFolders>\r\n	- <showInMenuOnly>\r\n	- <where>\r\n*/\r\n$queryLimit = (isset($queryLimit))? $queryLimit : 0;\r\n/*\r\n	Param: queryLimit\r\n\r\n	Purpose:\r\n	Number of documents to retrieve from the database, same as MySQL LIMIT\r\n\r\n	Options:\r\n	# - Any number\r\n	0 - automatic\r\n\r\n	Default:\r\n	0 - automatic\r\n	\r\n	Related:\r\n	- <where>\r\n*/\r\n$where = (isset($where))? $where : \"\";\r\n/*\r\n	Param: where\r\n\r\n	Purpose:\r\n	Custom MySQL WHERE statement\r\n\r\n	Options:\r\n	A valid MySQL WHERE statement using only document object items (no TVs)\r\n\r\n	Default:\r\n	[NULL]\r\n	\r\n	Related:\r\n	- <queryLimit>\r\n*/\r\n$noResults = isset($noResults)? $ditto->getParam($noResults,\"no_documents\") : $_lang[\'no_documents\'];\r\n/*\r\n	Param: noResults\r\n\r\n	Purpose:\r\n	Text or chunk to display when there are no results\r\n\r\n	Options:\r\n	Any valid chunk name or text\r\n\r\n	Default:\r\n	[LANG]\r\n*/\r\n$removeChunk = isset($removeChunk) ? explode(\",\",$removeChunk) : false;\r\n/*\r\n	Param: removeChunk\r\n\r\n	Purpose:\r\n 	Name of chunks to be stripped from content separated by commas\r\n	- Commonly used to remove comments\r\n\r\n	Options:\r\n	Any valid chunkname that appears in the output\r\n\r\n	Default:\r\n	[NULL]\r\n*/\r\n$hiddenFields = isset($hiddenFields) ? explode(\",\",$hiddenFields) : false;\r\n/*\r\n	Param: hiddenFields\r\n\r\n	Purpose:\r\n	Allow Ditto to retrieve fields its template parser cannot handle such as nested placeholders and [*fields*]\r\n\r\n	Options:\r\n	Any valid MODx fieldnames or TVs comma separated\r\n\r\n	Default:\r\n	[NULL]\r\n*/\r\n$offset = isset($start) ? $start : 0;\r\n$start = (isset($_GET[$dittoID.\'start\'])) ? intval($_GET[$dittoID.\'start\']) : 0;\r\n/*\r\n	Param: start\r\n\r\n	Purpose:\r\n 	Number of documents to skip in the results\r\n	\r\n	Options:\r\n	Any number\r\n\r\n	Default:\r\n	0\r\n*/\r\n$globalFilterDelimiter = isset($globalFilterDelimiter) ? $globalFilterDelimiter : \"|\";\r\n/*\r\n	Param: globalFilterDelimiter\r\n\r\n	Purpose:\r\n 	Filter delimiter used to separate filters in the filter string\r\n	\r\n	Options:\r\n	Any character not used in the filters\r\n\r\n	Default:\r\n	\"|\"\r\n	\r\n	Related:\r\n	- <localFilterDelimiter>\r\n	- <filter>\r\n	- <parseFilters>\r\n*/\r\n	\r\n$localFilterDelimiter = isset($localFilterDelimiter) ? $localFilterDelimiter : \",\";\r\n/*\r\n	Param: localFilterDelimiter\r\n\r\n	Purpose:\r\n	Delimiter used to separate individual parameters within each filter string\r\n	\r\n	Options:\r\n	Any character not used in the filter itself\r\n\r\n	Default:\r\n	\",\"\r\n	\r\n	Related:\r\n	- <globalFilterDelimiter>\r\n	- <filter>\r\n	- <parseFilters>\r\n*/\r\n$filters[\"custom\"] = isset($cFilters) ? array_merge($filters[\"custom\"],$cFilters) : $filters[\"custom\"];\r\n$filters[\"parsed\"] = isset($parsedFilters) ? array_merge($filters[\"parsed\"],$parsedFilters) : $filters[\"parsed\"];\r\n	// handle 2.0.0 compatibility\r\n$filter = (isset($filter) || ($filters[\"custom\"] != false) || ($filters[\"parsed\"] != false)) ? $ditto->parseFilters($filter,$filters[\"custom\"],$filters[\"parsed\"],$globalFilterDelimiter,$localFilterDelimiter) : false;\r\n/*\r\n	Param: filter\r\n\r\n	Purpose:\r\n	Removes items not meeting a critera. Thus, if pagetitle == joe then it will be removed.\r\n	Use in the format field,criteria,mode with the comma being the local delimiter\r\n\r\n	*Mode* *Meaning*\r\n	\r\n	1 - !=\r\n	2 - ==\r\n	3 - <\r\n	4 - >\r\n	5 - <=\r\n	6 - >=\r\n	7 - Text not in field value\r\n	8 - Text in field value\r\n	9 - case insenstive version of #7\r\n	10 - case insenstive version of #8\r\n	11 - checks leading character of the field\r\n	\r\n	@EVAL:\r\n		@EVAL in filters works the same as it does in MODx exect it can only be used \r\n		with basic filtering, not custom filtering (tagging, etc). Make sure that\r\n		you return the value you wish Ditto to filter by and that the code is valid PHP.\r\n\r\n	Default:\r\n	[NULL]\r\n	\r\n	Related:\r\n	- <localFilterDelimiter>\r\n	- <globalFilterDelimiter>\r\n	- <parseFilters>\r\n*/\r\n$keywords = (isset($keywords))? $keywords : 0;\r\n/*	\r\n	Param: keywords\r\n	\r\n	Purpose: \r\n	Enable fetching of associated keywords for each document\r\n	Can be used as [+keywords+] or as a tagData source\r\n	\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	0 - off\r\n*/\r\n\r\n$randomize = (isset($randomize))? $randomize : 0;\r\n/*	\r\n	Param: randomize\r\n	\r\n	Purpose: \r\n	Randomize the order of the output\r\n	\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	Any MODx field or TV for weighted random\r\n	\r\n	Default:\r\n	0 - off\r\n*/\r\n$save = (isset($save))? $save : 0;\r\n/*\r\n	Param: save\r\n\r\n	Purpose:\r\n	Saves the ditto object and results set to placeholders\r\n	for use by other snippets\r\n\r\n	Options:\r\n	0 - off; returns output\r\n	1 - remaining; returns output\r\n	2 - all;\r\n	3 - all; returns ph only\r\n\r\n	Default:\r\n		0 - off; returns output\r\n*/\r\n$templates = array(\r\n	\"default\" => \"@CODE\".$_lang[\'default_template\'],\r\n	\"base\" => $tpl,\r\n	\"alt\" => $tplAlt,\r\n	\"first\" => $tplFirst,\r\n	\"last\" => $tplLast,\r\n	\"current\" => $tplCurrentDocument\r\n);\r\n/*\r\n	Param: tpl\r\n\r\n	Purpose:\r\n	User defined chunk to format the documents \r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE\r\n	- File via @FILE\r\n\r\n	Default:\r\n	[LANG]\r\n*/\r\n/*\r\n	Param: tplAlt\r\n\r\n	Purpose:\r\n	User defined chunk to format every other document\r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE\r\n	- File via @FILE\r\n\r\n	Default:\r\n	&tpl\r\n*/\r\n/*\r\n	Param: tplFirst\r\n\r\n	Purpose:\r\n	User defined chunk to format the first document \r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE\r\n	- File via @FILE\r\n\r\n	Default:\r\n	&tpl\r\n*/\r\n/*\r\n	Param: tplLast\r\n\r\n	Purpose:\r\n	User defined chunk to format the last document \r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE\r\n	- File via @FILE\r\n\r\n	Default:\r\n	&tpl\r\n*/\r\n/*\r\n	Param: tplCurrentDocument\r\n\r\n	Purpose:\r\n	User defined chunk to format the current document\r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE\r\n	- File via @FILE\r\n\r\n	Default:\r\n	&tpl\r\n*/\r\n$orderBy = $ditto->parseOrderBy($orderBy,$randomize);\r\n/*\r\n	Param: orderBy\r\n\r\n	Purpose:\r\n	Sort the result set\r\n\r\n	Options:\r\n	Any valid MySQL style orderBy statement\r\n\r\n	Default:\r\n	createdon DESC\r\n*/\r\n//-------------------------------------------------------------------- */\r\n$templates = $ditto->template->process($templates);\r\n	// parse the templates for TV\'s and store them for later use\r\n\r\n$ditto->setDisplayFields($ditto->template->fields,$hiddenFields);\r\n	// parse hidden fields\r\n	\r\n$ditto->parseFields($placeholders,$seeThruUnpub,$dateSource,$randomize);\r\n	// parse the fields into the field array\r\n	\r\n$documentIDs = $ditto->determineIDs($IDs, $idType, $ditto->fields[\"backend\"][\"tv\"], $orderBy, $depth, $showPublishedOnly, $seeThruUnpub, $hideFolders, $hidePrivate, $showInMenuOnly, $where, $keywords, $dateSource, $queryLimit, $display, $filter,$paginate, $randomize);\r\n	// retrieves a list of document IDs that meet the criteria and populates the $resources array with them\r\n$count = count($documentIDs);\r\n	// count the number of documents to be retrieved\r\n$count = $count-$offset;\r\n	// handle the offset\r\n\r\nif ($count > 0) {\r\n	// if documents are returned continue with execution\r\n	\r\n	$total = ($total == \"all\") ? $count : min($total,$count);\r\n		// set total equal to count if all documents are to be included\r\n	\r\n	$display = ($display == \"all\") ? min($count,$total) : min($display,$total);\r\n		// allow show to use all option\r\n\r\n	$stop = ($save != \"1\") ? min($total-$start,$display) : min($count,$total);\r\n		// set initial stop count\r\n\r\n	if($paginate == 1) {\r\n		$paginateAlwaysShowLinks = isset($paginateAlwaysShowLinks)? $paginateAlwaysShowLinks : 0;\r\n		/*\r\n			Param: paginateAlwaysShowLinks\r\n\r\n			Purpose:\r\n			Determine whether or not to always show previous next links\r\n\r\n			Options:\r\n			0 - off\r\n			1 - on\r\n\r\n			Default:\r\n			0 - off\r\n		\r\n			Related:\r\n			- <paginate>\r\n			- <paginateSplitterCharacter>\r\n		*/\r\n		$paginateSplitterCharacter = isset($paginateSplitterCharacter)? $paginateSplitterCharacter : $_lang[\'button_splitter\'];\r\n		/*\r\n			Param: paginateSplitterCharacter\r\n\r\n			Purpose:\r\n			Splitter to use if always show is disabled\r\n\r\n			Options:\r\n			Any valid character\r\n\r\n			Default:\r\n			[LANG]\r\n		\r\n			Related:\r\n			- <paginate>\r\n			- <paginateSplitterCharacter>\r\n		*/\r\n		$tplPaginatePrevious = isset($tplPaginatePrevious)? $ditto->template->fetch($tplPaginatePrevious) : \"<a href=\'[+url+]\' class=\'ditto_previous_link\'>[+lang:previous+]</a>\";\r\n		/*\r\n			Param: tplPaginatePrevious\r\n\r\n			Purpose:\r\n			Template for the previous link\r\n\r\n			Options:\r\n			- Any valid chunk name\r\n			- Code via @CODE\r\n			- File via @FILE\r\n\r\n			Placeholders:\r\n			url - URL for the previous link\r\n			lang:previous - value of \'prev\' from the language file\r\n		\r\n			Related:\r\n			- <tplPaginateNext>\r\n			- <paginateSplitterCharacter>\r\n		*/\r\n		$tplPaginateNext = isset($tplPaginateNext)? $ditto->template->fetch($tplPaginateNext) : \"<a href=\'[+url+]\' class=\'ditto_next_link\'>[+lang:next+]</a>\";\r\n		/*\r\n			Param: tplPaginateNext\r\n\r\n			Purpose:\r\n			Template for the next link\r\n\r\n			Options:\r\n			- Any valid chunk name\r\n			- Code via @CODE\r\n			- File via @FILE\r\n\r\n			Placeholders:\r\n			url - URL for the next link\r\n			lang:next - value of \'next\' from the language file\r\n		\r\n			Related:\r\n			- <tplPaginatePrevious>\r\n			- <paginateSplitterCharacter>\r\n		*/\r\n		$tplPaginateNextOff = isset($tplPaginateNextOff)? $ditto->template->fetch($tplPaginateNextOff) : \"<span class=\'ditto_next_off ditto_off\'>[+lang:next+]</span>\";\r\n		/*\r\n			Param: tplPaginateNextOff\r\n\r\n			Purpose:\r\n			Template for the inside of the next link\r\n\r\n			Options:\r\n			- Any valid chunk name\r\n			- Code via @CODE\r\n			- File via @FILE\r\n\r\n			Placeholders:\r\n			lang:next - value of \'next\' from the language file\r\n		\r\n			Related:\r\n			- <tplPaginatePrevious>\r\n			- <paginateSplitterCharacter>\r\n		*/\r\n		$tplPaginatePreviousOff = isset($tplPaginatePreviousOff)? $ditto->template->fetch($tplPaginatePreviousOff) : \"<span class=\'ditto_previous_off ditto_off\'>[+lang:previous+]</span>\";\r\n		/*\r\n			Param: tplPaginatePreviousOff\r\n\r\n			Purpose:\r\n			Template for the previous link when it is off\r\n\r\n			Options:\r\n			- Any valid chunk name\r\n			- Code via @CODE\r\n			- File via @FILE\r\n\r\n			Placeholders:\r\n			lang:previous - value of \'prev\' from the language file\r\n	\r\n			Related:\r\n			- <tplPaginatePrevious>\r\n			- <paginateSplitterCharacter>\r\n		*/\r\n		$tplPaginatePage = isset($tplPaginatePage)? $ditto->template->fetch($tplPaginatePage) : \"<a class=\'ditto_page\' href=\'[+url+]\'>[+page+]</a>\";\r\n		/*\r\n			Param: tplPaginatePage\r\n\r\n			Purpose:\r\n			Template for the page link\r\n\r\n			Options:\r\n			- Any valid chunk name\r\n			- Code via @CODE\r\n			- File via @FILE\r\n\r\n			Placeholders:\r\n			url - url for the page\r\n			page - number of the page\r\n	\r\n			Related:\r\n			- <tplPaginatePrevious>\r\n			- <paginateSplitterCharacter>\r\n		*/\r\n		$tplPaginateCurrentPage = isset($tplPaginateCurrentPage)? $ditto->template->fetch($tplPaginateCurrentPage) : \"<span class=\'ditto_currentpage\'>[+page+]</span>\";\r\n		/*\r\n			Param: tplPaginateCurrentPage\r\n\r\n			Purpose:\r\n			Template for the current page link\r\n\r\n			Options:\r\n			- Any valid chunk name\r\n			- Code via @CODE\r\n			- File via @FILE\r\n\r\n			Placeholders:\r\n			page - number of the page\r\n	\r\n			Related:\r\n			- <tplPaginatePrevious>\r\n			- <paginateSplitterCharacter>\r\n		*/\r\n		\r\n		$ditto->paginate($start, $stop, $total, $display, $tplPaginateNext, $tplPaginatePrevious, $tplPaginateNextOff, $tplPaginatePreviousOff, $tplPaginatePage, $tplPaginateCurrentPage, $paginateAlwaysShowLinks, $paginateSplitterCharacter);\r\n			// generate the pagination placeholders\r\n	}\r\n\r\n	$dbFields = $ditto->fields[\"display\"][\"db\"];\r\n		// get the database fields\r\n	$TVs = $ditto->fields[\"display\"][\"tv\"];\r\n		// get the TVs\r\n	\r\n	switch($orderBy[\'parsed\'][0][1]) {\r\n		case \"DESC\":\r\n			$stop = ($ditto->prefetch === false) ? $stop + $start + $offset : $stop + $offset; \r\n			$start += $offset;\r\n		break;\r\n		case \"ASC\":\r\n			$start += $offset;\r\n			$stop += $start;\r\n		break;\r\n	}\r\n\r\n	if ($ditto->prefetch !== false) {\r\n		$documentIDs = array_slice($documentIDs,$start,$stop);\r\n			// set the document IDs equal to the trimmed array\r\n		$dbFields = array_diff($dbFields,$ditto->prefetch[\"fields\"][\"db\"]);\r\n			// calculate the difference between the database fields and those already prefetched\r\n		$dbFields[] = \"id\";\r\n			// append id to the db fields array\r\n		$TVs = array_diff($TVs,$ditto->prefetch[\"fields\"][\"tv\"]);\r\n			// calculate the difference between the tv fields and those already prefetched\r\n		$start = 0;\r\n		$stop = min($display,($queryLimit != 0) ? $queryLimit : $display,count($documentIDs));\r\n	} else {\r\n		$queryLimit = ($queryLimit == 0) ? \"\" : $queryLimit;\r\n	}\r\n	\r\n	$resource = $ditto->getDocuments($documentIDs, $dbFields, $TVs, $orderBy, $showPublishedOnly, 0, $hidePrivate, $where, $queryLimit, $keywords, $randomize, $dateSource);\r\n		// retrieves documents\r\n	$output = $header;\r\n		// initialize the output variable and send the header\r\n\r\n	if ($resource) {\r\n		if ($randomize != \"0\" && $randomize != \"1\") {\r\n			$resource = $ditto->weightedRandom($resource,$randomize,$stop);\r\n				// randomize the documents\r\n		}\r\n		\r\n		$resource = array_values($resource);\r\n\r\n		for ($x=$start;$x<$stop;$x++) {\r\n			$template = $ditto->template->determine($templates,$x,0,$stop,$resource[$x][\"id\"]);\r\n				// choose the template to use and set the code of that template to the template variable\r\n			$renderedOutput = $ditto->render($resource[$x], $template, $removeChunk, $dateSource, $dateFormat, $placeholders,$phx,abs($start-$x));\r\n				// render the output using the correct template, in the correct format and language\r\n			$modx->setPlaceholder($dittoID.\"item[\".abs($start-$x).\"]\",$renderedOutput);\r\n			/*\r\n				Placeholder: item[x]\r\n\r\n				Content:\r\n				Individual items rendered output\r\n			*/\r\n			$output .= $renderedOutput;\r\n				// send the rendered output to the buffer\r\n		}\r\n	} else {\r\n		$output .= $ditto->noResults($noResults,$paginate);\r\n			// if no documents are found return a no documents found string\r\n	}\r\n	$output .= $footer;\r\n		// send the footer\r\n\r\n	// ---------------------------------------------------\r\n	// Save Object\r\n	// ---------------------------------------------------\r\n\r\n	if($save) {\r\n		$modx->setPlaceholder($dittoID.\"ditto_object\", $ditto);\r\n		$modx->setPlaceholder($dittoID.\"ditto_resource\", ($save == \"1\") ? array_slice($resource,$display) : $resource);\r\n	}\r\n} else {\r\n	$output = $header.$ditto->noResults($noResults,$paginate).$footer;\r\n}\r\n// ---------------------------------------------------\r\n// Handle Debugging\r\n// ---------------------------------------------------\r\n\r\nif ($debug == 1) {\r\n	$ditto_params = func_get_args();\r\n	if (!isset($_GET[\"ditto_\".$dittoID.\"debug\"])) {\r\n	$_SESSION[\"ditto_debug_$dittoID\"] = $ditto->debug->render_popup($ditto, $ditto_base, $ditto_version, $ditto_params[1], $documentIDs, array(\"db\"=>$dbFields,\"tv\"=>$TVs), $display, $templates, $orderBy, $start, $stop, $total,$filter,$resource);\r\n	}\r\n	if (isset($_GET[\"ditto_\".$dittoID.\"debug\"])) {\r\n		switch ($_GET[\"ditto_\".$dittoID.\"debug\"]) {\r\n			case \"open\" :\r\n				exit($_SESSION[\"ditto_debug_$dittoID\"]);\r\n			break;\r\n			case \"save\" :\r\n				$ditto->debug->save($_SESSION[\"ditto_debug_$dittoID\"],\"ditto\".strtolower($ditto_version).\"_debug_doc\".$modx->documentIdentifier.\".html\");\r\n			break;\r\n		}\r\n	} else {\r\n		$output = $ditto->debug->render_link($dittoID,$ditto_base).$output;\r\n	}\r\n}\r\n\r\nreturn ($save != 3) ? $output : \"\";','0','','');
INSERT INTO `modx_site_snippets` VALUES ('4','eForm','<strong>1.4.4.5</strong> Robust form parser/processor with validation, multiple sending options, chunk/page support for forms and reports, and file uploads.','0','0','0','# eForm 1.4.4.5 - Electronic Form Snippet\r\n# Original created by Raymond Irving 15-Dec-2004.\r\n# Version 1.3+ extended by Jelle Jager (TobyL) September 2006\r\n# -----------------------------------------------------\r\n# local version: $Id: eform.snippet.tpl,v 1.2 2006/11/22 14:53:38 jelle Exp $\r\n# Captcha image support - thanks to Djamoer\r\n# Multi checkbox, radio, select support - thanks to Djamoer\r\n# Form Parser and extened validation - by Jelle Jager\r\n#\r\n# see eform/docs/eform.htm for history, usage and examples\r\n#\r\n\r\n# Set Snippet Paths\r\n$snipFolder = isset($snipFolder)?$snipFolder:\'eform\';\r\n$snipPath = $modx->config[\"base_path\"].\'assets/snippets/\'.$snipFolder.\'/\';\r\n\r\n\r\n# check if inside manager\r\nif ($modx->isBackend()) {\r\nreturn \'\'; # don\'t go any further when inside manager\r\n}\r\n\r\n//tidying up some casing errors in parameters\r\nif(isset($eformOnValidate)) $eFormOnValidate = $eformOnValidate;\r\nif(isset($eformOnBeforeMailSent)) $eFormOnBeforeMailSent = $eformOnBeforeMailSent;\r\nif(isset($eformOnMailSent)) $eFormOnMailSent = $eformOnMailSent;\r\nif(isset($eformOnValidate)) $eFormOnValidate = $eformOnValidate;\r\nif(isset($eformOnBeforeFormMerge)) $eFormOnBeforeFormMerge = $eformOnBeforeFormMerge;\r\nif(isset($eformOnBeforeFormParse)) $eFormOnBeforeFormParse = $eformOnBeforeFormParse;\r\n//for sottwell :)\r\nif(isset($eFormCSS)) $cssStyle = $eFormCSS;\r\n\r\n# Snippet customize settings\r\n$params = array (\r\n   // Snippet Path\r\n   \'snipPath\' => $snipPath, //includes $snipFolder\r\n     \'snipFolder\' => $snipFolder,\r\n\r\n// eForm Params\r\n   \'vericode\' => isset($vericode)? $vericode:\"\",\r\n   \'formid\' => isset($formid)? $formid:\"\",\r\n   \'from\' => isset($from)? $from:$modx->config[\'emailsender\'],\r\n   \'fromname\' => isset($fromname)? $fromname:$modx->config[\'site_name\'],\r\n   \'to\' => isset($to)? $to:$modx->config[\'emailsender\'],\r\n   \'cc\' => isset($cc)? $cc:\"\",\r\n   \'bcc\' => isset($bcc)? $bcc:\"\",\r\n   \'subject\' => isset($subject)? $subject:\"\",\r\n   \'ccsender\' => isset($ccsender)?$ccsender:0,\r\n   \'sendirect\' => isset($sendirect)? $sendirect:0,\r\n   \'mselector\' => isset($mailselector)? $mailselector:0,\r\n   \'mobile\' => isset($mobile)? $mobile:\'\',\r\n   \'mobiletext\' => isset($mobiletext)? $mobiletext:\'\',\r\n   \'autosender\' => isset($autosender)? $autosender:$from,\r\n   \'autotext\' => isset($automessage)? $automessage:\"\",\r\n   \'category\' => isset($category)? $category:0,\r\n   \'keywords\' => isset($keywords)? $keywords:\"\",\r\n   \'gid\' => isset($gotoid)? $gotoid:$modx->documentIdentifier,\r\n   \'noemail\' => isset($noemail)? ($noemail):false,\r\n   \'saveform\' => isset($saveform)? ($saveform? true:false):true,\r\n   \'tpl\' => isset($tpl)? $tpl:\"\",\r\n   \'report\' => isset($report)? $report:\"\",\r\n   \'allowhtml\' => isset($allowhtml)? $allowhtml:0,\r\n   //Added by JJ\r\n   \'replyto\' => isset($replyto)? $replyto:\"\",\r\n   \'language\' => isset($language)? $language:$modx->config[\'manager_language\'],\r\n   \'thankyou\' => isset($thankyou)? $thankyou:\"\",\r\n   \'isDebug\' => isset($debug)? $debug:0,\r\n   \'reportAbuse\' => isset($reportAbuse)? $reportAbuse:false,\r\n   \'disclaimer\' => isset($disclaimer)?$disclaimer:\'\',\r\n   \'sendAsHtml\' => isset($sendAsHtml)?$sendAsHtml:false,\r\n   \'sendAsText\' => isset($sendAsText)?$sendAsText:false,\r\n   \'sessionVars\' => isset($sessionVars)?$sessionVars:false,\r\n   \'postOverides\' => isset($postOverides)?$postOverides:0,\r\n   \'eFormOnBeforeMailSent\' => isset($eFormOnBeforeMailSent)?$eFormOnBeforeMailSent:\'\',\r\n   \'eFormOnMailSent\' => isset($eFormOnMailSent)?$eFormOnMailSent:\'\',\r\n   \'eFormOnValidate\' => isset($eFormOnValidate)?$eFormOnValidate:\'\',\r\n   \'eFormOnBeforeFormMerge\' => isset($eFormOnBeforeFormMerge)?$eFormOnBeforeFormMerge:\'\',\r\n   \'eFormOnBeforeFormParse\' => isset($eFormOnBeforeFormParse)?$eFormOnBeforeFormParse:\'\',\r\n   \'cssStyle\' => isset($cssStyle)?$cssStyle:\'\',\r\n   \'jScript\' => isset($jScript)?$jScript:\'\',\r\n   \'submitLimit\' => (isset($submitLimit) &&  is_numeric($submitLimit))?$submitLimit*60:0,\r\n   \'protectSubmit\' => isset($protectSubmit)?$protectSubmit:1,\r\n   \'requiredClass\' => isset($requiredClass)?$requiredClass:\"required\",\r\n   \'invalidClass\' => isset($invalidClass)?$invalidClass:\"invalid\",\r\n   \'runSnippet\' => ( isset($runSnippet) && !is_numeric($runSnippet) )?$runSnippet:\'\',\r\n   \'autoSenderName\' => isset($autoSenderName)?$autoSenderName:\'\',\r\n   \'version\' => \'1.4.4\'\r\n);\r\n\r\n# Start processing\r\n\r\ninclude_once ($snipPath.\"eform.inc.php\");\r\n\r\n$output = eForm($modx,$params);\r\n\r\n# Return\r\nreturn $output;','0','','');
INSERT INTO `modx_site_snippets` VALUES ('5','FirstChildRedirect','<strong>1.0</strong> Automatically redirects to the first child of a folder document.','0','0','0','/**\r\n * @name FirstChildRedirect\r\n * @author Jason Coward <jason@opengeek.com>\r\n * @modified-by Ryan Thrash <ryan@vertexworks.com>\r\n * @license Public Domain\r\n * @version 1.0\r\n * \r\n * This snippet redirects to the first child document of a folder in which this\r\n * snippet is included within the content (e.g. [!FirstChildRedirect!]).  This\r\n * allows MODx folders to emulate the behavior of real folders since MODx\r\n * usually treats folders as actual documents with their own content.\r\n * \r\n * Modified to make Doc ID a required parameter... now defaults to the current \r\n * Page/Folder you call the snippet from.\r\n * \r\n * &docid=`12` \r\n * Use the docid parameter to have this snippet redirect to the\r\n * first child document of the specified document.\r\n */\r\n\r\n$docid = (isset($docid))? $docid: $modx->documentIdentifier;\r\n\r\n$children= $modx->getActiveChildren($docid, \'menuindex\', \'ASC\');\r\nif (!$children === false) {\r\n    $firstChild= $children[0];\r\n    $firstChildUrl= $modx->makeUrl($firstChild[\'id\']);\r\n} else {\r\n    $firstChildUrl= $modx->makeUrl($modx->config[\'site_start\']);\r\n}\r\nreturn $modx->sendRedirect($firstChildUrl);\r\n','0','','');
INSERT INTO `modx_site_snippets` VALUES ('6','Jot','<strong>1.1.4</strong> User comments with moderation and email subscription.','0','0','0','/*####\r\n#\r\n#   Name: Jot\r\n#   Version: 1.1.4\r\n#   Author: Armand \"bS\" Pondman (apondman@zerobarrier.nl)\r\n#   Date: Aug 04, 2008\r\n#\r\n# Latest Version: http://modxcms.com/Jot-998.html\r\n# Jot Demo Site: http://projects.zerobarrier.nl/modx/\r\n# Documentation: http://wiki.modxcms.com/index.php/Jot (wiki)\r\n#\r\n####*/\r\n\r\n$jotPath = $modx->config[\'base_path\'] . \'assets/snippets/jot/\';\r\ninclude_once($jotPath.\'jot.class.inc.php\');\r\n\r\n$Jot = new CJot;\r\n$Jot->VersionCheck(\"1.1.4\");\r\n$Jot->Set(\"path\",$jotPath);\r\n$Jot->Set(\"action\", $action);\r\n$Jot->Set(\"postdelay\", $postdelay);\r\n$Jot->Set(\"docid\", $docid);\r\n$Jot->Set(\"tagid\", $tagid);\r\n$Jot->Set(\"subscribe\", $subscribe);\r\n$Jot->Set(\"moderated\", $moderated);\r\n$Jot->Set(\"captcha\", $captcha);\r\n$Jot->Set(\"badwords\", $badwords);\r\n$Jot->Set(\"bw\", $bw);\r\n$Jot->Set(\"sortby\", $sortby);\r\n$Jot->Set(\"numdir\", $numdir);\r\n$Jot->Set(\"customfields\", $customfields);\r\n$Jot->Set(\"guestname\", $guestname);\r\n$Jot->Set(\"canpost\", $canpost);\r\n$Jot->Set(\"canview\", $canview);\r\n$Jot->Set(\"canedit\", $canedit);\r\n$Jot->Set(\"canmoderate\", $canmoderate);\r\n$Jot->Set(\"trusted\", $trusted);\r\n$Jot->Set(\"pagination\", $pagination);\r\n$Jot->Set(\"placeholders\", $placeholders);\r\n$Jot->Set(\"subjectSubscribe\", $subjectSubscribe);\r\n$Jot->Set(\"subjectModerate\", $subjectModerate);\r\n$Jot->Set(\"subjectAuthor\", $subjectAuthor);\r\n$Jot->Set(\"notify\", $notify);\r\n$Jot->Set(\"notifyAuthor\", $notifyAuthor);\r\n$Jot->Set(\"validate\", $validate);\r\n$Jot->Set(\"title\", $title);\r\n$Jot->Set(\"authorid\", $authorid);\r\n$Jot->Set(\"css\", $css);\r\n$Jot->Set(\"cssFile\", $cssFile);\r\n$Jot->Set(\"cssRowAlt\", $cssRowAlt);\r\n$Jot->Set(\"cssRowMe\", $cssRowMe);\r\n$Jot->Set(\"cssRowAuthor\", $cssRowAuthor);\r\n$Jot->Set(\"tplForm\", $tplForm);\r\n$Jot->Set(\"tplComments\", $tplComments);\r\n$Jot->Set(\"tplModerate\", $tplModerate);\r\n$Jot->Set(\"tplNav\", $tplNav);\r\n$Jot->Set(\"tplNotify\", $tplNotify);\r\n$Jot->Set(\"tplNotifyModerator\", $tplNotifyModerator);\r\n$Jot->Set(\"tplNotifyAuthor\", $tplNotifyAuthor);\r\n$Jot->Set(\"tplSubscribe\", $tplSubscribe);\r\n$Jot->Set(\"debug\", $debug);\r\n$Jot->Set(\"output\", $output);\r\nreturn $Jot->Run();','0','','');
INSERT INTO `modx_site_snippets` VALUES ('7','ListIndexer','<strong>1.0.1</strong> Shows the most recent documents, highly flexible.','0','0','0','// --------------------\r\n// Snippet: ListIndexer\r\n// --------------------\r\n// Version: 1.0.1\r\n//\r\n// added in 1.0.1: hidePrivate (hide items from unauthorized users)\r\n//\r\n// Derived from ListIndex 0.6j by jaredc@honeydewdesign.com\r\n// Now supports Show In Menu\r\n//\r\n// This snippet was designed to be a VERY flexible way\r\n// of showing what has been recently added. You can use\r\n// this snippet to show news from one folder, or what has\r\n// been recently added site wide. You can even show what\'s\r\n// new in a \'section\' (everything under a defined folder)!\r\n//\r\n\r\n// Configuration Settings\r\n\r\n   // Set the following snippet defaults the way you would normally\r\n   // use this snippet, then use snippet variables in other situations\r\n   // to override the default behaviors.\r\n\r\n   // GENERAL OPTIONS\r\n\r\n   // $rootFolder [ NULL | string (comma separated page id\'s) ]\r\n   // Decide which folder to pull recent documents from.\r\n   // If you want to specify a few folders, comma separate them.\r\n   // The default NULL will set current page as root. Using 0\r\n   // would search site wide. Can be set with snippet parameter\r\n   // $LIn_root like:\r\n   // [[ListIndexer?LIn_root=3,6,88]] \r\n   $rootFolder = NULL;\r\n\r\n   // $descendentDepth [ int ]\r\n   // How many levels below the root folder(s) do you want to\r\n   // include? Can be overridden in snippet call with LIn_depth:\r\n   // [[ListIndexer?LIn_depth=2]]\r\n   // Uncomment one of these common two or create your own:\r\n   // $descendentDepth = 1; // just children of root folder(s)\r\n   $descendentDepth = 0; // all decendents of root folder(s)\r\n\r\n   // $seeThruUnpub [ true | false ]\r\n   // When using descendents, do you want to consider files below\r\n   // an unpublished (hidden) folder? Usually no. But you decide.\r\n   // Snippet parameter: LIn_seeThru\r\n   // [[ListIndexer?LIn_seeThru=1]]\r\n   $seeThruUnpub = false;\r\n\r\n   // $seeShowInMenu [ true | false ]\r\n   // When using descendents, do you want to consider files flagged\r\n   // to be hidden from the menus? Usually no. But you decide.\r\n   // Snippet parameter: LIn_seeShowInMenu\r\n   // [[ListIndexer?LIn_seeShowInMenu=1]]\r\n   $seeShowInMenu = false;\r\n   \r\n   // $hidePrivate [ true | false ]\r\n   // Hide items from users that don\'t have appropriate\r\n   // rights to view. Usually true. But you decide.\r\n   // Snippet parameter: LIn_hidePrivate\r\n   // [[ListIndexer?LIn_hidePrivate=0]]\r\n   $hidePrivate = true;\r\n\r\n   // $mode [ \'short\' | \'full\' ]\r\n   // Defines whether this list should be a full, paged\r\n   // list of all documents, or a short list of the most\r\n   // recent few (how many will be defined next). Can be\r\n   // overridden in snippet call with $LIn_mode:\r\n   // [[ListIndexer?LIn_mode=full]]\r\n   $mode = \'short\';\r\n   \r\n   // $sortBy [ \'alpha\' | \'date\' | \'menuindex\' ]\r\n   // The default date will sort by most recent items first, but\r\n   // by using the \'alpha\' option, and using full mode, you could\r\n   // use this to create an index, or directory.\r\n   // Settable with snippet call $LIn_sort:\r\n   // [[ListIndexer?LIn_sort=alpha]]\r\n   $sortBy = \'date\';\r\n   \r\n   // $sortDir [ \'ASC\' | \'DESC\' ]\r\n   // Sort direction ascending or descending. Is applied to whatever $sortBy\r\n   // field you have chosen above. If left blank, menuindex and alpha will sort\r\n   // ASC and date DESC.\r\n   // $LIn_dir in snippet call:\r\n   // [[ListIndexer?LIn_dir=ASC&LIn_sortBy=menuindex]]\r\n   $sortDir = \'\';\r\n\r\n   // WHAT TO DISPLAY\r\n\r\n   // $defaultTitle [ string ]\r\n   // If you want a default title for your list\r\n   // you can declare it here. Or use an empty\r\n   // string to leave this off. This can be overridden\r\n   // in the snippet call with the variable $LIn_title:\r\n   // [[ListIndexer?LIn_title=Some new title]]\r\n   $defaultTitle = \'\';\r\n\r\n   // $shortDesc [ true | false ]\r\n   // Show the description on the short list, or not. Snippet\r\n   // parameter $LIn_sDesc:\r\n   // [[ListIndexer?LIn_sDesc=0]]\r\n   $shortDesc = true;\r\n\r\n   // $fullDesc [ true | false ]\r\n   // Show the description on the full list, or not. Snippet\r\n   // parameter $LIn_fDesc:\r\n   // [[ListIndexer?LIn_fDesc=0]]\r\n   $fullDesc = true;\r\n\r\n   // $linkToIndex [ int ]\r\n   // If you have a page set up as an \'index\' for all the \r\n   // documents in this list, you can link to it by specifying \r\n   // its id- can also be set in snippet call with LIn_link:\r\n   // [[ListIndexer?LIn_link=8]]\r\n   // The default 0 will eliminate this link\r\n   $linkToIndex = 0;\r\n\r\n   // $indexText [ string ]\r\n   // If you want a link to an index (probably a page running this\r\n   // snippet in \"full\" mode), you can declare what you want that\r\n   // link to say here. Or in the snippet call with LIn_indexText:\r\n   // [[ListIndexer?LIn_indexText=Financial News Index]]\r\n   $indexText = \'Index\';\r\n\r\n   // $showCreationDate [ true | false ]\r\n   // Decide to include date of creation in output or not. From\r\n   // snippet call $LIn_showDate use 1 (true) or 0 (false)\r\n   // [[ListIndexer?LIn_showDate=1]]\r\n   $showCreationDate = true;\r\n\r\n   // $dateFormat [ string ]\r\n   // Used to define how date will be displayed (if using date)\r\n   // Y = 4 digit year     y = 2 digit year\r\n   // M = Jan - Dec        m = 01 - 12\r\n   // D = Sun - Sat        d = 01 -31\r\n   // Other standard PHP characters may be used\r\n   $dateFormat = \'Y.m.d\';\r\n\r\n   // $shortQty [ int ]\r\n   // Number of entries to list in the short list. Can be\r\n   // overridden in snippet call with $LIn_sQty:\r\n   //[[ListIndexer?LIn_sQty=3]]\r\n   $shortQty = 3;\r\n\r\n   // PAGING\r\n\r\n   // $fullQty [ int ]\r\n   // Number of entries PER PAGE to list in the full list\r\n   // Can be overridden in snippet call with $LIn_fQty:\r\n   // [[ListIndexer?LIn_fQty=20]]\r\n   // To show all set to 0 here or in snippet call\r\n   $fullQty = 10;\r\n   \r\n   // $pageSeparator [ string ]\r\n   // What you want your page number links to be separated by.\r\n   // You NEED to include spaces if you want them. They are NOT\r\n   // created automatically to facilitate styling ability.\r\n   // For instance, \" | \" will render links like:\r\n   // 1 | 2 | 3 | 4\r\n   $pageSeparator = \" | \";\r\n   \r\n   // $pgPosition [ \'top\' | \'bottom\' | \'both\']\r\n   // Pick where you want your pagination links to appear.\r\n   $pgPosition = \'both\';\r\n\r\n   // PERFORMANCE\r\n\r\n   // $useFastUrls [ true | false ]\r\n   // IMPORTANT- using fast urls will reduce database queries\r\n   // and improve performance WHEN IN FULL MODE ONLY and \r\n   // should NOT be used when multiple instances of this snippet\r\n   // appear on the same page. With snippet call LIn_fast use 1\r\n   // (true) or 0 (false)\r\n   // [[ListIndexer?LIn_fast=0]]\r\n   $useFastUrls = false;\r\n\r\n   // $newLinesForLists [ true | false ]\r\n   // Depending on how you want to style your list, you may\r\n   // or may not want your <li>s on new lines. Generally, if you\r\n   // are displaying then inline (horizontal, you do not want new\r\n   // lines, but standard vertical block styling you do. This is\r\n   // for IE, real browsers don\'t care.\r\n   $newLinesForLists = true;\r\n\r\n// Styles\r\n//\r\n// The following are the styles included in this snippet. It is up\r\n// to you to include these styles in your stylesheet to get them to\r\n// look the way you want.\r\n\r\n   // div.LIn_title {}          List title div\r\n   // ul.LIn_fullMode {}        UL class\r\n   // ul.LIn_shortMode {}       UL class\r\n   // span.LIn_date {}          Span surrounding pub/created date\r\n   // span.LIn_desc {}          Span surrounding description\r\n   // div.LIn_pagination        Div surrounding pagination links\r\n   // span.LIn_currentPage {}   Span surrounding current page of\r\n   //                           pagination (which wouldn\'t be css-able\r\n   //                           by virtue of its <a> tag)\r\n\r\n\r\n// **********************************************************************\r\n// END CONFIG SETTINGS\r\n// THE REST SHOULD TAKE CARE OF ITSELF\r\n// **********************************************************************\r\n\r\n// Take care of IE list issue\r\n$ie = ($newLinesForLists)? \"\\n\" : \'\' ;\r\n\r\n// Use snippet call defined variables if set\r\n$activeTitle = (isset($LIn_title))? $LIn_title : $defaultTitle ;\r\n$mode = (isset($LIn_mode))? $LIn_mode : $mode ;\r\n$descendentDepth = (isset($LIn_depth))? $LIn_depth : $descendentDepth ;\r\n$seeThruUnpub = (isset($LIn_seeThru))? $LIn_seeThru : $seeThruUnpub ;\r\n$seeShowInMenu = (isset($LIn_seeShowInMenu))? $LIn_seeShowInMenu : $seeShowInMenu ;\r\n$hidePrivate = (isset($LIn_hidePrivate))? $LIn_hidePrivate : $hidePrivate;\r\n$linkToIndex = (isset($LIn_link))? $LIn_link : $linkToIndex ;\r\n$rootFolder = (isset($LIn_root))? $LIn_root : $rootFolder ;\r\n$shortQty = (isset($LIn_sQty))? $LIn_sQty : $shortQty ;\r\n$fullQty = (isset($LIn_fQty))? $LIn_fQty : $fullQty ;\r\n$showCreationDate = (isset($LIn_showDate))? $LIn_showDate : $showCreationDate ;\r\n$indexText = (isset($LIn_indexText))? $LIn_indexText : $indexText ;\r\n$useFastUrls = (isset($LIn_fast))? $LIn_fast : $useFastUrls ;\r\n$sortBy = (isset($LIn_sort))? $LIn_sort : $sortBy;\r\n$shortDesc = (isset($LIn_sDesc))? $LIn_sDesc : $shortDesc ;\r\n$fullDesc = (isset($LIn_fDesc))? $LIn_fDesc : $fullDesc ;\r\n$sortDir = (isset($LIn_dir))? $LIn_dir : $sortDir ;\r\nif ($sortDir == \'\') $sortDir = ($sortBy == \'date\')? \'DESC\' : \'ASC\' ;\r\n\r\n\r\n// Make useful variable shortcut for the content table\r\n//$tbl = $modx->dbConfig[\'dbase\'] . \".\" . $modx->dbConfig[\'table_prefix\'] . \"site_content\";\r\n$tblsc = $modx->getFullTableName(\"site_content\");\r\n$tbldg = $modx->getFullTableName(\"document_groups\");\r\n\r\n// Initialize output\r\n$output = \'\';\r\n\r\n// ---------------------------------------------------\r\n// ---------------------------------------------------\r\n// Query db for parent folders, or not. First check to\r\n// see if a querystring cheat has been provided- this\r\n// should speed things up considerably when using this\r\n// in full mode. (a.k.a. fastUrls)\r\n// ---------------------------------------------------\r\n// ---------------------------------------------------\r\n$inFolder= isset($_GET[\'LIn_f\'])? $_GET[\'LIn_f\']: 0;\r\nif ((!$inFolder && $useFastUrls) || !$useFastUrls ){\r\n  // Only run all the database queries if we don\'t already\r\n  // know the folders AND fastUrls are desired.\r\n\r\n  // ---------------------------------------------------\r\n  // Seed list of viable parents\r\n  // ---------------------------------------------------\r\n\r\n  if ($rootFolder == NULL){\r\n    $rootFolder = $modx->documentIdentifier;\r\n  }\r\n  // Set root level parent array\r\n  $seedArray = explode(\',\',$rootFolder);\r\n  $parentsArray = array();\r\n  foreach($seedArray AS $seed){\r\n    $parentsArray[\'level_0\'][] = $seed;\r\n  }\r\n\r\n  // ---------------------------------------------------\r\n  // Make array of all allowed parents\r\n  // ---------------------------------------------------\r\n\r\n  // Process valid parents\r\n  $levelCounter = 1;\r\n\r\n  while (((count($parentsArray) < $descendentDepth) || ($descendentDepth == 0)) && ($levelCounter <= count($parentsArray)) && ($levelCounter < 10)){\r\n\r\n    // Find all decendant parents for this level\r\n    $pLevel = \'level_\'.($levelCounter - 1);\r\n    $tempLevelArray = $parentsArray[$pLevel];\r\n\r\n    foreach($tempLevelArray AS $p){\r\n\r\n      // Get children who are parents (isfolder = 1)\r\n      $validParentSql = \"\";\r\n      $validParentSql .= \"SELECT id FROM $tblsc sc WHERE \";\r\n      $validParentSql .= \"isfolder = 1 AND parent = $p \";\r\n      $validParentSql .= \"AND sc.deleted=0 \";\r\n      $validParentSql .= ($seeThruUnpub)? \";\" : \"AND sc.published = 1;\";\r\n\r\n      // Run statement\r\n      $rsTempParents = $modx->dbQuery($validParentSql);\r\n      // Get number of results\r\n      $countTempParents = $modx->recordCount($rsTempParents);\r\n\r\n      // If there are results, put them in an array\r\n      $tempValidArray = false;\r\n      if ($countTempParents){\r\n        for ($i=0;$i<$countTempParents;$i++){\r\n          $tempId = $modx->fetchRow($rsTempParents);\r\n          $tempValidArray[] = $tempId[\'id\'];\r\n        } // end while\r\n      } // end if\r\n\r\n	  // populate next level of array \r\n	  if ($tempValidArray){\r\n	    foreach($tempValidArray AS $kid){\r\n	      $kidLevel = \'level_\'.$levelCounter;\r\n	      $parentsArray[$kidLevel][] = $kid;\r\n	    } // end foreach\r\n\r\n	  } // end if\r\n    } // end foreach\r\n\r\n    // Do next level\r\n    $levelCounter++;\r\n\r\n  } // end while\r\n\r\n  // Finalize list of parents\r\n  $validParents = \'\';\r\n  foreach ($parentsArray AS $level){\r\n    foreach ($level AS $validP){\r\n      $validParents .= $validP . \',\';\r\n    }\r\n  }\r\n\r\n  // Remove trailing comma\r\n  $validParents = substr($validParents,0,strlen($validParents)-1);\r\n\r\n} else {\r\n  $validParents = $_GET[\'LIn_f\'];\r\n}\r\n\r\n// ---------------------------------------------------\r\n// Make appropriate SQL statement to pull recent items\r\n// ---------------------------------------------------\r\n\r\n// get document groups for current user\r\nif($docgrp = $modx->getUserDocGroups()) $docgrp = implode(\",\",$docgrp);\r\n\r\n$access = \" (\".($modx->isFrontend() ? \"sc.privateweb=0\":\"1=\'\".$_SESSION[\'mgrRole\'].\"\' OR sc.privatemgr=0\").\r\n          (!$docgrp ? \"\":\" OR dg.document_group IN ($docgrp)\").\") AND \";\r\n\r\n// Initialize\r\n$recentSql = \"\";\r\n$recentSql .= \"SELECT sc.id, pagetitle, description\";\r\n// Include pub_date or createdon date if date is desired\r\n$recentSql .= ($showCreationDate)? \", IF(pub_date > 0, pub_date, createdon) AS pubDate \": \" \" ;\r\n$recentSql .= \"FROM $tblsc sc LEFT JOIN $tbldg dg on dg.document = sc.id \";\r\n$recentSql .= \"WHERE \";\r\n$recentSql .= ($hidePrivate)? $access:\"\";\r\n// Look everywhere, or just under valid parents\r\n$recentSql .= (($rootFolder == 0) && $seeThruUnpub && ($descendentDepth == 0))? \"\" : \"parent IN ($validParents) AND \" ;\r\n// Published\r\n$recentSql .= \"sc.published = 1 \";\r\n// Show In Menu\r\n$recentSql .= ($seeShowInMenu)? \" \" : \" AND sc.hidemenu=0 \" ;\r\n// Not deleted\r\n$recentSql .= \"AND sc.deleted=0 \";\r\n// Choose sort method\r\nswitch ($sortBy){\r\n  case \'alpha\':\r\n    $recentSql .= \"ORDER BY pagetitle \";\r\n    break;\r\n  case \'menuindex\':\r\n    $recentSql .= \"ORDER BY menuindex \";\r\n    break;\r\n  default:\r\n    $recentSql .= \"ORDER BY IF(pub_date>0, pub_date, createdon) \";\r\n    break;\r\n}\r\n// Provide a sort direction\r\n$recentSql .= $sortDir;\r\n\r\n// If this is a short list, just pull a limited number\r\n$recentSql .= ($mode == \'short\')? \" LIMIT $shortQty;\" : \";\" ;\r\n\r\n// Run statement\r\n$rsRecent = $modx->dbQuery($recentSql);\r\n// Count records\r\n$recentLimit = $modx->recordCount($rsRecent);\r\n\r\n// ---------------------------------------------------\r\n// Generate pagination string if needed\r\n// ---------------------------------------------------\r\n$offsetParam = isset($_GET[\'LIn_o\'])? $_GET[\'LIn_o\']: 0;\r\n$offset = ($offsetParam && ($mode == \'full\'))? $offsetParam : 0 ;\r\n$pagination = \'\';\r\n\r\n// Don\'t bother unless there are enough records to justify it\r\nif ( ($mode == \'full\') && ($recentLimit > $fullQty) && ($fullQty) ){\r\n  $fullUrl = $_SERVER[\'REQUEST_URI\'];\r\n  $urlPieces = parse_url($fullUrl);\r\n  $urlPath = $urlPieces[\'path\'];\r\n  $otherQs = \'\';\r\n\r\n  if ($urlPieces[\'query\']){\r\n    foreach($_GET AS $qsKey=>$qsValue){\r\n	  if (($qsKey != \'LIn_o\') && ($qsKey != \'LIn_f\')){\r\n	    $otherQs .= \'&\'.$qsKey.\'=\'.$qsValue;\r\n	  }\r\n	}\r\n  } \r\n  \r\n  $fastUrl = $urlPath.\'?LIn_f=\'.$validParents.$otherQs;\r\n\r\n  // Determine number of pages needed to show results\r\n  $totalPages = ceil($recentLimit/$fullQty);\r\n  \r\n  // Make links\r\n  for ($j = 0 ; $j < $totalPages; $j++){\r\n    // only include links to OTHER pages, not current page\r\n    if($offset == $j*$fullQty){\r\n	  $pagination .= \'<span class=\"LIn_currentPage\">\'.($j+1) .\'</span>\';\r\n	} else {\r\n      $pagination .= \'<a href=\"\'.$fastUrl.\'&LIn_o=\'.($j*$fullQty).\'\" title=\"\'.($j+1).\'\">\'.($j+1) .\'</a>\';\r\n	}\r\n	if ($j < $totalPages-1){\r\n	  $pagination .= $pageSeparator;\r\n	}\r\n  }\r\n  \r\n  // Make final pagination link set in it\'s own div\r\n  $pagination = \'<div class=\"LIn_pagination\">\'.\"\\n\".$pagination.\"\\n</div>\\n\";\r\n  \r\n}\r\n\r\n\r\n// ---------------------------------------------------\r\n// Create title if wanted\r\n// ---------------------------------------------------\r\n\r\nif ($activeTitle){\r\n  $output .= \'<div class=\"LIn_title\">\'.$activeTitle.\'</div>\'.\"\\n\";\r\n}\r\n\r\n// ---------------------------------------------------\r\n// Create list of recent items\r\n// ---------------------------------------------------\r\n\r\n// Include pagination\r\n$output .= ($pgPosition == \'top\' || $pgPosition == \'both\')? $pagination : \'\' ;\r\n\r\n$output .= \'<ul class=\"LIn_\'.$mode.\'Mode\">\' . $ie;\r\n\r\n$recentCounter = $offset;\r\nif ($mode == \'short\') {\r\n  $recentCounterLimit = min($shortQty,$recentLimit);\r\n} else {\r\n  $recentCounterLimit = ($fullQty)? min(($fullQty+$offset),$recentLimit) : $recentLimit ;\r\n}\r\n\r\nwhile (($recentCounter < $recentCounterLimit) && $rsRecent && ($recentLimit > 0)){\r\n  mysql_data_seek($rsRecent,$recentCounter);\r\n  $recentRecord = $modx->fetchRow($rsRecent);\r\n  $output .= \'<li>\';\r\n  // Link to page\r\n  $output .= \'<a href=\"[~\'.$recentRecord[\'id\'].\'~]\" title=\"\'.strip_tags($recentRecord[\'pagetitle\']).\'\">\'.$recentRecord[\'pagetitle\'].\'</a> \';\r\n  // Date if desired\r\n  if ($showCreationDate){\r\n    $output .= \'<span class=\"LIn_date\">\'.date($dateFormat,$recentRecord[\'pubDate\']).\'</span> \';\r\n  }\r\n  // Description if desired\r\n  if ((($mode == \'short\') && ($shortDesc)) || (($mode == \'full\') && ($fullDesc))){\r\n   $output .= \'<span class=\"LIn_desc\">\'.$recentRecord[\'description\'].\'</span>\';\r\n  }\r\n  // wrap it up\r\n  $output .= \'</li>\' . $ie;\r\n  $recentCounter ++;\r\n}\r\n\r\n$output .= \'</ul>\' . $ie;\r\n\r\n$output .= ($pgPosition == \'bottom\' || $pgPosition == \'both\')? $pagination : \'\' ;\r\n\r\n// ---------------------------------------------------\r\n// Link to index\r\n// ---------------------------------------------------\r\n\r\nif ($linkToIndex) {\r\n\r\n  $output .= \'<div class=\"LIn_index\">\';\r\n  $output .= \'<a href=\"[~\'.$linkToIndex.\'~]\" title=\"\'.$indexText.\'\">\'.$indexText.\'</a>\';\r\n  $output .= \'</div>\';\r\n\r\n}\r\n\r\n// ---------------------------------------------------\r\n// Send to browser\r\n// ---------------------------------------------------\r\n\r\nreturn $output;','0','','');
INSERT INTO `modx_site_snippets` VALUES ('8','MemberCheck','<strong>1.0</strong> Selectively show chunks based on logged in Web User\' group memberships.','0','0','0','#::::::::::::::::::::::::::::::::::::::::\r\n# Snippet name: MemberCheck \r\n# Short Desc: checks logged in groups and displays a chunk\r\n# Version: 1.0\r\n# Created By Ryan Thrash (vertexworks.com)\r\n# Sanitized By Jason Coward (opengeek.com)\r\n#\r\n# Date: November 29, 2005\r\n#\r\n# Changelog: \r\n# Nov 29, 05 -- initial release\r\n# Jul 13, 06 -- adjusted Singleton to work under PHP4, added placeholder code (by: garryn)\r\n#\r\n#::::::::::::::::::::::::::::::::::::::::\r\n# Description: 	\r\n#	Checks to see if users belong to a certain group and \r\n#	displays the specified chunk if they do. Performs several\r\n#	sanity checks and allows to be used multiple times on a page.\r\n#\r\n# Params:\r\n#	&groups [array] (REQUIRED)\r\n#		array of webuser group-names to check against\r\n#\r\n#	&chunk [string] (REQUIRED)\r\n#		name of the chunk to use if passes the check\r\n#\r\n#	&ph [string] (optional)\r\n#		name of the placeholder to set instead of directly retuning chunk\r\n#\r\n#	&debug [boolean] (optional | false) \r\n#		turn on debug mode for extra troubleshooting\r\n#\r\n# Example Usage:\r\n#\r\n#	[[MemberCheck? &groups=`siteadmin, registered users` &chunk=`privateSiteNav` &ph=`MemberMenu` &debug=`true`]]\r\n#\r\n#	This would place the \'members-only\' navigation store in the chunk \'privateSiteNav\'\r\n#	into a placeholder (called \'MemberMenu\'). It will only do this as long as the user \r\n#	is logged in as a webuser and is a member of the \'siteadmin\' or the \'registered users\'\r\n#	groups. The optional debug parameter can be used to display informative error messages \r\n#	when configuring this snippet for your site. For example, if the developer had \r\n#	mistakenly typed \'siteowners\' for the first group, and none existed with debug mode on, \r\n#	it would have returned the error message: The group siteowners could not be found....\r\n#\r\n#::::::::::::::::::::::::::::::::::::::::\r\n\r\n# debug parameter\r\n$debug = isset ($debug) ? $debug : false;\r\n\r\n# check if inside manager\r\nif ($m = $modx->insideManager()) {\r\n	return \'\'; # don\'t go any further when inside manager\r\n}\r\n\r\nif (!isset ($groups)) {\r\n	return $debug ? \'<p>Error: No Group Specified</p>\' : \'\';\r\n}\r\n\r\nif (!isset ($chunk)) {\r\n	return $debug ? \'<p>Error: No Chunk Specified</p>\' : \'\';\r\n}\r\n\r\n# turn comma-delimited list of groups into an array\r\n$groups = explode(\',\', $groups);\r\n\r\nif (!class_exists(\'MemberCheck\')) {\r\n	class MemberCheck {\r\n		var $allGroups = NULL;\r\n		var $debug;\r\n\r\n		function getInstance($debug) {\r\n			static $instance;\r\n			if (!isset ($instance)) {\r\n				$instance = new MemberCheck($debug);\r\n			}\r\n			return $instance;\r\n		}\r\n\r\n		function MemberCheck($debug = false) {\r\n			global $modx;\r\n\r\n			$this->debug = $debug;\r\n			if ($debug) {\r\n				$this->allGroups = array ();\r\n				$tableName = $modx->getFullTableName(\'webgroup_names\');\r\n				$sql = \"SELECT name FROM $tableName\";\r\n				if ($rs = $modx->db->query($sql)) {\r\n					while ($row = $modx->db->getRow($rs)) {\r\n						array_push($this->allGroups, stripslashes($row[\'name\']));\r\n					}\r\n				}\r\n			}\r\n		}\r\n\r\n		function isValidGroup($groupName) {\r\n			$isValid = !(array_search($groupName, $this->allGroups) === false);\r\n			return $isValid;\r\n		}\r\n\r\n		function getMemberChunk(& $groups, $chunk) {\r\n			global $modx;\r\n			$o = \'\';\r\n			if (is_array($groups)) {\r\n				for ($i = 0; $i < count($groups); $i++) {\r\n					$groups[$i] = trim($groups[$i]);\r\n					if ($this->debug) {\r\n						if (!$this->isValidGroup($groups[$i])) {\r\n							return \"<p>The group <strong>\" . $groups[$i] . \"</strong> could not be found...</p>\";\r\n						}\r\n					}\r\n				}\r\n\r\n				$check = $modx->isMemberOfWebGroup($groups);\r\n\r\n				$chunkcheck = $modx->getChunk($chunk);\r\n\r\n				$o .= ($check && $chunkcheck) ? $chunkcheck : \'\';\r\n				if (!$chunkcheck)\r\n					$o .= $this->debug ? \"<p>The chunk <strong>$chunk</strong> not found...</p>\" : \'\';\r\n			} else {\r\n				$o .= \"<p>No valid group names were specified!</p>\";\r\n			}\r\n\r\n			return $o;\r\n		}\r\n	}\r\n}\r\n\r\n$memberCheck = MemberCheck :: getInstance($debug);\r\n\r\nif (!isset ($ph)) {\r\n	return $memberCheck->getMemberChunk($groups, $chunk);\r\n} else {\r\n	$modx->setPlaceholder($ph, $memberCheck->getMemberChunk($groups, $chunk));\r\n	return \'\';\r\n}','0','','');
INSERT INTO `modx_site_snippets` VALUES ('9','NewsPublisher','<strong>1.4</strong> Publish news articles directly from the web.','0','0','0','#::::::::::::::::::::::::::::::::::::::::\r\n#\r\n#  Snippet Name: NewsPublisher \r\n#  Short Desc: Create articles directly from front end (news, blogs, PR, etc.)\r\n#  Created By: Raymond Irving (xwisdom@yahoo.com), August 2005\r\n#\r\n#  Version: 1.4\r\n#  Modified: December 13, 2005\r\n#\r\n#  Changelog: \r\n#    Mar 05, 06 -- modx_ prefix removed [Mark]\r\n#    Dec 13, 05 -- Now inherrits web/manager docgroups thanks to Jared Carlow\r\n#\r\n#::::::::::::::::::::::::::::::::::::::::\r\n#  Description:     \r\n#    Checks to see if users belong to a certain group and \r\n#    displays the specified chunk if they do. Performs several\r\n#    sanity checks and allows to be used multiple times on a page.\r\n#    Only meant to be used once per page.\r\n#::::::::::::::::::::::::::::::::::::::::\r\n#  \r\n#  Parameters:\r\n#    &folder      - folder id where comments are stored\r\n#    &makefolder  - set to 1 to automatically convert the parent document to a folder. Defaults to 0\r\n#    &postid      - document id to load after posting news item. Defaults to the page created\r\n#    &canpost     - comma delimitted web groups that can post comments. leave blank for public posting\r\n#    &badwords    - comma delimited list of words not allowed in post\r\n#    &template    - name of template to use for news post\r\n#    &headertpl   - header template (chunk name) to be inserted at the begining of the news content\r\n#    &footertpl   - footer template (chunk name) to be inserted at the end of the news content\r\n#    &formtpl     - form template (chunk name)\r\n#    &rtcontent   - name of a richtext content form field \r\n#    &rtsummary   - name of a richtext summary form field \r\n#    &showinmenu  - sets the flag to true or false (1|0) as to whether or not it shows in the menu. defaults to false (0)\r\n#    &aliastitle  - set to 1 to use page title as alias suffix. Defaults to 0 - date created.\r\n#    &clearcache  - when set to 1 the system will automatically clear the site cache after publishing an article.\r\n#  \r\n#::::::::::::::::::::::::::::::::::::::::\r\n\r\n// get user groups that can post articles\r\n$postgrp = isset($canpost) ? explode(\",\",$canpost):array();\r\n$allowAnyPost = count($postgrp)==0 ? true : false;\r\n\r\n// get clear cache\r\n$clearcache  = isset($clearcache) ? 1:0;\r\n\r\n// get alias title\r\n$aliastitle  = isset($aliastitle) ? 1:0;\r\n\r\n// get folder id where we should store articles\r\n// else store in current document\r\n$folder = isset($folder) ? intval($folder):$modx->documentIdentifier;\r\n\r\n// set rich text content field\r\n$rtcontent = isset($rtcontent) ? $rtcontent:\'content\';\r\n\r\n// set rich text summary field\r\n$rtsummary = isset($rtsummary) ? $rtsummary:\'introtext\';\r\n\r\n// get header\r\n$header = isset($headertpl) ? \"{{\".$headertpl.\"}}\":\'\';\r\n\r\n// get footer\r\n$footer = isset($footertpl) ? \"{{\".$footertpl.\"}}\":\'\';\r\n\r\n// get postback status\r\n$isPostBack = isset($_POST[\'NewsPublisherForm\']) ? true:false;\r\n\r\n// get badwords\r\nif(isset($badwords)) {\r\n    $badwords = str_replace(\' \',\'\', $badwords);\r\n    $badwords = \"/\".str_replace(\',\',\'|\', $badwords).\"/i\";\r\n}\r\n\r\n// get menu status\r\n$hidemenu = isset($showinmenu) && $showinmenu==1 ? 0 : 1;\r\n\r\n// get template\r\n$template = isset($template) ? $modx->db->getValue(\'SELECT id FROM \'.$modx->getFullTableName(\'site_templates\').\' WHERE templatename=\\\'\'.mysql_escape_string($template).\'\\\'\'):$modx->config[\'default_template\'];\r\n\r\n$message = \'\';\r\n\r\n// get form template\r\nif(isset($formtpl)) $formTpl = $modx->getChunk($formtpl);\r\nif(empty($formTpl)) $formTpl = \'\r\n    <form name=\"NewsPublisher\" method=\"post\">\r\n        <input name=\"NewsPublisherForm\" type=\"hidden\" value=\"on\" />\r\n        Page title:<br /><input name=\"pagetitle\" type=\"text\" size=\"40\" value=\"[+pagetitle+]\" /><br />\r\n        Long title:<br /><input name=\"longtitle\" type=\"text\" size=\"40\" value=\"[+longtitle+]\" /><br />\r\n        Description:<br /><input name=\"description\" type=\"text\" size=\"40\" value=\"[+description+]\" /><br />\r\n        Published date:<br /><input name=\"pub_date\" type=\"text\" value=\"[+pub_date+]\" size=\"40\" readonly=\"readonly\" />\r\n        <a onClick=\"nwpub_cal1.popup();\" onMouseover=\"window.status=\\\'Select date\\\'; return true;\" onMouseout=\"window.status=\\\'\\\'; return true;\" style=\"cursor:pointer; cursor:hand\"><img align=\"absmiddle\" src=\"manager/media/style/MODxLight/images/icons/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Select date\" /></a>\r\n        <a onClick=\"document.NewsPublisher.pub_date.value=\\\'\\\'; return true;\" onMouseover=\"window.status=\\\'Remove date\\\'; return true;\" onMouseout=\"window.status=\\\'\\\'; return true;\" style=\"cursor:pointer; cursor:hand\"><img align=\"absmiddle\" src=\"manager/media/style/MODxLight/images/icons/cal_nodate.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Remove date\"></a><br />\r\n        Unpublished date:<br /><input name=\"unpub_date\" type=\"text\" value=\"[+unpub_date+]\" size=\"40\" readonly=\"readonly\" />\r\n        <a onClick=\"nwpub_cal2.popup();\" onMouseover=\"window.status=\\\'Select date\\\'; return true;\" onMouseout=\"window.status=\\\'\\\'; return true;\" style=\"cursor:pointer; cursor:hand\"><img align=\"absmiddle\" src=\"manager/media/style/MODxLight/images/icons/cal.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Select date\" /></a>\r\n        <a onClick=\"document.NewsPublisher.unpub_date.value=\\\'\\\'; return true;\" onMouseover=\"window.status=\\\'Remove date\\\'; return true;\" onMouseout=\"window.status=\\\'\\\'; return true;\" style=\"cursor:pointer; cursor:hand\"><img align=\"absmiddle\" src=\"manager/media/style/MODxLight/images/icons/cal_nodate.gif\" width=\"16\" height=\"16\" border=\"0\" alt=\"Remove date\"></a><br />\r\n        Summary:<br /><textarea name=\"introtext\" cols=\"50\" rows=\"5\">[+introtext+]</textarea><br />\r\n        Content:<br /><textarea name=\"content\" cols=\"50\" rows=\"8\">[+content+]</textarea><br />\r\n        <input name=\"send\" type=\"submit\" value=\"Submit\" />\r\n    </form>\r\n    <script language=\"JavaScript\" src=\"manager/media/script/datefunctions.js\"></script>\r\n    <script type=\"text/javascript\">\r\n        var elm_txt = {}; // dummy\r\n        var pub = document.forms[\"NewsPublisher\"].elements[\"pub_date\"];\r\n        var nwpub_cal1 = new calendar1(pub,elm_txt);\r\n        nwpub_cal1.path=\"[(base_url)]manager/media/\";\r\n        nwpub_cal1.year_scroll = true;\r\n        nwpub_cal1.time_comp = true;    \r\n\r\n        var unpub = document.forms[\"NewsPublisher\"].elements[\"unpub_date\"];\r\n        var nwpub_cal2 = new calendar1(unpub,elm_txt);\r\n        nwpub_cal2.path=\"[(base_url)]manager/media/\";\r\n        nwpub_cal2.year_scroll = true;\r\n        nwpub_cal2.time_comp = true;    \r\n    </script>\';\r\n\r\n\r\n// switch block\r\nswitch ($isPostBack) {\r\n    case true:\r\n        // process post back\r\n        // remove magic quotes from POST\r\n        if(get_magic_quotes_gpc()){\r\n            $_POST = array_map(\"stripslashes\", $_POST);\r\n        }   \r\n        if(trim($_POST[\'pagetitle\'])==\'\') $modx->webAlert(\'Missing page title.\');\r\n        elseif($_POST[$rtcontent]==\'\') $modx->webAlert(\'Missing news content.\');\r\n        else {\r\n            // get created date\r\n            $createdon = time();\r\n\r\n            // set alias name of document used to store articles\r\n            if(!$aliastitle) $alias = \'article-\'.$createdon;\r\n            else {\r\n                $alias = $modx->stripTags($_POST[\'pagetitle\']);\r\n                $alias = strtolower($alias);\r\n                $alias = preg_replace(\'/&.+?;/\', \'\', $alias); // kill entities\r\n                $alias = preg_replace(\'/[^\\.%a-z0-9 _-]/\', \'\', $alias);\r\n                $alias = preg_replace(\'/\\s+/\', \'-\', $alias);\r\n                $alias = preg_replace(\'|-+|\', \'-\', $alias);\r\n                $alias = trim($alias, \'-\');         \r\n                $alias = \'article-\'.$modx->db->escape($alias);\r\n            }\r\n\r\n            $user = $modx->getLoginUserName();\r\n            $userid = $modx->getLoginUserID();\r\n            if(!$user && $allowAnyPost) $user = \'anonymous\';\r\n\r\n            // check if user has rights\r\n            if(!$allowAnyPost && !$modx->isMemberOfWebGroup($postgrp)) {\r\n                return \'You are not allowed to publish articles\';\r\n            }\r\n\r\n            $allowedTags = \'<p><br><a><i><em><b><strong><pre><table><th><td><tr><img><span><div><h1><h2><h3><h4><h5><font><ul><ol><li><dl><dt><dd>\';\r\n\r\n            // format content\r\n            $content = $modx->stripTags($_POST[$rtcontent],$allowedTags);\r\n            $content = str_replace(\'[+user+]\',$user,$content);\r\n\r\n\r\n            $content = str_replace(\'[+createdon+]\',strftime(\'%d-%b-%Y %H:%M\',$createdon),$content);\r\n            foreach($_POST as $n=>$v) {\r\n                if(!empty($badwords)) $v = preg_replace($badwords,\'[Filtered]\',$v); // remove badwords\r\n                $v = $modx->stripTags(htmlspecialchars($v));\r\n                $v = str_replace(\"\\n\",\'<br />\',$v);\r\n                $content = str_replace(\'[+\'.$n.\'+]\',$v,$content);\r\n            }\r\n\r\n            $title = mysql_escape_string($modx->stripTags($_POST[\'pagetitle\']));\r\n            $longtitle = mysql_escape_string($modx->stripTags($_POST[\'longtitle\']));\r\n            $description = mysql_escape_string($modx->stripTags($_POST[\'description\']));\r\n            $introtext = mysql_escape_string($modx->stripTags($_POST[$rtsummary],$allowedTags));\r\n            $pub_date = $_POST[\'pub_date\'];\r\n            $unpub_date = $_POST[\'unpub_date\'];\r\n            $published = 1;\r\n\r\n            // check published date\r\n            if($pub_date==\"\") {\r\n                $pub_date=\"0\";\r\n            } else {\r\n                list($d, $m, $Y, $H, $M, $S) = sscanf($pub_date, \"%2d-%2d-%4d %2d:%2d:%2d\");\r\n                $pub_date = strtotime(\"$m/$d/$Y $H:$M:$S\");\r\n\r\n                if($pub_date < $createdon) {\r\n                    $published = 1;\r\n                }    elseif($pub_date > $createdon) {\r\n                    $published = 0; \r\n                }\r\n            }\r\n\r\n            // check unpublished date\r\n            if($unpub_date==\"\") {\r\n                $unpub_date=\"0\";\r\n            } else {\r\n                list($d, $m, $Y, $H, $M, $S) = sscanf($unpub_date, \"%2d-%2d-%4d %2d:%2d:%2d\");\r\n                $unpub_date = strtotime(\"$m/$d/$Y $H:$M:$S\");\r\n                if($unpub_date < $createdon) {\r\n                    $published = 0;\r\n                }\r\n            }\r\n\r\n            // set menu index\r\n            $mnuidx = $modx->db->getValue(\'SELECT MAX(menuindex)+1 as \\\'mnuidx\\\' FROM \'.$modx->getFullTableName(\'site_content\').\' WHERE parent=\\\'\'.$folder.\'\\\'\');\r\n            if($mnuidx<1) $mnuidx = 0;\r\n\r\n            // post news content\r\n            $flds = array(\r\n                \'pagetitle\'     => $title,\r\n                \'longtitle\'     => $longtitle,\r\n                \'description\' => $description,\r\n                \'introtext\'     => $introtext,\r\n                \'alias\'             => $alias,\r\n                \'parent\'            => $folder, \r\n                \'createdon\'     => $createdon,\r\n                \'createdby\'     => ($userid>0 ? $userid * -1:0),\r\n                \'editedon\'        => \'0\',\r\n                \'editedby\'        => \'0\',\r\n                \'published\'     => $published,\r\n                \'pub_date\'        => $pub_date,\r\n                \'unpub_date\'    => $unpub_date,\r\n                \'deleted\'         => \'0\',\r\n                \'hidemenu\'        => $hidemenu,\r\n                \'menuindex\'     => $mnuidx,\r\n                \'template\'        => $template,\r\n                \'content\'         => mysql_escape_string($header.$content.$footer)\r\n            );\r\n            $redirectid = $modx->db->insert($flds,$modx->getFullTableName(\'site_content\'));\r\n\r\n            // Doc group thing\r\n            // look in save_content.processor.php for tips (or below)\r\n            $lastInsertId = $modx->db->getInsertId();\r\n\r\n            // Get doc groups based on $folder (parent id)\r\n            $parentDocGroupsSql = \"SELECT * FROM \" . $modx->getFullTableName(\'document_groups\'). \" where document=\".$folder;\r\n            $parentDocGroupsRs = $modx->db->query($parentDocGroupsSql);\r\n            $parentDocGroupsLimit = $modx->db->getRecordCount($parentDocGroupsRs);\r\n            for ($pdgi = 0; $pdgi < $parentDocGroupsLimit; $pdgi++) { \r\n                $currentDocGroup = $modx->db->getRow($parentDocGroupsRs);\r\n                $parentDocGroupsArray[$pdgi] = $currentDocGroup[\'document_group\'];\r\n            }\r\n\r\n            // put the document in the document_groups it should be in\r\n            // check that up_perms are switched on!\r\n            if($modx->config[\'use_udperms\']==1) {\r\n                if(is_array($parentDocGroupsArray)) {\r\n                    foreach ($parentDocGroupsArray as $dgKey=>$dgValue) {\r\n                        $insertDocSql = \"INSERT INTO \".$modx->getFullTableName(\'document_groups\').\"(document_group, document) values(\".stripslashes($dgValue).\", $lastInsertId)\";\r\n                        $insertDocRs = $modx->db->query($insertDocSql);\r\n                        if(!$insertDocRs){\r\n                            exit;\r\n                        }\r\n                    } // end foreach\r\n                } // end if doc group array exists\r\n            } // end if perms are used\r\n\r\n            // Handle privateweb\r\n            $modx->db->query(\"UPDATE \".$modx->getFullTableName(\"site_content\").\" SET privateweb = 0 WHERE id=\'$lastInsertId\';\");\r\n            $privatewebSql =    \"\r\n                SELECT DISTINCT \".$modx->getFullTableName(\'document_groups\').\".document_group \r\n                FROM \".$modx->getFullTableName(\'document_groups\').\", \".$modx->getFullTableName(\'webgroup_access\').\" \r\n                WHERE \r\n                \".$modx->getFullTableName(\'document_groups\').\".document_group = \".$modx->getFullTableName(\'webgroup_access\').\".documentgroup \r\n                AND \r\n                \".$modx->getFullTableName(\'document_groups\').\".document = $lastInsertId;\";\r\n                $privatewebIds = $modx->db->getColumn(\"document_group\",$privatewebSql);\r\n                if(count($privatewebIds)>0) {\r\n                    $modx->db->query(\"UPDATE \".$modx->getFullTableName(\"site_content\").\" SET privateweb = 1 WHERE id = $lastInsertId;\");    \r\n                }\r\n\r\n                // And privatemgr\r\n                $modx->db->query(\"UPDATE \".$modx->getFullTableName(\"site_content\").\" SET privatemgr = 0 WHERE id=\'$lastInsertId\';\");\r\n                $privatemgrSql =    \"\r\n                    SELECT DISTINCT \".$modx->getFullTableName(\'document_groups\').\".document_group \r\n                    FROM \".$modx->getFullTableName(\'document_groups\').\", \".$modx->getFullTableName(\'membergroup_access\').\" \r\n                    WHERE \r\n                    \".$modx->getFullTableName(\'document_groups\').\".document_group = \".$modx->getFullTableName(\'membergroup_access\').\" .documentgroup \r\n                    AND \r\n                    \".$modx->getFullTableName(\'document_groups\').\".document = $lastInsertId;\";\r\n                    $privatemgrIds = $modx->db->getColumn(\"document_group\",$privatemgrSql);\r\n                    if(count($privatemgrIds)>0) {\r\n                        $modx->db->query(\"UPDATE \".$modx->getFullTableName(\"site_content\").\" SET privatemgr = 1 WHERE id = $lastInsertId;\");    \r\n                    }\r\n            // end of document_groups stuff!\r\n\r\n            if(!empty($makefolder)) {\r\n                // convert parent into folder\r\n                $modx->db->update(array(\'isfolder\'=>\'1\'),$modx->getFullTableName(\'site_content\'),\'id=\\\'\'.$folder.\'\\\'\');\r\n            }\r\n\r\n            // empty cache\r\n            if($clearcache==1){\r\n                include_once $modx->config[\'base_path\'].\"manager/processors/cache_sync.class.processor.php\";\r\n                $sync = new synccache();\r\n                $sync->setCachepath(\"assets/cache/\");\r\n                $sync->setReport(false);\r\n                $sync->emptyCache(); // first empty the cache       \r\n            }\r\n\r\n            // get redirect/post id\r\n            $redirectid = $modx->db->getValue(\'SELECT id as \\\'redirectid\\\' FROM \'.$modx->getFullTableName(\'site_content\').\' WHERE createdon=\\\'\'.$createdon.\'\\\'\');\r\n            $postid = isset($postid) ? $postid:$redirectid;\r\n\r\n            // redirect to post id\r\n            $modx->sendRedirect($modx->makeUrl($postid));\r\n        }\r\n\r\n    default: \r\n        // display news form\r\n        // check if user has rights to post comments\r\n        if(!$allowAnyPost && !$modx->isMemberOfWebGroup($postgrp)) {\r\n            $formTpl = \'\';\r\n        } else {\r\n            foreach($_POST as $n=>$v) {\r\n                $formTpl = str_replace(\'[+\'.$n.\'+]\',$v,$formTpl);\r\n            }\r\n        }\r\n        $formTpl = str_replace(\'[+base_url+]\', $modx->config[\'base_url\'], $formTpl);\r\n        // return form\r\n        return $message.$formTpl;\r\n        break;\r\n}','0','','');
INSERT INTO `modx_site_snippets` VALUES ('10','Personalize','<strong>2.0</strong> Basic personalization for web users.','0','0','0','#::::::::::::::::::::::::::::::::::::::::\r\n# Snippet Name: Personalize \r\n# Short Desc: calls a chunk if the user is logged in, otherwise calls another\r\n# Version: 2.0\r\n# Created By: 	Ryan Thrash (modx@vertexworks.com), and then\r\n#		powered up by kudo (kudo@kudolink.com)\r\n#\r\n# Date: Aug 03, 2006\r\n#\r\n# Changelog: \r\n# Dec 01, 05 -- initial release\r\n# Jun 19, 06 -- updated description\r\n# Jul 19, 06 -- hacked by kudo to output chunks\r\n# Aug 03, 06 -- added placeholder for username\r\n#\r\n#::::::::::::::::::::::::::::::::::::::::\r\n# Description: 	\r\n#	Checks to see if webusers are logged in and displays yesChunk if the user\r\n#	is logged or noChunk if user is not logged. Insert only the chunk name as\r\n#	param, without {{}}. Can use a placeholder to output the username.\r\n#	TESTED: can be used more than once per page.\r\n#	TESTED: chunks can contain snippets.\r\n#	\r\n#	\r\n# Params:\r\n#	&yesChunk [string] [REQUIRED]\r\n#		Output for LOGGED users\r\n#\r\n#	&noChunk [string] [REQUIRED] \r\n#		Output for NOT logged users\r\n#\r\n#	&ph [string] (optional) \r\n#		Placeholder for placing the username\r\n#		ATTENTION!: place this ph only in yesChunk!\r\n#	\r\n#\r\n# Example Usage:\r\n#\r\n#	[[LoggedOrNot? &yesChunk=`Link` &noChunk=`Register` &ph=`name`]]\r\n#\r\n#	Having Chunks named {{Link}} and another {{Register}}, the first will be\r\n#	published to registered user, the second to non-registered users.\r\n#\r\n#::::::::::::::::::::::::::::::::::::::::\r\n\r\n# prepare params and variables\r\n$o = \'\';\r\n$yesChunk = (isset($yesChunk))? $yesChunk : \'\';\r\n$noChunk = (isset($noChunk))? $noChunk : \'\';\r\n\r\n# do the work\r\n$test = $modx->getLoginUserName();\r\nif ($test) {\r\n    $o = $modx->getChunk($yesChunk);\r\n  } else {\r\n    $o = $modx->getChunk($noChunk);\r\n}\r\n\r\nif (isset($ph)) {\r\n	$modx->setPlaceholder($ph,$test);\r\n	return $o;\r\n} else {\r\n	return $o;\r\n}\r\n','0','','');
INSERT INTO `modx_site_snippets` VALUES ('11','Reflect','<strong>2.1</strong> Used with Ditto, creates archives of articles, blog entries, image galleries and more.','0','0','0','/*\r\n * Title: Reflect Snippet\r\n * \r\n * Description: \r\n * 		Generates date based archives using Ditto\r\n * \r\n * Author: \r\n * 		Mark Kaplan for MODx CMF\r\n * \r\n * Version: \r\n * 		2.1.0\r\n * \r\n * Note: \r\n * 		If Reflect is not retrieving its own documents, make sure that the\r\n *			Ditto call feeding it has all of the fields in it that you plan on\r\n *       calling in your Reflect template. Furthermore, Reflect will ONLY\r\n *			show what is currently in the Ditto result set.\r\n *       Thus, if pagination is on it will ONLY show that page\'s items.\r\n*/\r\n \r\n\r\n// ---------------------------------------------------\r\n//  Includes\r\n// ---------------------------------------------------\r\n\r\n$reflect_base = isset($reflect_base) ? $modx->config[\'base_path\'].$reflect_base : $modx->config[\'base_path\'].\"assets/snippets/reflect/\";\r\n/*\r\n	Param: ditto_base\r\n	\r\n	Purpose:\r\n	Location of Ditto files\r\n\r\n	Options:\r\n	Any valid folder location containing the Ditto source code with a trailing slash\r\n\r\n	Default:\r\n	[(base_path)]assets/snippets/ditto/\r\n*/\r\n\r\n$config = (isset($config)) ? $config : \"default\";\r\n/*\r\n	Param: config\r\n\r\n	Purpose:\r\n 	Load a custom configuration\r\n\r\n	Options:\r\n	\"default\" - default blank config file\r\n	CONFIG_NAME - Other configs installed in the configs folder or in any folder within the MODx base path via @FILE\r\n\r\n	Default:\r\n	\"default\"\r\n	\r\n	Related:\r\n	- <extenders>\r\n*/\r\n\r\nrequire($reflect_base.\"configs/default.config.php\");\r\nrequire($reflect_base.\"default.templates.php\");\r\nif ($config != \"default\") {\r\n	require((substr($config, 0, 5) != \"@FILE\") ? $reflect_base.\"configs/$config.config.php\" : $modx->config[\'base_path\'].trim(substr($config, 5)));\r\n}\r\n\r\n// ---------------------------------------------------\r\n//  Parameters\r\n// ---------------------------------------------------\r\n\r\n$id = isset($id) ? $id.\"_\" : false;\r\n/*\r\n	Param: id\r\n\r\n	Purpose:\r\n	Unique ID for this Ditto instance for connection with other scripts (like Reflect) and unique URL parameters\r\n\r\n	Options:\r\n	Any valid folder location containing the Ditto source code with a trailing slash\r\n\r\n	Default:\r\n	\"\" - blank\r\n*/\r\n$getDocuments = isset($getDocuments) ? $getDocuments : 0;\r\n/*\r\n	Param: getDocuments\r\n\r\n	Purpose:\r\n 	Force Reflect to get documents\r\n\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	0 - off\r\n*/\r\n$showItems = isset($showItems) ? $showItems : 1;\r\n/*\r\n	Param: showItems\r\n\r\n	Purpose:\r\n 	Show individual items in the archive\r\n\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	1 - on\r\n*/\r\n$groupByYears = isset($groupByYears)? $groupByYears : 1;\r\n/*\r\n	Param: groupByYears\r\n\r\n	Purpose:\r\n 	Group the archive by years\r\n\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	1 - on\r\n*/\r\n$targetID = isset($targetID) ? $targetID : $modx->documentObject[\'id\'];\r\n/*\r\n	Param: targetID\r\n\r\n	Purpose:\r\n 	ID for archive links to point to\r\n\r\n	Options:\r\n	Any MODx document with a Ditto call setup with extenders=`dateFilter`\r\n	\r\n	Default:\r\n	Current MODx Document\r\n*/\r\n$dateSource = isset($dateSource) ? $dateSource : \"createdon\";\r\n/*\r\n	Param: dateSource\r\n\r\n	Purpose:\r\n	Date source to display for archive items\r\n\r\n	Options:\r\n	# - Any UNIX timestamp from MODx fields or TVs such as createdon, pub_date, or editedon\r\n	\r\n	Default:\r\n	\"createdon\"\r\n	\r\n	Related:\r\n	- <dateFormat>\r\n*/\r\n$dateFormat = isset($dateFormat) ? $dateFormat : \"%d-%b-%y %H:%M\";	\r\n/*\r\n	Param: dateFormat\r\n\r\n	Purpose:\r\n	Format the [+date+] placeholder in human readable form\r\n\r\n	Options:\r\n	Any PHP valid strftime option\r\n\r\n	Default:\r\n	\"%d-%b-%y %H:%M\"\r\n	\r\n	Related:\r\n	- <dateSource>\r\n*/\r\n$yearSortDir = isset($yearSortDir) ? $yearSortDir : \"DESC\";\r\n/*\r\n	Param: yearSortDir\r\n\r\n	Purpose:\r\n 	Direction to sort documents\r\n\r\n	Options:\r\n	ASC - ascending\r\n	DESC - descending\r\n\r\n	Default:\r\n	\"DESC\"\r\n	\r\n	Related:\r\n	- <monthSortDir>\r\n*/\r\n$monthSortDir = isset($monthSortDir) ? $monthSortDir : \"ASC\";\r\n/*\r\n	Param: monthSortDir\r\n\r\n	Purpose:\r\n 	Direction to sort the months\r\n\r\n	Options:\r\n	ASC - ascending\r\n	DESC - descending\r\n\r\n	Default:\r\n	\"ASC\"\r\n	\r\n	Related:\r\n	- <yearSortDir>\r\n*/\r\n$start = isset($start)? intval($start) : 0;\r\n/*\r\n	Param: start\r\n\r\n	Purpose:\r\n 	Number of documents to skip in the results\r\n	\r\n	Options:\r\n	Any number\r\n\r\n	Default:\r\n	0\r\n*/	\r\n$phx = (isset($phx))? $phx : 1;\r\n/*\r\n	Param: phx\r\n\r\n	Purpose:\r\n 	Use PHx formatting\r\n\r\n	Options:\r\n	0 - off\r\n	1 - on\r\n	\r\n	Default:\r\n	1 - on\r\n*/\r\n\r\n// ---------------------------------------------------\r\n//  Initialize Ditto\r\n// ---------------------------------------------------\r\n$placeholder = ($id != false && $getDocuments == 0) ? true : false;\r\nif ($placeholder === false) {\r\n	$rID = \"reflect_\".rand(1,1000);\r\n	$itemTemplate = isset($tplItem) ? $tplItem: \"@CODE:\".$defaultTemplates[\'item\'];\r\n	$dParams = array(\r\n		\"id\" => \"$rID\",\r\n		\"save\" => \"3\",	\r\n		\"summarize\" => \"all\",\r\n		\"tpl\" => $itemTemplate,\r\n	);\r\n	\r\n	$source = $dittoSnippetName;\r\n	$params = $dittoSnippetParameters;\r\n		// TODO: Remove after 3.0\r\n		\r\n	if (isset($params)) {\r\n		$givenParams = explode(\"|\",$params);\r\n		foreach ($givenParams as $parameter) {\r\n			$p = explode(\":\",$parameter);\r\n			$dParams[$p[0]] = $p[1];\r\n		}\r\n	}\r\n	/*\r\n		Param: params\r\n\r\n		Purpose:\r\n	 	Pass parameters to the Ditto instance used to retreive the documents\r\n\r\n		Options:\r\n		Any valid ditto parameters in the format name:value \r\n		with multiple parameters separated by a pipe (|)\r\n		\r\n		Note:\r\n		This parameter is only needed for config, start, and phx as you can\r\n		now simply use the parameter as if Reflect was Ditto\r\n\r\n		Default:\r\n		[NULL]\r\n	*/\r\n	\r\n	$allParameters = func_get_args();\r\n	$reflectParameters = array(\'reflect_base\',\'config\',\'id\',\'getDocuments\',\'showItems\',\'groupByYears\',\'targetID\',\'yearSortDir\',\'monthSortDir\',\'start\',\'phx\',\'tplContainer\',\'tplYear\',\'tplMonth\',\'tplMonthInner\',\'tplItem\',\'save\');\r\n	foreach ($allParameters[1] as $param=>$value) {\r\n		if (!in_array($param,$reflectParameters) && substr($param,-3) != \'tpl\') {\r\n			$dParams[$param] = $value;\r\n		}\r\n	}\r\n\r\n	$source = isset($source) ? $source : \"Ditto\";\r\n	/*\r\n		Param: source\r\n\r\n		Purpose:\r\n		Name of the Ditto snippet to use\r\n\r\n		Options:\r\n		Any valid snippet name\r\n\r\n		Default:\r\n		\"Ditto\"\r\n	*/\r\n	$snippetOutput = $modx->runSnippet($source,$dParams);\r\n	$ditto = $modx->getPlaceholder($rID.\"_ditto_object\");\r\n	$resource = $modx->getPlaceholder($rID.\"_ditto_resource\");\r\n} else {\r\n	$ditto = $modx->getPlaceholder($id.\"ditto_object\");\r\n	$resource = $modx->getPlaceholder($id.\"ditto_resource\");\r\n}\r\nif (!is_object($ditto) || !isset($ditto) || !isset($resource)) {\r\n	return !empty($snippetOutput) ? $snippetOutput : \"The Ditto object is invalid. Please check it.\";\r\n}\r\n\r\n// ---------------------------------------------------\r\n//  Templates\r\n// ---------------------------------------------------\r\n\r\n$templates[\'tpl\'] = isset($tplContainer) ? $ditto->template->fetch($tplContainer): $defaultTemplates[\'tpl\'];\r\n/*\r\n	Param: tplContainer\r\n\r\n	Purpose:\r\n	Container template for the archive\r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE:\r\n	- File via @FILE:\r\n\r\n	Default:\r\n	See default.tempates.php\r\n*/\r\n$templates[\'year\'] = isset($tplYear) ? $ditto->template->fetch($tplYear): $defaultTemplates[\'year\'];\r\n/*\r\n	Param: tplYear\r\n\r\n	Purpose:\r\n	Template for the year item\r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE:\r\n	- File via @FILE:\r\n\r\n	Default:\r\n	See default.tempates.php\r\n*/\r\n$templates[\'year_inner\'] = isset($tplYearInner) ? $ditto->template->fetch($tplYearInner): $defaultTemplates[\'year_inner\'];\r\n/*\r\n	Param: tplYearInner\r\n\r\n	Purpose:\r\n	Template for the year item (the ul to hold the year template)\r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE:\r\n	- File via @FILE:\r\n\r\n	Default:\r\n	See default.tempates.php\r\n*/\r\n$templates[\'month\'] = isset($tplMonth) ? $ditto->template->fetch($tplMonth): $defaultTemplates[\'month\'];\r\n/*\r\n	Param: tplMonth\r\n\r\n	Purpose:\r\n	Template for the month item\r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE:\r\n	- File via @FILE:\r\n\r\n	Default:\r\n	See default.tempates.php\r\n*/\r\n$templates[\'month_inner\'] = isset($tplMonthInner) ? $ditto->template->fetch($tplMonthInner): $defaultTemplates[\'month_inner\'];\r\n/*\r\n	Param: tplMonthInner\r\n\r\n	Purpose:\r\n	Template for the month item  (the ul to hold the month template)\r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE:\r\n	- File via @FILE:\r\n\r\n	Default:\r\n	See default.tempates.php\r\n*/\r\n$templates[\'item\'] = isset($tplItem) ? $ditto->template->fetch($tplItem): $defaultTemplates[\'item\'];\r\n/*\r\n	Param: tplItem\r\n\r\n	Purpose:\r\n	Template for the individual item\r\n\r\n	Options:\r\n	- Any valid chunk name\r\n	- Code via @CODE:\r\n	- File via @FILE:\r\n\r\n	Default:\r\n	See default.tempates.php\r\n*/\r\n\r\n$ditto->addField(\"date\",\"display\",\"custom\");\r\n	// force add the date field if receiving data from a Ditto instance\r\n\r\n// ---------------------------------------------------\r\n//  Reflect\r\n// ---------------------------------------------------\r\n\r\nif (function_exists(\"reflect\") === FALSE) {\r\nfunction reflect($templatesDocumentID, $showItems, $groupByYears, $resource, $templatesDateSource, $dateFormat, $ditto, $templates,$id,$start,$yearSortDir,$monthSortDir) {\r\n	global $modx;\r\n	$cal = array();\r\n	$output = \'\';\r\n	$ph = array(\'year\'=>\'\',\'month\'=>\'\',\'item\'=>\'\',\'out\'=>\'\');\r\n	$build = array();\r\n	$stop = count($resource);\r\n\r\n	// loop and fetch all the results\r\n	for ($i = $start; $i < $stop; $i++) {\r\n		$date = getdate($resource[$i][$templatesDateSource]);\r\n		$year = $date[\"year\"];\r\n		$month = $date[\"mon\"];\r\n		$cal[$year][$month][] = $resource[$i];\r\n	}\r\n	if ($yearSortDir == \"DESC\") {\r\n		krsort($cal);\r\n	} else {\r\n		ksort($cal);\r\n	}\r\n	foreach ($cal as $year=>$months) {\r\n		if ($monthSortDir == \"ASC\") {\r\n			ksort($months);\r\n		} else {\r\n			krsort($months);\r\n		}\r\n		$build[$year] = $months;\r\n	}\r\n	\r\n	foreach ($build as $year=>$months) {\r\n		$r_year = \'\';\r\n		$r_month = \'\';\r\n		$r_month_2 = \'\';\r\n		$year_count = 0;\r\n		$items = array();\r\n		\r\n		foreach ($months as $mon=>$month) {\r\n			$month_text = strftime(\"%B\", mktime(10, 10, 10, $mon, 10, $year));\r\n			$month_url = $ditto->buildURL(\"month=\".$mon.\"&year=\".$year.\"&day=false&start=0\",$templatesDocumentID,$id);\r\n			$month_count = count($month);\r\n			$year_count += $month_count;\r\n			$r_month = $ditto->template->replace(array(\"year\"=>$year,\"month\"=>$month_text,\"url\"=>$month_url,\"count\"=>$month_count),$templates[\'month\']);\r\n			if ($showItems) {\r\n				foreach ($month as $item) {\r\n					$items[$year][$mon][\'items\'][] = $ditto->render($item, $templates[\'item\'], false, $templatesDateSource, $dateFormat, array(),$phx);\r\n				}\r\n				$r_month_2 = $ditto->template->replace(array(\'wrapper\' => implode(\'\',$items[$year][$mon][\'items\'])),$templates[\'month_inner\']);\r\n				$items[$year][$mon] = $ditto->template->replace(array(\'wrapper\' => $r_month_2),$r_month);\r\n			} else {\r\n				$items[$year][$mon] = $r_month;\r\n			}\r\n		}\r\n		if ($groupByYears) {\r\n			$year_url = $ditto->buildURL(\"year=\".$year.\"&month=false&day=false&start=0\",$templatesDocumentID,$id);\r\n			$r_year =  $ditto->template->replace(array(\"year\"=>$year,\"url\"=>$year_url,\"count\"=>$year_count),$templates[\'year\']);\r\n			$var = $ditto->template->replace(array(\'wrapper\'=>implode(\'\',$items[$year])),$templates[\'year_inner\']);\r\n			$output .= $ditto->template->replace(array(\'wrapper\'=>$var),$r_year);\r\n		} else {\r\n			$output .= implode(\'\',$items[$year]);\r\n		}\r\n	}\r\n\r\n	$output = $ditto->template->replace(array(\'wrapper\'=>$output),$templates[\'tpl\']);\r\n	$modx->setPlaceholder($id.\'reset\',$ditto->buildURL(\'year=false&month=false&day=false\',$templatesDocumentID,$id));\r\n\r\nreturn $output;\r\n	\r\n}\r\n}\r\n\r\nreturn reflect($targetID, $showItems, $groupByYears, $resource, $dateSource, $dateFormat, $ditto, $templates,$id,$start,$yearSortDir,$monthSortDir);','0','','');
INSERT INTO `modx_site_snippets` VALUES ('12','UltimateParent','<strong>2.0 beta</strong> - Travels up the document tree from a specified document and returns the \"ultimate\" parent.','0','0','0','/**\r\n * @name UltimateParent\r\n * @version 2.0 beta (requires MODx 0.9.5+)\r\n * @author Jason Coward <modx@opengeek.com>\r\n * \r\n * @param &id The id of the document whose parent you want to find.\r\n * @param &top The top node for the search.\r\n * @param &topLevel The top level node for the search (root = level 1)\r\n * \r\n * @license Public Domain, use as you like.\r\n * \r\n * @example [[UltimateParent? &id=`45` &top=`6`]] \r\n * Will find the ultimate parent of document 45 if it is a child of document 6;\r\n * otherwise it will return 45.\r\n * \r\n * @example [[UltimateParent? &topLevel=`2`]]\r\n * Will find the ultimate parent of the current document at a depth of 2 levels\r\n * in the document hierarchy, with the root level being level 1.\r\n * \r\n * This snippet travels up the document tree from a specified document and\r\n * returns the \"ultimate\" parent.  Version 2.0 was rewritten to use the new\r\n * getParentIds function features available only in MODx 0.9.5 or later.\r\n * \r\n * Based on the original UltimateParent 1.x snippet by Susan Ottwell\r\n * <sottwell@sottwell.com>.  The topLevel parameter was introduced by staed and\r\n * adopted here.\r\n */\r\n$top= isset ($top) && intval($top) ? $top : 0;\r\n$id= isset ($id) && intval($id) ? intval($id) : $modx->documentIdentifier;\r\n$topLevel= isset ($topLevel) && intval($topLevel) ? intval($topLevel) : 0;\r\nif ($id && $id != $top) {\r\n    $pid= $id;\r\n    if (!$topLevel || count($modx->getParentIds($id)) >= $topLevel) {\r\n        while ($parentIds= $modx->getParentIds($id, 1)) {\r\n            $pid= array_pop($parentIds);\r\n            if ($pid == $top) {\r\n                break;\r\n            }\r\n            $id= $pid;\r\n            if ($topLevel && count($modx->getParentIds($id)) < $topLevel) {\r\n                break;\r\n            }\r\n        }\r\n    }\r\n}\r\nreturn $id;','0','','');
INSERT INTO `modx_site_snippets` VALUES ('13','Wayfinder','<strong>2.0</strong> Completely template-driven menu builder that\'s simple and fast to configure.','0','0','0','/*\r\n::::::::::::::::::::::::::::::::::::::::\r\n Snippet name: Wayfinder\r\n Short Desc: builds site navigation\r\n Version: 2.0\r\n Authors: \r\n	Kyle Jaebker (muddydogpaws.com)\r\n	Ryan Thrash (vertexworks.com)\r\n Date: February 27, 2006\r\n::::::::::::::::::::::::::::::::::::::::\r\nDescription:\r\n    Totally refactored from original DropMenu nav builder to make it easier to\r\n    create custom navigation by using chunks as output templates. By using templates,\r\n    many of the paramaters are no longer needed for flexible output including tables,\r\n    unordered- or ordered-lists (ULs or OLs), definition lists (DLs) or in any other\r\n    format you desire.\r\n::::::::::::::::::::::::::::::::::::::::\r\nExample Usage:\r\n    [[Wayfinder? &startId=`0`]]\r\n::::::::::::::::::::::::::::::::::::::::\r\n*/\r\n\r\n$wayfinder_base = $modx->config[\'base_path\'].\"assets/snippets/wayfinder/\";\r\n\r\n//Include a custom config file if specified\r\n$config = (isset($config)) ? \"{$wayfinder_base}configs/{$config}.config.php\" : \"{$wayfinder_base}configs/default.config.php\";\r\nif (file_exists($config)) {\r\n	include_once(\"$config\");\r\n}\r\n\r\ninclude_once(\"{$wayfinder_base}wayfinder.inc.php\");\r\n\r\nif (class_exists(\'Wayfinder\')) {\r\n   $wf = new Wayfinder();\r\n} else {\r\n    return \'error: Wayfinder class not found\';\r\n}\r\n\r\n$wf->_config = array(\r\n	\'id\' => isset($startId) ? $startId : $modx->documentIdentifier,\r\n	\'level\' => isset($level) ? $level : 0,\r\n	\'includeDocs\' => isset($includeDocs) ? $includeDocs : 0,\r\n	\'excludeDocs\' => isset($excludeDocs) ? $excludeDocs : 0,\r\n	\'ph\' => isset($ph) ? $ph : FALSE,\r\n	\'debug\' => isset($debug) ? TRUE : FALSE,\r\n	\'ignoreHidden\' => isset($ignoreHidden) ? $ignoreHidden : FALSE,\r\n	\'hideSubMenus\' => isset($hideSubMenus) ? $hideSubMenus : FALSE,\r\n	\'useWeblinkUrl\' => isset($useWeblinkUrl) ? $useWeblinkUrl : TRUE,\r\n	\'fullLink\' => isset($fullLink) ? $fullLink : FALSE,\r\n	\'nl\' => isset($removeNewLines) ? \'\' : \"\\n\",\r\n	\'sortOrder\' => isset($sortOrder) ? strtoupper($sortOrder) : \'ASC\',\r\n	\'sortBy\' => isset($sortBy) ? $sortBy : \'menuindex\',\r\n	\'limit\' => isset($limit) ? $limit : 0,\r\n	\'cssTpl\' => isset($cssTpl) ? $cssTpl : FALSE,\r\n	\'jsTpl\' => isset($jsTpl) ? $jsTpl : FALSE,\r\n	\'rowIdPrefix\' => isset($rowIdPrefix) ? $rowIdPrefix : FALSE,\r\n	\'textOfLinks\' => isset($textOfLinks) ? $textOfLinks : \'menutitle\',\r\n	\'titleOfLinks\' => isset($titleOfLinks) ? $titleOfLinks : \'pagetitle\',\r\n	\'displayStart\' => isset($displayStart) ? $displayStart : FALSE,\r\n);\r\n\r\n//get user class definitions\r\n$wf->_css = array(\r\n	\'first\' => isset($firstClass) ? $firstClass : \'\',\r\n	\'last\' => isset($lastClass) ? $lastClass : \'last\',\r\n	\'here\' => isset($hereClass) ? $hereClass : \'active\',\r\n	\'parent\' => isset($parentClass) ? $parentClass : \'\',\r\n	\'row\' => isset($rowClass) ? $rowClass : \'\',\r\n	\'outer\' => isset($outerClass) ? $outerClass : \'\',\r\n	\'inner\' => isset($innerClass) ? $innerClass : \'\',\r\n	\'level\' => isset($levelClass) ? $levelClass: \'\',\r\n	\'self\' => isset($selfClass) ? $selfClass : \'\',\r\n	\'weblink\' => isset($webLinkClass) ? $webLinkClass : \'\',\r\n);\r\n\r\n//get user templates\r\n$wf->_templates = array(\r\n	\'outerTpl\' => isset($outerTpl) ? $outerTpl : \'\',\r\n	\'rowTpl\' => isset($rowTpl) ? $rowTpl : \'\',\r\n	\'parentRowTpl\' => isset($parentRowTpl) ? $parentRowTpl : \'\',\r\n	\'parentRowHereTpl\' => isset($parentRowHereTpl) ? $parentRowHereTpl : \'\',\r\n	\'hereTpl\' => isset($hereTpl) ? $hereTpl : \'\',\r\n	\'innerTpl\' => isset($innerTpl) ? $innerTpl : \'\',\r\n	\'innerRowTpl\' => isset($innerRowTpl) ? $innerRowTpl : \'\',\r\n	\'innerHereTpl\' => isset($innerHereTpl) ? $innerHereTpl : \'\',\r\n	\'activeParentRowTpl\' => isset($activeParentRowTpl) ? $activeParentRowTpl : \'\',\r\n	\'categoryFoldersTpl\' => isset($categoryFoldersTpl) ? $categoryFoldersTpl : \'\',\r\n	\'startItemTpl\' => isset($startItemTpl) ? $startItemTpl : \'\',\r\n);\r\n\r\n//Process Wayfinder\r\n$output = $wf->run();\r\n\r\nif ($wf->_config[\'debug\']) {\r\n	$output .= $wf->renderDebugOutput();\r\n}\r\n\r\n//Ouput Results\r\nif ($wf->_config[\'ph\']) {\r\n    $modx->setPlaceholder($wf->_config[\'ph\'],$output);\r\n} else {\r\n    return $output;\r\n}','0','','');
INSERT INTO `modx_site_snippets` VALUES ('14','WebChangePwd','<strong>1.0</strong> Web User Change Password Snippet.','0','0','0','# WebChangePwd 1.0\r\n# Created By Raymond Irving April, 2005\r\n#::::::::::::::::::::::::::::::::::::::::\r\n# Usage: 	\r\n#	Allows a web user to change his/her password from the website\r\n#\r\n# Params:	\r\n#\r\n#	&tpl			- (Optional)\r\n#		Chunk name or document id to use as a template\r\n#				  \r\n#	Note: Templats design:\r\n#			section 1: change pwd template\r\n#			section 2: notification template \r\n#\r\n# Examples:\r\n#\r\n#	[[WebChangePwd? &tpl=`ChangePwd`]] \r\n\r\n# Set Snippet Paths \r\n$snipPath  = (($modx->insideManager())? \"../\":\"\");\r\n$snipPath .= \"assets/snippets/\";\r\n\r\n# check if inside manager\r\nif ($m = $modx->insideManager()) {\r\n	return \'\'; # don\'t go any further when inside manager\r\n}\r\n\r\n\r\n# Snippet customize settings\r\n$tpl		= isset($tpl)? $tpl:\"\";\r\n\r\n# System settings\r\n$isPostBack		= count($_POST) && isset($_POST[\'cmdwebchngpwd\']);\r\n\r\n# Start processing\r\ninclude_once $snipPath.\"weblogin/weblogin.common.inc.php\";\r\ninclude_once $snipPath.\"weblogin/webchangepwd.inc.php\";\r\n\r\n# Return\r\nreturn $output;\r\n\r\n\r\n\r\n','0','&tpl=Template;string; ','');
INSERT INTO `modx_site_snippets` VALUES ('15','WebLogin','<strong>1.0</strong> Web User Login Snippet.','0','0','0','# WebLogin 1.0\r\n# Created By Raymond Irving 2004\r\n#::::::::::::::::::::::::::::::::::::::::\r\n# Usage: 	\r\n#	Allows a web user to login to the website\r\n#\r\n# Params:	\r\n#	&loginhomeid 	- (Optional)\r\n#		redirects the user to first authorized page in the list.\r\n#		If no id was specified then the login home page id or \r\n#		the current document id will be used\r\n#\r\n#	&logouthomeid 	- (Optional)\r\n#		document id to load when user logs out	\r\n#\r\n#	&pwdreqid 	- (Optional)\r\n#		document id to load after the user has submited\r\n#		a request for a new password\r\n#\r\n#	&pwdactid 	- (Optional)\r\n#		document id to load when the after the user has activated\r\n#		their new password\r\n#\r\n#	&logintext		- (Optional) \r\n#		Text to be displayed inside login button (for built-in form)\r\n#\r\n#	&logouttext 	- (Optional)\r\n#		Text to be displayed inside logout link (for built-in form)\r\n#	\r\n#	&tpl			- (Optional)\r\n#		Chunk name or document id to as a template\r\n#				  \r\n#	Note: Templats design:\r\n#			section 1: login template\r\n#			section 2: logout template \r\n#			section 3: password reminder template \r\n#\r\n#			See weblogin.tpl for more information\r\n#\r\n# Examples:\r\n#\r\n#	[[WebLogin? &loginhomeid=`8` &logouthomeid=`1`]] \r\n#\r\n#	[[WebLogin? &loginhomeid=`8,18,7,5` &tpl=`Login`]] \r\n\r\n# Set Snippet Paths \r\n$snipPath = $modx->config[\'base_path\'] . \"assets/snippets/\";\r\n\r\n# check if inside manager\r\nif ($m = $modx->insideManager()) {\r\n	return \'\'; # don\'t go any further when inside manager\r\n}\r\n\r\n# deprecated params - only for backward compatibility\r\nif(isset($loginid)) $loginhomeid=$loginid;\r\nif(isset($logoutid)) $logouthomeid = $logoutid;\r\nif(isset($template)) $tpl = $template;\r\n\r\n# Snippet customize settings\r\n$liHomeId	= isset($loginhomeid)? explode(\",\",$loginhomeid):array($modx->config[\'login_home\'],$modx->documentIdentifier);\r\n$loHomeId	= isset($logouthomeid)? $logouthomeid:$modx->documentIdentifier;\r\n$pwdReqId	= isset($pwdreqid)? $pwdreqid:0;\r\n$pwdActId	= isset($pwdactid)? $pwdactid:0;\r\n$loginText	= isset($logintext)? $logintext:\'Login\';\r\n$logoutText	= isset($logouttext)? $logouttext:\'Logout\';\r\n$tpl		= isset($tpl)? $tpl:\"\";\r\n\r\n# System settings\r\n$webLoginMode = isset($_REQUEST[\'webloginmode\'])? $_REQUEST[\'webloginmode\']: \'\';\r\n$isLogOut		= $webLoginMode==\'lo\' ? 1:0;\r\n$isPWDActivate	= $webLoginMode==\'actp\' ? 1:0;\r\n$isPostBack		= count($_POST) && (isset($_POST[\'cmdweblogin\']) || isset($_POST[\'cmdweblogin_x\']));\r\n$txtPwdRem 		= isset($_REQUEST[\'txtpwdrem\'])? $_REQUEST[\'txtpwdrem\']: 0;\r\n$isPWDReminder	= $isPostBack && $txtPwdRem==\'1\' ? 1:0;\r\n\r\n$site_id = isset($site_id)? $site_id: \'\';\r\n$cookieKey = substr(md5($site_id.\"Web-User\"),0,15);\r\n\r\n# Start processing\r\ninclude_once $snipPath.\"weblogin/weblogin.common.inc.php\";\r\ninclude_once ($modx->config[\'base_path\'] . \"manager/includes/crypt.class.inc.php\");\r\n\r\nif ($isPWDActivate || $isPWDReminder || $isLogOut || $isPostBack) {\r\n	# include the logger class\r\n	include_once $modx->config[\'base_path\'] . \"manager/includes/log.class.inc.php\";\r\n	include_once $snipPath.\"weblogin/weblogin.processor.inc.php\";\r\n}\r\n\r\ninclude_once $snipPath.\"weblogin/weblogin.inc.php\";\r\n\r\n# Return\r\nreturn $output;\r\n','0','&loginhomeid=Login Home Id;string; &logouthomeid=Logout Home Id;string; &logintext=Login Button Text;string; &logouttext=Logout Button Text;string; &tpl=Template;string; ','');
INSERT INTO `modx_site_snippets` VALUES ('16','WebSignup','<strong>1.1</strong> Web User Signup Snippet.','0','0','0','# WebSignup 1.1\r\n# Created By Raymond Irving April, 2005\r\n#::::::::::::::::::::::::::::::::::::::::\r\n# Usage:     \r\n#    Allows a web user to signup for a new web account from the website\r\n#    This snippet provides a basic set of form fields for the signup form\r\n#    You can customize this snippet to create your own signup form\r\n#\r\n# Params:    \r\n#\r\n#    &tpl        - (Optional) Chunk name or document id to use as a template\r\n#    &groups     - Web users groups to be assigned to users\r\n#    &useCaptcha - (Optional) Determine to use (1) or not to use (0) captcha\r\n#                  on signup form - if not defined, will default to system\r\n#                  setting. GD is required for this feature. If GD is not \r\n#                  available, useCaptcha will automatically be set to false;\r\n#                  \r\n#    Note: Templats design:\r\n#        section 1: signup template\r\n#        section 2: notification template \r\n#\r\n# Examples:\r\n#\r\n#    [[WebSignup? &tpl=`SignupForm` &groups=`NewsReaders,WebUsers`]] \r\n\r\n# Set Snippet Paths \r\n$snipPath = $modx->config[\'base_path\'] . \"assets/snippets/\";\r\n\r\n# check if inside manager\r\nif ($m = $modx->insideManager()) {\r\n    return \'\'; # don\'t go any further when inside manager\r\n}\r\n\r\n\r\n# Snippet customize settings\r\n$tpl = isset($tpl)? $tpl:\"\";\r\n$useCaptcha = isset($useCaptcha)? $useCaptcha : $modx->config[\'use_captcha\'] ;\r\n// Override captcha if no GD\r\nif ($useCaptcha && !gd_info()) $useCaptcha = 0;\r\n\r\n# setup web groups\r\n$groups = isset($groups) ? explode(\',\',$groups):array();\r\nfor($i=0;$i<count($groups);$i++) $groups[$i] = trim($groups[$i]);\r\n\r\n# System settings\r\n$isPostBack        = count($_POST) && isset($_POST[\'cmdwebsignup\']);\r\n\r\n$output = \'\';\r\n\r\n# Start processing\r\ninclude_once $snipPath.\"weblogin/weblogin.common.inc.php\";\r\ninclude_once $snipPath.\"weblogin/websignup.inc.php\";\r\n\r\n# Return\r\nreturn $output;','0','&tpl=Template;string; ','');
INSERT INTO `modx_site_snippets` VALUES ('18','linksFeed','','0','0','0','\r\n$parent = isset($parent) ? $parent : 0;\r\n\r\necho \'<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\'.\"\\n\".\r\n     \'<links>\'.\"\\n\";\r\n\r\n$categories = convertObjArray($modx->getChildIds($parent, 1));\r\n\r\nfor($i = 0; $i< count($categories); $i++)\r\n{\r\n    $ref = $modx->getDocument($categories[$i]);\r\n    createLinkNode($modx, $ref[pagetitle], convertObjArray($modx->getChildIds($ref[id])));\r\n}\r\n\r\necho \'</links>\';\r\n\r\nfunction createLinkNode($modx, $catTitle, $childIds)\r\n{\r\n    echo \"\\t\".\'<category id=\"\'.$catTitle.\'\">\'.\"\\n\";\r\n	\r\n    for($i = 0; $i < count($childIds); $i++)\r\n    {\r\n        $weblink = $modx->getDocument($childIds[$i]);\r\n        \r\n        echo \"\\t\\t\".\'<link title=\"\'.$weblink[pagetitle].\'\" url=\"\'.$weblink[content].\'\">\'.\"\\n\".\r\n             \"\\t\\t\\t\".\'<description>\'.$weblink[description].\'</description>\'.\"\\n\".\r\n             \"\\t\\t\".\'</link>\'.\"\\n\";\r\n    }\r\n\r\n    echo \"\\t\".\'</category>\'.\"\\n\";\r\n}\r\n\r\nfunction convertObjArray($objectArray)\r\n{\r\n    return array_values($objectArray);\r\n}\r\n','0','',' ');
INSERT INTO `modx_site_snippets` VALUES ('19','xmlTreeParser','','0','0','0','\r\n$parentID = isset($parentID) ? $parentID : 0;\r\n$parentName = isset($parentName) ? $parentName : \'item\';\r\n\r\necho \'<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\'.\"\\n\".\r\n     \'<\'.$parentName.\'>\'.\"\\n\";\r\n\r\n$categories = convertObjArray($modx->getChildIds($parent, 1));\r\n\r\nfor($i = 0; $i< count($categories); $i++)\r\n{\r\n    $ref = $modx->getDocument($categories[$i]);\r\n    createLinkNode($ref[pagetitle], convertObjArray($modx->getChildIds($ref[id])));\r\n}\r\n\r\necho \'</\'.$parentName.\'>\';\r\n\r\nfunction createLinkNode($catTitle, $childIds)\r\n{\r\n    global $modx;\r\n	\r\n    echo \"\\t\".\'<category id=\"\'.$catTitle.\'\">\'.\"\\n\";\r\n	\r\n    for($i = 0; $i < count($childIds); $i++)\r\n    {\r\n        $child = $modx->getDocument($childIds[$i]);\r\n        $user = $modx->getUserInfo($child[createdby]);\r\n        \r\n        echo \"\\t\\t\".\'<link title=\"\'.$child[pagetitle].\'\" url=\"http://www.lookinginspeakingout.com/index.php?id=\'.$child[id].\'\" date=\"\'.date(\"D, d-m-Y\", $child[publishedon]).\'\">\'.\"\\n\".\r\n        	 \"\\t\\t\\t\".\'<author>\'.$user[fullname].\'</author>\'.\"\\n\".\r\n             \"\\t\\t\\t\".\'<description>\'.$child[description].\'</description>\'.\"\\n\".\r\n             \"\\t\\t\".\'</link>\'.\"\\n\";\r\n    }\r\n\r\n    echo \"\\t\".\'</category>\'.\"\\n\";\r\n}\r\n\r\nfunction convertObjArray($objectArray)\r\n{\r\n    return array_values($objectArray);\r\n}\r\n','0','',' ');
INSERT INTO `modx_site_snippets` VALUES ('20','authorDate','','0','0','0','\r\n$id = $modx->documentObject[\'id\'];\r\n$child = $modx->getDocument( $id );\r\n$user = $modx->getUserInfo($child[createdby]);\r\necho $user[fullname].\' \'. date(\"D, d-m-Y\", $child[publishedon]);\r\n','0','',' ');

# --------------------------------------------------------

#
# Table structure for table `modx_site_templates`
#

CREATE TABLE `modx_site_templates` (
  `id` int(10) NOT NULL auto_increment,
  `templatename` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default 'Template',
  `editor_type` int(11) NOT NULL default '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL default '0' COMMENT 'category id',
  `icon` varchar(255) NOT NULL default '' COMMENT 'url to icon file',
  `template_type` int(11) NOT NULL default '0' COMMENT '0-page,1-content',
  `content` mediumtext,
  `locked` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='Contains the site templates.';

#
# Dumping data for table `modx_site_templates`
#

INSERT INTO `modx_site_templates` VALUES ('5','Video Post','Used to create an RSS Video Post','0','0','','0','[*content*]','0');
INSERT INTO `modx_site_templates` VALUES ('6','Straight Text','no extra content','0','0','','0','[*content*]','0');
INSERT INTO `modx_site_templates` VALUES ('4','RSS Post','Template to be used for RSS Posts','0','0','','0','[*content*]','1');
INSERT INTO `modx_site_templates` VALUES ('7','Resource Format','Used for the resources category','0','0','','0','<font size=\"14\"><b>[*pagetitle*]</b></font><br />\r\n[[authorDate]]<br /><br />\r\n[*content*]','0');

# --------------------------------------------------------

#
# Table structure for table `modx_site_tmplvar_access`
#

CREATE TABLE `modx_site_tmplvar_access` (
  `id` int(10) NOT NULL auto_increment,
  `tmplvarid` int(10) NOT NULL default '0',
  `documentgroup` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for template variable access permissions.';

#
# Dumping data for table `modx_site_tmplvar_access`
#


# --------------------------------------------------------

#
# Table structure for table `modx_site_tmplvar_contentvalues`
#

CREATE TABLE `modx_site_tmplvar_contentvalues` (
  `id` int(11) NOT NULL auto_increment,
  `tmplvarid` int(10) NOT NULL default '0' COMMENT 'Template Variable id',
  `contentid` int(10) NOT NULL default '0' COMMENT 'Site Content Id',
  `value` text,
  PRIMARY KEY  (`id`),
  KEY `idx_tmplvarid` (`tmplvarid`),
  KEY `idx_id` (`contentid`)
) ENGINE=MyISAM AUTO_INCREMENT=146 DEFAULT CHARSET=latin1 COMMENT='Site Template Variables Content Values Link Table';

#
# Dumping data for table `modx_site_tmplvar_contentvalues`
#

INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('25','1','98','assets/images/resources/essays/abstractart.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('26','1','99','assets/images/resources/essays/2003-11-21-2003-Earthshine.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('27','1','100','assets/images/resources/essays/clayset.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('28','1','105','assets/images/resources/facts/1101851209_400.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('29','1','106','assets/images/resources/facts/drinking1.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('30','1','107','assets/images/resources/facts/iStock_Teen_Suicide.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('31','1','108','assets/images/resources/facts/prescription-drugs.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('32','1','109','assets/images/resources/facts/SaintSebastianCaredForByIrene.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('33','1','110','assets/images/resources/interview/albert-einstein.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('34','1','111','assets/images/resources/interview/tonya_needle.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('35','1','112','assets/images/resources/interview/jeanne Randolph.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('36','1','113','assets/images/resources/interview/VanGogh_VanGogh.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('37','1','114','assets/images/resources/assignments/hospital460.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('38','1','115','assets/images/resources/assignments/draw-071120-02-749102.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('39','1','116','assets/images/resources/assignments/write-your-goals1.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('127','2','181','assets/files/heatherBonbons.flv');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('128','3','181','assets/images/videos/heatherBonbons.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('129','2','186','assets/files/picturesOfSelfHarmExcerpts.flv');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('130','3','186','assets/images/videos/selfHarmExcerpts.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('131','2','187','assets/files/gimmeMyFix.flv');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('132','3','187','assets/images/videos/gimmeMyFix.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('133','1','188','assets/images/news/imaginenative.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('134','1','189','assets/images/news/imaginenative.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('135','2','191','assets/files/WhiteToRed.flv');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('136','3','191','assets/images/videos/WTR.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('137','2','192','assets/files/empty.flv');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('138','3','192','assets/images/videos/empty.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('139','1','193','assets/images/news/selfharm_banner(1).jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('140','2','194','assets/files/meaningOfAddiction.flv');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('141','3','194','assets/images/videos/MeaningOfAddiction.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('142','2','195','assets/files/bonbons.flv');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('143','3','195','assets/images/videos/heatherBonbons.jpg');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('144','2','202','assets/files/natureVsIndustrial.flv');
INSERT INTO `modx_site_tmplvar_contentvalues` VALUES ('145','3','202','assets/images/videos/nvi.jpg');

# --------------------------------------------------------

#
# Table structure for table `modx_site_tmplvar_templates`
#

CREATE TABLE `modx_site_tmplvar_templates` (
  `tmplvarid` int(10) NOT NULL default '0' COMMENT 'Template Variable id',
  `templateid` int(11) NOT NULL default '0',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`tmplvarid`,`templateid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Site Template Variables Templates Link Table';

#
# Dumping data for table `modx_site_tmplvar_templates`
#

INSERT INTO `modx_site_tmplvar_templates` VALUES ('1','4','0');
INSERT INTO `modx_site_tmplvar_templates` VALUES ('2','5','0');
INSERT INTO `modx_site_tmplvar_templates` VALUES ('3','5','0');

# --------------------------------------------------------

#
# Table structure for table `modx_site_tmplvars`
#

CREATE TABLE `modx_site_tmplvars` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(20) NOT NULL default '',
  `name` varchar(50) NOT NULL default '',
  `caption` varchar(80) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `editor_type` int(11) NOT NULL default '0' COMMENT '0-plain text,1-rich text,2-code editor',
  `category` int(11) NOT NULL default '0' COMMENT 'category id',
  `locked` tinyint(4) NOT NULL default '0',
  `elements` text,
  `rank` int(11) NOT NULL default '0',
  `display` varchar(20) NOT NULL default '' COMMENT 'Display Control',
  `display_params` text COMMENT 'Display Control Properties',
  `default_text` text,
  PRIMARY KEY  (`id`),
  KEY `indx_rank` (`rank`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Site Template Variables';

#
# Dumping data for table `modx_site_tmplvars`
#

INSERT INTO `modx_site_tmplvars` VALUES ('1','image','newsImage','Post an Image','Image to appear with your news Item','0','0','0','','0','image','&align=none','');
INSERT INTO `modx_site_tmplvars` VALUES ('2','file','video','video','','0','0','0','','0','','&align=none','');
INSERT INTO `modx_site_tmplvars` VALUES ('3','image','videoThumb','thumbnail used for video','','0','0','0','','0','image','&align=none','assets/images/videos/default.jpg');

# --------------------------------------------------------

#
# Table structure for table `modx_system_eventnames`
#

CREATE TABLE `modx_system_eventnames` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `service` tinyint(4) NOT NULL default '0' COMMENT 'System Service number',
  `groupname` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=latin1 COMMENT='System Event Names.';

#
# Dumping data for table `modx_system_eventnames`
#

INSERT INTO `modx_system_eventnames` VALUES ('1','OnDocPublished','5','');
INSERT INTO `modx_system_eventnames` VALUES ('2','OnDocUnPublished','5','');
INSERT INTO `modx_system_eventnames` VALUES ('3','OnWebPagePrerender','5','');
INSERT INTO `modx_system_eventnames` VALUES ('4','OnWebLogin','3','');
INSERT INTO `modx_system_eventnames` VALUES ('5','OnBeforeWebLogout','3','');
INSERT INTO `modx_system_eventnames` VALUES ('6','OnWebLogout','3','');
INSERT INTO `modx_system_eventnames` VALUES ('7','OnWebSaveUser','3','');
INSERT INTO `modx_system_eventnames` VALUES ('8','OnWebDeleteUser','3','');
INSERT INTO `modx_system_eventnames` VALUES ('9','OnWebChangePassword','3','');
INSERT INTO `modx_system_eventnames` VALUES ('10','OnWebCreateGroup','3','');
INSERT INTO `modx_system_eventnames` VALUES ('11','OnManagerLogin','2','');
INSERT INTO `modx_system_eventnames` VALUES ('12','OnBeforeManagerLogout','2','');
INSERT INTO `modx_system_eventnames` VALUES ('13','OnManagerLogout','2','');
INSERT INTO `modx_system_eventnames` VALUES ('14','OnManagerSaveUser','2','');
INSERT INTO `modx_system_eventnames` VALUES ('15','OnManagerDeleteUser','2','');
INSERT INTO `modx_system_eventnames` VALUES ('16','OnManagerChangePassword','2','');
INSERT INTO `modx_system_eventnames` VALUES ('17','OnManagerCreateGroup','2','');
INSERT INTO `modx_system_eventnames` VALUES ('18','OnBeforeCacheUpdate','4','');
INSERT INTO `modx_system_eventnames` VALUES ('19','OnCacheUpdate','4','');
INSERT INTO `modx_system_eventnames` VALUES ('20','OnLoadWebPageCache','4','');
INSERT INTO `modx_system_eventnames` VALUES ('21','OnBeforeSaveWebPageCache','4','');
INSERT INTO `modx_system_eventnames` VALUES ('22','OnChunkFormPrerender','1','Chunks');
INSERT INTO `modx_system_eventnames` VALUES ('23','OnChunkFormRender','1','Chunks');
INSERT INTO `modx_system_eventnames` VALUES ('24','OnBeforeChunkFormSave','1','Chunks');
INSERT INTO `modx_system_eventnames` VALUES ('25','OnChunkFormSave','1','Chunks');
INSERT INTO `modx_system_eventnames` VALUES ('26','OnBeforeChunkFormDelete','1','Chunks');
INSERT INTO `modx_system_eventnames` VALUES ('27','OnChunkFormDelete','1','Chunks');
INSERT INTO `modx_system_eventnames` VALUES ('28','OnDocFormPrerender','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('29','OnDocFormRender','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('30','OnBeforeDocFormSave','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('31','OnDocFormSave','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('32','OnBeforeDocFormDelete','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('33','OnDocFormDelete','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('34','OnPluginFormPrerender','1','Plugins');
INSERT INTO `modx_system_eventnames` VALUES ('35','OnPluginFormRender','1','Plugins');
INSERT INTO `modx_system_eventnames` VALUES ('36','OnBeforePluginFormSave','1','Plugins');
INSERT INTO `modx_system_eventnames` VALUES ('37','OnPluginFormSave','1','Plugins');
INSERT INTO `modx_system_eventnames` VALUES ('38','OnBeforePluginFormDelete','1','Plugins');
INSERT INTO `modx_system_eventnames` VALUES ('39','OnPluginFormDelete','1','Plugins');
INSERT INTO `modx_system_eventnames` VALUES ('40','OnSnipFormPrerender','1','Snippets');
INSERT INTO `modx_system_eventnames` VALUES ('41','OnSnipFormRender','1','Snippets');
INSERT INTO `modx_system_eventnames` VALUES ('42','OnBeforeSnipFormSave','1','Snippets');
INSERT INTO `modx_system_eventnames` VALUES ('43','OnSnipFormSave','1','Snippets');
INSERT INTO `modx_system_eventnames` VALUES ('44','OnBeforeSnipFormDelete','1','Snippets');
INSERT INTO `modx_system_eventnames` VALUES ('45','OnSnipFormDelete','1','Snippets');
INSERT INTO `modx_system_eventnames` VALUES ('46','OnTempFormPrerender','1','Templates');
INSERT INTO `modx_system_eventnames` VALUES ('47','OnTempFormRender','1','Templates');
INSERT INTO `modx_system_eventnames` VALUES ('48','OnBeforeTempFormSave','1','Templates');
INSERT INTO `modx_system_eventnames` VALUES ('49','OnTempFormSave','1','Templates');
INSERT INTO `modx_system_eventnames` VALUES ('50','OnBeforeTempFormDelete','1','Templates');
INSERT INTO `modx_system_eventnames` VALUES ('51','OnTempFormDelete','1','Templates');
INSERT INTO `modx_system_eventnames` VALUES ('52','OnTVFormPrerender','1','Template Variables');
INSERT INTO `modx_system_eventnames` VALUES ('53','OnTVFormRender','1','Template Variables');
INSERT INTO `modx_system_eventnames` VALUES ('54','OnBeforeTVFormSave','1','Template Variables');
INSERT INTO `modx_system_eventnames` VALUES ('55','OnTVFormSave','1','Template Variables');
INSERT INTO `modx_system_eventnames` VALUES ('56','OnBeforeTVFormDelete','1','Template Variables');
INSERT INTO `modx_system_eventnames` VALUES ('57','OnTVFormDelete','1','Template Variables');
INSERT INTO `modx_system_eventnames` VALUES ('58','OnUserFormPrerender','1','Users');
INSERT INTO `modx_system_eventnames` VALUES ('59','OnUserFormRender','1','Users');
INSERT INTO `modx_system_eventnames` VALUES ('60','OnBeforeUserFormSave','1','Users');
INSERT INTO `modx_system_eventnames` VALUES ('61','OnUserFormSave','1','Users');
INSERT INTO `modx_system_eventnames` VALUES ('62','OnBeforeUserFormDelete','1','Users');
INSERT INTO `modx_system_eventnames` VALUES ('63','OnUserFormDelete','1','Users');
INSERT INTO `modx_system_eventnames` VALUES ('64','OnWUsrFormPrerender','1','Web Users');
INSERT INTO `modx_system_eventnames` VALUES ('65','OnWUsrFormRender','1','Web Users');
INSERT INTO `modx_system_eventnames` VALUES ('66','OnBeforeWUsrFormSave','1','Web Users');
INSERT INTO `modx_system_eventnames` VALUES ('67','OnWUsrFormSave','1','Web Users');
INSERT INTO `modx_system_eventnames` VALUES ('68','OnBeforeWUsrFormDelete','1','Web Users');
INSERT INTO `modx_system_eventnames` VALUES ('69','OnWUsrFormDelete','1','Web Users');
INSERT INTO `modx_system_eventnames` VALUES ('70','OnSiteRefresh','1','');
INSERT INTO `modx_system_eventnames` VALUES ('71','OnFileManagerUpload','1','');
INSERT INTO `modx_system_eventnames` VALUES ('72','OnModFormPrerender','1','Modules');
INSERT INTO `modx_system_eventnames` VALUES ('73','OnModFormRender','1','Modules');
INSERT INTO `modx_system_eventnames` VALUES ('74','OnBeforeModFormDelete','1','Modules');
INSERT INTO `modx_system_eventnames` VALUES ('75','OnModFormDelete','1','Modules');
INSERT INTO `modx_system_eventnames` VALUES ('76','OnBeforeModFormSave','1','Modules');
INSERT INTO `modx_system_eventnames` VALUES ('77','OnModFormSave','1','Modules');
INSERT INTO `modx_system_eventnames` VALUES ('78','OnBeforeWebLogin','3','');
INSERT INTO `modx_system_eventnames` VALUES ('79','OnWebAuthentication','3','');
INSERT INTO `modx_system_eventnames` VALUES ('80','OnBeforeManagerLogin','2','');
INSERT INTO `modx_system_eventnames` VALUES ('81','OnManagerAuthentication','2','');
INSERT INTO `modx_system_eventnames` VALUES ('82','OnSiteSettingsRender','1','System Settings');
INSERT INTO `modx_system_eventnames` VALUES ('83','OnFriendlyURLSettingsRender','1','System Settings');
INSERT INTO `modx_system_eventnames` VALUES ('84','OnUserSettingsRender','1','System Settings');
INSERT INTO `modx_system_eventnames` VALUES ('85','OnInterfaceSettingsRender','1','System Settings');
INSERT INTO `modx_system_eventnames` VALUES ('86','OnMiscSettingsRender','1','System Settings');
INSERT INTO `modx_system_eventnames` VALUES ('87','OnRichTextEditorRegister','1','RichText Editor');
INSERT INTO `modx_system_eventnames` VALUES ('88','OnRichTextEditorInit','1','RichText Editor');
INSERT INTO `modx_system_eventnames` VALUES ('89','OnManagerPageInit','2','');
INSERT INTO `modx_system_eventnames` VALUES ('90','OnWebPageInit','5','');
INSERT INTO `modx_system_eventnames` VALUES ('91','OnLoadWebDocument','5','');
INSERT INTO `modx_system_eventnames` VALUES ('92','OnParseDocument','5','');
INSERT INTO `modx_system_eventnames` VALUES ('93','OnManagerLoginFormRender','2','');
INSERT INTO `modx_system_eventnames` VALUES ('94','OnWebPageComplete','5','');
INSERT INTO `modx_system_eventnames` VALUES ('95','OnLogPageHit','5','');
INSERT INTO `modx_system_eventnames` VALUES ('96','OnBeforeManagerPageInit','2','');
INSERT INTO `modx_system_eventnames` VALUES ('97','OnBeforeEmptyTrash','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('98','OnEmptyTrash','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('99','OnManagerLoginFormPrerender','2','');
INSERT INTO `modx_system_eventnames` VALUES ('200','OnCreateDocGroup','1','Documents');
INSERT INTO `modx_system_eventnames` VALUES ('999','OnPageUnauthorized','1','');
INSERT INTO `modx_system_eventnames` VALUES ('1000','OnPageNotFound','1','');

# --------------------------------------------------------

#
# Table structure for table `modx_system_settings`
#

CREATE TABLE `modx_system_settings` (
  `setting_name` varchar(50) NOT NULL default '',
  `setting_value` text,
  UNIQUE KEY `setting_name` (`setting_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains Content Manager settings.';

#
# Dumping data for table `modx_system_settings`
#

INSERT INTO `modx_system_settings` VALUES ('manager_theme','MODx');
INSERT INTO `modx_system_settings` VALUES ('settings_version','0.9.6.3');
INSERT INTO `modx_system_settings` VALUES ('server_offset_time','0');
INSERT INTO `modx_system_settings` VALUES ('server_protocol','http');
INSERT INTO `modx_system_settings` VALUES ('manager_language','english');
INSERT INTO `modx_system_settings` VALUES ('modx_charset','UTF-8');
INSERT INTO `modx_system_settings` VALUES ('site_name','Looking In Speaking Out');
INSERT INTO `modx_system_settings` VALUES ('site_start','1');
INSERT INTO `modx_system_settings` VALUES ('error_page','1');
INSERT INTO `modx_system_settings` VALUES ('unauthorized_page','1');
INSERT INTO `modx_system_settings` VALUES ('site_status','1');
INSERT INTO `modx_system_settings` VALUES ('site_unavailable_message','The site is currently unavailable');
INSERT INTO `modx_system_settings` VALUES ('track_visitors','0');
INSERT INTO `modx_system_settings` VALUES ('resolve_hostnames','0');
INSERT INTO `modx_system_settings` VALUES ('top_howmany','10');
INSERT INTO `modx_system_settings` VALUES ('default_template','4');
INSERT INTO `modx_system_settings` VALUES ('old_template','4');
INSERT INTO `modx_system_settings` VALUES ('publish_default','0');
INSERT INTO `modx_system_settings` VALUES ('cache_default','1');
INSERT INTO `modx_system_settings` VALUES ('search_default','1');
INSERT INTO `modx_system_settings` VALUES ('friendly_urls','1');
INSERT INTO `modx_system_settings` VALUES ('friendly_url_prefix','');
INSERT INTO `modx_system_settings` VALUES ('friendly_url_suffix','');
INSERT INTO `modx_system_settings` VALUES ('friendly_alias_urls','1');
INSERT INTO `modx_system_settings` VALUES ('use_alias_path','0');
INSERT INTO `modx_system_settings` VALUES ('use_udperms','1');
INSERT INTO `modx_system_settings` VALUES ('udperms_allowroot','0');
INSERT INTO `modx_system_settings` VALUES ('failed_login_attempts','3');
INSERT INTO `modx_system_settings` VALUES ('blocked_minutes','60');
INSERT INTO `modx_system_settings` VALUES ('use_captcha','0');
INSERT INTO `modx_system_settings` VALUES ('captcha_words','MODx,Access,Better,BitCode,Cache,Desc,Design,Excell,Enjoy,URLs,TechView,Gerald,Griff,Humphrey,Holiday,Intel,Integration,Joystick,Join(),Tattoo,Genetic,Light,Likeness,Marit,Maaike,Niche,Netherlands,Ordinance,Oscillo,Parser,Phusion,Query,Question,Regalia,Righteous,Snippet,Sentinel,Template,Thespian,Unity,Enterprise,Verily,Veri,Website,WideWeb,Yap,Yellow,Zebra,Zygote');
INSERT INTO `modx_system_settings` VALUES ('emailsender','you@yourdomain.com');
INSERT INTO `modx_system_settings` VALUES ('emailsubject','Your login details');
INSERT INTO `modx_system_settings` VALUES ('signupemail_message','Hello [+uid+] \r\n\r\nHere are your login details for [+sname+] Content Manager:\r\n\r\nUsername: [+uid+]\r\nPassword: [+pwd+]\r\n\r\nOnce you log into the Content Manager at [+surl+], you can change your password.\r\n\r\nRegards,\r\nSite Administrator');
INSERT INTO `modx_system_settings` VALUES ('websignupemail_message','Hello [+uid+] \r\n\r\nHere are your login details for [+sname+]:\r\n\r\nUsername: [+uid+]\r\nPassword: [+pwd+]\r\n\r\nOnce you log into [+sname+] at [+surl+], you can change your password.\r\n\r\nRegards,\r\nSite Administrator');
INSERT INTO `modx_system_settings` VALUES ('webpwdreminder_message','Hello [+uid+]\r\n\r\nTo active you new password click the following link:\r\n\r\n[+surl+]\r\n\r\nIf successful you can use the following password to login:\r\n\r\nPassword:[+pwd+]\r\n\r\nIf you did not request this email then please ignore it.\r\n\r\nRegards,\r\nSite Administrator');
INSERT INTO `modx_system_settings` VALUES ('number_of_logs','100');
INSERT INTO `modx_system_settings` VALUES ('number_of_messages','30');
INSERT INTO `modx_system_settings` VALUES ('number_of_results','20');
INSERT INTO `modx_system_settings` VALUES ('use_editor','1');
INSERT INTO `modx_system_settings` VALUES ('use_browser','1');
INSERT INTO `modx_system_settings` VALUES ('rb_base_dir','/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/assets/');
INSERT INTO `modx_system_settings` VALUES ('rb_base_url','assets/');
INSERT INTO `modx_system_settings` VALUES ('which_editor','TinyMCE');
INSERT INTO `modx_system_settings` VALUES ('fe_editor_lang','english');
INSERT INTO `modx_system_settings` VALUES ('fck_editor_toolbar','standard');
INSERT INTO `modx_system_settings` VALUES ('fck_editor_autolang','0');
INSERT INTO `modx_system_settings` VALUES ('editor_css_path','');
INSERT INTO `modx_system_settings` VALUES ('editor_css_selectors','');
INSERT INTO `modx_system_settings` VALUES ('strip_image_paths','1');
INSERT INTO `modx_system_settings` VALUES ('upload_images','jpg,jpeg,png,gif,psd,ico,bmp');
INSERT INTO `modx_system_settings` VALUES ('upload_media','mp3,wav,au,wmv,avi,mpg,mpeg,flv');
INSERT INTO `modx_system_settings` VALUES ('upload_flash','swf,flv');
INSERT INTO `modx_system_settings` VALUES ('upload_files','txt,php,html,htm,xml,js,css,cache,zip,gz,rar,z,tgz,tar,htaccess,mp3,mp4,aac,wav,au,wmv,avi,mpg,mpeg,pdf,doc,xls,txt,flv');
INSERT INTO `modx_system_settings` VALUES ('upload_maxsize','5048576');
INSERT INTO `modx_system_settings` VALUES ('new_file_permissions','0644');
INSERT INTO `modx_system_settings` VALUES ('new_folder_permissions','0755');
INSERT INTO `modx_system_settings` VALUES ('show_preview','0');
INSERT INTO `modx_system_settings` VALUES ('filemanager_path','/nfs/c03/h02/mnt/52268/domains/lookinginspeakingout.com/html/');
INSERT INTO `modx_system_settings` VALUES ('theme_refresher','');
INSERT INTO `modx_system_settings` VALUES ('manager_layout','4');
INSERT INTO `modx_system_settings` VALUES ('custom_contenttype','text/css,text/html,text/javascript,text/plain,text/xml');
INSERT INTO `modx_system_settings` VALUES ('auto_menuindex','1');
INSERT INTO `modx_system_settings` VALUES ('session.cookie.lifetime','604800');
INSERT INTO `modx_system_settings` VALUES ('mail_check_timeperiod','60');
INSERT INTO `modx_system_settings` VALUES ('manager_lang_attribute','en');
INSERT INTO `modx_system_settings` VALUES ('manager_direction','ltr');
INSERT INTO `modx_system_settings` VALUES ('tinymce_editor_theme','custom');
INSERT INTO `modx_system_settings` VALUES ('tinymce_custom_plugins','style,advimage,advlink,searchreplace,print,contextmenu,paste,fullscreen,nonbreaking,xhtmlxtras,visualchars,media');
INSERT INTO `modx_system_settings` VALUES ('tinymce_custom_buttons1','undo,redo,selectall,separator,pastetext,pasteword,separator,search,replace,separator,nonbreaking,separator,link,unlink,separator,cleanup,removeformat,separator,fullscreen,print,code');
INSERT INTO `modx_system_settings` VALUES ('tinymce_custom_buttons2','bold,italic,underline');
INSERT INTO `modx_system_settings` VALUES ('tree_show_protected','0');
INSERT INTO `modx_system_settings` VALUES ('validate_referer','0');
INSERT INTO `modx_system_settings` VALUES ('site_id','495841fdabf6f');
INSERT INTO `modx_system_settings` VALUES ('rss_url_news','http://feeds.feedburner.com/modx-announce');
INSERT INTO `modx_system_settings` VALUES ('rss_url_security','http://feeds.feedburner.com/modxsecurity');
INSERT INTO `modx_system_settings` VALUES ('xhtml_urls','0');
INSERT INTO `modx_system_settings` VALUES ('site_unavailable_page','');
INSERT INTO `modx_system_settings` VALUES ('allow_duplicate_alias','0');
INSERT INTO `modx_system_settings` VALUES ('automatic_alias','0');
INSERT INTO `modx_system_settings` VALUES ('rb_webuser','1');
INSERT INTO `modx_system_settings` VALUES ('tinymce_custom_buttons3','');
INSERT INTO `modx_system_settings` VALUES ('tinymce_custom_buttons4','');
INSERT INTO `modx_system_settings` VALUES ('tinymce_css_selectors','');
INSERT INTO `modx_system_settings` VALUES ('reset_template','1');

# --------------------------------------------------------

#
# Table structure for table `modx_user_attributes`
#

CREATE TABLE `modx_user_attributes` (
  `id` int(10) NOT NULL auto_increment,
  `internalKey` int(10) NOT NULL default '0',
  `fullname` varchar(100) NOT NULL default '',
  `role` int(10) NOT NULL default '0',
  `email` varchar(100) NOT NULL default '',
  `phone` varchar(100) NOT NULL default '',
  `mobilephone` varchar(100) NOT NULL default '',
  `blocked` int(1) NOT NULL default '0',
  `blockeduntil` int(11) NOT NULL default '0',
  `blockedafter` int(11) NOT NULL default '0',
  `logincount` int(11) NOT NULL default '0',
  `lastlogin` int(11) NOT NULL default '0',
  `thislogin` int(11) NOT NULL default '0',
  `failedlogincount` int(10) NOT NULL default '0',
  `sessionid` varchar(100) NOT NULL default '',
  `dob` int(10) NOT NULL default '0',
  `gender` int(1) NOT NULL default '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(5) NOT NULL default '',
  `state` varchar(25) NOT NULL default '',
  `zip` varchar(25) NOT NULL default '',
  `fax` varchar(100) NOT NULL default '',
  `photo` varchar(255) NOT NULL default '' COMMENT 'link to photo',
  `comment` varchar(255) NOT NULL default '' COMMENT 'short comment',
  PRIMARY KEY  (`id`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Contains information about the backend users.';

#
# Dumping data for table `modx_user_attributes`
#

INSERT INTO `modx_user_attributes` VALUES ('1','1','Nicholas Hillier','1','nicholas@ghostmonk.com','','','0','0','0','81','1255139066','1255370094','0','99aa4342f52c756726fc50bcb09b0bf2','0','0','','','','','','');
INSERT INTO `modx_user_attributes` VALUES ('2','2','Alison Davis','2','newmedia@crossingcommunities.org','','','0','0','0','38','1254851982','1254943920','0','89b3eb47544e0ab999ab2e65feb9ff6d','0','0','','','','','','');
INSERT INTO `modx_user_attributes` VALUES ('3','3','Diary Testing','3','logossuck@gmail.com','','','0','0','0','0','0','0','0','','0','0','','','','','assets/images/diaryProfilePics/profile.jpg','');

# --------------------------------------------------------

#
# Table structure for table `modx_user_messages`
#

CREATE TABLE `modx_user_messages` (
  `id` int(10) NOT NULL auto_increment,
  `type` varchar(15) NOT NULL default '',
  `subject` varchar(60) NOT NULL default '',
  `message` text,
  `sender` int(10) NOT NULL default '0',
  `recipient` int(10) NOT NULL default '0',
  `private` tinyint(4) NOT NULL default '0',
  `postdate` int(20) NOT NULL default '0',
  `messageread` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains messages for the Content Manager messaging system.';

#
# Dumping data for table `modx_user_messages`
#


# --------------------------------------------------------

#
# Table structure for table `modx_user_roles`
#

CREATE TABLE `modx_user_roles` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `frames` int(1) NOT NULL default '0',
  `home` int(1) NOT NULL default '0',
  `view_document` int(1) NOT NULL default '0',
  `new_document` int(1) NOT NULL default '0',
  `save_document` int(1) NOT NULL default '0',
  `publish_document` int(1) NOT NULL default '0',
  `delete_document` int(1) NOT NULL default '0',
  `empty_trash` int(1) NOT NULL default '0',
  `action_ok` int(1) NOT NULL default '0',
  `logout` int(1) NOT NULL default '0',
  `help` int(1) NOT NULL default '0',
  `messages` int(1) NOT NULL default '0',
  `new_user` int(1) NOT NULL default '0',
  `edit_user` int(1) NOT NULL default '0',
  `logs` int(1) NOT NULL default '0',
  `edit_parser` int(1) NOT NULL default '0',
  `save_parser` int(1) NOT NULL default '0',
  `edit_template` int(1) NOT NULL default '0',
  `settings` int(1) NOT NULL default '0',
  `credits` int(1) NOT NULL default '0',
  `new_template` int(1) NOT NULL default '0',
  `save_template` int(1) NOT NULL default '0',
  `delete_template` int(1) NOT NULL default '0',
  `edit_snippet` int(1) NOT NULL default '0',
  `new_snippet` int(1) NOT NULL default '0',
  `save_snippet` int(1) NOT NULL default '0',
  `delete_snippet` int(1) NOT NULL default '0',
  `edit_chunk` int(1) NOT NULL default '0',
  `new_chunk` int(1) NOT NULL default '0',
  `save_chunk` int(1) NOT NULL default '0',
  `delete_chunk` int(1) NOT NULL default '0',
  `empty_cache` int(1) NOT NULL default '0',
  `edit_document` int(1) NOT NULL default '0',
  `change_password` int(1) NOT NULL default '0',
  `error_dialog` int(1) NOT NULL default '0',
  `about` int(1) NOT NULL default '0',
  `file_manager` int(1) NOT NULL default '0',
  `save_user` int(1) NOT NULL default '0',
  `delete_user` int(1) NOT NULL default '0',
  `save_password` int(11) NOT NULL default '0',
  `edit_role` int(1) NOT NULL default '0',
  `save_role` int(1) NOT NULL default '0',
  `delete_role` int(1) NOT NULL default '0',
  `new_role` int(1) NOT NULL default '0',
  `access_permissions` int(1) NOT NULL default '0',
  `bk_manager` int(1) NOT NULL default '0',
  `new_plugin` int(1) NOT NULL default '0',
  `edit_plugin` int(1) NOT NULL default '0',
  `save_plugin` int(1) NOT NULL default '0',
  `delete_plugin` int(1) NOT NULL default '0',
  `new_module` int(1) NOT NULL default '0',
  `edit_module` int(1) NOT NULL default '0',
  `save_module` int(1) NOT NULL default '0',
  `delete_module` int(1) NOT NULL default '0',
  `exec_module` int(1) NOT NULL default '0',
  `view_eventlog` int(1) NOT NULL default '0',
  `delete_eventlog` int(1) NOT NULL default '0',
  `manage_metatags` int(1) NOT NULL default '0' COMMENT 'manage site meta tags and keywords',
  `edit_doc_metatags` int(1) NOT NULL default '0' COMMENT 'edit document meta tags and keywords',
  `new_web_user` int(1) NOT NULL default '0',
  `edit_web_user` int(1) NOT NULL default '0',
  `save_web_user` int(1) NOT NULL default '0',
  `delete_web_user` int(1) NOT NULL default '0',
  `web_access_permissions` int(1) NOT NULL default '0',
  `view_unpublished` int(1) NOT NULL default '0',
  `import_static` int(1) NOT NULL default '0',
  `export_static` int(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Contains information describing the user roles.';

#
# Dumping data for table `modx_user_roles`
#

INSERT INTO `modx_user_roles` VALUES ('1','Administrator','Site administrators have full access to all functions','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1');
INSERT INTO `modx_user_roles` VALUES ('2','Content Manger','manages content for the site','1','1','1','1','1','1','1','1','1','1','1','0','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','1','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','1','1','0','1','0','0');
INSERT INTO `modx_user_roles` VALUES ('3','Diary Participant','Makes diary entries','1','1','1','1','1','0','0','0','1','1','1','0','0','0','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','1','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');

# --------------------------------------------------------

#
# Table structure for table `modx_user_settings`
#

CREATE TABLE `modx_user_settings` (
  `user` int(11) NOT NULL default '0',
  `setting_name` varchar(50) NOT NULL default '',
  `setting_value` text,
  KEY `setting_name` (`setting_name`),
  KEY `user` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains backend user settings.';

#
# Dumping data for table `modx_user_settings`
#

INSERT INTO `modx_user_settings` VALUES ('1','manager_language','english');
INSERT INTO `modx_user_settings` VALUES ('1','manager_direction','ltr');
INSERT INTO `modx_user_settings` VALUES ('1','manager_lang_attribute','en');
INSERT INTO `modx_user_settings` VALUES ('1','allow_manager_access','1');
INSERT INTO `modx_user_settings` VALUES ('2','allow_manager_access','1');
INSERT INTO `modx_user_settings` VALUES ('2','manager_lang_attribute','en');
INSERT INTO `modx_user_settings` VALUES ('2','manager_direction','ltr');
INSERT INTO `modx_user_settings` VALUES ('3','manager_lang_attribute','en');
INSERT INTO `modx_user_settings` VALUES ('3','manager_direction','ltr');
INSERT INTO `modx_user_settings` VALUES ('3','manager_language','english');
INSERT INTO `modx_user_settings` VALUES ('3','allow_manager_access','1');
INSERT INTO `modx_user_settings` VALUES ('2','manager_language','english');

# --------------------------------------------------------

#
# Table structure for table `modx_web_groups`
#

CREATE TABLE `modx_web_groups` (
  `id` int(10) NOT NULL auto_increment,
  `webgroup` int(10) NOT NULL default '0',
  `webuser` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `modx_web_groups`
#


# --------------------------------------------------------

#
# Table structure for table `modx_web_user_attributes`
#

CREATE TABLE `modx_web_user_attributes` (
  `id` int(10) NOT NULL auto_increment,
  `internalKey` int(10) NOT NULL default '0',
  `fullname` varchar(100) NOT NULL default '',
  `role` int(10) NOT NULL default '0',
  `email` varchar(100) NOT NULL default '',
  `phone` varchar(100) NOT NULL default '',
  `mobilephone` varchar(100) NOT NULL default '',
  `blocked` int(1) NOT NULL default '0',
  `blockeduntil` int(11) NOT NULL default '0',
  `blockedafter` int(11) NOT NULL default '0',
  `logincount` int(11) NOT NULL default '0',
  `lastlogin` int(11) NOT NULL default '0',
  `thislogin` int(11) NOT NULL default '0',
  `failedlogincount` int(10) NOT NULL default '0',
  `sessionid` varchar(100) NOT NULL default '',
  `dob` int(10) NOT NULL default '0',
  `gender` int(1) NOT NULL default '0' COMMENT '0 - unknown, 1 - Male 2 - female',
  `country` varchar(5) NOT NULL default '',
  `state` varchar(25) NOT NULL default '',
  `zip` varchar(25) NOT NULL default '',
  `fax` varchar(100) NOT NULL default '',
  `photo` varchar(255) NOT NULL default '' COMMENT 'link to photo',
  `comment` varchar(255) NOT NULL default '' COMMENT 'short comment',
  PRIMARY KEY  (`id`),
  KEY `userid` (`internalKey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains information for web users.';

#
# Dumping data for table `modx_web_user_attributes`
#


# --------------------------------------------------------

#
# Table structure for table `modx_web_user_settings`
#

CREATE TABLE `modx_web_user_settings` (
  `webuser` int(11) NOT NULL default '0',
  `setting_name` varchar(50) NOT NULL default '',
  `setting_value` text,
  KEY `setting_name` (`setting_name`),
  KEY `webuserid` (`webuser`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains web user settings.';

#
# Dumping data for table `modx_web_user_settings`
#


# --------------------------------------------------------

#
# Table structure for table `modx_web_users`
#

CREATE TABLE `modx_web_users` (
  `id` int(10) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL default '',
  `password` varchar(100) NOT NULL default '',
  `cachepwd` varchar(100) NOT NULL default '' COMMENT 'Store new unconfirmed password',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

#
# Dumping data for table `modx_web_users`
#


# --------------------------------------------------------

#
# Table structure for table `modx_webgroup_access`
#

CREATE TABLE `modx_webgroup_access` (
  `id` int(10) NOT NULL auto_increment,
  `webgroup` int(10) NOT NULL default '0',
  `documentgroup` int(10) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `modx_webgroup_access`
#


# --------------------------------------------------------

#
# Table structure for table `modx_webgroup_names`
#

CREATE TABLE `modx_webgroup_names` (
  `id` int(10) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Contains data used for web access permissions.';

#
# Dumping data for table `modx_webgroup_names`
#
