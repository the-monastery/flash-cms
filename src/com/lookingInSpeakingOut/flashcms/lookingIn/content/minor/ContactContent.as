package com.lookingInSpeakingOut.flashcms.lookingIn.content.minor {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.events.FormEvent;
	import com.ghostmonk.ui.composed.ClickableSprite;
	import com.lookingInSpeakingOut.flashcms.cmsUtils.CleanHTML;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.forms.ContactForm;
	import com.lookingInSpeakingOut.flashcms.ui.TabFrameLabelButton;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	import items.contact.ContactSectionAsset;
	import items.form.ResponseDisplayAsset;
	
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class ContactContent extends BaseContent {
		
		
		
		private var _form:ContactForm;
		private var _logo:Sprite;
		private var _address:TextField;
		private var _responseDisplay:ResponseDisplayAsset;
		private var _resetButton:TabFrameLabelButton;
		private var _data:String;
		
		
		
		public function ContactContent( sectionW:int, sectionH:int ) {
		
			super( sectionW, sectionH );
			
			createDisplay();
			
		}
		
		
		
		override protected function onDataLoaded( data:String ):void {
			
			_address.htmlText = "<b>"+CleanHTML.replacePTagsWithBR( data )+"</b>";
			super.onDataLoaded( data );
			
			
		}
		
		
		
		override public function buildIn():void {
			
			Tweener.addTween( this, { alpha:1, time:0.3, delay:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		private function createDisplay() : void {
			
			var contactAssets:ContactSectionAsset = new ContactSectionAsset();
			_form = new ContactForm( contactAssets.contactForm );
			_form.addEventListener( FormEvent.FORM_SUBMIT, displayReset );
			_logo = contactAssets.logo;
			_address = contactAssets.addressField;
			initializeResponseDisplay( contactAssets.responseDisplay );
			
			addChild( _form.view );
			addChild( _logo );
			addChild( _address );	
			
		}
		
		
		
		private function initializeResponseDisplay( display:ResponseDisplayAsset ) : void {
			
			_responseDisplay = display;
			_responseDisplay.alpha = 0;
			_responseDisplay.formReturnBtn.label.text = "Reset";
			_responseDisplay.formSentMsg.text = "Thank you for your inquiry, someone will respond shortly.";
			addChild( _responseDisplay );
						
			_resetButton = new TabFrameLabelButton( _responseDisplay.formReturnBtn, hideReset );
			_resetButton.disable();
			
		}
		
		
		
		private function displayReset( e:FormEvent ) : void {
			
			_resetButton.enable();
			addChild( _responseDisplay );
			Tweener.addTween( _responseDisplay, { alpha:1, time:0.3, delay:0.2, transition:Equations.easeNone } );
			
		}
		
		
		
		private function hideReset( e:MouseEvent ) : void {
			
			_resetButton.disable();
			addChild( _form.view );
			_form.reset();
			Tweener.addTween( _responseDisplay, { alpha:0, time:0.3, transition:Equations.easeNone } );
			
		}

		

	}
}