package com.lookingInSpeakingOut.flashcms.framework.controller {
	
	import com.ghostmonk.net.XMLLoader;
	import com.lookingInSpeakingOut.flashcms.AppFacade;
	import com.lookingInSpeakingOut.flashcms.data.StartupData;
	import com.lookingInSpeakingOut.flashcms.framework.model.ConfigProxy;
	import com.lookingInSpeakingOut.flashcms.framework.model.SpeakingOutProxy;
	import com.lookingInSpeakingOut.flashcms.framework.model.URLRequestProxy;
	
	import flash.display.Stage;
	import flash.events.ErrorEvent;
	import flash.system.Security;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	
	/**
	 * Sets up stageMediator and add ConfigProxy to the Facade
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class StartupCommand extends SimpleCommand {
		
		
		
		private var _configLoader:XMLLoader;
		private var _stage:Stage;
		
		
		
		/**
		 * 
		 * @param notification
		 * 
		 */
		override public function execute( notification:INotification ):void {
			
			var startupData:StartupData = notification.getBody() as StartupData;
			_stage = startupData.stage;
			
			_configLoader = new XMLLoader( startupData.configURL, xmlLoaded );
			_configLoader.errorCall = onError;
			
		}
		
		
		
		private function onError( e:ErrorEvent ) : void {
			
			trace( e );
			
		} 
		
		
		
		private function xmlLoaded( data:String ) : void {
			
			var config:ConfigProxy = new ConfigProxy( XML( data ) );
			
			facade.registerProxy( new URLRequestProxy() );
			facade.registerProxy( new SpeakingOutProxy( config.messageSubmitEndpoint, config.trunkEndpoint, config.leafEndpoint ) );
			facade.registerProxy( config );
			
			setSecurityDomains( config.security );
			
			facade.sendNotification( AppFacade.CREATE_APP_DEPENDENCIES, _stage );
			
		}
		
		
		
		private function setSecurityDomains( xml:XML ):void {
			
			for each( var domain:XML in xml.domain ) {
				Security.allowDomain( domain );
			}
			
		}
		
		
		
	}
}