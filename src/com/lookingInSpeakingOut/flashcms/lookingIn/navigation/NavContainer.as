package com.lookingInSpeakingOut.flashcms.lookingIn.navigation {
	
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.ColorShortcuts;
	
	import com.ghostmonk.display.graphics.shapes.Polygon;
	import com.lookingInSpeakingOut.flashcms.events.core.MenuEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	[Event (type="com.lookingspeakingout.flashcms.events.MenuEvent", name="morph")]
	[Event (type="com.lookingspeakingout.flashcms.events.NavigationEvent", name="subNavigation")]
	[Event (type="com.lookingspeakingout.flashcms.events.NavigationEvent", name="mainNavigation")]
	
	/**
	 * Navigation menu... self explanitory for the most part.. for now 
	 * Mostly a presentation and display device maintaining the logic for displaying, opening and closing 
	 * itself... will contain a number of other objects for mediation
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class NavContainer extends Sprite {	
		
		
		private var _background:NavBackground;
		private var _mainMenu:Menu;
		private var _subMenu:Menu;
		private var _indicator:Polygon;
		
		private var _yPos:Number;
		
		private var _isOpen:Boolean;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get isOpen():Boolean {
			 
			return _isOpen;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get yPos():Number {
			 
			return _yPos;
			 
		}
		
		
		/**
		 * 
		 * @param bg		-NavBackground, background object
		 * @param main		-Menu - main menu dispatching NavigationEvents
		 * @param sub		-Menu - sub menu dispatching NavigationEvents... used for footer information
		 * @param search	-SearchForm - A form that dispatches a SearchEvent
		 * @param login		-LoginForm - A form that dispatches a LoginEvent
		 * @param indicator	-Polygon - a simple Shape object that indicated opening and closing of the menu
		 * 
		 */
		public function NavContainer( bg:NavBackground, main:Menu, sub:Menu, indicator:Polygon ) {
					
			ColorShortcuts.init();

			_background = bg;
			_mainMenu = main;
			_subMenu = sub;
			
			_indicator = indicator;
			_subMenu.enable();
			
			_isOpen = false;
			
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			_mainMenu.addEventListener( NavigationEvent.MAJOR_SECTION, onNavigation );
			_subMenu.addEventListener( NavigationEvent.MINOR_SECTION, onNavigation );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function enable():void {
			
			addEventListener( MouseEvent.ROLL_OVER, open );
			addEventListener( MouseEvent.ROLL_OUT, close );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function disable():void {
			
			removeEventListener( MouseEvent.ROLL_OVER, open );
			removeEventListener( MouseEvent.ROLL_OUT, close );
			
		}
		
		
		
		/**
		 * Positions the navigation menu to the bottom center of the stage.
		 * Should only be called when NavContainer object has access to the stage.
		 */
		public function onStageResize():void {
			
			var destY:Number = stage.stageHeight - _background.restHeight / 2 - 20;
			_yPos = destY - _background.restHeight - 20;
			
			Tweener.addTween (
				this, {
					x:stage.stageWidth / 2, 
					y:destY, 
					time:0.3
				}
			);
			
		}
		
		
		
		/**
		 * 
		 * @param isOpen	- Boolean inidicating state of menu (open or closed)
		 * @return 			- Point relative to the stage of the top left corner of the stage when open or closed	
		 * 
		 */
		public function globalPositionPoint( isOpen:Boolean ):Point {
			
			var yVal:Number = isOpen ? -_background.openHeight : -_background.restHeight;
			return localToGlobal( new Point( _background.x, yVal - 20 ) );
			
		}
		
		
		
		private function onAddedToStage( e:Event ):void {
			
			x = stage.stageWidth/2;
			y = stage.stageHeight - _background.restHeight/2 - 20;
			
			positionAssets();
			
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
			addChild( _background );
			addChild( _subMenu );
			
			_background.buildIn( backGroundReady );
			
		}
		
		
		
		private function backGroundReady():void {
			
			enable();
			
			var currentMousePoint:Point = new Point( stage.mouseX, stage.mouseY );
			var objectsUnderMousePoint:Array = stage.getObjectsUnderPoint( currentMousePoint );
			
			for each( var obj:Object in objectsUnderMousePoint ) {
				if( obj == _background ) {
					open();
				}
			}
			
			addChild( _indicator );
			dispatchEvent( new MenuEvent( MenuEvent.INITIALIZED, false ) );
			
		}
		
		
		
		private function open( e:MouseEvent = null ):void {
			
			dispatchEvent( new MenuEvent( MenuEvent.MORPH, true ) );
			_background.open( onOpenComplete );
			
			Tweener.addTween (
				_indicator, {
					_color:0xFFFFFF, 
					rotation:90, 
					y:-_background.openHeight + 30, 
					time:0.3
				}
			);
			
		}
		
		
		
		private function close( e:MouseEvent = null ):void {
			
			dispatchEvent( new MenuEvent( MenuEvent.MORPH, false ) );
			//Only call hide on the following elements if onOpenComplete() was successfully called
			
			if( _isOpen ) {
				_mainMenu.hide();
			}
			
			_isOpen = false;
			_background.close();
			
			Tweener.addTween( 
				_indicator, { 
					_color:_indicator.color, 
					rotation:-90, 
					y:0, 
					time:0.3 
				} 
			);
			
		}
		
		
		
		private function onOpenComplete():void {
			
			_isOpen = true;
			addChild( _mainMenu );
			_mainMenu.display();
			
		}
		
		
		
		private function positionAssets():void {
			
			_subMenu.x = -_subMenu.width / 2;
			_subMenu.y = -_subMenu.height / 2;
			_mainMenu.x = -_mainMenu.width / 2;
			_mainMenu.y = - _background.openHeight + 20;
			_indicator.x = -_background.width / 2 + 15;
			
		}
		
		
		
		private function onNavigation( e:NavigationEvent ):void {
			
			dispatchEvent( e );
			
		}
		
		
		
	}
} 