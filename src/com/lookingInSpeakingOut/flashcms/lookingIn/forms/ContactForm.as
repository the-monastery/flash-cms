package com.lookingInSpeakingOut.flashcms.lookingIn.forms {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.events.FormEvent;
	import com.ghostmonk.form.FormField;
	import com.ghostmonk.form.FormFieldCollection;
	import com.ghostmonk.form.utils.FieldRestrictions;
	import com.ghostmonk.form.utils.RegExFormEvaluations;
	import com.ghostmonk.ui.composed.ClickableSprite;
	import com.lookingInSpeakingOut.flashcms.ui.TabFrameLabelButton;
	
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	[Event (name="formSubmit", type="com.ghostmonk.events.FormEvent")]
	
	
	public class ContactForm extends EventDispatcher {
		
		
		
		private var _form:ContactFormAsset;
		private var _formFieldCollection:FormFieldCollection;
		
		private var _message:FormField;
		private var _email:FormField;
		private var _name:FormField;
		
		private var _submit:TabFrameLabelButton;
		
		
		public function ContactForm( view:ContactFormAsset ) {
			
			_form = view;
			_formFieldCollection = new FormFieldCollection();
			createFormFunctionality();
			
		}
		
		
		
		public function get view() : ContactFormAsset {
			
			return _form;
			
		}
		
		
		
		public function reset() : void {
			
			_name.reset();
			_email.reset();
			_message.reset();
			_submit.enable();
			
			Tweener.addTween( _form, { alpha:1, time:0.3, delay:0.2, transition:Equations.easeNone } ); 
			
		}
		
		
		
		private function createFormFunctionality() : void {
			
			_submit = new TabFrameLabelButton( _form.submitBtn, onSubmit );
			
			var regEx:RegExFormEvaluations = new RegExFormEvaluations();
			var restrictions:FieldRestrictions = new FieldRestrictions();
			
			_message = new FormField( 0, "Your Message Here", "Please Enter a Message", _form.messageField, _form.messageBG );
			_name = new FormField( 1, "Your Full Name", "You must enter your name.", _form.nameField, _form.nameBG );
			_email = new FormField( 2, "Your Email", "You must enter a valid email.", _form.emailField, _form.emailBG, regEx.email );
			
			_name.restriction = restrictions.alphaSpace;
			_email.restriction = restrictions.email;
			_message.isRequired = true;
			_name.isRequired = true;
			_email.isRequired = true;
			
			_formFieldCollection.addField( _message );
			_formFieldCollection.addField( _name );
			_formFieldCollection.addField( _email );
			
		}
		
		
		
		private function onSubmit( e:MouseEvent ) : void {
			
			_form.errorField.text = "";
			var valid:Boolean = true;
			
			for each( var field:FormField in _formFieldCollection.fields ) {
				valid = validate( field );
				if( !valid ) {
					break;
				}
			}
			
			if( valid ) {
				submitForm();
			}
			
		}
		
		
		private function validate( field:FormField ) : Boolean {
			
			if( !field.isValid ) {
				_form.errorField.text = field.errorText;
				_form.stage.focus = field.textField;
				return false;
			}
			
			return true;
			
		}
		
		
		
		private function submitForm() : void {
			
			_submit.disable();
			Tweener.addTween( _form, { alpha:0, time:0.3, transition:Equations.easeNone, onComplete:formBuildOutComplete } );
			dispatchEvent( new FormEvent( FormEvent.FORM_SUBMIT ) );
			
			var vars:URLVariables = new URLVariables();
            vars.name =  _name.textField.text;
            vars.email = _email.textField.text;
            vars.msg = _message.textField.text;
            vars.fromFlash = "conundrum";
			
            var request:URLRequest = new URLRequest( "http://www.lookinginspeakingout.com/signup/submitEmail.php" );
            request.method = URLRequestMethod.POST;            
            request.data = vars;
            
            var urlLoader:URLLoader = new URLLoader();
            //urlLoader.load( request );
        	
		}
		
		
		
		private function formBuildOutComplete() : void {
			
			if( _form.parent ) {
				_form.parent.removeChild( _form );
			}
			
		}
		
		
		
		
	}
}