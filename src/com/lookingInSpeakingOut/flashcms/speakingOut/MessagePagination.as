package com.lookingInSpeakingOut.flashcms.speakingOut {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	import com.lookingInSpeakingOut.flashcms.events.core.PaginationEvent;
	
	import flash.events.MouseEvent;
	import flash.events.TextEvent;
	import flash.text.StyleSheet;
	
	import speakingoutassets.pagination.PaginationMenuAsset;
	
	[Event (name="pageNavigation", type="com.lookingInSpeakingOut.flashcms.events.core.PaginationEvent")]

	/**
	 * 
	 * @author ghostmonk 2009-10-17
	 * 
	 */
	public class MessagePagination extends PaginationMenuAsset {
		
		
		
		private static const ETC_NEXT:String = "etcNext";
		private static const ETC_BACK:String = "etcBack";
		private static const SET_LENGTH:int = 10;
		
		private var _yPos:int;
		private var _previous:FrameLabelButton;
		private var _next:FrameLabelButton;
		
		private var _currentPage:int;
		private var _currentSet:int;
		
		private var _totalPages:int;
		private var _totalSets:int;
		
		private var _isOpen:Boolean;
		private var _styleSheet:StyleSheet;
		
		
		
		public function MessagePagination( yPos:int ) {
			
			alpha = 0;
			_yPos = yPos;
			y = _yPos + 20;
			
			_previous = new FrameLabelButton( previousBtn );
			_previous.addClickCallback( onPreviousPage );
			
			_next = new FrameLabelButton( nextBtn );
			_next.addClickCallback( onNextPage );
			
			currentPage = totalPages = 1;
			_isOpen = false;0
			
			_styleSheet = new StyleSheet();
			_styleSheet.setStyle( "a:hover", {color:"#FFFFFF"} );
			_styleSheet.setStyle( ".current", {color:"#FFFFFF"} );
			paginatedLine.styleSheet = _styleSheet;
			
		}
		
		
		
		public function set totalPages( value:int ) : void {
			
			_totalPages = value;
			_totalSets = Math.ceil( _totalPages / SET_LENGTH );
			currentPage = _currentPage;	
			
		}
		
		
		
		public function set currentPage( value:int ) : void {
			
			_currentPage = Math.min( _totalPages, Math.max( 0, value ) );
			_currentSet = Math.ceil( _currentPage / SET_LENGTH );
			testBtns();
			setPaginationLine();
			
		}
		
		
		
		public function buildIn() : void {
			
			if( _totalPages > 1 ) {
				Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone, onComplete:buildInComplete } );
				Tweener.addTween( this, { y:_yPos, time:0.3 } );
				_isOpen = true;
			}
			
		}
		
		
		
		public function buildOut() : void {
			
			if( _isOpen ) {
				disable();
				_isOpen = false;
				Tweener.addTween( this, { alpha:0, time:0.3, transition:Equations.easeNone, onComplete:buildOutComplete } );
				Tweener.addTween( this, {  y:_yPos + 20, time:0.3 } );
			}
			
		}
		
		
		
		public function enable() : void {
			
			mouseChildren = true;
			testBtns();
			paginatedLine.alpha = 1;
			paginatedLine.styleSheet = _styleSheet;
			paginatedLine.addEventListener( TextEvent.LINK, onLink );
			
		}
		
		
		
		public function disable() : void {
			
			mouseChildren = false;
			disableBtn( _next );
			disableBtn( _previous );
			paginatedLine.alpha = 0.5;
			paginatedLine.styleSheet = null;
			paginatedLine.removeEventListener( TextEvent.LINK, onLink );
			
		}
		
		
		
		private function buildOutComplete() : void {
			
			if( parent ) {
				parent.removeChild( this );
			}
			
		}
		
		
		
		private function buildInComplete() : void {
			
			enable();
			
		}
		
		
		private function onPreviousPage( e:MouseEvent ) : void {
			
			currentPage = Math.max( 1, --_currentPage );
			dispatchPageUpdate();
			
		}
		
		
		
		private function onNextPage( e:MouseEvent ) : void {
			
			currentPage = Math.min( _totalPages, ++_currentPage );
			dispatchPageUpdate();
			
		}
		
		
		
		private function testBtns() : void {
			
			_currentPage == _totalPages ? disableBtn( _next ) : enableBtn( _next );
			_currentPage == 1 ? disableBtn( _previous ) : enableBtn( _previous );			
			
		}
		
		
		
		private function dispatchPageUpdate() : void {
			
			dispatchEvent( new PaginationEvent( PaginationEvent.PAGE_NAVIGATION, _currentPage ) );
			
		}
		
		
		
		private function disableBtn( btn:FrameLabelButton ) : void {
			
			btn.disable();
			Tweener.removeTweens( btn.view );
			btn.view.gotoAndStop( 1 );
			btn.view.alpha = 0.5;
			
		}
		
		
		
		private function enableBtn( btn:FrameLabelButton ) : void {
			
			btn.view.gotoAndStop( 1 );
			btn.enable();
			btn.view.alpha = 1;
			
		}
		
		
		private function setPaginationLine() : void {
			
			var leftMost:int = ( ( _currentSet - 1 ) * SET_LENGTH ) + 1;
			var iterations:int = _totalPages <= SET_LENGTH ?  _totalPages :  leftMost + SET_LENGTH - 1;
			
			var htmlText:String = _currentSet > 1 ? "<b><a href='event:"+ETC_BACK+"'>... </a>" : "<b>";
			
			for( var i:int = leftMost; i <= iterations; i++ ) {
				htmlText += getPageLink( i, iterations );
			}
			
			htmlText += _currentSet < _totalSets ? "<a href='event:"+ETC_NEXT+"'> ...</a></b>" : "</b>";
			
			paginatedLine.htmlText = htmlText;
			
		}
		
		
		
		private function getPageLink( page:int, lastIndex:int ) : String {
			
			var output:String = "";
			
			if( page == _currentPage ) {
				output += "<span class='current'>" + page + "</span>";
			}
			else {
				output += "<a href='event:" + page + "'>" + page + "</a>";
			}
			
			if( page != lastIndex ) {
				output += " - ";
			}
			
			return output;
			
		}
		
		
		
		private function onLink( e:TextEvent ) : void {
			
			switch( e.text ) {
				
				case ETC_BACK:
					_currentSet-=2;
					
				case ETC_NEXT:
					currentPage = _currentSet * SET_LENGTH + 1;
					break;
					
				default:
					currentPage = parseInt( e.text );
					break;
			}
			
			dispatchPageUpdate();
			
		}
		
		
		
	}
}