<?php 
	session_start();
	if( !isset($_SESSION['username']) ) {
		$url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);
		if( ( substr( $url, -1 ) == '/' ) OR substr( $url, -1 ) == '\\' ) {
			$url = substr( $url, 0, -1 );
		}
		$url .= '/index.php';
		header( "Location: $url" );
		exit();
	}
?>