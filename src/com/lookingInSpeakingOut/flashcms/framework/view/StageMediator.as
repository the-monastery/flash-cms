package com.lookingInSpeakingOut.flashcms.framework.view {
	
	import com.ghostmonk.display.graphics.shapes.SimpleRectangle;
	import com.lookingInSpeakingOut.flashcms.events.LISOEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.framework.FrameworkEvents;
	import com.lookingInSpeakingOut.flashcms.lookingIn.clients.MajorSectionManager;
	import com.lookingInSpeakingOut.flashcms.rootView.Header;
	import com.lookingInSpeakingOut.flashcms.rootView.Sky;
	import com.lookingInSpeakingOut.flashcms.rootView.Title;
	import com.lookingInSpeakingOut.flashcms.speakingOut.Landscape;
	
	import flash.display.DisplayObject;
	import flash.display.Stage;
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	
	/**
	 *  
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class StageMediator extends Mediator {
		
		
		public static const NAME:String = "StageMediator";
		
		private var _sky:Sky;
		private var _landscape:Landscape;
		private var _title:Title;
		private var _header:Header;
		private var _stageCover:SimpleRectangle;
		private var _manager:MajorSectionManager;
		
		
		
		
		/**
		 * 
		 * @param viewComponent
		 * @param background
		 * @param header
		 * 
		 */
		public function StageMediator( viewComponent:Stage, sky:Sky, landscape:Landscape, header:Header, title:Title ) {
			
			//min width and height 1008 X 661
			super( NAME, viewComponent );
			
			_stageCover = new SimpleRectangle( 0, stage.stageWidth, stage.stageHeight );
			
			_sky = sky;
			stage.addChild( _sky );
			
			_header = header;
			stage.addChild( _header );
			_header.alpha = 0;
			_header.stop();
			
			_title = title;
			_title.addEventListener( LISOEvent.LOOKING_IN_APP, onTitleSwap )
			_title.addEventListener( LISOEvent.SPEAKING_OUT_APP, onTitleSwap )
			stage.addChild( _title );
			
			_landscape = landscape;	
			stage.addChild( _landscape.tree );
			stage.addChild( _landscape.stump );
			
			stage.addEventListener( Event.RESIZE, onResize );
			stage.stageFocusRect = false;
			
		}
		
		
		
		public function get stage():Stage {
			
			return viewComponent as Stage;
			
		}
		
		
		
		/**
		 * Make sure any object added to the stage is beneath the navigation container
		 * @param view
		 * 
		 */
		public function addChildUnderNavBar( view:DisplayObject ):void {
			
			var navContainerMediator:NavContainerMediator = facade.retrieveMediator( NavContainerMediator.NAME ) as NavContainerMediator;
			
			if( navContainerMediator ) {
				stage.addChildAt( view, stage.getChildIndex( navContainerMediator.navContainer ) );
			}
			else {
				stage.addChild( view );
			}
			
		}
		
		
		
		override public function listNotificationInterests():Array {
			
			return [
				FullScreenEvent.FULL_SCREEN,
				NavigationEvent.MAJOR_SECTION,
				NavigationEvent.MINOR_SECTION,
				SectionEvent.SECTIONS_REMOVED,
				NavigationEvent.STARTING_SECTION,
				LISOEvent.LOOKING_IN_APP,
				LISOEvent.SPEAKING_OUT_APP,
				"mainMenuSpeakingout"
			];
			
		}
		
		
		
		override public function handleNotification(note:INotification):void {
			
			switch( note.getName() ) {
					
				case FullScreenEvent.FULL_SCREEN: 	
					onFullScreen(); 	
					break;
					
				case NavigationEvent.MAJOR_SECTION:		
				case NavigationEvent.MINOR_SECTION:
					_header.alpha = 1;
					_title.setLookingIn();
					break;
					
				case SectionEvent.SECTIONS_REMOVED:
					_title.setSpeakingOut();
					break;
					
				case NavigationEvent.STARTING_SECTION:
					_title.enable();
					break;
				
				case LISOEvent.LOOKING_IN_APP:
					_header.fadeIn();
					break;
					
				case LISOEvent.SPEAKING_OUT_APP:
					_header.fadeOut();
					break;
				
				case "mainMenuSpeakingout":	
					setSpeakingOut();
					break;
					
			}
			
		}
		
		
		
		public function set manager( value:MajorSectionManager ):void {
			
			_manager = value;
			addChildUnderNavBar( _manager );
			_header.position( stage, _manager.y );
			_header.alpha = 1;
			_header.play();
			
			_title.y = _header.y + _header.titleMarker.y;
			_title.x = _header.x + _header.titleMarker.x;
			
			_landscape.positionLandscape( stage );
			_landscape.buildIn();
			
		}
		
		
		
		public function setSpeakingOut() : void {
			
			_title.setSpeakingOut();
			sendNotification( LISOEvent.SPEAKING_OUT_APP );
			
		}
		
		
		
		private function onResize( e:Event ):void {
			
			_stageCover.height = stage.stageHeight;
			_stageCover.width = stage.stageWidth;
			sendNotification( FrameworkEvents.STAGE_RESIZE, stage );
			_header.position( stage, _manager.y );
			_title.y = _header.y + _header.titleMarker.y;
			_title.x = _header.x + _header.titleMarker.x;
			_sky.position();
			_landscape.positionLandscape( stage );
			
		}
		
		
		
		private function onFullScreen():void {
			
			if( stage.displayState == StageDisplayState.FULL_SCREEN ) {
				stage.displayState = StageDisplayState.NORMAL;
			}
			else {
				stage.addChild( _stageCover );
				stage.displayState = StageDisplayState.FULL_SCREEN;
				stage.addEventListener( FullScreenEvent.FULL_SCREEN, onNormalScreen );
			}
			
		}
		
		
		
		private function onNormalScreen( e:Event = null ):void {
			
			stage.removeEventListener( FullScreenEvent.FULL_SCREEN, onNormalScreen );
			stage.removeChild( _stageCover );
			stage.displayState = StageDisplayState.NORMAL;
			sendNotification( StageDisplayState.NORMAL );
			
		}
		
		
		
		private function onTitleSwap( e:LISOEvent ) : void {
			
			sendNotification( e.type );
			
		}
		
		
		
	}
}