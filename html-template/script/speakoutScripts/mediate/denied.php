<?php
	
	$title = "Rejected Stories";	
	include 'pageParts/checkSession.php';
	include 'pageParts/header.php';
	include 'pageParts/navigation.php';
	include 'pageParts/functions.php';
	
	mediateMsg( $mysqli, $_GET['approve'], $_GET['id'] );
	
	$page = isset( $_GET['page'] ) ? $_GET['page'] : 1;	
	$messages = getMessagesPrintPagination( $mysqli, 0, 1, $page );
	
	printTableRows( $messages, $page );
	
	$mysqli->close();
	
	include 'pageParts/footer.php';
	
?>