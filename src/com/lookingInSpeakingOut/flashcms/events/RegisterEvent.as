package com.lookingInSpeakingOut.flashcms.events {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class RegisterEvent extends Event
	{
		
		
		
		public static const REGISTER:String = "register";
		
		
		
		/**
		 * 
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function RegisterEvent( type:String, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			super( type, bubbles, cancelable );
			
		}
		
		
		
	}
}