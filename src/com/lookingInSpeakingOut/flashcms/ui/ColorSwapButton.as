package com.lookingInSpeakingOut.flashcms.ui {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.composed.InteractiveSprite;
	import com.ghostmonk.utils.ReadDisplayProps;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	
	public class ColorSwapButton {
		
		
		
		private var _mouseFunctions:InteractiveSprite;
		
		private var _icon:Sprite;
		private var _iconOverColor:uint;
		private var _iconBaseColor:uint;		
		
		private var _bg:Sprite;
		private var _bgOverColor:uint;
		private var _bgBaseColor:uint;
	
		private var _clickCallback:Function;
		
		
		public function get view():Sprite {
			
			return _mouseFunctions.view;
			
		}
		
		
		
		public function set iconOverColor( color:uint ):void {
			
			_iconOverColor = color;
			
		}
		
		
		
		public function set bgOverColor( color:uint ):void {
			
			_bgOverColor = color;
			
		}
		
		
		
		/**
		 * Changes the color of specified elements of a button on mouse interaction  
		 * The icon color is swaped with the background color
		 * @param view
		 * 
		 */
		public function ColorSwapButton( view:Sprite, icon:Sprite, bg:Sprite, clickCallBack:Function ) {
			
			var _mouseFunctions:InteractiveSprite = new InteractiveSprite( view );
			_mouseFunctions.mouseClickFunc = onMouseClick;
			_mouseFunctions.mouseOutFunc = onMouseOut;
			_mouseFunctions.mouseOverFunc = onMouseOver;
			
			_icon = icon;
			_iconBaseColor = ReadDisplayProps.getObjectColor( icon );
			
			_bg = bg;
			_bgBaseColor = ReadDisplayProps.getObjectColor( bg );
			
			_iconOverColor = _bgBaseColor;
			_bgOverColor = _iconBaseColor;
			
			_clickCallback = clickCallBack;
			
		}
		
		
		
		/**
		 * Enable mouse interactivity 
		 * 
		 */
		public function enable():void {
			
			_mouseFunctions.enable();
			
		}
		
		
		
		/**
		 * Disable mouse interactivity
		 * 
		 */
		public function disable():void {
			
			_mouseFunctions.disable();
			
		}
		
		
		
		private function onMouseOver( e:MouseEvent ):void {
			
			tweenColor( _icon, _iconOverColor );
			tweenColor( _bg, _bgOverColor );
			
		}
		
		
		
		private function onMouseOut( e:MouseEvent ):void {
			
			tweenColor( _icon, _iconBaseColor );
			tweenColor( _bg, _bgBaseColor );
				
		}
		
		
		
		private function onMouseClick( e:MouseEvent ):void {
		
			_clickCallback( e )
		
		}
		
		
		private function tweenColor( item:Sprite, destColor:uint ):void {
			
			Tweener.addTween( 
				item, {
					_color:destColor,
					time:0.2,
					transition:Equations.easeNone	
				}
			);
			
		}

	}
}