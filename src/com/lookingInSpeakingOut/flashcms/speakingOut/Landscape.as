package com.lookingInSpeakingOut.flashcms.speakingOut {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.MessageDataCollection;
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.SubmitMessageData;
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.XMLMessageData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.PaginationEvent;
	import com.lookingInSpeakingOut.flashcms.events.speakOut.FormSubmitEvent;
	import com.lookingInSpeakingOut.flashcms.events.speakOut.ReplyEvent;
	import com.lookingInSpeakingOut.flashcms.events.speakOut.TrunkMessageEvent;
	import com.lookingInSpeakingOut.flashcms.speakingOut.forms.SpeakOutForm;
	import com.lookingInSpeakingOut.flashcms.speakingOut.messageDisplay.LeafMessage;
	import com.lookingInSpeakingOut.flashcms.speakingOut.messageDisplay.TrunkMessage;
	import com.lookingInSpeakingOut.flashcms.ui.HelpBtn;
	import com.lookingInSpeakingOut.flashcms.ui.StartThreadButton;
	
	import flash.display.BlendMode;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.utils.Dictionary;
	
	import speakingoutassets.landscape.LandscapeStump;
	import speakingoutassets.landscape.LandscapeTree;
	
	[Event (name="submitTrunkMessage", type="com.lookingInSpeakingOut.flashcms.events.FormSubmitEvent")]
	[Event (name="pageNavigation", type="com.lookingInSpeakingOut.flashcms.events.core.PaginationEvent")]
	
	/**
	 * 
	 * @author ghostmonk 2009-19-09
	 * 
	 */
	public class Landscape extends EventDispatcher {
		
		
		
		private const TREE_X_OFFSET:Number = 362;
		private const STUMP_Y_OFFSET:Number = 369;
		private const MSG_LIMIT:int = 20;
		
		private var _submitEndpoint:String;
		
		private var _stump:LandscapeStump;
		private var _tree:LandscapeTree;
		private var _paginationMenu:MessagePagination;
		
		private var _calculatedWidth:Number;
		private var _calculatedHeight:Number;
		
		private var _form:SpeakOutForm;
		private var _startThreadBtn:StartThreadButton;
		private var _helpBtn:HelpBtn;
		
		private var _closedFormPoint:Point;	
		private var _messageCache:Dictionary;
		
		private var _threadMarkers:Array;
		private var _leafViews:Array;
		
		private var _currentFormParent:DisplayObjectContainer;
		private var _isLookingInMode:Boolean;
		
		private var _helpScreen:HelpScreen;
		
		private var _isFirstMessageDisplay:Boolean;
		
		
		
		public function Landscape( submitEndpoint:String, helpTitle:String, helpMain:String ) {
			
			_submitEndpoint = submitEndpoint;
			
			_stump = new LandscapeStump();
			
			_tree = new LandscapeTree();
			_stump.startThreadView.alpha = 0;
			_tree.blendMode = _stump.blendMode = BlendMode.LAYER;
			_tree.alphaAnimation.stop();
			_stump.alphaAnimation.stop();
			_tree.mouseChildren = false;
			_tree.mouseEnabled = false;
			
			_paginationMenu = new MessagePagination( _stump.height - 110 );
			_paginationMenu.x = ( ( _stump.width + TREE_X_OFFSET ) - _paginationMenu.width ) * 0.5 + 30;
			_paginationMenu.addEventListener( PaginationEvent.PAGE_NAVIGATION, onPageNavigation );	
			
			_calculatedWidth = _tree.width + TREE_X_OFFSET;
			_calculatedHeight = _tree.height;
			
			_form = new SpeakOutForm();
			_form.addEventListener( DisplayEvent.CLOSE_BTN_CLICKED, closeSubmitForm );
			_form.addEventListener( FormSubmitEvent.SUBMIT_SPEAKOUT_MESSAGE, onFormSubmit );
			
			_startThreadBtn = new StartThreadButton( _stump.startThreadView );
			_startThreadBtn.view.tabEnabled = false;
			_startThreadBtn.addEventListener( TrunkMessageEvent.ADD_MESSAGE, addTrunkMessage );
			
			_helpScreen = new HelpScreen( helpTitle, helpMain );
			_helpBtn = new HelpBtn( _stump.helpBtn, onHelp );
			_helpBtn.view.tabEnabled = false;	
			
			_messageCache = new Dictionary();
			
			_threadMarkers = new Array();
			_leafViews = new Array();
			fillMessageItemArrays();
			
			_isLookingInMode = true;
			_isFirstMessageDisplay = true;
			
		}
		
		
		
		public function get width() : Number {
			
			return _calculatedWidth;
			
		}
		
		
		
		public function get height() : Number {
			
			return _calculatedHeight;
			
		}
		
		
		
		public function get stump() : LandscapeStump {
			
			return _stump;
			
		}
		
		
		
		public function get tree() : LandscapeTree {
			
			return _tree;
			
		}
		
		
		
		public function set messages( collection:MessageDataCollection ) : void {
			
			for each( var clearMarker:ThreadMarker in _threadMarkers ) {
				clearMarker.clearData();
			}
			
			var index:int = 0;
			_paginationMenu.totalPages = collection.totalPages;
			_paginationMenu.currentPage = collection.currentPage;
			for each( var data:XMLMessageData in collection.messages ) {
				 var marker:ThreadMarker = _threadMarkers[ index ] as ThreadMarker;
				 marker.setData( data );
				 index++;
			}
			
			initializeMessageView();
			
			if( _isLookingInMode == false ) {
				showMessageMarkers();
			}
			
		}
		
		
		
		public function buildIn() : void {
			
			var treeAlpha:MovieClip = _tree.alphaAnimation;
			var stumpAlpha:MovieClip = _stump.alphaAnimation;
			
			var treeTime:Number = ( treeAlpha.totalFrames - treeAlpha.currentFrame ) / 31;
			var stumpTime:Number = ( stumpAlpha.totalFrames - stumpAlpha.currentFrame ) / 31;
			
			Tweener.addTween( _stump.startThreadView, { alpha:1, time:1, delay:2, transition:Equations.easeNone } );
			Tweener.addTween( treeAlpha, { _frame:treeAlpha.totalFrames, time:treeTime, delay:2.5, transition:Equations.easeNone } );
			Tweener.addTween( stumpAlpha, { _frame:stumpAlpha.totalFrames, time:stumpTime, delay:2, transition:Equations.easeNone } );
			
		}
		
		
		
		public function buildOut() : void {
			
			var treeAlpha:MovieClip = _tree.alphaAnimation;
			var stumpAlpha:MovieClip = _stump.alphaAnimation;
			
			var treeTime:Number = treeAlpha.currentFrame / 31;
			var stumpTime:Number = stumpAlpha.currentFrame / 31;
			
			Tweener.addTween( _stump.startThreadView, { alpha:0, time:1, transition:Equations.easeNone } );
			Tweener.addTween( treeAlpha, { _frame:0, time:stumpTime, delay:3.5, transition:Equations.easeNone } );
			Tweener.addTween( stumpAlpha, { _frame:0, time:stumpTime, delay:2, transition:Equations.easeNone } );	
			_helpScreen.buildOut();	
			
		}
		
		
		
		public function hideMessageView() : void {
			
			_isLookingInMode = true;
			positionLandscape( _tree.stage, true );
			_tree.stage.removeEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			
			_startThreadBtn.buildOut();	
			_helpBtn.buildOut();
			_helpScreen.buildOut();
			
			if( _closedFormPoint ) {
				_form.buildOut( _closedFormPoint );
			}
			
			hideMessageMarkers();
			_paginationMenu.buildOut();
			
		}
		
		
		
		public function showMessageView() : void {
			
			if( _isFirstMessageDisplay ) {
				_isFirstMessageDisplay = false;
				onHelp( null );
			}
			
			_isLookingInMode = false;
			positionLandscape( _tree.stage, true );
			_tree.stage.addEventListener( MouseEvent.MOUSE_MOVE, onMouseMove );
			
			_startThreadBtn.buildIn();	
			_helpBtn.buildIn();	
			
			showMessageMarkers();
			
			_stump.addChild( _paginationMenu );
			_paginationMenu.buildIn();
			
		}
		
		
		
		public function positionLandscape( stage:Stage, doAnimation:Boolean = false ) : void {
			
			var yPos:Number = stage.stageHeight - height;
				
			var tyPos:int = _isLookingInMode ? Math.max( yPos, ( yPos ) * 0.5 ) : yPos;
			var syPos:int = tyPos + STUMP_Y_OFFSET;
			
			var sxPos:int = ( stage.stageWidth - _calculatedWidth ) * 0.5;
			var txPos:int = sxPos + TREE_X_OFFSET;
			
			if( doAnimation ) {
				Tweener.addTween( _stump, { x:sxPos, y:syPos, time:1, transition:Equations.easeOutSine } );
				Tweener.addTween( _tree, { x:txPos, y:tyPos, time:0.5, transition:Equations.easeOutSine } );
			}
			else {
				_stump.x = sxPos;
				_stump.y = syPos;
				_tree.y = tyPos;
				_tree.x = txPos;
			}
			
			_helpScreen.centerToStage( stage );
			
		}
		
		
		
		private function hideMessageMarkers() : void {
			
			for each( var marker:ThreadMarker in _threadMarkers ) {
				if( marker.hasData ) {
					marker.disable();
					marker.buildOut();	
				}
			}
			
		}
		
		
		
		private function showMessageMarkers() : void {
			
			for each( var marker:ThreadMarker in _threadMarkers ) {
				if( marker.hasData ) {
					_stump.addChild( marker );
					marker.enable();
					marker.buildIn();
				}
			}
			
		}
		
		
		
		private function onMouseMove( e:MouseEvent ) : void {
			
			var stage:Stage = _tree.stage;
			var mouseXPercent:Number = stage.mouseX / stage.stageWidth;
			
		}
		
		
		
		private function initializeMessageView() : void {
			
			var index:int = 0;
			for each( var marker:ThreadMarker in _threadMarkers ) {
				if( marker.hasData ) {
					var posData:Object = TempPositionGraph.MAP[ index ];
					index++;
					marker.positionScale( posData.x, posData.y, posData.s );
				}
			}
					
			_paginationMenu.enable();
			
		}
		
		
		
		private function fillMessageItemArrays() : void {
			
			for( var i:int = 0; i < MSG_LIMIT; i++ ) {
				createThreadMarker();
				_leafViews.push( new LeafMessage() );
			}
			
		}
		
		
		
		private function createThreadMarker() : void {
			
			var trunkMsg:TrunkMessage = new TrunkMessage();
			var marker:ThreadMarker = new ThreadMarker( trunkMsg );
			marker.addEventListener( ReplyEvent.SPEAKOUT_REPLY, onTrunkReply );
			_threadMarkers.push( marker );
			
		}
		
		
		
		private function addTrunkMessage( e:TrunkMessageEvent ) : void {
			
			_currentFormParent = _startThreadBtn.view;
			addCachedMessage();
			
			_closedFormPoint = new Point( _startThreadBtn.view.x + 22, _startThreadBtn.view.y + 20 );
			_startThreadBtn.disable();
			_stump.addChildAt( _form, 1 );
			_form.buildIn( _closedFormPoint );
			_form.replyId = 0;
			_helpScreen.buildOut();
			
		}
		
		
		
		
		private function closeSubmitForm( e:DisplayEvent ) : void {
			
			_messageCache[ _currentFormParent ] = _form.messageInput.text;
			
			_startThreadBtn.enable();
			_form.buildOut( _closedFormPoint );
			_startThreadBtn.enable();
			
			for each( var marker:ThreadMarker in _threadMarkers ) {
				marker.enable();
			}
			
		}
		
		
		
		private function onFormSubmit( e:FormSubmitEvent ) : void {
			
			var data:SubmitMessageData = e.data;
			data.endpoint = _submitEndpoint;
			dispatchEvent( new FormSubmitEvent( FormSubmitEvent.SUBMIT_SPEAKOUT_MESSAGE, data ) );
			
		}
		
		
		
		private function onTrunkReply( e:ReplyEvent ) : void {
			
			_closedFormPoint = new Point( 10, 15 );
			
			var target:ThreadMarker = e.target as ThreadMarker;
			target.disable();
			
			_currentFormParent = target;
			addCachedMessage();
			
			_form.scaleX = _form.scaleY = 0;
			_form.x = _form.y = 0;	
			_startThreadBtn.disable();
			_form.replyId = target.replyId;
			
			target.addChildAt( _form, 0 );
			_form.buildIn( new Point( 20, 30 ) );
			
			for each( var marker:ThreadMarker in _threadMarkers ) {
				if( marker != target ) marker.disable( true );
			}
				
		}
		
		
		
		private function addCachedMessage() : void {
			
			if( _messageCache[ _currentFormParent ] ) {
				_form.messageInput.text = _messageCache[ _currentFormParent ]; 
			}
			else {
				_form.messageInput.text = "";
			}
			
		}
		
		
		
		private function onPageNavigation( e:PaginationEvent ) : void {
			
			_paginationMenu.disable();
			dispatchEvent( e );
			hideMessageMarkers();
			
		}
		
		
		
		private function onHelp( e:MouseEvent ) : void {
			
			_stump.stage.addChild( _helpScreen );
			_helpScreen.buildIn();
			
		}
		
		
	}
}