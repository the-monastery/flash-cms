package com.lookingInSpeakingOut.flashcms.events.speakOut {
	
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.SubmitMessageData;
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 2009-10-01
	 * 
	 */
	public class FormSubmitEvent extends Event {
		
		
		
		public static const SUBMIT_SPEAKOUT_MESSAGE:String = "submitSpeakoutMessage";
		
		private var _data:SubmitMessageData;
		
		
		public function FormSubmitEvent( type:String, data:SubmitMessageData, bubbles:Boolean = false, cancelable:Boolean = false ) {
			
			_data = data;
			super( type, bubbles, cancelable );
			
		}
		
		
		
		public function get data() : SubmitMessageData {
			
			return _data;
			
		}
		
		
		
		override public function clone() : Event {
			
			return new FormSubmitEvent( type, data, bubbles, cancelable );
			
		}
		
		
	}
}