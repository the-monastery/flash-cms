package com.lookingInSpeakingOut.flashcms.ui {
	
	import com.ghostmonk.ui.graveyard.IScrollerAsset;
	
	import flash.display.BlendMode;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	
	import scroller.MajorScrollerAsset;
	import scroller.MinorScrollerAsset;
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class ScrollerAsset implements IScrollerAsset {
		
		
		public static const MAJOR_TYPE:int = 1;
		public static const MINOR_TYPE:int = 2;
		
		
		
		private var _container:Sprite; 
		private var _track:Sprite; 
		private var _handle:MovieClip; 
		private var _grip:Sprite; 
		private var _upBtn:MovieClip; 
		private var _downBtn:MovieClip;
		private var _background:Sprite;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get container():Sprite {
			 
			return _container;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get track():Sprite {
			 
			return _track;
				 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get handle():MovieClip {
			 
			return _handle;
				 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get grip():Sprite {
			
			return _grip;		 
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get upBtn():MovieClip {
			 
			return _upBtn;
				 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get downBtn():MovieClip {
			 
		 	return _downBtn;
		 		
		}
		
		
		
		/**
		 * 
		 * @param scrollerType
		 * 
		 */
		public function ScrollerAsset( scrollerType:int ):void {
			
			if( scrollerType == MINOR_TYPE ) {
				minorScroller();
			}
			else {
				majorScroller();
			}
			
		}
		
		
		
		/**
		 * 
		 * @param value
		 * 
		 */
		public function size( value:Number ):void {
			
			_container.height = value;
			_track.height = value;
			//_handle.height = value;
			_background.height = value;
			
		}
		
		
		
		private function majorScroller():void {
			
			var asset:MajorScrollerAsset = new MajorScrollerAsset();
			asset.blendMode = BlendMode.MULTIPLY;
			
			_container = asset;
			_track = asset.track;
			_handle = asset.handle;
			_grip = asset.grip;
			_upBtn = asset.up;
			_downBtn = asset.down;
			
		}
		
		
		
		private function minorScroller():void {
			
			var asset:MinorScrollerAsset = new MinorScrollerAsset();
			
			_container = asset;
			_track = asset.track;
			_handle = asset.handle;
			_background = asset.background;
			_grip = null;
			_upBtn = null;
			_downBtn = null;
			
		}
		
		
		
	}
}