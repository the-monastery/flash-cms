package com.lookingInSpeakingOut.flashcms.cmsUtils {
	
	public class CleanHTML {
		
		
		
		public static function removePTagsAndWhiteSpace( input:String ) : String {
			
			var whiteSpacePattern:RegExp = /\s\n{1,}/g;
			var output:String = input.replace( /\<p\>/g, "" );
			output = output.replace( whiteSpacePattern, " " );
			output = output.replace( /\<\/p\>/g, "" );
			return output;
			
		}
		
		
		
		
		public static function removeWhiteSpace( input:String ) : String {
			
			var carriage:RegExp = /\B/g;
			var output:String = input.replace( carriage, "" );
			return output;
		}
		
		
		
		public static function removeCharacters( input:String, characters:Array ) : String {
			
			var output:String = input;
			for each( var char:String in characters ) { 
				var pattern:RegExp = new RegExp( char, "g" );
				output = output.replace( pattern, "" );
			}
			return output;
			
		}
		
		
		 
		public static function replacePTagsWithBR( input:String ) : String {
			
			var whiteSpacePattern:RegExp = /\s\n{1,}/g;
			var pTag:RegExp = /\<p\>/g;
			var pCloseTag:RegExp = /\<\/p\>/g;
			var output:String = input.replace( pTag, "" );
			output = output.replace( whiteSpacePattern, " " );
			output = output.replace( pCloseTag, "<br />" );
			return output;
			
		} 
		
		
		
		
		public static function replacePTageWith2BR( input:String ) : String {
			
			var whiteSpacePattern:RegExp = /\s\n{1,}/g;
			var pTag:RegExp = /\<p\>/g;
			var pCloseTag:RegExp = /\<\/p\>/g;
			var ulCloseTag:RegExp = /\<\/ul\>/g;
			var output:String = input.replace( pTag, "" );
			output = output.replace( whiteSpacePattern, " " );
			output = output.replace( pCloseTag, "<br /><br />" );
			output = output.replace( ulCloseTag, "</ul><br />" );
			return output;
			
		}
		
		
		public static function replaceStrongWithBold( input:String ) : String {
			
			var strongO:RegExp = /\<strong\>/g;
			var strongC:RegExp = /\<\/strong\>/g;
			var output:String = input.replace( strongO, "<b>" );
			output = output.replace( strongC, "</b>" );
			return output;
			
		}
		
		
		public static function addUnderlineToLinks( input:String ) : String {
			
			var anchorO:RegExp = /\<a/g;
			var anchorC:RegExp = /\<\/a\>/g;
			var output:String = input.replace( anchorO, "<u><a" );
			output = output.replace( anchorC, "</a></u>" );
			return output;
			
		}
		
		
				
	}
}