package com.lookingInSpeakingOut.flashcms.navigation {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.text.EmbeddedText;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.ui.EventCallbackButton;
	
	import flash.display.Sprite;
	import flash.events.Event;
	
	[Event (name="mainNavigation", type="com.lookingspeakingout.flashcms.events.NavigationEvent")]
	[Event (name="subNavigation", type="com.lookingspeakingout.flashcms.events.NavigationEvent")]
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class Menu extends Sprite {
		
		
		private var _links:Array;
		private var _config:XML;
		private var _font:String;
		private var _divider:String;
		private var _linkInfo:XML;
		
		
		
		/**
		 * 
		 * @param factory - MenuFactory creates links and passes in callback function when those links are clicked
		 * 
		 */
		public function Menu( eventType:String, config:XML, fontName:String, addSpeakOutLink:Boolean = false ) {
			
			_config = config;
			_linkInfo = config.nav[ 0 ];
			_font = fontName;
			_divider = _linkInfo.@divider.toString();
			alpha = 0;
			_links = new Array();		
			createItems( onMenuClick, eventType, addSpeakOutLink );
			
		}
		
		
		
		/**
		 * 
		 * @param callBack
		 * @param eventType
		 * @return 
		 * 
		 */
		 public function createItems( callBack:Function, eventType:String, addSpeakOutLink:Boolean ) : void {
			
			var posX:Number = 0;
			
			if( addSpeakOutLink ) {
				var speakoutLink:EventCallbackButton = new EventCallbackButton( 
					new Event( "speakout" ), 
					"speakout", 
					"speakout", 
					_linkInfo, 
					_font, 
					onSpeakOut
				);
				_links.push( speakoutLink );
				speakoutLink.x = posX;
				addChild( speakoutLink );
				var sDivider:EmbeddedText = createDivider( speakoutLink );
				posX += speakoutLink.width + sDivider.width;
			}
			
			for( var i:int = 0; i < _config.section.length(); i++ ) {
				
				var link:EventCallbackButton = createLink( i, eventType, callBack );
				link.x = posX;
				
				if( i < _config.section.length() - 1 ) {
					var divider:EmbeddedText = createDivider( link );
					posX += link.width + divider.width;
				}
				
				_links.push( link );
				addChild( link );
			}
			
		} 
		
		
		
		public function enableById( id:String ) : void {
			
			for each( var link:EventCallbackButton in _links ) {
				if( link.id == id ) {
					link.enable();
				}
			}
			
		}
			
		
		
		
		public function display():void {
			
			enable();
			Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function hide():void {
			
			disable();
			parent.removeChild( this );
			Tweener.removeTweens( this );
			alpha = 0;
			
		}
		
		
		
		public function enable( isSpeakingOut:Boolean = false ):void {
			
			for each( var link:EventCallbackButton in _links ) {
				link.enable();
				if( isSpeakingOut && link.id == "speakout" ) {
					link.disable( true );
				}
			}
			
			Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function disable():void {
			
			for each( var link:EventCallbackButton in _links ) {
				link.disable();
			}
			
		}
		
		
		
		public function activeDisableLink( id:String ) : void {
			
			for each( var link:EventCallbackButton in _links ) {
				if( link.id  == id ) {
					link.disable( true );
					return;
				}
			}
			
		}
		
		
		
		private function createLink( i:int, eventType:String, callback:Function ) : EventCallbackButton {
			
			var sectionDataItems:XMLList = _config.section;
			var sectionData:SectionData = new SectionData( sectionDataItems[ i ] );		
			var navEvent:NavigationEvent = new NavigationEvent( eventType, sectionData );
			var link:EventCallbackButton = new EventCallbackButton( 
				navEvent, 
				sectionDataItems[ i ].@label, 
				sectionDataItems[ i ].@id, 
				_linkInfo, 
				_font, 
				callback
			);
			
			return link;
			
		}
		
		
		
		private function createDivider( link:EventCallbackButton ) : EmbeddedText {
			
			var div:EmbeddedText = new EmbeddedText( _font, _linkInfo.@size, _linkInfo.@color, true );
			div.text = _divider;
			div.x = link.x + link.width;
			addChild( div );
			return div;
			
		}
		
		
		
		private function onSpeakOut( e:Event ) : void {
			
			dispatchEvent( e );
			
		}
		
		
		
		private function onMenuClick( e:NavigationEvent ):void {
			
			dispatchEvent( e );
			
		}
		
		
		
	}
}