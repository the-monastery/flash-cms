<?php

function printError() {
	
	print <<< END
<?xml version="1.0" encoding="UTF-8"?>
<error>
	<msg>Problem connecting to the Database</msg>
</error>
END;

}
	
	
function printSuccess( $msqliResult, $totalPages, $currentPage ) {
	
    print '<?xml version="1.0" encoding="UTF-8"?>'."\n";
	print '<messages totalPages="'.$totalPages.'" currentPage="'.$currentPage.'">'."\n";   	
    
	while( $row = mysqli_fetch_assoc( $msqliResult ) ){
		$id = stripslashes( $row[id] );
		$replies = stripslashes( $row[replies] );
		$name = stripslashes( $row[name] );
		$maintext = stripslashes( $row[text] );
		$xPos = stripslashes( $row[xPos] );
		$yPos = stripslashes( $row[yPos] );
		$date = stripslashes( $row[date] );
		print <<< END

	<message id="$id" replies="$replies">
		<user>$name</user>
		<text><![CDATA[$maintext]]></text>
		<position x="$xPos" y="$yPos" />
		<date>$date</date>
	</message>

END;
	} 

    print '</messages>';
    
}
    
?>