package com.lookingInSpeakingOut.flashcms.data.speakingOutData {
	import items.ReturnTabAsset;
	
	
	public class SubmitMessageData {
		
		
		
		private var _isTrunk:Boolean;
		private var _name:String;
		private var _xPos:int;
		private var _yPos:int;
		private var _parentId:int;
		private var _text:String;
		private var _endpoint:String;
		
		
		
		public function get isTrunk() : Boolean {
			
			return _isTrunk;
			
		}
		
		public function set isTrunk( value:Boolean ) : void {
			
			_isTrunk = value;
			
		}
		
		
		
		public function get name() : String {
			
			return _name;
			
		}
		
		public function set name( value:String ) : void {
			
			_name = value;
			
		}
		
		
		
		public function get xPos() : int {
			
			return _xPos;
			
		}
		
		public function set xPos( value:int ) : void {
			
			_xPos = value;
			
		}
		
		
		
		public function get yPos() : int {
			
			return _yPos;
			
		}
		
		public function set yPos( value:int ) : void {
			
			_yPos = value;
			
		}
		
		
		
		public function get parentId() : int {
			
			return _parentId;
			
		}
		
		public function set parentId( value:int ) : void {
			
			_parentId = value;
			
		}
		
		
		
		public function get text() : String {
			
			return _text;
			
		}
		
		public function set text( value:String ) : void {
			
			_text = value;
			
		}
		
		
		
		public function get endpoint() : String {
			
			return _endpoint;
			
		}
		
		public function set endpoint( value:String ) : void {
			
			_endpoint = value;
			
		}
		
		

	}
}