<?php
	
	include '../../../modules/errors/errorReporting.php';
	include '../../../modules/connection/speakingOutConnect.php';
	
	$isTrunk = $mysqli->real_escape_string( $_POST[ 'isTrunk' ] );
	$parentId = $mysqli->real_escape_string( $_POST[ 'parentId' ] ); 
	$name = $mysqli->real_escape_string( $_POST[ 'name' ] );
	$xPos = $mysqli->real_escape_string( $_POST[ 'xPos' ] );
	$yPos = $mysqli->real_escape_string( $_POST[ 'yPos' ] );
	$text = $mysqli->real_escape_string( $_POST[ 'text' ] );
	
	if( $isTrunk == "false" ) {
		//update tunk date
		$mysqli->query( "UPDATE messages SET date = NOW(), replies = replies+1 WHERE id =$parentId LIMIT 1 " );	
	}
	
	$mysqli->query( "INSERT INTO userdata ( name ) VALUES ( '$name' )" );	
	$userID = $mysqli->insert_id;
	
	$insertResult = $mysqli->query( "INSERT INTO messages ( userId, parentId, isTrunk, isComposite, text, xPos, yPos ) VALUES ( $userID, $parentId, $isTrunk, $isTrunk, '$text', $xPos, $yPos )" );
	$messageID = $mysqli->insert_id;
	
	$mysqli->query( "INSERT INTO messagemap( parentId, childId ) VALUES ( $parentId, $messageID )" );
	
	if( $insertResult ) {
		$message = 'Please Mediate the following entry at http://www.lookinginspeakingout.com/speakingOut/mediate'.
			"\n" . 'Name = ' . stripslashes( $name ) .
			"\n" . 'Story = ' . stripslashes( $text );
		
		mail( 'sscott@crossingcommunities.org', 'Someone submitted a story to Speaking out', $message );
	}
	
	$mysqli->close();
	
?>