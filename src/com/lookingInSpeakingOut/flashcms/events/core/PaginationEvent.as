package com.lookingInSpeakingOut.flashcms.events.core {
	
	import flash.events.Event;

	public class PaginationEvent extends Event {
		
		
		
		public static const PAGE_NAVIGATION:String = "pageNavigation";
		
		private var _page:int;
		
		
		
		public function get page() : int {
			
			return _page;
			
		}
		
		
		
		public function PaginationEvent( type:String, page:int, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			_page = page;
			super( type, bubbles, cancelable );
			
		}
		
		
		
		override public function clone():Event {
			
			return new PaginationEvent( type, page, bubbles, cancelable );
			
		}
		
		
		
	}
}