//Get Responses

<?php
	
	include '../../../modules/errors/errorReporting.php';
	include '../../../modules/connection/speakingOutConnect.php';
	include '../printXML.php';
	
	$pId = $_GET[ 'parentId' ] ? $_GET[ 'parentId' ]  : NULL;
	
	if( $pId ) {
		
		$result = $mysqli->query(
			"SELECT messages.id, name, text, xPos, yPos, messages.date
			FROM messages, userdata
			WHERE isTrunk =0
			AND messages.userId = userdata.id
			AND messages.parentId = $pId
			ORDER BY messages.date DESC
			LIMIT 30" );
		
		if( $result ) {
			printSuccess( $result );
		}
		else {
			printError( $result );
		}
		
	}
	else {
		print 'Error: You must get your request with "parentId".';
	}
		
	$mysqli->close(); 
	
?>