package com.lookingInSpeakingOut.flashcms.flickrViewer {
	
	
	
	public class FlickrThumbCollection {
		
		
		
		private var _thumbs:Array;
		
		
		
		public function FlickrThumbCollection() {
			
			_thumbs = new Array();
			
		}
		
		
		
		public function push( thumb:FlickrThumb ):void {
			
			_thumbs.push( thumb );
			
		}
		
		
		
		public function get length():int {
			
			return _thumbs.length;
			
		}
		
		
		
		public function get list() : Array {
			
			return _thumbs;
			
		}
		
		
		
		public function getThumb( index:uint ):FlickrThumb {
			
			var thumb:FlickrThumb = index >= _thumbs.length ? null : _thumbs[ index ];
			return thumb;
						
		}
		
		
		
		public function getThumbById( id:String ):FlickrThumb {
			
			var target:FlickrThumb;
			
			for each( var thumb:FlickrThumb in _thumbs ) {
				
				if( thumb.id == id ) {
					target = thumb;
				}
				
			}
			
			return target;
			
		}
		
		
		
		public function getNext( id:String ):FlickrThumb {
			
			var target:FlickrThumb;
			
			for( var i:int = 0; i < _thumbs.length; i++ ) {
				if( ( _thumbs[ i ] as FlickrThumb ).id == id ) {
					target = _thumbs[ Math.min( _thumbs.length - 1, i + 1 ) ];
				} 		
			}
			
			return target;
		}
		
		
		
		public function getPrevious( id:String ):FlickrThumb {
			
			var target:FlickrThumb;
			
			for( var i:int = 0; i < _thumbs.length; i++ ) {
				if( ( _thumbs[ i ] as FlickrThumb ).id == id ) {
					target = _thumbs[ Math.max( 0, i - 1 ) ];
				} 		
			}
			
			return target;
			
		}
		
		
		
		public function enableThumbs( disableID:String = null ):void {
			
			for each( var thumb:FlickrThumb in _thumbs ) {
				if( thumb.id == disableID ) {
					thumb.disable();
				}
				else {
					thumb.enable();
				}
			}
			
		}
		
		//pop
		//swap
		//removebyindex
		//removebyitem
		//ifexists
		//etc etc etc etc
		//indexOF
		
		
	}
}