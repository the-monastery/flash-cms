package com.lookingInSpeakingOut.flashcms.lookingIn.content.minor {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.lookingInSpeakingOut.flashcms.cmsUtils.CleanHTML;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.minor.items.HTMLItem;
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class HTMLContent extends BaseContent {
		
		
		
		public function HTMLContent( sectionW:int, sectionH:int ) {
		
			super( sectionW, sectionH );
			
		}
		
		
		
		override public function buildIn():void {
			
			Tweener.addTween( this, { alpha:1, time:0.3, delay:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function setTextDirectly( text:String, w:Number, h:Number ) : void {
			
			addChild( new HTMLItem( text, w, h ) );
			
		}
		
		
		
		override protected function onDataLoaded( data:String ):void {
			
			data = CleanHTML.replacePTageWith2BR( data );
			
			addChild( new HTMLItem( data, width, height ) );
			super.onDataLoaded( data );
			
		}
		
		
		
	}
}