package com.lookingInSpeakingOut.flashcms.lookingIn.content.major {
	
	import assets.resources.ResourceFullStoryAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.net.SimpleURLLoader;
	import com.ghostmonk.utils.StringCleaner;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.ResourceData;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items.ResourceCategoryItem;
	
	import flash.events.DataEvent;
	import flash.net.URLRequest;
	import flash.text.TextFieldAutoSize;
	
	/**
	 * Adds functionality to Categories Class that allows for removing items from the display and 
	 * loading in item content
	 *  
	 * @author ghostmonk
	 * 
	 */
	public class ResourcesContent extends Categories {
		
		
		
		private var _fullStory:ResourceFullStoryAsset;
		private var _bgHeight:Number;
		private var _bgWidth:Number;
		
		
		
		public function ResourcesContent( sectionW:Number, sectionH:Number ) {
			
			super( sectionW, sectionH );
			
		}
		
		
		
		override protected function onDataLoaded( data:String ):void {
			
			itemClass = ResourceCategoryItem;
			itemDataClass = ResourceData;
			var xmlData:XML = XML( data );
			
			super.createItems( xmlData );
			
			_fullStory = new ResourceFullStoryAsset();
			_fullStory.content.autoSize = TextFieldAutoSize.LEFT;
			
			_bgHeight = 0;
			_bgWidth = _fullStory.background.width;
			
			
			for each( var category:ResourceCategoryItem in items ) {
				category.addEventListener( SectionEvent.SECTION_OPEN, onSectionOpen );
				category.addEventListener( DataEvent.DATA, onItemClicked );
			}
			
			
		}
		
		
		
		/**
		 * Build in and display list of ResourceCategoryItems 
		 * 
		 */
		override public function listView():void {
			
			for each( var category:ResourceCategoryItem in items ) {
				addChild(category);
				category.buildIn();
				removeFullStory();
			}
			
		}
		
		
		
		private function onSectionOpen( e:SectionEvent ):void {
			
			for each( var category:ResourceCategoryItem in items ) {
				if( category != e.target ) {
					category.hideItems();
				}
			}
			
		}
		
		
		
		private function onItemClicked( e:DataEvent ):void {
			
			new SimpleURLLoader( new URLRequest( e.data ), onFullStoryLoad );
			
			for each( var category:ResourceCategoryItem in items ) {
				category.buildOut();
			}
			
			aggregateItemClicked();
			
		}  
		
		
		
		private function revealFullStory():void {
			
			_fullStory.background.width = _fullStory.background.height =  0;
			_fullStory.background.x = _bgWidth/2;
			_fullStory.background.y = height/2;
			addChild(_fullStory);
			
			Tweener.addTween(
				_fullStory.background, {
					x:0,
					y:0,
					width:_bgWidth,
					height:_bgHeight,
					time:0.3,
					onUpdate:displayUpdate
				}
			);
			
			Tweener.addTween(
				_fullStory.content, {
					alpha:1,
					time:0.3,
					delay:0.1,
					transition:Equations.easeNone
				}
			);
			
		}
		
		
		
		private function removeFullStory():void {
			
			_fullStory.content.alpha = 0;
			
			Tweener.addTween(
				_fullStory.background, {
					x:width/2,
					y:height/2,
					width:0,
					height:0,
					time:0.3,
					onComplete:removeChild,
					onCompleteParams:[ _fullStory ]
				}
			);
			
		}
		
		
		
		private function onFullStoryLoad( data:String ):void {
			
			var formatter:StringCleaner = new StringCleaner();
			_fullStory.content.htmlText = formatter.htmlOutput( data );
			_fullStory.content.alpha = 0;
			_bgHeight = 2*_fullStory.content.y + _fullStory.content.height;
			revealFullStory();
			
		}
		
		
		
	}
}