package com.lookingInSpeakingOut.flashcms.events {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class SearchEvent extends Event {
		
		
		
		public static const SEARCH:String = "search";
		
		
		
		private var _searchTerms:Array;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get searchTerms():Array {
			
			return _searchTerms;
			
		}
		
		
		
		/**
		 * 
		 * @param type
		 * @param searchTerms
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function SearchEvent( type:String, searchTerms:Array, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			super( type, bubbles, cancelable );
			_searchTerms = searchTerms;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		override public function clone():Event {
			
			return new SearchEvent( type, searchTerms );
			
		}
		
		
		
	}
}