package com.lookingInSpeakingOut.flashcms.framework.view {
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.LISOEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.MenuEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.framework.FrameworkEvents;
	import com.lookingInSpeakingOut.flashcms.navigation.NavContainer;
	
	import flash.events.Event;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	
	/**
	 * Controls display and composition of Navigation elemements
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class NavContainerMediator extends Mediator {
		
		
		
		public static const NAME:String = "NavContainerMediator"; 
		
		
		
		public function NavContainerMediator( navContainer:NavContainer ) {
			
			super( NAME, navContainer );
			
		}
		
		
		
		public function get navContainer():NavContainer {
			
			return viewComponent as NavContainer;
			
		}
		
		
		
		override public function onRegister():void {
			
			StageMediator( facade.retrieveMediator( StageMediator.NAME ) ).stage.addChild( navContainer );
			navContainer.disable();
			
			navContainer.addEventListener( NavigationEvent.MAJOR_SECTION, onMenuClick );
			navContainer.addEventListener( NavigationEvent.MINOR_SECTION, onMenuClick );
			navContainer.addEventListener( MenuEvent.MORPH, onMenuMorph );
			navContainer.addEventListener( "speakout", onSpeakout );
			
		}
		
		
		
		override public function listNotificationInterests():Array {
			
			return [
				NavigationEvent.MINOR_SECTION,
				NavigationEvent.MAJOR_SECTION,
				FrameworkEvents.MINOR_CLOSE_BTN_CLICK,
				NavigationEvent.STARTING_SECTION,
				FrameworkEvents.STAGE_RESIZE,
				LISOEvent.SPEAKING_OUT_APP,
				LISOEvent.LOOKING_IN_APP
			];
			
		}
		
		
		
		private function onStartingSection( id:String ) : void {
			
			navContainer.setMainSection( id );
			navContainer.enable();
			
		}
		
		
		
		override public function handleNotification( note:INotification ):void {
			
			switch( note.getName() ) {
				
				case FrameworkEvents.STAGE_RESIZE: 
					onStageResize(); 
					break;
				
				case NavigationEvent.MINOR_SECTION:
					navContainer.enableMainMenu();
					break;
				
				case FrameworkEvents.MINOR_CLOSE_BTN_CLICK:
				case NavigationEvent.MAJOR_SECTION:
					navContainer.enableSubMenu();
					break;
				
				case LISOEvent.SPEAKING_OUT_APP:
					navContainer.enableSubMenu();
					navContainer.enableMainMenu( true );
					break;
					
				case NavigationEvent.STARTING_SECTION:
					onStartingSection( ( note.getBody() as SectionData ).id );
					break;
					
			}
			
		}
		
		
		
		private function onStageResize():void {
			
			navContainer.onStageResize();
			
		}
		
		
		
		private function onMenuClick( e:NavigationEvent ):void {
			
			sendNotification( e.type, e.data );
			
		}
		
		
		
		private function onMenuMorph( e:MenuEvent ):void {
			
			sendNotification( e.type, e.isOpen );
			
		}
		
		
		
		private function onSpeakout( e:Event ) : void {
			
			sendNotification( "mainMenuSpeakingout" );
			
		}
		
		
		
	}
}