package com.lookingInSpeakingOut.flashcms.lookingIn.content.major {
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.CategoryData;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.LinkData;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items.LinksCategoryItem;
	
	import flash.display.DisplayObject;
	import flash.events.Event;
	
	/**
	 * Use when content is categorized into main sections.
	 * 
	 * <p>The main sections contain items. 
	 * Usage: Classes representations of the items and instantiation data is passed in the constructor.</p>
	 *  
	 * @author ghostmonk
	 * 
	 */
	public class Categories extends BaseContent {		
		
		
		
		private const PADDING:int = 5;
		private var _items:Array; 
		private var _itemClass:Class;
		private var _itemDataClass:Class;
		
		
		
		public function Categories( sectionW:Number, sectionH:Number ) {
			
			super( sectionW, sectionH );
			
		}
		
		
		/**
		 * An array of items made from the ItemClass (passed in as a parameter in the constructor)
		 * 
		 * @return  
		 * 
		 */
		protected function get items():Array {
			
			return _items;
				
		}
		
		
		
		protected function set itemClass( value:Class ) : void {
			
			_itemClass = value;
				
		}
		
		
		
		protected function set itemDataClass( value:Class ) : void {
			
			_itemDataClass = value;
			
		}
		
		
		
		override protected function onDataLoaded( data:String ):void {
			
			_itemClass = LinksCategoryItem;
			_itemDataClass = LinkData;
			createItems( XML( data ) );
			
		}		
		
		
		
		protected function createItems( data:XML ):void {
			
			var list:XMLList = data.category;
			_items = [];
			
			for each( var item:XML in list ) {
				var currentItem:DisplayObject = new _itemClass( new CategoryData( item, _itemDataClass ) ); 
				currentItem.addEventListener( SectionEvent.SECTION_VIEW_UPDATE, positionAssets );
				addChild( currentItem );
				_items.push( currentItem );
			}
			
			positionAssets();
			super.onDataLoaded( data );
		}
		
		
		
		private function positionAssets( e:Event = null ):void {
			
			var yPos:int = 0;
			
			for each( var item:DisplayObject in _items ) {
				item.y = yPos;
				yPos += item.height + PADDING;
			}
			
			displayUpdate();
			
		}
		
		
		
	}
}