package com.lookingInSpeakingOut.flashcms.lookingIn.navigation {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.text.EmbeddedText;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.ui.EventCallbackButton;
	
	import flash.display.Sprite;
	
	[Event (name="mainNavigation", type="com.lookingspeakingout.flashcms.events.NavigationEvent")]
	[Event (name="subNavigation", type="com.lookingspeakingout.flashcms.events.NavigationEvent")]
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class Menu extends Sprite {
		
		
		private var _links:Array;
		private var _config:XML;
		private var _font:String;
		private var _divider:String;
		private var _linkInfo:XML;
		
		
		
		/**
		 * 
		 * @param factory - MenuFactory creates links and passes in callback function when those links are clicked
		 * 
		 */
		public function Menu( eventType:String, config:XML, fontName:String ) {
			
			_config = config;
			_linkInfo = config.nav[ 0 ];
			_font = fontName;
			_divider = _linkInfo.@divider.toString();
			alpha = 0;
			_links = createItems( onMenuClick, eventType );
			
		}
		
		
		
		/**
		 * 
		 * @param callBack
		 * @param eventType
		 * @return 
		 * 
		 */
		public function createItems( callBack:Function, eventType:String ):Array {
			
			var links:Array = new Array();
			var posX:Number = 0;
			
			for( var i:int = 0; i < _config.section.length(); i++ ) {
				
				var sectionDataItems:XMLList = _config.section;
				var sectionData:SectionData = new SectionData( sectionDataItems[ i ] );
				var navEvent:NavigationEvent = new NavigationEvent( eventType, sectionData );
				var link:EventCallbackButton = new EventCallbackButton( navEvent, sectionDataItems[ i ].@label, _linkInfo, _font, callBack );
				
				link.x = posX;
				
				//If current item is not the final item insert a divider
				if( i < sectionDataItems.length() - 1 ) {
					var div:EmbeddedText = new EmbeddedText( _font, _linkInfo.@size, _linkInfo.@color, true );
					div.text = _divider;
					div.x = link.x + link.width;
					addChild( div );
					posX += link.width + div.width;
				}
				
				links.push( link );
				addChild( link );
			}
			
			//add generated array "links" to parent object
			return links;
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function display():void {
			
			enable();
			Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function hide():void {
			
			disable();
			parent.removeChild( this );
			Tweener.removeTweens( this );
			alpha = 0;
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function enable():void {
			
			Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function disable():void {
			
		}
		
		
		
		private function onMenuClick( e:NavigationEvent ):void {
			
			dispatchEvent( e );
			
		}
		
		
		
	}
}