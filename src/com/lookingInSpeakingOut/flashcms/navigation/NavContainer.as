package com.lookingInSpeakingOut.flashcms.navigation {
	
	import caurina.transitions.Tweener;
	import caurina.transitions.properties.ColorShortcuts;
	
	import com.ghostmonk.display.graphics.shapes.Polygon;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.MenuEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;

	[Event (type="com.lookingspeakingout.flashcms.events.MenuEvent", name="morph")]
	[Event (type="com.lookingspeakingout.flashcms.events.NavigationEvent", name="subNavigation")]
	[Event (type="com.lookingspeakingout.flashcms.events.NavigationEvent", name="mainNavigation")]
	
	/**
	 * Navigation menu... self explanitory for the most part.. for now 
	 * Mostly a presentation and display device maintaining the logic for displaying, opening and closing 
	 * itself... will contain a number of other objects for mediation
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class NavContainer extends Sprite {	
		
		
		private var _background:NavBackground;
		private var _mainMenu:Menu;
		private var _subMenu:Menu;
		private var _indicator:Polygon;
		
		private var _yPos:Number;
		
		private var _isOpen:Boolean;
		
		private var _activeMainSectionIds:Array;
		
		
		/**
		 * 
		 * @param bg		-NavBackground, background object
		 * @param main		-Menu - main menu dispatching NavigationEvents
		 * @param sub		-Menu - sub menu dispatching NavigationEvents... used for footer information
		 * @param search	-SearchForm - A form that dispatches a SearchEvent
		 * @param login		-LoginForm - A form that dispatches a LoginEvent
		 * @param indicator	-Polygon - a simple Shape object that indicated opening and closing of the menu
		 * 
		 */
		public function NavContainer( bg:NavBackground, main:Menu, sub:Menu, indicator:Polygon ) {
					
			ColorShortcuts.init();

			_background = bg;
			_mainMenu = main;
			_mainMenu.addEventListener( "speakout", onSpeakout );
			_subMenu = sub;
			
			_indicator = indicator;
			_subMenu.enable();
			
			_isOpen = false;
			_activeMainSectionIds = new Array();
			
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			_mainMenu.addEventListener( NavigationEvent.MAJOR_SECTION, onMainNavigation );
			_subMenu.addEventListener( NavigationEvent.MINOR_SECTION, onSubNavigation );
			_subMenu.addEventListener( DisplayEvent.CLOSE_BTN_CLICKED, onClosedBtnClicked );
			
		}
		
		
		
		public function get yPos():Number {
			 
			return _yPos;
			 
		}
		
		
		
		public function enable():void {
			
			_subMenu.enable();
			_mainMenu.enable();
			
		}
		
		
		
		public function disable():void {
			
			_subMenu.disable();
			_mainMenu.disable();
			
		}
		
		
		
		/**
		 * Positions the navigation menu to the bottom center of the stage.
		 * Should only be called when NavContainer object has access to the stage.
		 */
		public function onStageResize():void {
			
			var destY:Number = stage.stageHeight - _background.restHeight * 0.5 - 10;
			var xPos:int = stage.stageWidth * 0.5;
			
			_yPos = destY - _background.restHeight - 10;
			
			y = destY;
			x = xPos;
			
		}
		
		
		
		/**
		 * 
		 * @param isOpen	- Boolean inidicating state of menu (open or closed)
		 * @return 			- Point relative to the stage of the top left corner of the stage when open or closed	
		 * 
		 */
		public function globalPositionPoint( isOpen:Boolean ):Point {
			
			var yVal:Number = isOpen ? -_background.openHeight : -_background.restHeight;
			return localToGlobal( new Point( _background.x, yVal - 20 ) );
			
		}
		
		
		
		public function sectionsOnStage( idList:Array ) : void {
			
			_mainMenu.enable();
			for each( var sectionId:String in idList ) {		
				setMainSection( sectionId );
			} 
			
		}
		
		
		
		public function setMainSection( id:String ) : void {
			
			if( _activeMainSectionIds.length == 2 ) {
				_activeMainSectionIds.pop();
			}
			
			_activeMainSectionIds.unshift( id );
			
			for each( var id:String in _activeMainSectionIds ) {
				_mainMenu.activeDisableLink( id );
			}
			
		}
		
		
		
		public function enableMainNavLinkById( id:String ) : void {
			
			_activeMainSectionIds.splice( _activeMainSectionIds.indexOf( id ), 1 );
			_mainMenu.enableById( id );
			
		}
		
		
		
		public function enableMainMenu( isSpeakingOut:Boolean = false ) : void {
			
			_mainMenu.enable( isSpeakingOut );
			
		}
		
		
		
		public function enableSubMenu() : void {
			
			_subMenu.enable();
			
		}
		
		
		
		private function onAddedToStage( e:Event ):void {
			
			x = stage.stageWidth * 0.5;
			y = stage.stageHeight - _background.restHeight * 0.5 - 10;
			
			positionAssets();
			
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );		
			
			_background.buildIn( backGroundReady );
			
		}
		
		
		
		private function backGroundReady():void {
			
			enable();
			
			var currentMousePoint:Point = new Point( stage.mouseX, stage.mouseY );
			var objectsUnderMousePoint:Array = stage.getObjectsUnderPoint( currentMousePoint );
			
			for each( var obj:Object in objectsUnderMousePoint ) {
				if( obj == _background ) {
					open();
				}
			}
			
			addChild( _indicator );
			dispatchEvent( new MenuEvent( MenuEvent.INITIALIZED, false ) );
			
		}
		
		
		
		private function open( e:MouseEvent = null ):void {
			
			dispatchEvent( new MenuEvent( MenuEvent.MORPH, true ) );
			_background.open( showMenu );
			
			Tweener.addTween (
				_indicator, {
					_color:0xFFFFFF, 
					rotation:90, 
					y:-_background.openHeight + 30, 
					time:0.3
				}
			);
			
		}
		
		
		
		private function close( e:MouseEvent = null ):void {
			
			dispatchEvent( new MenuEvent( MenuEvent.MORPH, false ) );
			//Only call hide on the following elements if onOpenComplete() was successfully called
			
			if( _isOpen ) {
				_mainMenu.hide();
			}
			
			_isOpen = false;
			_background.close();
			
			Tweener.addTween( 
				_indicator, { 
					_color:_indicator.color, 
					rotation:-90, 
					y:0, 
					time:0.3 
				} 
			);
			
		}
		
		
		
		private function showMenu():void {
			
			_isOpen = true;
			addChild( _mainMenu );
			_mainMenu.display();
			
		}
		
		
		
		private function positionAssets():void {
			
			_subMenu.x = -_subMenu.width * 0.5;
			_subMenu.y = 8;
			_mainMenu.x = -_mainMenu.width * 0.5;
			_mainMenu.y = - _mainMenu.height + 10;
			_indicator.x = -_background.width * 0.5 + 15;
			
			addChild( _background );
			showMenu();
			addChild( _subMenu );
			
		}
		
		
		
		private function onMainNavigation( e:NavigationEvent ) : void {
			
			dispatchEvent( e );
			
		}
		
		
		
		private function onSubNavigation( e:NavigationEvent ) : void {
			
			dispatchEvent( e );
			_subMenu.enable();
			_subMenu.activeDisableLink( e.data.id );
			
		}
		
		
		private function onClosedBtnClicked( e:DisplayEvent ) : void {
			
			_subMenu.enable();
			
		}
		
		
		private function onSpeakout( e:Event ) : void {
			
			dispatchEvent( e );
			
		}
		
		
		
	}
} 