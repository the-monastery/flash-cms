package com.lookingInSpeakingOut.flashcms.framework.model {
	
	import com.ghostmonk.net.XMLLoader;
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.MessageDataCollection;
	
	import org.puremvc.as3.multicore.patterns.proxy.Proxy;

	/**
	 * 
	 * @author ghostmonk 2009-09-20
	 * 
	 */
	public class SpeakingOutProxy extends Proxy {
		
		
		
		public static const NAME:String = "SpeakingOutProxy";
		
		public static const TRUNK_MESSAGES_READY:String = "trunkMessagesReady";
		public static const LEAF_MESSAGES_READY:String = "leafMessagesReady";
		
		private var _submitURL:String;
		private var _trunkURL:String;
		private var _leafURL:String;
		
		private var _loader:XMLLoader;
		
		
		
		public function SpeakingOutProxy( submitEndPoint:String, trunkEndPoint:String, leafEndPoint:String ) {
			
			super( NAME );
			_leafURL = leafEndPoint;
			_submitURL = submitEndPoint;
			_trunkURL = trunkEndPoint;
			retrieveTrunkMsgs();
			
		}
		
		
		
		public function retrieveTrunkMsgs( page:int = 1 ) : void {
			
			var url:String = _trunkURL + ( page == 1 ? ".xml" : "-0" + page + ".xml" ); 
			_loader = new XMLLoader( url, onTrunkData );
			
		}
		
		
		
		public function retrieveLeafMsgs( parentId:String ) : void {
			
			_loader = new XMLLoader( _leafURL + "?parentId=" + parentId, onLeafData );
			
		}
		
		
		
		public function sendMessage( data:Object ) : void {
			
			
			
		}
		
		
		
		private function onTrunkData( data:String ) : void {
			
			_loader = null;
			var msgData:MessageDataCollection = new MessageDataCollection();
			msgData.createMessages( XML( data ) );
			
			sendNotification( TRUNK_MESSAGES_READY, msgData );
			
		}
		
		
		
		private function onLeafData( data:String ) : void {
			
			_loader = null;
			sendNotification( LEAF_MESSAGES_READY, data );
			
		}
		
		
		
	}
}