package com.lookingInSpeakingOut.flashcms.data.sectionData {
	
	/**
	 * Data sent through the URLRequestProxy 
	 * 
	 * @author ghostmonk 17/01/2009
	 * 
	 */
	public class ResponseData {
		
		
		
		private var _section:String;
		private var _content:XML;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get section():String {
			 
			return _section;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get content():XML {
				 
			return _content;
			 
		}
		
		
		
		/**
		 * 
		 * @param section
		 * @param content
		 * 
		 */
		public function ResponseData( section:String, content:XML ) {
			
			_section = section;
			_content = content;
			
		}



	}
}