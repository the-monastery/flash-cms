package com.lookingInSpeakingOut.flashcms.events.core {
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	
	import flash.events.Event;

	
 	/**
 	 * 
 	 * @author ghostmonk 30/30/2008
 	 * 
 	 */
 	public class NavigationEvent extends Event {
		
		
		public static const MAJOR_SECTION:String = "com.lookingspeakingout.flashcms.model.datastructures.SectionData.topSection";
		public static const MINOR_SECTION:String = "com.lookingspeakingout.flashcms.model.datastructures.SectionData.footerSection";
		public static const STARTING_SECTION:String = "com.lookingspeakingout.flashcms.model.datastructures.SectionData.startingSections";
		
		
		
		private var _data:SectionData;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get data():SectionData {
			
			return _data;
			
		}
	
	
	
		/**
		 * 
		 * @param type
		 * @param data
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function NavigationEvent( type:String, data:SectionData, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			super( type, bubbles, cancelable );
			_data = data;
			
		}
		
		
		
		override public function clone():Event {
			
			return new NavigationEvent( type, data );
			
		} 
		
		
		
	}
}