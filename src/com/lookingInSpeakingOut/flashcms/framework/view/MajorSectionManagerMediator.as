package com.lookingInSpeakingOut.flashcms.framework.view {
	
	import com.ghostmonk.media.video.events.VideoLoadEvent;
	import com.lookingInSpeakingOut.flashcms.AppFacade;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.ResponseData;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.LISOEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.MenuEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickrGalleryEvent;
	import com.lookingInSpeakingOut.flashcms.framework.FrameworkEvents;
	import com.lookingInSpeakingOut.flashcms.framework.model.URLRequestProxy;
	import com.lookingInSpeakingOut.flashcms.lookingIn.clients.MajorSectionManager;
	import com.lookingInSpeakingOut.flashcms.navigation.NavContainer;
	
	import flash.events.Event;
	import flash.geom.Point;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	
	/**
	 * Controls the state of the MainModuleManager
	 * 
	 * @author ghostmonk 30/12/2008
	 * 
	 */
	public class MajorSectionManagerMediator extends Mediator {
		
		
		public static const NAME:String = "MajorSectionManagerMediator";
		
		private var _navContiner:NavContainer;
		
		
		
		/**
		 * 
		 * @param viewComponent
		 * 
		 */
		public function MajorSectionManagerMediator( viewComponent:MajorSectionManager ) {
			
			super( NAME, viewComponent );
			
			manager.addEventListener( SectionEvent.SHOW_SECTIONS, onShowSections );
			manager.addEventListener( SectionEvent.SECTIONS_REMOVED, onSectionsRemoved );
			manager.addEventListener( FlickrGalleryEvent.PHOTO_GALLERY_CLICK, onGalleryClick );
			manager.addEventListener( FlickrGalleryEvent.PHOTO_GALLERY_DATA, onGalleryCreation );
			manager.addEventListener( VideoLoadEvent.LOAD_VIDEO, onVideoLoad );	
			manager.addEventListener( DisplayEvent.CLOSE_BTN_CLICKED, onClosedBtnClicked );
			
		}
		
		
		
		public function get manager():MajorSectionManager {
			
			return viewComponent as MajorSectionManager;
			
		}
		
		
		
		override public function onRegister():void {
			
			//retrieve a copy of the navContainer... it's state will effect the moduleView
			_navContiner = NavContainerMediator( facade.retrieveMediator( NavContainerMediator.NAME ) ).navContainer;
			StageMediator( facade.retrieveMediator( StageMediator.NAME ) ).manager =  manager;
			
		}
		
		
		
		override public function listNotificationInterests():Array {
			
			return [
				NavigationEvent.STARTING_SECTION,
				FrameworkEvents.MINOR_CLOSE_BTN_CLICK,
				FrameworkEvents.STAGE_RESIZE,
				MenuEvent.MORPH,
				URLRequestProxy.REQUEST_COMPLETE,
				FrameworkEvents.SHOW_CURRENT_SECTIONS,
				LISOEvent.SPEAKING_OUT_APP,
				LISOEvent.LOOKING_IN_APP,
				NavigationEvent.MAJOR_SECTION,
				NavigationEvent.MINOR_SECTION
			];
			
		}
		
		
		
		/**
		 * 
		 * @param notification - sending the notifcation straight to the proper function for processing simplifies my switch statement 
		 * 
		 */
		override public function handleNotification( note:INotification ):void {
			
			switch( note.getName() ) {
				
				case NavigationEvent.MAJOR_SECTION: 					
					onMainNavigation( note );
					_navContiner.sectionsOnStage( manager.sectionOnStageIds );
					break;
				
				case LISOEvent.SPEAKING_OUT_APP:
				case NavigationEvent.MINOR_SECTION: 					
					hideSections();
					break;
					
				case FrameworkEvents.MINOR_CLOSE_BTN_CLICK:	
					onMinorCloseBtnClick();		
					break;
					
				case FrameworkEvents.STAGE_RESIZE: 						
					onStageResize(); 			
					break;
					
				case MenuEvent.MORPH: 								
					onMenuMorph( note ); 			
					break;
					
				case URLRequestProxy.REQUEST_COMPLETE: 					
					onRequestComplete( note );  	
					break;
				
				case LISOEvent.LOOKING_IN_APP:	
				case FrameworkEvents.SHOW_CURRENT_SECTIONS:					
					showCurrentSections();	
					_navContiner.sectionsOnStage( manager.sectionOnStageIds );
					break;
					
				case NavigationEvent.STARTING_SECTION:
					addSection( ( note.getBody() as SectionData ), false, true );
					break;
				
				
			}
			
		}
		
		
		
		private function onMenuMorph( note:INotification ):void {
			
			manager.setPosition();
			
		}
		
		
		
		private function onShowSections( e:Event ):void {
			
			manager.setPosition();
			
		}
		
		
		
		private function onMainNavigation( note:INotification ):void {
			
			var data:SectionData = note.getBody() as SectionData;
		 	sendNotification( AppFacade.REQUEST_DATA, data );
			addSection( data, true, false );
			
		}
		
		
		private function addSection( data:SectionData, isMenuOpen:Boolean, isDefault:Boolean ):void {
			
			var menuYPos:Number = manager.globalToLocal( _navContiner.globalPositionPoint( isMenuOpen ) ).y;
			manager.addSection( data, menuYPos, isDefault );
			
		}
		
		
		
		private function hideSections():void {
			
			manager.hideSections();
			
		}
		
		
		
		private function onMinorCloseBtnClick():void {
			
			_navContiner.sectionsOnStage( manager.sectionOnStageIds );	
			manager.revealSections();
			
		}
		
		
		
		private function onStageResize():void {
				
			manager.setPosition();
			
		}
		
		
		
		private function onRequestComplete(note:INotification):void {
			
			var data:ResponseData = note.getBody() as ResponseData;
			
		}
		
		
		
		private function onVideoLoad( e:VideoLoadEvent ):void {
			
			sendNotification( e.type, e.url );
			manager.hideSections();
			
		}
		
		
		
		private function showCurrentSections():void {
			
			manager.revealSections();
			
		}
		
		
		
		private function onGalleryCreation( e:FlickrGalleryEvent ):void {
			
			sendNotification( e.type, e );
			
		}
		
		
		
		private function onGalleryClick( e:FlickrGalleryEvent ):void {
			
			sendNotification( e.type, e );
			
			if( e.revealGallery ) {
				manager.hideSections();
			}
			
		}
		
		
		
		private function onSectionsRemoved( e:SectionEvent ) : void {
			
			sendNotification( e.type );
				
		}
		
		
		
		private function onClosedBtnClicked( e:DisplayEvent ) : void {
			
			_navContiner.enableMainNavLinkById( e.id );
			
		}
			
		
		
	}
}