<?php 



define( 'NUM_ROWS', 100 );

function mediateMsg( $mysqli, $submitApprove, $submitId ) {
	if( $submitId ) {
		$mysqli->query(
			"UPDATE messages
			SET isMediated = 1, 
			isApproved = $submitApprove
			WHERE id = $submitId
			ORDER BY messages.date DESC"
		);
		//print "Submit ID ".$submitId;
		//print $mysqli->info; 
	}
	
}




function getMessagesPrintPagination( $mysqli, $isApproved, $isMediated, $page ) {
	
	$allMsgs = $mysqli->query(
			"SELECT *
			FROM messages, userdata
			WHERE messages.userId = userdata.id
			AND isApproved = $isApproved
			AND isMediated = $isMediated");
	
	$totalPages = ceil( $allMsgs->num_rows / NUM_ROWS );
	$page = $totalPages < $page ? $totalPages : $page;
	
	createPaginationMenu( $totalPages, $page );
	
	$startEntry = ( $page - 1 ) * NUM_ROWS;
	$numRows = NUM_ROWS;
	
	$messages = $mysqli->query(
			"SELECT *
			FROM messages, userdata
			WHERE messages.userId = userdata.id
			AND isApproved = $isApproved
			AND isMediated = $isMediated
			ORDER BY messages.date DESC
			LIMIT $startEntry, $numRows");
	
	return $messages;
	
}



function createPaginationMenu( $totalPages, $page ) {
	
	$next = $page == $totalPages ? -1 : $page + 1 ;
	$previous = $page - 1;
	
	echo '<div id="pagination">';
	
	if( $totalPages > 1 ) {
		printPageMenu( $next, $totalPages, $previous, $page );
	}
	
	echo '</div>';
	
}



function printPageMenu( $next, $total, $previous, $currentPage ) {
	
	if( $previous == 0 ) {
		echo ' &nbsp; <span class="disabled">Previous</span> &nbsp; ';
	}
	else {
		echo '<a href="?page='.$previous.'">&lt;&lt;Previous</a> &nbsp; ';
	}
	
	for( $i = 1; $i <= $total; $i++ ) {
		$output = $i != $currentPage ? '<a href="?page='.$i.'">'. $i .'</a>' : '<span class="active">'.$i.'</span>';
		echo $output;
		
		if( $i < $total ) {
			echo ' | ';
		}
	}
	
	if( $next == -1 ) {
		echo ' &nbsp; <span class="disabled">Next</span>';
	}
	else {
		echo ' &nbsp; <a href="?page='.$next.'">Next&gt;&gt;</a>';
	}
	
}



?>