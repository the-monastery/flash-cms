-- phpMyAdmin SQL Dump
-- version 2.11.0
-- http://www.phpmyadmin.net
--
-- Host: internal-db.s52268.gridserver.com
-- Generation Time: Oct 18, 2009 at 07:12 AM
-- Server version: 4.1.25
-- PHP Version: 4.4.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `db52268_speakingoutMessages`
--

-- --------------------------------------------------------

--
-- Table structure for table `messagemap`
--

CREATE TABLE `messagemap` (
  `id` int(255) NOT NULL auto_increment,
  `parentId` int(255) NOT NULL default '0',
  `childId` int(255) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `parentId` (`parentId`,`childId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `messagemap`
--


-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(255) NOT NULL auto_increment,
  `userId` int(255) NOT NULL default '0',
  `parentId` int(255) NOT NULL default '0',
  `isTrunk` tinyint(1) NOT NULL default '0',
  `isComposite` tinyint(1) NOT NULL default '0',
  `replies` tinyint(4) NOT NULL default '0',
  `text` text NOT NULL,
  `xPos` int(6) NOT NULL default '0',
  `yPos` int(6) NOT NULL default '0',
  `isApproved` tinyint(1) NOT NULL default '0',
  `isMediated` tinyint(1) NOT NULL default '0',
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `userId` (`userId`,`parentId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `messages`
--


-- --------------------------------------------------------

--
-- Table structure for table `userdata`
--

CREATE TABLE `userdata` (
  `id` int(255) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `userdata`
--

