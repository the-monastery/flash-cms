package com.lookingInSpeakingOut.flashcms.ui {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.lookingInSpeakingOut.flashcms.events.speakOut.TrunkMessageEvent;
	
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import speakingout.assets.startThread.StartThreadBtnView;
	
	[Event (name="addMessage", type="com.lookingInSpeakingOut.flashcms.events.TrunkMessageEvent")]

	/**
	 * 
	 * @author ghostmonk 19-09-2009
	 * 
	 */
	public class StartThreadButton extends EventDispatcher {
		
		
		
		private var _toolTip:ToolTip;
		private var _view:StartThreadBtnView;
		private var _midPoint:Point;
		
		
		public function StartThreadButton( view:StartThreadBtnView ) {
			
			_view = view;
			_toolTip = new ToolTip( view.toolTip, view.toolTip.text );
			_midPoint = new Point( _view.bottomBGLayer.width * 0.5, _view.bottomBGLayer.height * 0.5 );
			//_toolTip.buildOut( _midPoint );
			_view.alpha = 0;
			_view.mouseChildren = false;
			_view.visible = false;
			
		}
		
		
		
		public function get view() : StartThreadBtnView {
			
			return _view;
			
		}
		
		
		
		public function buildIn() : void {
			
			enable();
			_view.visible = true;
			Tweener.addTween( _view, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		public function buildOut() : void {
			
			disable();
			Tweener.addTween( _view, { alpha:0, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function enable() : void {
			
			_view.addEventListener( MouseEvent.CLICK, onClick );
			_view.addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_view.addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			_view.buttonMode = true;
			_toolTip.buildIn();
			
		}
		
		
		
		public function disable() : void {
			
			_view.removeEventListener( MouseEvent.CLICK, onClick );
			_view.removeEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_view.removeEventListener( MouseEvent.ROLL_OUT, onRollOut );
			_view.buttonMode = false;
			_toolTip.buildOut( _midPoint );
			
		}
		
		
		
		
		private function onClick( e:MouseEvent ) : void {
			
			dispatchEvent( new TrunkMessageEvent( TrunkMessageEvent.ADD_MESSAGE ) );
			
		}
		
		
		
		private function onRollOut( e:MouseEvent ) : void {
			
			//_toolTip.buildOut( _midPoint );
			
		} 
		
		
		
		private function onRollOver( e:MouseEvent ) : void {
			
			
			//_toolTip.buildIn();
		}
		
		
		
	}
}