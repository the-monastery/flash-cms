package com.lookingInSpeakingOut.flashcms.data.sectionData.parse {
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class LinkData {
		
		
		
		private var _title:String;
		private var _url:String;
		private var _description:String;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get title():String {
			
			return _title;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get url():String {
			 
			return _url;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get description():String {
			 
			return _description;
			 
		}
		
		
		
		/**
		 * 
		 * @param xml
		 * 
		 */
		public function LinkData( xml:XML ) {
			
			_title = xml.@title.toString();
			_url = xml.@url.toString();
			_description = xml.description.toString();
			
		}
		
		
		
	}
}