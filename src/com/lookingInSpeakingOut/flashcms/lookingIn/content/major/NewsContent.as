package com.lookingInSpeakingOut.flashcms.lookingIn.content.major {
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.RSSData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items.NewsItem;
	
	[Event (name="itemClicked", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	[Event (name="buildOutComplete", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	[Event (name="animationUpdate", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class NewsContent extends BaseContent {
		
		
		
		private const BASE_URL:String = "";//"http://www.lookinginspeakingout.com/";
		private const DEFAULT_IMG:String = "assets/images/news/default.jpg";
		
		private var _items:Array;
		
		
		
		public function NewsContent( sectionW:int, sectionH:int ) {
		
			super( sectionW, sectionH );
			
		}
		
		
		
		override public function removeItems() : void {
			
			for each( var item:NewsItem in _items ) {
				item.buildOut();
			}
			
			_items = [];
		} 
		
		
		
		/**
		 * 
		 * 
		 */
		override public function listView():void {
			
			for each( var item:NewsItem in _items ) {
				addChild( item );
				if( item.isFullDisplay ) {
					item.setShortDisplay();
				}	
				item.buildIn();
			}
			
			showPageNav();
			
		}
		
		
		
		override protected function onDataLoaded( data:String ):void {
			
			var xml:XML = XML( data );
			_items = [];
			
			var yPos:int = 0;
			for each( var item:XML in xml.channel.item ) {
				var newsItem:NewsItem = new NewsItem( new RSSData( item, BASE_URL, DEFAULT_IMG ) );
				_items.push( newsItem );
				newsItem.yPos = yPos;
				yPos += newsItem.background.height + 10;
				newsItem.addEventListener( SectionEvent.ITEM_CLICKED, onItemClicked );
				newsItem.addEventListener( SectionEvent.SECTION_VIEW_UPDATE, onAnimationUpdate );
				newsItem.addEventListener( DisplayEvent.BUILD_OUT_COMPLETE, onBuildOutComplete );
				addChild( newsItem );
			}
			
			
			hasAggregatePages( xml.totalPages, yPos );
			super.onDataLoaded( data );
			
			
		}
		
		
		
		private function onItemClicked( e:SectionEvent ):void {
			
			var target:NewsItem = e.target as NewsItem;
			
			for each( var item:NewsItem in _items ) {
				if( item != target ) {
					item.buildOut();
				}
			}
			
			aggregateItemClicked();
			
		}
		
		
		
		private function onAnimationUpdate( e:SectionEvent ):void {
			
			displayUpdate();
			y = 15;
			
		}
		
		
		
		private function onBuildOutComplete( data:String ):void {
			
		}
		
		
		
	}
}