package com.lookingInSpeakingOut.flashcms.events.flickr {
	
	import com.lookingInSpeakingOut.flashcms.flickrViewer.FlickrThumbCollection;
	
	import flash.events.Event;

	public class FlickrGalleryEvent extends Event {
		
		
		
		public static const PHOTO_GALLERY_CLICK:String = "photoGalleryClick";
		public static const PHOTO_GALLERY_DATA:String = "photoGalleryData";
		public static const REMOVE_FROM_VIEW:String = "removeFromView";
		
		private var _id:String;
		private var _thumbCollection:FlickrThumbCollection;
		private var _revealGallery:Boolean;
		
		
		
		public function get revealGallery():Boolean {
			
			return _revealGallery;
			
		}
		
		
		public function set revealGallery( value:Boolean ):void {
			
			_revealGallery = value;
			
		}
		
		
		
		public function get thumbCollection():FlickrThumbCollection {
			
			return _thumbCollection;
			
		}
		
		
		
		public function get id():String {
			
			return _id;
			
		}
		
		
		
		public function FlickrGalleryEvent( type:String, thumbCollection:FlickrThumbCollection = null, id:String = "", bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			_thumbCollection = thumbCollection;
			_id = id;
			super( type, bubbles, cancelable );
			
		}
		
		
		
		override public function clone():Event {
			
			var galleryEvent:FlickrGalleryEvent = new FlickrGalleryEvent( type, thumbCollection, id, bubbles, cancelable ); 
			galleryEvent._revealGallery = _revealGallery;
			
			return galleryEvent; 
			
		}
		
		
		
	}
}