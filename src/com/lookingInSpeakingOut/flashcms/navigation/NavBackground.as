package com.lookingInSpeakingOut.flashcms.navigation {
	
	import caurina.transitions.Tweener;
	
	import flash.display.Shape;
	import flash.geom.Rectangle;

	/**
	 * Navigation Background display, with function to open and close
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class NavBackground extends Shape {
		
		
		
		private var _width:Number;
		private var _restHeight:Number;
		private var _openHeight:Number;
		private var _scaleY:Number;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get restHeight():Number {
			
			return _restHeight;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get openHeight():Number {
			
			return _openHeight;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		override public function get width():Number {
			
			return _width;
			
		}
		
		
		
		/**
		 * 
		 * @param config
		 * 
		 */
		public function NavBackground( config:XML ) {
			
			_width = config.width;
			_restHeight = config.height;
			_openHeight = config.openHeight;
			_scaleY = _openHeight / _restHeight;
			
			graphics.beginFill( config.color );
			graphics.drawRoundRect( 0, 0, _width, _restHeight, config.cornerRad, config.cornerRad );
			graphics.endFill();
			scale9Grid = new Rectangle( width * 0.5, height * 0.5, 1, 1 );
			
			scaleX = 0;
			scaleY = 0;
			
		}
		
		
		
		/**
		 * 
		 * @param onCompleteCallback
		 * 
		 */
		public function open( onCompleteCallback:Function = null ):void	{
			
			//mutate( _scaleY, -_openHeight + _restHeight * 0.5, onCompleteCallback );
			
		}
		
		
		
		/**
		 * 
		 * @param onCompleteCallback
		 * 
		 */
		public function close( onCompleteCallback:Function = null ):void {
			
			//mutate( 1, -_restHeight * 0.5, onCompleteCallback );
			
		}
		
		
		
		/**
		 * 
		 * @param onCompleteCallback
		 * 
		 */
		public function buildIn( onCompleteCallback:Function = null ):void {
			
			var xPos:int = -_width * 0.5;
			var yPos:int = -_restHeight * 0.5;
			
			Tweener.addTween (
				this, {
					scaleX:1, 
					scaleY:1, 
					x:xPos, 
					y:yPos, 
					time:0.3, 
					onComplete:onCompleteCallback
				}
			);
			
		}
		
		
		
		private function mutate( yScale:Number, yPos:Number, callBack:Function = null ):void {
			
			Tweener.removeTweens( this );
			
			Tweener.addTween (
				this, {
					scaleY:yScale, 
					y:yPos, 
					time:0.3, 
					onComplete:callBack
				}
			);
			
		}
		
		
		
	}
}