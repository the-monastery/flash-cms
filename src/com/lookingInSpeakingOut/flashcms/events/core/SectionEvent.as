package com.lookingInSpeakingOut.flashcms.events.core {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class SectionEvent extends Event {
		
		
		
		public static const ITEM_CLICKED:String = "itemClicked";
		public static const SECTION_VIEW_UPDATE:String = "sectionViewUpdate";
		public static const START_SECTION_BUILDOUT:String = "startSectionBuildOut";
		public static const SHOW_SECTIONS:String = "showSections";
		public static const SECTIONS_REMOVED:String = "sectionsRemoved";
		public static const SECTION_OPEN:String = "sectionOpen";
		
		
		
		
		/**
		 * 
		 * @param type
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function SectionEvent( type:String, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			super(type, bubbles, cancelable);
			
		}
		
		
		
		override public function clone():Event {
			
			return new SectionEvent( type );
			
		}
		
		
		
	}
}