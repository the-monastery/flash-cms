package com.lookingInSpeakingOut.flashcms.lookingIn.content {
	
	public class ContentDefinition {
		
		private static const HASH_MAP:Object = new Object();
		
		public static const NEWS:ContentDefinition = new ContentDefinition( "news" );
		public static const GALLERY:ContentDefinition = new ContentDefinition( "gallery" );
		public static const VIDEO:ContentDefinition = new ContentDefinition( "videos" );
		public static const DIARIES:ContentDefinition = new ContentDefinition( "diaries" );
		public static const RESOURCES:ContentDefinition = new ContentDefinition( "resources" );
		public static const WEB_LINKS:ContentDefinition = new ContentDefinition( "weblinks" );
		
		public static const HTML:ContentDefinition = new ContentDefinition( "html" );
		public static const CONTACT:ContentDefinition = new ContentDefinition( "contact" );
		
		
		
		public function ContentDefinition( id:String ) {
			
			HASH_MAP[ id ] = this; 
			
		}
		
		
		
		public static function getDefinition( id:String ):ContentDefinition {
			
			if( HASH_MAP[ id ] == null ) {
				
				throw new Error( "The string " + id + " is an invalid form of content" ); 
			}
			
			return HASH_MAP[ id ]; 
			
		} 

		
		
	}
}