package com.lookingInSpeakingOut.flashcms.video {
	
	import com.ghostmonk.events.ToggleEvent;
	
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	
	import items.ReturnTabAsset;
	
	[Event (name="toggle", type="com.ghostmonk.events.ToggleEvent")]
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class PlayToggleBtn
	{
		
		
		
		private var _view:MovieClip;
		private var _clickCall:Function;
		private var _isPlay:Boolean;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get isPlay():Boolean {
			 
			return _isPlay;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get view():MovieClip {
			 
			return _view as MovieClip;
			 
		}
		
		
		
		/**
		 * 
		 * @param view
		 * @param onClick
		 * 
		 */
		public function PlayToggleBtn( view:MovieClip, onClick:Function ) {
			
			_view = view;
			_view.stop();
			_clickCall = onClick;
			_isPlay = false;
			enable();
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function setToPause():void {
			
			_isPlay = false;
			_view.gotoAndStop( 1 );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function enable():void {
			
			_view.buttonMode = true;
			_view.addEventListener( MouseEvent.CLICK, onMouseClick );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function disable():void {
			
			_view.buttonMode = false;
			_view.removeEventListener( MouseEvent.CLICK, onMouseClick );
			
		}
		
		
		
		private function onMouseClick( e:MouseEvent ):void {
			
			_isPlay = !_isPlay;
			_view.gotoAndStop( _isPlay ? 2 : 1 );
			_clickCall( new ToggleEvent( ToggleEvent.TOGGLE, _isPlay ) );
			
		}
		
		
		
	}
}