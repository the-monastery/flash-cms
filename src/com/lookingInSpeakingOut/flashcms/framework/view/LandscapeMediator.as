package com.lookingInSpeakingOut.flashcms.framework.view {
	
	import com.lookingInSpeakingOut.flashcms.AppFacade;
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.MessageDataCollection;
	import com.lookingInSpeakingOut.flashcms.events.LISOEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.PaginationEvent;
	import com.lookingInSpeakingOut.flashcms.events.speakOut.FormSubmitEvent;
	import com.lookingInSpeakingOut.flashcms.framework.model.SpeakingOutProxy;
	import com.lookingInSpeakingOut.flashcms.speakingOut.Landscape;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	/**
	 * 
	 * @author ghostmonk 2009-09-20
	 * 
	 */
	public class LandscapeMediator extends Mediator {
		
		
		
		public static const NAME:String = "LandscapeMediator";
		
		private var _dataProvider:SpeakingOutProxy;
		
		
		
		public function LandscapeMediator( view:Landscape ) {
			
			super( NAME, view );		
			view.addEventListener( FormSubmitEvent.SUBMIT_SPEAKOUT_MESSAGE, onTrunkSubmit );
			view.addEventListener( PaginationEvent.PAGE_NAVIGATION, onPageNavigation );
			
		}
		
		
		
		public function get landscape() : Landscape {
			
			return viewComponent as Landscape;
			
		}
		
		
		
		override public function onRegister() : void {
			
			_dataProvider = facade.retrieveProxy( SpeakingOutProxy.NAME ) as SpeakingOutProxy;
			
		}
		
		
		
		override public function listNotificationInterests() : Array {
			
			return [
				SpeakingOutProxy.TRUNK_MESSAGES_READY,
				SpeakingOutProxy.LEAF_MESSAGES_READY,
				LISOEvent.LOOKING_IN_APP,
				LISOEvent.SPEAKING_OUT_APP,
				NavigationEvent.MAJOR_SECTION,
				NavigationEvent.MINOR_SECTION
			];
			
		}
		
		
		
		
		override public function handleNotification( note:INotification ) : void {
			
			switch( note.getName() ) {
				
				case SpeakingOutProxy.TRUNK_MESSAGES_READY:
					landscape.messages = note.getBody() as MessageDataCollection;
					break;
					
				case SpeakingOutProxy.LEAF_MESSAGES_READY:
					break;
				
				case NavigationEvent.MAJOR_SECTION:
				case NavigationEvent.MINOR_SECTION:
				case LISOEvent.LOOKING_IN_APP:
					landscape.hideMessageView();
					break;
					
				case LISOEvent.SPEAKING_OUT_APP:
					landscape.showMessageView();
					break;
				
			}
			
		}
		
		
		
		private function onTrunkSubmit( e:FormSubmitEvent ) : void {
			
			sendNotification( AppFacade.SUBMIT_MESSAGE, e.data );
			
		}
		
		
		private function onPageNavigation( e:PaginationEvent ) : void {
			
			_dataProvider.retrieveTrunkMsgs( e.page );
			
		}
		
			
	}
}