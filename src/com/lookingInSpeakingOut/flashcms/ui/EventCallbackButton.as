package com.lookingInSpeakingOut.flashcms.ui {
	
	import com.ghostmonk.ui.graveyard.buttons.TextButton;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
		
	
	/**
	 * Extends a display button with logic to present an embedded text field and use rollOver and rollOut mouseEvents
	 * This class adds the click functionality and dispatches the same event that is used in its creation
	 * 
	 * @author ghostmonk 31/12/2008
	 * 
	 */
	public class EventCallbackButton extends TextButton {
		
		
		
		private var _event:Event;
		private var _callback:Function;
		private var _id:String;
		
		
		
		/**
		 * 
		 * @param navEvent	- NavigationEvent  
		 * @param config	- XML node with "size", "color" and "baseColor" attribute 
		 * @param font		- String of font name to be embeded
		 * @param callback  - Function to be called, should expect same event passed into constructor  
		 * 
		 */
		public function EventCallbackButton( event:Event, label:String, id:String, config:XML, font:String, callback:Function ) {
			
			_callback = callback;
			_event = event;
			_id = id;
			super( font, config.@size, label, config.@color, config.@baseColor );
			isBgTextColorSwap = false;
			
		}
		
		
		
		public function get id() : String {
			
			return _id;
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		override public function enable():void {
			
			super.enable();
			addEventListener( MouseEvent.CLICK, onClick );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		override public function disable( isActive:Boolean = false ):void {
			
			super.disable( isActive );
			removeEventListener( MouseEvent.CLICK, onClick );
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
			
			_callback( _event );
			
		}
		
		
		
	}
}