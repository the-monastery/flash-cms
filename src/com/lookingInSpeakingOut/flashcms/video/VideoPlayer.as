package com.lookingInSpeakingOut.flashcms.video {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.events.PercentageEvent;
	import com.ghostmonk.media.video.CoreVideo;
	import com.ghostmonk.media.video.events.CustomNetStreamEvent;
	import com.ghostmonk.media.video.events.MetaInfoEvent;
	import com.ghostmonk.media.video.events.VideoControlEvent;
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	import flash.geom.Point;
	
	import video.assets.VideoDisplayAsset;

	[Event(name="fullScreen", type="flash.events.FullScreenEvent")]
	[Event(name="removeVideoPlayer", type="com.lookingspeakingout.flashcms.components.video.VideoDisplayAsset")]
	
	
	/**
	 * 
	 * @author ghostmonk 05/03/2009 
	 * 
	 */
	public class VideoPlayer extends VideoDisplayAsset {
		
		
		public static const REMOVE_VIDEO_PLAYER:String = "removeVideoPlayer";
		
		
		
		private var _coreVideo:CoreVideo;
		private var _closeBtn:FrameLabelButton;
		private var _controlPanel:VideoControlPanel;
		private var _fullScreenCtrlBg:FullScreenCtrlBg;	//controlPanel background when video is in full screen
		private var _controlsPoint:Point;				//maintains a reference to the x and y position of the controls in normal mode
		
		
		
		/**
		 * Composes CoreVideo class and VideoControl Panel to playbackvideo.
		 * This class extends VideoDisplayAsset, which is a Flash IDE Asset that includes various 
		 * components for displaying and controlling playback of video
		 * 
		 */
		public function VideoPlayer() {
			
			_coreVideo = new CoreVideo( );
			_closeBtn = new FrameLabelButton( closeBtn );
			
			_closeBtn.addClickCallback( buildOut );
			holder.addChild( _coreVideo.video );
			
			_controlPanel = new VideoControlPanel( controls );
			_fullScreenCtrlBg = new FullScreenCtrlBg();
			_controlsPoint = new Point( controls.x, controls.y );
			
			_coreVideo.addEventListener( PercentageEvent.LOAD_CHANGE, onVideoLoad );
			_coreVideo.addEventListener( MetaInfoEvent.META_INFO_READY, onMetaInfo );
			_coreVideo.addEventListener( CustomNetStreamEvent.STATUS, onStreamStatus );	
			
			_controlPanel.addEventListener( PercentageEvent.CHANGE, onPercentChange );
			_controlPanel.addEventListener( VideoControlEvent.PAUSE, onPause );
			_controlPanel.addEventListener( VideoControlEvent.PLAY, onPlay );
			_controlPanel.addEventListener( FullScreenEvent.FULL_SCREEN, onFullscreenClick );
			
			addEventListener( Event.ADDED_TO_STAGE, buildIn );
			
			scaleX = scaleY = 0; //VideoPlayer starts from a scaled to nothing postion
		}
		
		
		
		/**
		 * 
		 * @param event: 
		 * Event is generic and optional.
		 * Animates the video in from the scaled to 0 state to fullscale
		 * Transition has a slight pop when it completes
		 * 
		 */
		public function buildIn( event:Event = null ):void {
			
			Tweener.addTween (
				this, {	
					scaleX:1, 
					scaleY:1, 
					time:0.3, 
					onUpdate:dispatchEvent, 
					onUpdateParams:[ new Event( Event.RESIZE ) ], 
					transition:Equations.easeOutBack
				}
			);	
			
		}
		
		
		
		/**
		 * 
		 * @param event: 
		 * Event is generic and optional
		 * Animates the player out to 0 scale and removes itself from the state
		 * As the instance animates out, it displatces a REMOVED_FROM_STAGE event
		 * 
		 */
		public function buildOut( event:Event = null, dispatch:Boolean = true ):void {
			
			Tweener.addTween (
				this, {	
					scaleX:0, 
					scaleY:0, 
					time:0.3, 
					onUpdate:dispatchEvent, 
					onUpdateParams:[ new Event( Event.RESIZE ) ], 
					onComplete:stage.removeChild, 
					onCompleteParams:[ this ]
				}
			);
			
			_coreVideo.close();
			stage.displayState = StageDisplayState.NORMAL;
			
			if( dispatch ) {
				dispatchEvent( new Event( REMOVE_VIDEO_PLAYER ) );
			}
			
		}
		
		
		
		/**
		 * 
		 * @param url
		 * 
		 */
		public function load( url:String ):void {
			
			if( _coreVideo.customNetStream ) {
				_coreVideo.close();
			}
			_coreVideo.load( url, false, true );
			_controlPanel.reset( true );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function fullscreenMode():void {
			
			stage.addChild( _coreVideo.video );
			createFullScreenCtrl();
			stage.addChild( _fullScreenCtrlBg );
			_coreVideo.fullStageMode( stage );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function normalScreenMode():void {
			
			_coreVideo.scaleAndCenter( holder );
			holder.addChild( _coreVideo.video );
			controls.y = _controlsPoint.y;
			stage.removeChild( _fullScreenCtrlBg );
			addChild( controls );
			
		}
		
		
		
		private function createFullScreenCtrl():void {
			
			_fullScreenCtrlBg.x = ( stage.stageWidth - _fullScreenCtrlBg.width ) / 2;
			_fullScreenCtrlBg.y = stage.stageHeight - _fullScreenCtrlBg.height - 30;
			controls.y = ( _fullScreenCtrlBg.height - controls.height ) * 0.5;
			_fullScreenCtrlBg.addChild( controls );
			
		}
		
		
		
		private function onMetaInfo( e:MetaInfoEvent ):void {
			
			_coreVideo.scaleAndCenter( holder );
			_controlPanel.setTotalDuration( e.metaData.duration );
			
			
		}
		
		
		
		private function onPlay( e:VideoControlEvent ):void {
			
			addEventListener( Event.ENTER_FRAME, playProgress );
			_coreVideo.play();
			
		}
		
		
		
		private function onPause( e:VideoControlEvent ):void {
			
			removeEventListener( Event.ENTER_FRAME, playProgress );
			_coreVideo.pause();
			
		}
		
		
		
		private function playProgress( e:Event ):void {
			
			_controlPanel.setScrubHead( _coreVideo.timeAsPercent );
			
		}
		
		
		
		private function onPercentChange( e:PercentageEvent ):void {
			
			_coreVideo.timeAsPercent = e.percent;
			
		}
		
		
		
		private function onVideoLoad( e:PercentageEvent ):void {
			
			_controlPanel.onLoadProgress(e);
			
		}
		
		
		
		private function onStreamStatus( e:CustomNetStreamEvent ) : void {
			
			if( e.info.code == CustomNetStreamEvent.PLAY_STOP ) {
				resetVideo();
			}
				
		}
		
		
		
		private function resetVideo():void {
			
			_controlPanel.reset( false );
			_coreVideo.pause();
			_coreVideo.seek( 0 );
			
			removeEventListener( Event.ENTER_FRAME, playProgress );
			
		}
		
		
		
		private function onFullscreenClick( e:FullScreenEvent ):void {
			
			dispatchEvent( e );
			
			if( stage.displayState == StageDisplayState.FULL_SCREEN ) {
				fullscreenMode();
			}
			
		}



	}
}