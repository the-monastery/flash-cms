package com.lookingInSpeakingOut.flashcms.framework.model {
	
	import com.ghostmonk.net.XMLLoader;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.ResponseData;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	
	import org.puremvc.as3.multicore.patterns.proxy.Proxy;

	/**
	 * Make a network request for specific data, then sends out a notification with 
	 * the conent and a reference to the appropriate section the content should be 
	 * placed  
	 * 
	 * @author ghostmonk 17/01/2009
	 * 
	 */
	public class URLRequestProxy extends Proxy {
		
		
		
		public static const NAME:String = "URLRequestProxy";
		public static const REQUEST_COMPLETE:String = "requestComplete";
		
		
		
		private var _section:String;
		
		
		
		/**
		 * 
		 * 
		 */
		public function URLRequestProxy() {
			
			super( NAME );
			
		}
		
		
		
		/**
		 * 
		 * @param data
		 * 
		 */
		public function sendRequest( data:SectionData ):void {
			
			_section = data.label;
			new XMLLoader( data.request.url, onComplete );
			
		}
		
		
		
		private function onComplete( xml:XML ):void {
			
			sendNotification( REQUEST_COMPLETE, new ResponseData( _section, xml ) );
			
		}
		
		
		
	}
}