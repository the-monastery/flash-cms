package com.lookingInSpeakingOut.flashcms.lookingIn {
	
	import caurina.transitions.Equations;
	
	import com.lookingInSpeakingOut.flashcms.lookingIn.sections.BaseSection;
	
	import flash.display.DisplayObjectContainer;
	
	
	/**
	 * Sets up the all tweens necessary for building in and out content in the Sections
	 * 
	 * @author ghostmonk 15/02/2009
	 * 
	 */
	public class DisplayTweens {
		
		
		
		private var _scrollerOpen:Object;
		private var _scrollerClose:Object;
		
		private var _bgOpen:Object;
		private var _bgClose:Object;
		
		private var _titleOpen:Object;
		private var _titleClose:Object;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get scrollerOpen():Object { 
		
			return _scrollerOpen; 
		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get scrollerClose():Object { 
		
			return _scrollerClose; 
		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get bgOpen():Object { 
		
			return _bgOpen; 
		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get bgClose():Object { 
		
			return _bgClose; 
		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get titleOpen():Object { 
		
			return _titleOpen; 
		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get titleClose():Object { 
		
			return _titleClose; 
		
		}
		
		
		
		/**
		 * 
		 * @param titleText
		 * @param scrollerX
		 * @param scrollerY
		 * @param delay
		 * 
		 */
		public function createBuildIn( titleText:String, scrollerX:Number, scrollerY:Number, delay:Number = 0 ):void {
			
			if( _scrollerOpen == null ) {
				
				_scrollerOpen = {
					x:scrollerX, 
					y:scrollerY, 
					scaleX:1, 
					scaleY:1, 
					delay:delay, 
					time:0.3
				};
				
				_bgOpen = {
					scaleX:1, 
					scaleY:1, 
					x:0, y:0, 
					delay:delay, 
					time:0.3, 
					transition:Equations.easeOutBack
				};
				
				_titleOpen 	= {
					_text:titleText, 
					delay:delay, 
					time:0.3, transition:Equations.easeNone
				};
				
			}
			
		}
		
		
		
		/**
		 * 
		 * @param parent - 	this may be a bit of a complicated piece, but the parent is used to remove itself 
		 * 					from the stage after the buildout is complete
		 * @param centerX
		 * @param centerY
		 * @param bgDelay
		 * 
		 */
		public function createBuildOut( parent:DisplayObjectContainer, centerX:Number, centerY:Number, bgDelay:Number ):void {
			
			_scrollerClose = {
				x:centerX, 
				y:centerY, 
				scaleX:0, 
				scaleY:0, 
				time:0.3
			};
			
			_bgClose = {
				scaleX:0, 
				scaleY:0, 
				x:centerX, 
				y:centerY, 
				delay:bgDelay, 
				time:0.3, 
				onComplete:onTweenComplete, 
				onCompleteParams:[parent]
			};
			
			_titleClose = {
				_text:"", 
				time:0.2, 
				transition:Equations.easeNone
			};
			
		}
		
		
		
		private function onTweenComplete( parent:BaseSection ):void {
			
			if( parent.abortTween ) {
				parent.buildIn();
			}
			else { 
				if( parent.parent != null ) {
					parent.parent.removeChild( parent );	
				}
			}
			
		}



	}
}