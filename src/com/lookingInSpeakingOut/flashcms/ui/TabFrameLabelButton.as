package com.lookingInSpeakingOut.flashcms.ui {
	
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	
	import flash.display.MovieClip;
	import flash.events.FocusEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class TabFrameLabelButton extends FrameLabelButton {
		
		
		
		private var _onClick:Function;
		
		
		
		public function TabFrameLabelButton( clip:MovieClip, clickCall:Function ) {
			
			super( clip );
			addClickCallback( onSuperClick );
			
			view.addEventListener( FocusEvent.FOCUS_IN, onFocusIn );
			view.addEventListener( FocusEvent.FOCUS_OUT, onFocusOut );
			
			_onClick = clickCall;
			
		}
		
		
		override public function disable() : void {
			
			super.disable();
			view.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			view.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			view.alpha = 0.5;
			
		}
		
		
		override public function enable() : void {
			
			super.enable();
			view.addEventListener( FocusEvent.FOCUS_IN, onFocusIn );
			view.addEventListener( FocusEvent.FOCUS_OUT, onFocusOut );
			view.alpha = 1;
			
		}
		
		
		
		private function onSuperClick( e:MouseEvent ) : void {
			
			_onClick( e );
			
			
		}
		
		
		
		private function onFocusIn( e:FocusEvent ) : void {
			
			view.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			view.addEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			view.gotoAndStop( "rollOver" );
			
		}
		
		
		
		private function onFocusOut( e:FocusEvent ) : void {
			
			view.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			view.removeEventListener( KeyboardEvent.KEY_UP, onKeyUp );
			view.gotoAndStop( 0 );
			
		}
		
		
		
		private function onKeyDown( e:KeyboardEvent ) : void {
			
			view.removeEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			
			if( e.keyCode == Keyboard.ENTER ) {
				onSuperClick( null );
			}
			
		}
		
		
		
		private function onKeyUp( e:KeyboardEvent ) : void {
			
			view.addEventListener( KeyboardEvent.KEY_DOWN, onKeyDown );
			
		}



	}
}