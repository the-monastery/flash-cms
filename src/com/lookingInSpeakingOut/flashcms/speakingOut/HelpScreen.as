package com.lookingInSpeakingOut.flashcms.speakingOut {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.display.graphics.shapes.RoundedRectangle;
	import com.ghostmonk.text.EmbeddedText;
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	import com.lookingInSpeakingOut.flashcms.cmsUtils.CleanHTML;
	import com.lookingInSpeakingOut.flashcms.globals.Fonts;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.minor.HTMLContent;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;

	public class HelpScreen extends Sprite {
		
		
		
		private const WIDTH:int = 500;
		private const HEIGHT:int = 300;
		private const RAD:int = 20;
		private const PADDING:int = 10;
		private const COLOR:uint = 0xf0ebdf;
		
		private var _textView:HTMLContent;
		private var _bg:RoundedRectangle;
		private var _close:FrameLabelButton;
		private var _titleBar:RoundedRectangle;
		
		private var _title:EmbeddedText;
		private var _mainText:EmbeddedText;
		private var _inlay:RoundedRectangle;
		
		
		
		public function HelpScreen( title:String, mainText:String ) {
			
			createMainText( CleanHTML.removeWhiteSpace( mainText ) );
			createTitleText( CleanHTML.removeWhiteSpace( title ) );
			createFrame();
			
			_titleBar.addChild( _title );
			_inlay.addChild( _mainText );	
			addChild( _bg );
			_bg.addChild( _inlay );
			addChild( _close.view );
			addChild( _titleBar );
			
			alpha = 0;
			
		}
		
		
		
		public function centerToStage( stage:Stage ) : void {
			
			x = ( stage.stageWidth - WIDTH ) * 0.5;
			y = ( stage.stageHeight - HEIGHT ) * 0.5;
			
		}
		
		
		
		public function buildIn() : void {
			
			centerToStage( stage );
			Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		public function buildOut() : void {
			
			Tweener.addTween( this, { alpha:0, time:0.3, transition:Equations.easeNone, onComplete:removeThis } );
			
		}
		
		
		private function removeThis() : void {
			
			if( parent ) {
				parent.removeChild( this );
			}
			
		}
		
		
		private function onClose( e:MouseEvent ) : void {
			
			buildOut();
			
		}
		
		
		
		private function createMainText( text:String ) : void {
			
			_mainText = new EmbeddedText( Fonts.VERDANA.fontName, 12, 0x3b050e );
			_mainText.width = WIDTH - 50;
			
			_mainText.htmlText = text;
			_mainText.x = PADDING;
			_mainText.y = PADDING;
			
		}
		
		
		
		private function createTitleText( text:String ) : void {
			
			_titleBar = new RoundedRectangle( WIDTH * 0.5, 30, 20, 0x3b050e, 1 );
			_titleBar.x = ( WIDTH - _titleBar.width ) * 0.5;
			_titleBar.y = - _titleBar.height * 0.5;
			
			_title = new EmbeddedText( Fonts.VERDANA.fontName, 14, 0xd76982, true, true, false );
			_title.htmlText = text;
			_title.x = ( _titleBar.width - _title.width ) * 0.5;
			_title.y = 3;
			
		}
		
		
		
		private function createFrame() : void {
			
			_close = new FrameLabelButton( new MinorClosebtn() );
			_close.addClickCallback( onClose );
			_close.view.x = WIDTH;
			var paddingX2:int = PADDING * 2;
			_inlay= new RoundedRectangle( WIDTH - paddingX2, _mainText.height + paddingX2, RAD, COLOR, 0.8 );
			_bg = new RoundedRectangle( WIDTH, _inlay.height + paddingX2, RAD, COLOR, 0.8 );
			_inlay.x = PADDING;
			_inlay.y = PADDING;
			
		}
		
		
		
	}
}