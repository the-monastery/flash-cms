package com.lookingInSpeakingOut.flashcms.events.speakOut {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 2009-20-2009
	 * 
	 */
	public class TrunkMessageEvent extends Event {
		
		
		
		public static const ADD_MESSAGE:String = "trunkAddMessage";
		
		private var _id:int;
		
		
		
		public function TrunkMessageEvent( type:String, id:int = 0, bubbles:Boolean = false, cancelable:Boolean = false ) {
			
			_id = id;
			super( type, bubbles, cancelable );
			
		}
		
		
		
		public function get id() : int {
			
			return _id;
			
		}
		
		
		
		override public function clone() : Event {
			
			return new TrunkMessageEvent( type, id, bubbles, cancelable );
			
		} 
		
		
		
	}
}