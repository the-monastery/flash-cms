package com.lookingInSpeakingOut.flashcms.flickrViewer {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.net.AssetLoader;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickerSetEvent;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickrGalleryEvent;
	
	import flash.display.Bitmap;
	import flash.display.MovieClip;
	import flash.events.EventDispatcher;
	import flash.events.MouseEvent;

	[Event (name="photoGalleryClick", type="com.lookingspeakingout.flashcms.events.FlickrGalleryEvent")]

	/**
	 * 
	 * @author ghostmonk 30/05/2009
	 * 
	 */
	public class FlickrThumb extends EventDispatcher {
		
		
		private var _view:MovieClip;
		private var _index:int;
		private var _thumbImgId:String;
		private var _title:String;
		private var _description:String;
		
		private var _secret:String;
		private var _server:String;
		private var _farm:String;
		private var _setID:String;
		
		private var _isPhotoSet:Boolean;
		private var _loader:AssetLoader;
		
		private var _fullWidth:Number;
		private var _fullHeight:Number;
		
		
		
		public function FlickrThumb( data:XML, index:int, view:MovieClip ) {
			
			_index = index;
			_view = view;
			
			_isPhotoSet = data.@primary.toString().length > 0;
			
			_fullWidth = view.width;
			_fullHeight = view.height;
			
			if( data != null ) {
				
				_title = data.@title || data.title;
				_secret = data.@secret;
				_setID = data.@id.toString();
				_thumbImgId = data.@primary || data.@id.toString();
				_server = data.@server;
				_farm = data.@farm;
				_description =  data.description;
				_loader = new AssetLoader( createURL( "_s" ), onThumbLoaded );
				
			}
			
		}
		
		
		
		public function get fullWidth() : int  {
			
			return _fullWidth;
			
		}
		
		
		
		public function get fullHeight() : int {
			
			return _fullHeight;
			
		}
		
		
		
		public function get view() : MovieClip {
			
			return _view;
			
		}
		
		
		
		public function get mainURL() : String {
				
			return createURL();
			
		}
		
		
		
		public function get id() : String {
			
			return _thumbImgId;
			
		}
		
		
		
		public function get title() : String {
			
			return _title;
			
		}
		
		
		
		public function get index() : int {
			
			return _index;
			
		}
		
		
		
		public function get description() : String {
			
			return _description;
			
		}
		
		
		
		public function enable():void {
			
			if( !_view.screen.hitTestPoint( _view.mouseX, _view.mouseY ) ) {
				_view.screen.alpha = 1;
			}
			
			_view.buttonMode = true;
			_view.addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_view.addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			_view.addEventListener( MouseEvent.CLICK, onClick );
			
		}
		
		
		
		public function disable():void {
			
			_view.screen.alpha = 0;
			
			_view.buttonMode = false;
			_view.removeEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_view.removeEventListener( MouseEvent.ROLL_OUT, onRollOut );
			_view.removeEventListener( MouseEvent.CLICK, onClick );
			
		}
		
		
		
		
		public function buildIn( xPos:int, yPos:int ) : void {
			
			enable();
			_view.scaleX = _view.scaleY = 0;
			_view.x = xPos + _fullWidth * 0.5;
			_view.y = yPos + _fullHeight * 0.5;
			Tweener.addTween( _view, { scaleX:1, scaleY:1, x:xPos, y:yPos, time:0.3 } );
				
		}
		
		
		
		public function buildOut() : void {
			
			disable();
			var xPos:int = _view.x + _fullWidth * 0.5;
			var yPos:int = _view.y + _fullHeight * 0.5;
			Tweener.addTween( _view, { scaleX:0, scaleY:0, x:xPos, y:yPos, time:0.3, onComplete:buildOutComplete } );
			
		}
		
		
		
		private function buildOutComplete() : void {
			
			if( _view.parent ) {
				_view.parent.removeChild( _view );
			}
			
		}
		
		
		
		
		private function onRollOver( e:MouseEvent ):void {
			
			Tweener.addTween( _view.screen, {
					alpha:0,
					time:0.3,
					transition:Equations.easeNone				
				}
			);
			
		}
		
		
		
		private function onRollOut( e:MouseEvent ):void {
			
			Tweener.addTween( _view.screen, {
					alpha:1,
					time:0.3,
					transition:Equations.easeNone				
				}
			);
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
			
			if( _isPhotoSet ) {
				dispatchEvent( new FlickerSetEvent( FlickerSetEvent.FLICKER_SET_CLICK, _setID ) );
			}
			else {
				dispatchEvent( new FlickrGalleryEvent( FlickrGalleryEvent.PHOTO_GALLERY_CLICK, null, _thumbImgId ) );
			}
			
		}
		
		
		
		private function onThumbLoaded( bitmap:Bitmap ):void {
			
			bitmap.smoothing = true;
			bitmap.width = 100;
			bitmap.height = 100;
			bitmap.alpha = 0;
			_view.holder.addChild( bitmap );
			
			if( _view.titleFld ) {
				_view.titleFld.text = _title;
			}
			
			enable();
			
			Tweener.addTween( bitmap, { alpha:1, time:0.2, transition:Equations.easeNone } );
			_loader.cleanUp();
			
		}
		
		
		private function createURL( size:String = "" ):String {
			
			return "http://farm" + _farm + ".static.flickr.com/" + _server + "/"+ _thumbImgId + "_"+ _secret + size + ".jpg"
			
		}
		
		
		
	}
}