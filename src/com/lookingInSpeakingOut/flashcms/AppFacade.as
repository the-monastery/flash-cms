package com.lookingInSpeakingOut.flashcms {
	
	import com.lookingInSpeakingOut.flashcms.data.StartupData;
	import com.lookingInSpeakingOut.flashcms.framework.controller.CreationCommand;
	import com.lookingInSpeakingOut.flashcms.framework.controller.LookingInCommand;
	import com.lookingInSpeakingOut.flashcms.framework.controller.SectionDataCommand;
	import com.lookingInSpeakingOut.flashcms.framework.controller.StartupCommand;
	import com.lookingInSpeakingOut.flashcms.framework.controller.SubmitMessageCommand;
	
	import org.puremvc.as3.multicore.patterns.facade.Facade;

	
	/**
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class AppFacade extends Facade {
		
		
		
		public static const START_UP_APP:String = "startUpApp";
		public static const REQUEST_DATA:String = "requestData";
		public static const CREATE_APP_DEPENDENCIES:String = "createAppDependencies";
		public static const SHOW_LOOKING_IN:String = "showLookingIn";
		public static const SUBMIT_MESSAGE:String = "submitMessage";
		
		
		
		
		/**
		 * Instead of placing all the project event constants in this class, I thought to place them 
		 * in applicably associated classes. This creates a closer connection between a command/Mediator/Proxy's 
		 * intentions and their execution. More thought needs to be considered in variable placement, but will 
		 * overall strengthen understanding of the system.
		 */
		public function AppFacade( key:String ) {
			
			super( key );
			
		}
		
		
		
		/**
		 * Returns either a reference to an instance of an AppFacade, or returns a new on if it does not already exist
		 * 
		 * @param key a referene to the application facade
		 * @return returns instance of referenced AppFacade
		 * 
		 */
		public static function getInstance( key:String ):AppFacade {
			
			if( instanceMap[ key ] == null ) {		
				instanceMap[ key ] = new AppFacade( key );
			}
			
			return instanceMap[ key ] as AppFacade;	
		}
		
		
		
	 	override protected function initializeController():void {
	 		
	 		super.initializeController();
	 		registerCommand( START_UP_APP, StartupCommand );
	 		registerCommand( REQUEST_DATA, SectionDataCommand );
	 		registerCommand( CREATE_APP_DEPENDENCIES, CreationCommand );
	 		registerCommand( SHOW_LOOKING_IN, LookingInCommand );
	 		registerCommand( SUBMIT_MESSAGE, SubmitMessageCommand );
	 		
	 	}
	 	
	 	
	 	
	 	/**
	 	 * Sends notification to the AppFacade that the StartupCommand should be initialized.
	 	 * 
	 	 * @param startupObject Information needed to start up the application 
	 	 * 
	 	 */
	 	public function startup( startupObject:StartupData ):void {
	 		
	 		sendNotification( START_UP_APP, startupObject );
			
		}
		
		
		
	}
}