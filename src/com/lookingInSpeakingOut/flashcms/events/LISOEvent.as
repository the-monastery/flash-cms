package com.lookingInSpeakingOut.flashcms.events {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 2009-10-02
	 * 
	 */
	public class LISOEvent extends Event {
		
		
		public static const LOOKING_IN_APP:String = "lookingInApp";
		public static const SPEAKING_OUT_APP:String = "speakingOutApp";
		
		
		
		public function LISOEvent( type:String, bubbles:Boolean=false, cancelable:Boolean = false ) {
			
			super( type, bubbles, cancelable );
			
		}
		
		
		
		override public function clone() : Event {
			
			return new LISOEvent( type, bubbles, cancelable );
			
		}
		
		
		
	}
}