package com.lookingInSpeakingOut.flashcms.data.sectionData.parse {
	
	import flash.net.URLRequest;
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class RSSData {
		
		
		
		private var _baseURL:String;
		private var _defaultImg:String;
		
		private var _title:String;
		private var _shortDescription:String;
		private var _author:String;
		private var _date:String;
		private var _photoRequest:URLRequest;
		private var _assetLink:URLRequest;
		private var _fullLink:URLRequest;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get title():String {
			 
			return _title;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get shortDescription():String {
			 
			return _shortDescription;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get author():String	{
			 
			return _author;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get date():String {
			 
			return _date;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get photoRequest():URLRequest {
			 
			return _photoRequest;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get assetLink():URLRequest {
			 
			return _assetLink;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get fullLink():URLRequest {
			 
			return _fullLink;
			 
		} 
		
		
		
		/**
		 * 
		 * @param item
		 * @param baseURL
		 * @param defaultIMG
		 * 
		 */
		public function RSSData( item:XML, baseURL:String, defaultIMG:String ) {
			
			_baseURL = baseURL;
			_defaultImg = defaultIMG;
			
			var ns:Namespace = item.namespace( "dc" );
			
			_title = item.title.toString();
			_shortDescription = parseDescription( item.description[ 0 ] );;
			_author = item.ns::creator.toString();
			_date = item.pubDate.toString();
			_photoRequest = parseURL( item.description[ 0 ] );
			_assetLink = new URLRequest( _baseURL+item.link );
			_fullLink = new URLRequest( item.guid.toString() );
			
		}
		
		
		
		private function parseDescription( node:XML ):String {
			
			var str:String = node.toString();
			var pattern:RegExp = /\s{1,}/;
			var index:int = str.indexOf( ">" );
			
			if( index > 0) {
				str = str.substr( index + 1 );
			} 
			
			str = str.replace( pattern, "" );
			
			return str;
			
		}
		
		
		
		private function parseURL( node:XML ):URLRequest {
			
			var str:String = node.toString();
			var start:int = str.search( '<img src="' );
			var end:int = str.search( '" />' );
			
			var url:String;
			
			if( start >= 0 ) {
				url = _baseURL + str.substring( start + 10, end );
			}
			else {
				return null;
			}
			
			return new URLRequest( url );
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function toString():String {
			
			var photoString:String = _photoRequest ?  "Photo: " + _photoRequest.url + "\n" : "";
			
			return 	"Title: " + _title + "\n" +
					"Desc: " + _shortDescription + "\n" +
					"Auth: " + _author + "\n" +
					"Date: " + _date + "\n" +
					photoString +
					"Asset: " + _assetLink.url + "\n" +
					"Full: " + _fullLink.url + "\n";
					
		}
 
 
 
	}
}