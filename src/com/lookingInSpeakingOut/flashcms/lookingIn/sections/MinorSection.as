package com.lookingInSpeakingOut.flashcms.lookingIn.sections {
	
	import com.ghostmonk.events.MeasureEvent;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.ContentDefinition;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.minor.ContactContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.minor.HTMLContent;
	import com.lookingInSpeakingOut.flashcms.ui.ScrollerAsset;
	
	import flash.display.MovieClip;
	import flash.events.Event; 

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class MinorSection extends BaseSection {
		
		
		private var _label:String;
		
		
		public function MinorSection( data:SectionData, closeBtn:MovieClip, scrollerAsset:ScrollerAsset ) {
			
			_label = data.label;
			super( data, closeBtn, scrollerAsset );
			
			content = createContent( ContentDefinition.getDefinition( data.factoryId ), data.contentWidth, data.contentHeight  );
			content.addEventListener( MeasureEvent.HEIGHT_READY, onContentReady );
			content.load( data.request );
			addChild( content );
			
		}
		
		
		
		override public function createContent( type:ContentDefinition, width:int, height:int ) : BaseContent {
			
			var output:BaseContent;
			
			switch( type ) {
				
				case ContentDefinition.CONTACT :
					output = new ContactContent( width, height );
					break;
				
				case ContentDefinition.HTML :
					output = new HTMLContent( width, height );
					break;
					
			}
			
			return output;
			
		}
		
		
		
		/**
		 * 
		 * @param event
		 * 
		 */
		override protected function doLayout( event:Event = null ):void {
			
			title.y = 15;
			title.x = 15;
			
			closeBtn.view.x = configWidth - 5;
			closeBtn.view.y = 5;
			closeBtn.view.alpha = 1;
			
			content.x = 15;
			content.y = 55;
			
			var scrollerX:Number = closeBtn.view.x - scroller.view.width - 10;
			var scrollerY:Number = content.y;
			
			displayTweens.createBuildIn( _label.toString(), scrollerX, scrollerY, 0.3 );
			displayTweens.createBuildOut( this, configWidth * 0.5, configHeight * 0.5, 0.2 );
			
			super.doLayout( event );
			
		}
		
		
		
	}
}