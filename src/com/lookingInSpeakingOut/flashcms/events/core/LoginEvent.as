package com.lookingInSpeakingOut.flashcms.events.core {
	
	import com.lookingspeakingout.flashcms.model.datastructures.LoginData;
	
	import flash.events.Event;
	
	import org.puremvc.as3.multicore.interfaces.INotification;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class LoginEvent extends Event {
		
		
		
		public static const LOGIN:String = "login";
		
		
		
		private var _loginData:LoginData;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get loginData():LoginData {
			
			return _loginData;
			
		}
		
		
		
		/**
		 * 
		 * @param type
		 * @param loginData
		 * @param bubbles
		 * @param cancelable
		 * 
		 */
		public function LoginEvent( type:String, loginData:LoginData, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			super( type, bubbles, cancelable );
			_loginData = loginData;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		override public function clone():Event {
			
			return new LoginEvent( type, loginData );
			
		}
		
		
		
	}
}