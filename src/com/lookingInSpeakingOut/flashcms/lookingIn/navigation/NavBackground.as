package com.lookingInSpeakingOut.flashcms.lookingIn.navigation {
	
	import caurina.transitions.Tweener;
	
	import flash.display.Shape;
	import flash.geom.Rectangle;

	/**
	 * Navigation Background display, with function to open and close
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class NavBackground extends Shape {
		
		
		
		private var _width:Number;
		private var _restHeight:Number;
		private var _openHeight:Number;
		private var _scaleY:Number;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get restHeight():Number {
			
			return _restHeight;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get openHeight():Number {
			
			return _openHeight;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		override public function get width():Number {
			
			return _width;
			
		}
		
		
		
		/**
		 * 
		 * @param config
		 * 
		 */
		public function NavBackground( config:XML ) {
			
			_width = config.width;
			_restHeight = config.height;
			_openHeight = config.openHeight;
			_scaleY = _openHeight / _restHeight;
			
			graphics.beginFill( config.color );
			graphics.drawRoundRect( 0, 0, _width, _restHeight, config.cornerRad, config.cornerRad );
			graphics.endFill();
			scale9Grid = new Rectangle( width / 2, height / 2, 1, 1 );
			
			scaleX = 0;
			scaleY = 0;
			
		}
		
		
		
		/**
		 * 
		 * @param onCompleteCallback
		 * 
		 */
		public function open( onCompleteCallback:Function = null ):void	{
			
			mutate( _scaleY, -_openHeight + _restHeight / 2, onCompleteCallback );
			
		}
		
		
		
		/**
		 * 
		 * @param onCompleteCallback
		 * 
		 */
		public function close( onCompleteCallback:Function = null ):void {
			
			mutate(1,-_restHeight/2, onCompleteCallback);
			
		}
		
		
		
		/**
		 * 
		 * @param onCompleteCallback
		 * 
		 */
		public function buildIn( onCompleteCallback:Function = null ):void {
			
			Tweener.addTween (
				this, {
					scaleX:1, 
					scaleY:1, 
					x:-_width / 2, 
					y:-_restHeight / 2, 
					time:0.3, 
					onComplete:onCompleteCallback
				}
			);
			
		}
		
		
		
		private function mutate( yScale:Number, yPos:Number, callBack:Function = null ):void {
			
			Tweener.removeTweens( this );
			
			Tweener.addTween (
				this, {
					scaleY:yScale, 
					y:yPos, 
					time:0.3, 
					onComplete:callBack
				}
			);
			
		}
		
		
		
	}
}