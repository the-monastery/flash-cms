package com.lookingInSpeakingOut.flashcms.ui {
	
	import assets.miniloader.LoadIndicatorAsset;
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class LoadIndicator extends LoadIndicatorAsset {
		
		
		
		private var _removeCall:Function;
		private var _frameRate:int;
		
		
		
		/**
		 * 
		 * @param onRemoved
		 * 
		 */
		public function LoadIndicator( onRemoved:Function ) {
			
			_removeCall = onRemoved;
			stop();
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function contentLoaded():void {
			
			gotoAndStop( "start" );
			
			Tweener.addTween(
				this, {
					_frame:0, 
					time:currentFrame / _frameRate, 
					transition:Equations.easeNone, 
					onComplete:removeIndicator
				}
			);
			
		}
		
		
		
		private function removeIndicator():void {
			
			if( parent != null ) {
				parent.removeChild( this );
			}
			
			if( _removeCall != null ) {
				_removeCall();
			}
			 
		}
		
		
		
		private function onAddedToStage( e:Event ):void {
			
			play();
			_frameRate = stage.frameRate;
			
		}
		
		
		
	}
}