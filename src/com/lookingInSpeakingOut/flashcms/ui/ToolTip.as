package com.lookingInSpeakingOut.flashcms.ui {
	import caurina.transitions.Tweener;
	
	import flash.display.DisplayObjectContainer;
	import flash.geom.Point;
	import flash.text.TextField;
	
	
	public class ToolTip {
		
		
		
		private var _view:DisplayObjectContainer;
		private var _field:TextField;
		private var _origin:Point;
		
		
		
		public function ToolTip( view:DisplayObjectContainer, textField:TextField ) {
			
			_view = view;
			_field = textField;
			_origin = new Point( _view.x, _view.y );
			_view.cacheAsBitmap = true;
				
		}
	
		
		
		public function get view() : DisplayObjectContainer {
			
			return _view;
			
		}
		
		
		
		public function buildIn() : void {
			
			Tweener.addTween( _view, { scaleX:1, scaleY:1, x:_origin.x, y:_origin.y, time:0.3 } );
			
		}
		
		
		
		public function buildOut( dest:Point ) : void {
			
			Tweener.addTween( _view, { scaleX:0, scaleY:0, x:dest.x, y:dest.y, time:0.3 } );
			
		}
		
		
		
		public function set text( value:String ) : void {
			
			_field.text = value;
			_view.cacheAsBitmap = true;
			
		}
	
		
	
	}
}