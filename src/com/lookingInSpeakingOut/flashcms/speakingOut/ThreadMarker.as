package com.lookingInSpeakingOut.flashcms.speakingOut {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.XMLMessageData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.speakOut.ReplyEvent;
	import com.lookingInSpeakingOut.flashcms.speakingOut.messageDisplay.TrunkMessage;
	
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import speakingout.assets.threadMarker.ThreadMarkerAsset;
	
	[Event ( name="speakoutReply", type="com.lookingInSpeakingOut.flashcms.events.speakOut.ReplyEvent" )]
	
	/**
	 * 
	 * @author ghostmonk 19-09-2009
	 * 
	 */
	public class ThreadMarker extends ThreadMarkerAsset {
		
		
		
		private var _msgView:TrunkMessage;
		private var _hasData:Boolean;
		private var _msgOpenPoint:Point;
		private var _msgClosedPoint:Point;
		
		private var _hasOpened:Boolean;
		
		private var _flowerScale:Number;
		private var _outPos:Point;
		
		
		
		public function ThreadMarker( msgView:TrunkMessage ) {
			
			tabEnabled = false;
			_msgView = msgView;
			_msgView.addEventListener( ReplyEvent.SPEAKOUT_REPLY, onReply );
			_msgView.addEventListener( DisplayEvent.BUILD_IN_COMPLETE, onFlowerActivated );
			_hasData = false;
			//counter.mouseChildren = false;
			//counter.mouseEnabled = false;
			_msgClosedPoint = new Point( width * 0.5, height * 0.5 );
			_msgOpenPoint = new Point( -( _msgView.width * 0.5 ) + 20, -_msgView.height + 20 );
			flower.stop();
			
			_hasOpened = false;
				
		}
		
		
		
		public function get replyId() : int {
			
			return _msgView.replyId;
			
		}
		
		
		
		public function positionScale( x:Number, y:Number, scale:Number ) : void {
			
			this.x = x;
			this.y = y;
			//counter.scaleX = counter.scaleY = 
			flower.scaleX = flower.scaleY = scale;
			flower.x = flower.y = 0;
			//counter.x = flower.width * 0.5 + 5;
			//counter.y = flower.height * 0.5 + 5;
			
			_flowerScale = scale;
			_outPos = new Point( flower.width * 0.5, flower.height );
			
			_msgClosedPoint = new Point( flower.width * 0.5, flower.height * 0.5 );
			_msgView.scaleX = _msgView.scaleY = 1;
			_msgOpenPoint = new Point( -( _msgView.width * 0.5 ) + 15, -_msgView.height + 15 );
			_msgView.scaleX = _msgView.scaleY = 0;
			
			_msgView.x = _msgClosedPoint.x;
			_msgView.y = _msgClosedPoint.y;
			
			flower.scaleX = flower.scaleY = 0;
			flower.x = _outPos.x;
			flower.y = _outPos.y;
			//counter.alpha = 0;	
			disable();
			
		}
		
		
		
		public function get hasData() : Boolean {
			
			return _hasData;
			
		}
		
		
		
		public function clearData() : void {
			
			_hasData = false;
			
		}
		
		
		
		public function enable() : void {
			
			addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			buttonMode = true;
			Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone } );	
			
		}
		
		
		
		public function disable( fade:Boolean = false ) : void {
			
			_msgView.buildOut( _msgClosedPoint );
			removeEventListener( MouseEvent.ROLL_OVER, onRollOver );
			removeEventListener( MouseEvent.ROLL_OUT, onRollOut );
			buttonMode = false;
			
			if( fade ) {
				Tweener.addTween( this, { alpha:0.5, time:0.3, transition:Equations.easeNone } );
			}
			
		}
		
		
		public function buildOut() : void {
			
			var blitBitmap:Bitmap = new Bitmap( new BitmapData( width, height, true, 0x00000000 ) );
			blitBitmap.bitmapData.draw( this );
			blitBitmap.scaleX = blitBitmap.scaleY = scaleX;
			addChild( blitBitmap );
			if( flower.parent == this ) {
				removeChild( flower );
			}
			var rand:Number = Math.random();
			Tweener.addTween( blitBitmap, { scaleX:0, scaleY:0, x:_outPos.x, y:_outPos.y, time:rand, transition:Equations.easeNone } );
			//Tweener.addTween( counter, { alpha:0, time:rand, transition:Equations.easeNone, onComplete:removeView } );
			
		}
		
		
		
		public function buildIn() : void {
			
			resetView();
			var rand:Number = Math.random();
			addChild( flower );
			Tweener.addTween( flower, { scaleX:_flowerScale, scaleY:_flowerScale, x:0, y:0, time:rand, transition:Equations.easeNone } );
			//Tweener.addTween( counter, { alpha:1, time:rand, transition:Equations.easeNone } );
			
		}
		
		
		
		public function setData( data:XMLMessageData ) : void {
			
			_hasData = true;
			//counter.label.text = data.responseCount.toString();	
					
			_msgView.setData( data );
			_msgView.scaleX = _msgView.scaleY = 1; 	
			_msgView.scaleX = _msgView.scaleY = 0;
			
		}
		
		
		
		private function resetView() : void {
			
			flower.scaleX = flower.scaleY = 0;
			flower.x = _msgClosedPoint.x;
			flower.y = _msgClosedPoint.y;
			
		}
		
		
		
		private function removeView() : void {
			
			if( parent ) {
				parent.removeChild( this );
			}
			
		}
		
		
		
		
		private function onRollOver( e:MouseEvent ) : void {
			
			_msgView.buildIn( _msgOpenPoint );
			addChildAt( _msgView, 0 );
			parent.addChild( this );
			openFlower();				
			
		}
		
		
		
		
		private function onRollOut( e:MouseEvent ) : void {
			
			_msgView.buildOut( _msgClosedPoint );
			
			if( !_hasOpened ) {
				closeFlower();			
			}
			
		}
		
		
		private function openFlower() : void {
			
			var time:Number = flower.totalFrames / stage.frameRate;
			
			Tweener.addTween( 
				flower, { 
					_frame:flower.totalFrames, 
					time:time,
					transition:Equations.easeNone 
				} 
			);
			
		}
		
		
		private function closeFlower() : void {
			
			var time:Number = flower.currentFrame / stage.frameRate;
			Tweener.addTween( flower, { _frame:0, time:time, transition:Equations.easeNone } );
			
		}
		
		
		
		private function onReply( e:ReplyEvent ) : void {
			
			dispatchEvent( e );
			
		}
		
		
		
		private function onFlowerActivated( e:DisplayEvent ) : void {
			
			_msgView.removeEventListener( DisplayEvent.BUILD_IN_COMPLETE, onFlowerActivated );
			_hasOpened = true;
			
		}
		
		
		
	}
}