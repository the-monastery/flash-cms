package com.lookingInSpeakingOut.flashcms.lookingIn.clients {
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.sections.MinorSection;
	import com.lookingInSpeakingOut.flashcms.ui.ScrollerAsset;
	
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	
	[Event (name="closeBtnClicked", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	
	
	/**
	 * Manages the creation and display of minor sections
	 * Holds named references to new sections and only creates new sections if they do not already exist 
	 * 
	 * @author ghostmonk 01/01/2009
	 * 
	 */
	public class MinorSectionManager extends Sprite {
		
		
		
		private var _sections:Object;
		private var _currentSection:MinorSection;
		private var _sectionOnStage:Boolean;
		
		
		
		/**
		 * Creates a new generic Object to hold named references to new Minor Sections 
		 * 
		 */
		public function MinorSectionManager() {
			
			_sections = [];
			
		}
		
		
		
		/**
		 * Creates a new section and adds a named reference to object
		 * 
		 * @param data - data struct used to create new Minor Section 
		 * 
		 */
		public function createSection( data:SectionData ):void {
			
			//If named reference does not exist in the sections object, create a new section
			if( _sections[ data.label ] == null ) {
				
				var closeBtn:MinorClosebtn = new MinorClosebtn();
				
				var scrollerAsset:ScrollerAsset = new ScrollerAsset( ScrollerAsset.MINOR_TYPE );
				scrollerAsset.size( data.contentHeight );
				
				var section:MinorSection = new MinorSection( data, closeBtn, scrollerAsset );
				section.addEventListener( SectionEvent.START_SECTION_BUILDOUT, onBuildOut );
				section.addEventListener( DisplayEvent.CLOSE_BTN_CLICKED, onCloseBtnClicked );
				
				_sections[ data.label ] = section;
				
			} 
			
			var requestedSectionOnStage:Boolean = _sectionOnStage && _currentSection == _sections[ data.label ];
			
			if( requestedSectionOnStage == false) {
				
				buildInSection( data.label );
			
			}
			
		}
	
	
		
		/**
		 * remove current section from stage 
		 * 
		 */
		public function clear():void {
			
			//Sections will notify system of build out causing a cascade of reactions
			//Do not call build out unless the current section is actually on the stage
			if(  _sectionOnStage ) {
				
				_currentSection.buildOut();
			
			}
			
		}
		
		
		
		/**
		 * Use this to position the current section on the stage
		 * 
		 * @param stage - reference to stage
		 * 
		 */
		public function position( stage:Stage ):void {
			
			if( _sectionOnStage ) {
				
				_currentSection.x = ( stage.stageWidth - _currentSection.configWidth ) / 2;
				_currentSection.y = ( stage.stageHeight - _currentSection.configHeight ) / 2;
			
			}
			
		}
		
		
		
		private function buildInSection( reference:String ):void {
			
			if( _sectionOnStage ) {
				
				_currentSection.buildOut();
			
			}
			
			_currentSection = _sections[ reference ];
			_currentSection.x = ( stage.stageWidth - _currentSection.configWidth ) / 2;
			_currentSection.y = ( stage.stageHeight - _currentSection.configHeight ) / 2;
			addChild( _currentSection );
			
			_sectionOnStage = true;
			
		}
		
		
		
		private function onBuildOut( e:Event ):void {
			
			_sectionOnStage = false;
			
		}
		
		
		
		private function onCloseBtnClicked( e:Event ):void {
			
			dispatchEvent( e );
			
		}
		
		

	}
}