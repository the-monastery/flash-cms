package com.lookingInSpeakingOut.flashcms.speakingOut.forms {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	import com.lookingInSpeakingOut.flashcms.data.speakingOutData.SubmitMessageData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.speakOut.FormSubmitEvent;
	import com.lookingInSpeakingOut.flashcms.ui.TabFrameLabelButton;
	
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	import speakingout.assets.form.SpeakOutFormAsset;
	
	[Event (name="closeBtnClicked", type="com.lookingInSpeakingOut.flashcms.events.DisplayEvent")]
	[Event (name="submitSpeakoutMessage", type="com.lookingInSpeakingOut.flashcms.events.FormSubmitEvent")]
	
	/**
	 * 
	 * @author ghostmonk 19-09-2009
	 * 
	 */
	public class SpeakOutForm extends SpeakOutFormAsset {
		
		
		
		private const MAX_MSG_LENGTH:int = 350;
		private const MIN_MSG_LENGTH:int = 20;
		
		private var _buildInPoint:Point;
		private var _submit:TabFrameLabelButton;
		private var _closeBtn:FrameLabelButton;
		private var _msg:SpeakOutFormField;
		private var _name:SpeakOutFormField;
		private var _replyId:int;
		
		private var _realWidth:Number;
		private var _realHeight:Number;
		
		
		
		public function SpeakOutForm() {
			
			screen.x = background.width * 0.5;
			screen.y = background.height * 0.5;
			screen.scaleX = screen.scaleY = 0;
			nameInput.text = messageInput.text = "";
			
			_realHeight = height;
			_realWidth = width;
			scaleX = scaleY = alpha = 0;
			
			_submit = new TabFrameLabelButton( submitBtn, onSubmit );		
			
			_closeBtn = new FrameLabelButton( closeBtn );
			closeBtn.tabEnabled = false;
			_closeBtn.addClickCallback( onClose );
			
			characters.text = MAX_MSG_LENGTH.toString();
			messageInput.addEventListener( KeyboardEvent.KEY_DOWN, calculateLength );
			messageInput.addEventListener( KeyboardEvent.KEY_UP, calculateLength );
			
			errorTxt.text = "";
			_name = new SpeakOutFormField( 1, "Anonymous", "", nameInput, nameBG );
			_name.restriction = "a-zA-Z ";
			_msg = new SpeakOutFormField( 2, "", "Please Enter Your Message.", messageInput, messageBG );
			_msg.isRequired = true;
			_msg.textField.text = "";
			
			screen.mouseChildren = false;
			_replyId = 0;
			
		}
		
		
		
		public function set replyId( value:int ) : void {
			
			_replyId = value;
			_replyId = Math.max( 0, value );
			
			if( _replyId > 0 ) {
				replyView.visible = true;
			}
			else {
				replyView.visible = false;
			}
			
		}
		
		
		
		
		public function buildIn( dest:Point ) : void {
			
			calculateLength();
			x = dest.x;
			y = dest.y;
			var xPos:Number = dest.x - ( _realWidth * 0.5 );
			var yPos:Number = dest.y - _realHeight;
			Tweener.addTween( this, { alpha:1, x:xPos, y:yPos, scaleX:1, scaleY:1, time:0.3 } );
			
		}
		
		
		
		public function buildOut( dest:Point ) : void {
			
			Tweener.addTween( this, { alpha:0, x:dest.x, y:dest.y, scaleX:0, scaleY:0, time:0.3, onComplete:removeFromStage } );
			
		}
		
		
		
		private function removeFromStage() : void {
			
			if( parent ) {
				parent.removeChild( this );
			}
			
		}
		
		
		
		private function setTabIndexes() : void {
			
			nameInput.tabIndex = 1;
			messageInput.tabIndex = 2;
			submitBtn.tabIndex = 3;
			
		}
		
		
		
		private function onSubmit( e:MouseEvent ) : void {
			
			errorTxt.text = "";
			var errorStr:String = "";
			
			if( _name.isValid ) {
				if( _name.textField.text.length < 3 ) {
					errorStr = "Name must be at least 3 characters."; 
					Tweener.addTween( errorTxt, { _text:errorStr, time:0.3, transition:Equations.easeNone } );
					return;
				}
			}
			
			if( _msg.isValid ) { 
				if( _msg.textField.text.length < MIN_MSG_LENGTH ) {
					errorStr = "Entries must be at least " + MIN_MSG_LENGTH + " characters."; 
					Tweener.addTween( errorTxt, { _text:errorStr, time:0.3, transition:Equations.easeNone } );
					return;
				}
				submitSuccess();
			}
			else {		
				Tweener.addTween( errorTxt, { _text:_msg.errorText, time:0.3, transition:Equations.easeNone } );
			}
			
		}
		
		
		
		private function submitSuccess() : void {
			
			var data:SubmitMessageData = new SubmitMessageData();	
			data.isTrunk = _replyId == 0;	
			data.text = _msg.textField.text;
			data.name = _name.textField.text;
			data.xPos = 0;
			data.yPos = 0;
			data.parentId = _replyId;
			
			dispatchEvent( new FormSubmitEvent( FormSubmitEvent.SUBMIT_SPEAKOUT_MESSAGE, data ) );
			
			_msg.textField.text = "";
			screen.label.text = "Sorry, this is only a demo. Submissions are now closed.";
			Tweener.addTween( screen, { x:background.x, y:background.y, scaleX:1, scaleY:1, time:0.3 }  );
			
		}
		
		
		
		private function onClose( e:MouseEvent ) : void {
			
			errorTxt.text = "";
			dispatchEvent( new DisplayEvent( DisplayEvent.CLOSE_BTN_CLICKED ) );
			closeScreen();
			
		}
		
		
		
		private function calculateLength( e:Event = null ) : void {
			
			characters.text = ( MAX_MSG_LENGTH - ( messageInput.text.length ) ).toString();
			
		}
		
		
		
		private function closeScreen( e:MouseEvent = null ) : void {
			
			var xPos:int = background.width * 0.5;
			var yPos:int = background.height * 0.5;
			Tweener.addTween( screen, { x:xPos, y:xPos, scaleX:0, scaleY:0, time:0.3 }  );
			
		}
		
		
		
	}
}