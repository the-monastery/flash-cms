package com.lookingInSpeakingOut.flashcms.framework.model {
	
	import com.ghostmonk.net.XMLLoader;
	import com.lookingInSpeakingOut.flashcms.data.ConfigData;
	
	import org.puremvc.as3.multicore.patterns.proxy.Proxy;


	/**
	 * This must load and send a CONFIG_LOADED notification before the application will start.
	 * StageMediator is waiting for this to complete.
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class ConfigProxy extends Proxy {
		
		
		
		public static const NAME:String = "StartupProxy";
		public static const CONFIG_LOADED:String = "configLoaded";
		
		private var _xmlLoader:XMLLoader;
		
		
		
		
		/**
		 * 
		 * @param url
		 * 
		 */
		public function ConfigProxy( xmlData:XML ) {
			
			super( NAME );
			data = new ConfigData( xmlData );	
			
		}
		
		
		
		private function get configData():ConfigData {
			
			return data as ConfigData;
			
		}
		
		
		
		public function get majorSectionData() : XML {
			
			return configData.majorSections;
			
		}
		
		
		
		public function get minorSectionData() : XML {
			
			return configData.minorSections;
			
		}
		
		
		
		public function get arrowColor() : uint {
			
			return configData.navContainer.arrow.@color;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get background():XML {
			 
			return configData.background;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get navContainer():XML {
			 
			return configData.navContainer;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get majorSections():XML {
			
			return configData.majorSections;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get minorSections():XML {
			 
			return configData.minorSections;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get video():XML {
			 
			return configData.video;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get policyFiles():XML {
			
			return configData.policyFiles;
				
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get security():XML {
			
			return configData.security;
				
		}
		
		
		
		public function get helpData() : XML {
			
			return configData.speakingOut.help[ 0 ];
			
		}
		
		
		
		public function get messageSubmitEndpoint() : String {
			
			return configData.speakingOut.api[ 0 ].submit.@url;
			
		}
		
		
		
		public function get trunkEndpoint() : String {
			
			return configData.speakingOut.api[ 0 ].trunk.@url;
			
		}
		
		
		
		public function get leafEndpoint() : String {
			
			return configData.speakingOut.api[ 0 ].leaves.@url;
			
		}
		
		
		
	}
}