package com.lookingInSpeakingOut.flashcms.lookingIn.clients {
	
	import assets.module.MajorCloseBtn;
	
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.media.video.events.VideoLoadEvent;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickrGalleryEvent;
	import com.lookingInSpeakingOut.flashcms.globals.ErrorLogger;
	import com.lookingInSpeakingOut.flashcms.lookingIn.sections.MajorSection;
	import com.lookingInSpeakingOut.flashcms.ui.ScrollerAsset;
	
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Point;
	
	[Event (name="loadVideo", type="com.ghostmonk.events.video.VideoLoadEvent")]
	[Event (name="photoGalleryClick", type="com.lookingspeakingout.flashcms.events.PhotoGalleryEvent")]
	[Event (name="photoGalleryData", type="com.lookingspeakingout.flashcms.events.PhotoGalleryEvent")]
	[Event (name="showSections", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	[Event (name="hideSections", type="com.lookingspeakingout.flashcms.events.SectionEvent")]

	/**
	 * Manages the creating and display of modules
	 * 
	 * @author ghostmonk 02/01/2009
	 * 
	 */
	public class MajorSectionManager extends Sprite {
		
		
		
		private const PADDING:Number = 20;
		
		private var _sectionsOnStage:Array;
		
		private var _managerWidth:Number;
		private var _mangerHeight:Number;
		
		private var _sections:Object;
		private var _defaultSections:Array;
		private var _isHidden:Boolean;
		
		
		
		public function MajorSectionManager( unitWidth:Number, unitHeight:Number ) {
			
			_sectionsOnStage = new Array();
			_defaultSections = new Array();
			
			_managerWidth = unitWidth * 2 + PADDING;
			_mangerHeight = unitHeight;
			
			_sections = [];
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			_isHidden = false;
			
		}
		
		
		
		public function get mangerWidth():Number {
			 
			return _managerWidth;
			 
		}
		
		
		
		public function addSection( sectionInfo:SectionData, titleYPos:Number, isDefault:Boolean ):void {
			
			if( isDefault ) {
				_defaultSections.push( sectionInfo.label );
			}
			
			if( _isHidden ) {
				revealSections();
			}
			
			if( _sections[ sectionInfo.label ] == null ) {
				createNewSection( sectionInfo );
			}
			
			var isOffStage:Boolean = _sectionsOnStage.indexOf( _sections[ sectionInfo.label ] ) == -1; 
			
			if( isOffStage ) { 
				_sectionsOnStage.push( _sections[ sectionInfo.label ] );
				positionSections( titleYPos );
			}
			
		}
		
		
		
		public function getSection( id:String ):MajorSection {
			
			for each( var item:MajorSection in _sectionsOnStage ) {
				if( item.id == id ) {
					return item;
				}
			}
			
			return null;
			
		}
		
		
		
		public function get sectionOnStageIds() : Array {
			
			var output:Array = [];
			
			for each( var section:MajorSection in _sectionsOnStage ) {
				output.push( section.id );
			}
			
			return output;
			
		}
		
		
		
		public function setPosition():void {
			
			x = ( stage.stageWidth - _managerWidth ) / 2;
			y = ( stage.stageHeight - _mangerHeight ) / 2 + 20;		
				
		}
		
		
		
		public function hideSections():void {
			
			_isHidden = true;
			
			for each( var section:MajorSection in _sectionsOnStage ) {
				section.hide();	
			}
			
		}
		
		
		
		public function revealSections():void {
			
			if( _sectionsOnStage.length == 0 && _defaultSections.length > 1 ) {
				_sectionsOnStage.push( _sections[ _defaultSections[ 0 ] ] );
				_sectionsOnStage.push( _sections[ _defaultSections[ 1 ] ] );
				transitionSections( _sectionsOnStage[ 0 ], _sectionsOnStage[ 1 ] ); 
			}
			
			_isHidden = false;
			
			for each( var section:MajorSection in _sectionsOnStage ) {
				addChild(section);
				section.show();
			}
			
			dispatchEvent( new SectionEvent( SectionEvent.SHOW_SECTIONS ) );
			
		}
		
		
		
		private function createNewSection( sectionData:SectionData ) : void {
			
			var scrollerAsset:ScrollerAsset = new ScrollerAsset( ScrollerAsset.MAJOR_TYPE );
			var newSection:MajorSection = new MajorSection( sectionData, new MajorCloseBtn(), scrollerAsset );
			newSection.addEventListener( VideoLoadEvent.LOAD_VIDEO, onLoadVideo );
			newSection.addEventListener( SectionEvent.START_SECTION_BUILDOUT, onStartSectionBuildout );
			newSection.addEventListener( FlickrGalleryEvent.PHOTO_GALLERY_CLICK, onPhotoGallery );
			newSection.addEventListener( FlickrGalleryEvent.PHOTO_GALLERY_DATA, onPhotoGallery );
			newSection.addEventListener( DisplayEvent.CLOSE_BTN_CLICKED, onClosedBtnClicked );	
			_sections[ sectionData.label ] = newSection;
			
		} 
		
		
		
		private function positionSections( yPos:Number ):void {
				
			switch( _sectionsOnStage.length ) {
				
				case 0: 	
					ErrorLogger.error( "MajorSectionManager::positionSections: Should never be zero sections on the stage" ); 	
					break;					
				case 1: 	
					transitionSections( _sectionsOnStage[ 0 ], null ); 						
					break;
				case 3: 	
					removeModule( _sectionsOnStage[ 0 ] );
				case 2: 	
					transitionSections( _sectionsOnStage[ 0 ], _sectionsOnStage[ 1 ] ); 		
					break;
				default: 	
					ErrorLogger.error(	"MajorSectionManager::positionSections: too Many sections on stage" ); 					
					break;
					
			}
			
			var mod:MajorSection = _sectionsOnStage[ _sectionsOnStage.length - 1 ];
			mod.abortTween = true;
			addChild( mod );
		}
		
		
		
		private function transitionSections( leftSection:MajorSection, rightSection:MajorSection = null ):void {
			
			Tweener.addTween( leftSection, { x:0, time:0.4 } );
			leftSection.positionTitleX( 0 );
			
			if( rightSection != null ) {
				rightSection.x = ( _managerWidth - PADDING ) / 2 + PADDING;
				rightSection.positionTitleRight();
			}
			
		}
		
		
		
		private function onStartSectionBuildout( e:SectionEvent ):void {
			
			_sectionsOnStage.splice( _sectionsOnStage.indexOf( e.target ), 1 );
			
			if( _sectionsOnStage.length == 0 ) {
				dispatchEvent( new SectionEvent( SectionEvent.SECTIONS_REMOVED ) );
			}
			
		}
		
		
		
		private function removeModule( module:MajorSection ):void {
			
			module.buildOut();
			
		}
		
		
		
		private function onLoadVideo( e:VideoLoadEvent ):void {
			
			dispatchEvent( e );
			
		}
		
		
		
		private function onPhotoGallery( e:FlickrGalleryEvent ):void {
			
			dispatchEvent( e );
			
		}
		
		
		
		private function onAddedToStage( e:Event ):void {
			
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			setPosition();
			
		}
		
		
		private function onClosedBtnClicked( e:SectionEvent ) : void {
				
			dispatchEvent( new DisplayEvent( DisplayEvent.CLOSE_BTN_CLICKED, e.target.id ) );
			
		}
		
		
		
	}
}