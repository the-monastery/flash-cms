package com.lookingInSpeakingOut.flashcms.framework.controller {
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.framework.model.URLRequestProxy;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class SectionDataCommand extends SimpleCommand {
		
		
				
		public static const GET_DATA:String = "getData";
		
		
		
		override public function execute( note:INotification ):void {
			
			var data:SectionData = note.getBody() as SectionData;
			var requestProxy:URLRequestProxy = facade.retrieveProxy( URLRequestProxy.NAME ) as URLRequestProxy;
			
			requestProxy.sendRequest( data );
			
		}
		
		
		
	}
}