package com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.media.video.events.VideoLoadEvent;
	import com.ghostmonk.net.AssetLoader;
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	import com.lookingInSpeakingOut.flashcms.cmsUtils.CleanHTML;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.RSSData;
	
	import flash.display.Bitmap;
	import flash.errors.IOError;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.TextFieldAutoSize;
	
	import items.videos.VideoItemAsset;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class VideoItem extends VideoItemAsset {
		
		
		private var _yPos:Number;
		private var _videoLink:URLRequest;
		private var _playBtnThumb:FrameLabelButton;
		
		
		/**
		 * 
		 * @param data
		 * 
		 */
		public function VideoItem( data:RSSData ) {
			
			var imageLoader:AssetLoader = new AssetLoader( data.photoRequest.url, onImageLoaded, onImageError );
			
			var title:String = "<b>"+data.title+"</b>";
			var summary:String = CleanHTML.removePTagsAndWhiteSpace( data.shortDescription );
			//var postInfo:String = "\nPosted By : "+data.author+"\n"+data.date;
			var postInfo:String = "\nPosted On : "+data.date;
			
			description.htmlText = title+"\n\n"+summary+"\n"+postInfo
			description.mouseWheelEnabled = false;
			
			description.autoSize = TextFieldAutoSize.RIGHT;
			background.height = Math.max( background.height, description.height + 30 );
			
			_videoLink = data.assetLink;
			_playBtnThumb = new FrameLabelButton( playBtnThumb ); 
			_playBtnThumb.view.mouseEnabled = false;
			_playBtnThumb.view.mouseChildren = false;
			screen.mouseEnabled = false;
			enable();
			
		}
		
		
		
		/**
		 * 
		 * @param value
		 * 
		 */
		public function set yPos( value:Number ):void {
			 
			y = _yPos = value;
			 
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function buildIn( e:Event = null ):void {
			
			Tweener.addTween( this, { x:0, y:_yPos, time:0.4 } );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function buildOut():void {
			
			Tweener.addTween(
				this, {
					x:-width-20, 
					time:0.2, 
					onComplete:parent.removeChild, 
					onCompleteParams:[ this ]
				}
			);
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function enable():void {
			
			maskHitarea.buttonMode = true;
			maskHitarea.addEventListener( MouseEvent.ROLL_OVER, onMouseOver );
			maskHitarea.addEventListener( MouseEvent.CLICK, onClick );
			maskHitarea.addEventListener( MouseEvent.ROLL_OUT, onMouseOut );
			_playBtnThumb.enable();
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		public function disable():void {
			
			maskHitarea.buttonMode = false;
			maskHitarea.removeEventListener( MouseEvent.ROLL_OVER, onMouseOver );
			maskHitarea.removeEventListener( MouseEvent.CLICK, onClick );
			maskHitarea.removeEventListener( MouseEvent.ROLL_OUT, onMouseOut ); 
			_playBtnThumb.disable();
			
		}
		
		
		
		private function onMouseOver( e:MouseEvent ):void {
			
			Tweener.addTween(
				screen, {
					alpha:0, 
					time:0.3, 
					transition:Equations.easeNone
				}
			);
			_playBtnThumb.onRollover( e );
			
		}
		
		
		
		private function onMouseOut( e:MouseEvent ):void {
			
			Tweener.addTween(
				screen, {
					alpha:1, 
					time:0.3, 
					transition:Equations.easeNone
				}
			);
			_playBtnThumb.onRollout( e );
			
		}
		
		
		
		private function onClick( e:MouseEvent ):void {
			
			dispatchEvent( new VideoLoadEvent( VideoLoadEvent.LOAD_VIDEO, _videoLink.url ) );
			
		}
		
		
		
		private function onImageLoaded( bitmap:Bitmap ):void {
			
			bitmap.alpha = 0;
			holder.addChild( bitmap );
			Tweener.addTween( bitmap, { alpha:1, time:0.2, transition:Equations.easeNone } );
			
		}
		
		
		
		private function onImageError( e:IOErrorEvent ):void {
			
			trace( e.text );
			
		}
		
		
		
	}
}