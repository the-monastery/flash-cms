package com.lookingInSpeakingOut.flashcms.data.sectionData.parse {
	
	import flash.net.URLRequest;
	
	/**
	 * Data Struct that parses XML and holds data for Resources section.
	 * 
	 * <p>All member data is exposed with read-only accessors</p>
	 *  
	 * @author ghostmonk
	 * 
	 */
	public class ResourceData {
		
		
		
		private var _title:String;
		private var _request:URLRequest;
		private var _description:String;
		private var _author:String;
		private var _date:String;
		
		
		
		/**
		 * Title of the resource item 
		 * @return 
		 * 
		 */
		public function get title():String {
			
			return _title;
				
		}
		
		
		
		/**
		 * A URLRequest pointing to the complete resource item 
		 * @return 
		 * 
		 */
		public function get request():URLRequest {
			
			return _request;
			
		}
		
		
		
		/**
		 * A short description used in the unopened version of the resource item 
		 * @return 
		 * 
		 */
		public function get description():String {
		
			return _description;
		
		}
		
		
		
		/**
		 * The full name of the person who posted the data 
		 * @return 
		 * 
		 */
		public function get author():String {
		
			return _author;
		
		}
		
		
		
		/**
		 * Publish date of the post in Day of Week - Day of Month, Month, Year 
		 * @return 
		 * 
		 */
		public function get date():String {
		
			return _date;
		
		}
		
		
		
		/**
		 * XML is expected to be in the following format.
		 * 
		 * <pre>
		 * &lt;item&gt;
		 *		&lt;category id="essays"&gt;
		 *			&lt;link title="Essay on Abstract Art" url="http://www.lookinginspeakingout.com/index.php?id=98" date="Sat, 21-03-2009"&gt;
		 *				&lt;author&gt;Alison Davis&lt;/author&gt;
		 *				&lt;description&gt;check out this amazing essay&lt;/description&gt;
		 *			&lt;/link&gt;
		 *		&lt;/category&gt;
		 *	&lt;/item&gt;</pre>
 		 *
		 *  
		 * @param xml
		 * 
		 */
		public function ResourceData( xml:XML ) {
			
			_title = xml.@title;
			_request = new URLRequest( xml.@url.toString() );
			_description = xml.description;
			_author = xml.author;
			_date = xml.@date;
			
		}

		/**
		 * Use this in a trace statement to verify that all the values have been inserted correctly
		 * @return 
		 * 
		 */
		public function toString():String {
			
			return 	"Title: " + _title + "\n" +
					"Request: " + _request.url + "\n" +
					"Description: " + _description + "\n"+
					"Author: " + _author + "\n"+
					"Date: " + _date + "\n";
					
		}

	}
}