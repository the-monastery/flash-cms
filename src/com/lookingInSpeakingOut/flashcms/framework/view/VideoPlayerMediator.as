package com.lookingInSpeakingOut.flashcms.framework.view {
	
	import com.ghostmonk.media.video.events.VideoLoadEvent;
	import com.lookingInSpeakingOut.flashcms.events.LISOEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	import com.lookingInSpeakingOut.flashcms.framework.FrameworkEvents;
	import com.lookingInSpeakingOut.flashcms.video.VideoPlayer;
	
	import flash.display.StageDisplayState;
	import flash.events.Event;
	import flash.events.FullScreenEvent;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.mediator.Mediator;

	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class VideoPlayerMediator extends Mediator {
		
		
		private var stageMediator:StageMediator;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get player():VideoPlayer {
			
			return viewComponent as VideoPlayer;
			
		}
		
		
		
		/**
		 * 
		 * @param viewComponent
		 * 
		 */
		public function VideoPlayerMediator( viewComponent:VideoPlayer ) {
			
			super( NAME, viewComponent );
			
			player.addEventListener( Event.RESIZE, position );
			player.addEventListener( FullScreenEvent.FULL_SCREEN, onFullScreen );
			player.addEventListener( VideoPlayer.REMOVE_VIDEO_PLAYER, onRemoveVideoPlayer );
			
		}
		
		
		
		/**
		 * 
		 * 
		 */
		override public function onRegister():void {
			
		 	stageMediator = facade.retrieveMediator( StageMediator.NAME ) as StageMediator;
		 		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		override public function listNotificationInterests():Array {
			
			return [	
				VideoLoadEvent.LOAD_VIDEO,
				FrameworkEvents.STAGE_RESIZE,
				StageDisplayState.NORMAL,
				NavigationEvent.MAJOR_SECTION,
				NavigationEvent.MINOR_SECTION,
				LISOEvent.SPEAKING_OUT_APP
			];
			
		}
		
		
		
		/**
		 * 
		 * @param note
		 * 
		 */
		override public function handleNotification( note:INotification ):void {
			
			switch( note.getName() ) {
				
				case VideoLoadEvent.LOAD_VIDEO: 	
					loadVideo(note); 			
					break;
					
				case FrameworkEvents.STAGE_RESIZE: 	
					position();					
					break;
					
				case StageDisplayState.NORMAL:		
					onNormalState();			
					break;
					
				case LISOEvent.SPEAKING_OUT_APP:
				case NavigationEvent.MAJOR_SECTION:
				case NavigationEvent.MINOR_SECTION: 
					removePlayerNoDispatch(); 	
					break;
					
			}
			
		}
		
		
		
		private function loadVideo( note:INotification ):void {
			
			position();
			stageMediator.addChildUnderNavBar( player );
			player.load( note.getBody() as String );
			
		}
		
		
		
		private function position( e:Event = null ):void {
			
			player.x = ( stageMediator.stage.stageWidth - player.width ) * 0.5;
			player.y = ( stageMediator.stage.stageHeight - player.height ) * 0.5 - 10;
			
		}
		
		
		
		private function onFullScreen( e:FullScreenEvent ):void {
			
			sendNotification( FullScreenEvent.FULL_SCREEN, player );
			
		}
		
		
		
		private function onNormalState():void {
			
			player.normalScreenMode();
			
		}
		
		
		
		private function onRemoveVideoPlayer( e:Event ):void {
			
			sendNotification( FrameworkEvents.SHOW_CURRENT_SECTIONS );
			
		}
		
		
		
		private function removePlayerNoDispatch():void {
			
			if( player.stage ) {
				
				player.buildOut( null, false );
				
			}	
			
		}
		
		
		
	}
}