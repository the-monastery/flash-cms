package com.lookingInSpeakingOut.flashcms.framework.controller {
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.core.NavigationEvent;
	
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import org.puremvc.as3.multicore.interfaces.INotification;
	import org.puremvc.as3.multicore.patterns.command.SimpleCommand;

	public class LookingInCommand extends SimpleCommand {
		
		
		
		override public function execute( note:INotification ):void {
			
			sendNotification( NavigationEvent.STARTING_SECTION, new SectionData( note.getBody().section1 )  );
			sendNotification( NavigationEvent.STARTING_SECTION, new SectionData( note.getBody().section2 ) );
			
		}
		
		
	}
}