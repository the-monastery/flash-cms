package com.lookingInSpeakingOut.flashcms.lookingIn.content {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.events.MeasureEvent;
	import com.ghostmonk.net.SimpleURLLoader;
	import com.lookingInSpeakingOut.flashcms.events.core.PaginationEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.navigation.PaginationNavigator;
	
	import flash.display.Sprite;
	import flash.events.IOErrorEvent;
	import flash.geom.Rectangle;
	import flash.net.URLRequest;
	
	[Event (name="pageNavigation", type="com.lookingInSpeakingOut.flashcms.events.PaginationEvent")]
	[Event (name="animationUpdate", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	[Event (name="itemClicked", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	[Event (name="heightReady", type="com.ghostmonk.events.MeasureEvent")]
	
	public class BaseContent extends Sprite {
		
		
		
		private var _dataLoader:SimpleURLLoader;
		private var _scrollRect:Rectangle;
		
		private var _paginationNav:PaginationNavigator;
		
		
		
		public function BaseContent( sectionW:Number, sectionH:Number ) {
			
			_scrollRect = new Rectangle( 0, 0, sectionW, sectionH );
			scrollRect = _scrollRect;
			alpha = 0;
			
		}
		
		
		
		public function get fullHeight():Number {
			
			return transform.pixelBounds.height;
			
		}
		
		
		
		public function load( url:URLRequest ) :void {
			
			_dataLoader = new SimpleURLLoader( url, onDataLoaded, onError );
			
		}
		
		
		
		public function buildIn():void {
			
			Tweener.addTween( this, { alpha:1, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function buildOut():void {
			
			Tweener.addTween( this, { alpha:0, time:0.3, transition:Equations.easeNone } );
			
		}
		
		
		
		public function listView():void {
			
		}
		
		
		
		public function removeItems() : void {
			
		}
		
		
		
		public function scroll( percent:Number ):void {
		
			var limit:Number = fullHeight - height;
			_scrollRect.y = limit * percent;
			scrollRect = _scrollRect;
		
		}
		
		
		
		protected function hasAggregatePages( numberOfPages:int, yPos:int ) : void {
			
			if( numberOfPages > 1 ) {
				
				if( _paginationNav == null ) {
					_paginationNav = new PaginationNavigator( numberOfPages );
					_paginationNav.addEventListener( PaginationEvent.PAGE_NAVIGATION, onPaginationNav );
				}
				_paginationNav.y = yPos;
				showPageNav();
				
			}
			
		}
		
		
		
		protected function removePageNav() : void {
			
			if( _paginationNav && _paginationNav.stage ) {
				removeChild( _paginationNav );
			}
			
		}
		
		
		
		protected function showPageNav() : void {
			
			if( _paginationNav ) {
				addChild( _paginationNav );
			}
			
		}
		
		
		
		protected function aggregateItemClicked():void {
			
			removePageNav();
			dispatchEvent( new SectionEvent( SectionEvent.ITEM_CLICKED ) );
			
		}
		
		
		
		protected function displayUpdate():void {
			
			dispatchEvent( new SectionEvent( SectionEvent.SECTION_VIEW_UPDATE ) );
			
		}
				
		
		
		protected function onPaginationNav( e:PaginationEvent ) : void {
			
			removePageNav();
			dispatchEvent( e );
			
		}
		
		
		
		protected function onDataLoaded( data:String ) : void {
			
			dispatchEvent( new MeasureEvent( MeasureEvent.HEIGHT_READY, fullHeight ) );
						
		}
		
		
		
		protected function onError( e:IOErrorEvent ) : void {
			
			trace( e );
			
		}
		
		
		
	}
}