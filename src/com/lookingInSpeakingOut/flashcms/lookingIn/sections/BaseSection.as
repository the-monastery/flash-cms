package com.lookingInSpeakingOut.flashcms.lookingIn.sections {
	
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.display.graphics.shapes.RoundedRectangle;
	import com.ghostmonk.events.MeasureEvent;
	import com.ghostmonk.events.PercentageEvent;
	import com.ghostmonk.text.EmbeddedText;
	import com.ghostmonk.ui.graveyard.Scroller;
	import com.ghostmonk.ui.graveyard.buttons.FrameLabelButton;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.SectionData;
	import com.lookingInSpeakingOut.flashcms.events.DisplayEvent;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.globals.Fonts;
	import com.lookingInSpeakingOut.flashcms.lookingIn.DisplayTweens;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.ContentDefinition;
	import com.lookingInSpeakingOut.flashcms.ui.LoadIndicator;
	import com.lookingInSpeakingOut.flashcms.ui.ScrollerAsset;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	[Event (name="startSectionBuildout", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	[Event (name="closeBtnClicked", type="com.lookingspeakingout.flashcms.events.SectionEvent")]
	
	
	/**
	 * <p>Holds basic functionality of sections for project</p>
	 * <p>Meant to be extended not instantiated</p>
	 *  
	 * @author ghostmonk 06/03/2009
	 * 
	 */
	public class BaseSection extends Sprite {
		
		
		
		private var _id:String;
		private var _configWidth:Number;
		private var _configHeight:Number;
		private var _abortTween:Boolean;
		
		private var _content:BaseContent;
		
		private var _scroller:Scroller;
		private var _displayTweens:DisplayTweens;
		private var _closeBtn:FrameLabelButton;
		private var _title:EmbeddedText;
		private var _bg:RoundedRectangle;
		
		private var _isContentInitialized:Boolean;
		private var _loadIndicator:LoadIndicator;
		
		
		
		/**
		 * 
		 * @param data
		 * @param closeBtn
		 * @param scrollerAsset
		 * 
		 */
		public function BaseSection( data:SectionData, closeBtn:MovieClip, scrollerAsset:ScrollerAsset ) {
			
			_id = data.id;
			
			_configWidth = data.width;
			_configHeight = data.height;
			
			_scroller = new Scroller( scrollerAsset );
			_scroller.view.scaleY = 0;
			_scroller.addEventListener( PercentageEvent.CHANGE, onScroll );
			_scroller.disable( 0 );
			
			_bg = new RoundedRectangle( _configWidth, _configHeight, data.cornerRad, data.color, data.alpha );
			_bg.scaleX = _bg.scaleY = 0;	
			
			_title = new EmbeddedText( Fonts.BIG_ED.fontName, data.titleSize, data.titleColor, true );
			
			_closeBtn = new FrameLabelButton( closeBtn );
			closeBtn.tabEnabled = false;
			_closeBtn.addClickCallback( onCloseBtnClick );
			
			_displayTweens = new DisplayTweens();
			
			addEventListener( Event.ADDED_TO_STAGE, doLayout );
			
			_isContentInitialized = false;	
			_abortTween = false;
			
			_loadIndicator = new LoadIndicator( buildContent );
			_loadIndicator.x = _configWidth * 0.5;
			_loadIndicator.y = _configHeight * 0.5;
			
		}
		
		
		
		public function get abortTween():Boolean {
			 
			return _abortTween;
			 
		}
		
		public function set abortTween(value:Boolean):void { 
			
			_abortTween = value; 
			
		}
		
		
		
		
		public function get id():String { 
			
			return _id; 
			
		}
		
		
		
		public function get configWidth():Number { 
			
			return _configWidth; 
			
		}
		


		public function get configHeight():Number { 
			
			return _configHeight; 
			
		}
		
		
		
		public function buildIn():void {
			
			tabChildren = true;
			show();
			
		}
		
		

		public function show():void {
			
			if( _isContentInitialized ) {
				_content.buildIn();
			}
			Tweener.addTween( _scroller.view, _displayTweens.scrollerOpen );
			Tweener.addTween( _bg, _displayTweens.bgOpen );
			Tweener.addTween( _title, _displayTweens.titleOpen ); 
			
		}
		
		
		
		public function buildOut():void {
			
			tabChildren = false;
			hide();
			dispatchEvent( new SectionEvent( SectionEvent.START_SECTION_BUILDOUT ) );
			
		}
		
		
		
		public function hide():void {
			
			abortTween = false;
			_content.buildOut();
			Tweener.addTween( _scroller.view, _displayTweens.scrollerClose );
			Tweener.addTween( _bg, _displayTweens.bgClose );
			Tweener.addTween( _title, _displayTweens.titleClose ); 
			
		}
		
		
		
		public function enableScrollWheel():void {
			
			addEventListener( MouseEvent.MOUSE_WHEEL, onMouseWheel );
			
		}
		
		

		public function disableScrollWheel():void {
			
			removeEventListener( MouseEvent.MOUSE_WHEEL, onMouseWheel );
			
		}
		
		
		
		/**
		 * Must override this function  
		 * @return 
		 * 
		 */
		public function createContent( type:ContentDefinition, width:int, height:int ) : BaseContent {
			
			_content.addEventListener( MeasureEvent.HEIGHT_READY, onContentReady );
			return null;
			
		}
		


		public function resetScroll() : void {
			
			_content.scroll( 0 );
			
		}
		
		
		
		protected function get title():EmbeddedText { 
			
			return _title;
			
		}
		
		

		protected function get scroller():Scroller { 
			
			return _scroller; 
					
		} 
		
	
		
		protected function get content():BaseContent { 
			
			return _content; 
			
		}
		
		protected function set content( value:BaseContent ) : void {
			
			_content = value;
			
		}
		
		
		
		protected function get closeBtn():FrameLabelButton { 
			
			return _closeBtn; 
			
		}
		
		

		protected function get displayTweens():DisplayTweens { 
			
			return _displayTweens; 
			
		}
		 
		
		
		protected function get bg():RoundedRectangle { 
			
			return _bg; 
			
		}
		
		
		
		protected function doLayout( event:Event = null ):void {
				
			_bg.x = _configWidth * 0.5;
			_bg.y = _configHeight * 0.5;
						
			addChild( _bg );
			addChild( _scroller.view );
			
			if( !_isContentInitialized ) {
				addChild( _loadIndicator );
			}
			
			_bg.addChild( _closeBtn.view );
			addChild( _content );
			addChild( _title );
			
			buildIn();
		}
		
		
		
		protected function showLoadIndicator() : void {
			
			addChild( _loadIndicator );
			
		}
		
		
		
		protected function hideLoadIndicator() : void {
			
			_loadIndicator.contentLoaded();
			
		}
		
		
		
		protected function get loadIndicator() : LoadIndicator {
			
			return _loadIndicator;
			
		}
		
		
		protected function onContentReady( e:MeasureEvent ):void {
			
			resetScroll();
			var contentHeight:Number = e.value;
			_isContentInitialized = true;
			
			_scroller.setUp( contentHeight, _content.height );
			contentHeight <= _content.height ? disableScrollWheel() : enableScrollWheel();
			
			_loadIndicator.contentLoaded();
			
		}
		
		
		
		private function buildContent():void {
			
			_content.buildIn();
			
		}
		
		

		private function onCloseBtnClick( e:MouseEvent ):void {
			
			buildOut();
			dispatchEvent( new SectionEvent( DisplayEvent.CLOSE_BTN_CLICKED ) );
			
		}
		
		
		
		private function onScroll( e:PercentageEvent ):void {	
			
			_content.scroll( e.percent );
			
		}
		
		
		
		private function onMouseWheel( e:MouseEvent ):void {
			
			_scroller.scroll( e.delta * -2 );
			
		}
		
		
		
	}
}