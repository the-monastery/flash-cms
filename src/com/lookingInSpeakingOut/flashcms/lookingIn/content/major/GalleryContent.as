package com.lookingInSpeakingOut.flashcms.lookingIn.content.major {
	
	
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.net.SimpleURLLoader;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickerSetEvent;
	import com.lookingInSpeakingOut.flashcms.events.flickr.FlickrGalleryEvent;
	import com.lookingInSpeakingOut.flashcms.flickrViewer.FlickrThumb;
	import com.lookingInSpeakingOut.flashcms.flickrViewer.FlickrThumbCollection;
	import com.lookingInSpeakingOut.flashcms.lookingIn.content.BaseContent;
	
	import flash.net.URLRequest;
	
	import items.SimpleThumbAsset;
	import items.photogallery.FlickerSetThumbAsset;
	
	[Event (name="photoGalleryClick", type="com.lookingspeakingout.flashcms.events.FlickrGalleryEvent")]
	[Event (name="photoGalleryData", type="com.lookingspeakingout.flashcms.events.FlickrGalleryEvent")]
	
	public class GalleryContent extends BaseContent {
		
		
		private const FLICKR_API_KEY:String = "bf36794dc3904ca5872ce41e7e6a33e0";
		
		private var _flickrSetThumbCollection:FlickrThumbCollection;
		private var _currentPhotoThumbsCollection:FlickrThumbCollection;
		private var _currentDisplayCollection:FlickrThumbCollection;
		
		private var _galleryEventCallback:Function;
		private var _loader:SimpleURLLoader;
		
		
		
		public function GalleryContent( sectionW:int, sectionH:int ) {
			
			super( sectionW, sectionH );
			
		}
		
		
		
		public function get thumbs() : FlickrThumbCollection {
			
			return _currentPhotoThumbsCollection;
			
		}
		
		
		
		override public function buildIn() : void {
			
			super.buildIn();
			positionThumbs();
			
		}
		

		
		override public function listView() : void {
			
			removeDisplay();
			_currentDisplayCollection = _flickrSetThumbCollection;
			positionThumbs(); 
			
		}


		
		override protected function onDataLoaded( data:String ) : void {
			
			_currentDisplayCollection = _flickrSetThumbCollection = new FlickrThumbCollection();	 
			createThumbs( XML( data ).photosets.photoset, FlickerSetThumbAsset, FlickerSetEvent.FLICKER_SET_CLICK, onSetClick );
			super.onDataLoaded( data );
			
		}
		
		
		
		private function onPhotoSetLoaded( data:String ) : void {
			
			_currentDisplayCollection = _currentPhotoThumbsCollection = new FlickrThumbCollection();
			createThumbs( XML( data ).photoset.photo, SimpleThumbAsset, FlickrGalleryEvent.PHOTO_GALLERY_CLICK, onPhotoClick );
			dispatchEvent( new FlickrGalleryEvent( FlickrGalleryEvent.PHOTO_GALLERY_DATA, thumbs ) );
			
		}
		
		
		
		
		private function createThumbs( dataList:XMLList, viewClass:Class, event:String, callback:Function  ) : void {
			
			var index:int = 0;
			
			for each( var data:XML in dataList ) {
				var thumb:FlickrThumb = new FlickrThumb( data, index, new viewClass() );
				_currentDisplayCollection.push( thumb );
				thumb.addEventListener( event, callback );
				index++;
			}
			
			positionThumbs();
			
		}
		
		
		
		private function positionThumbs() : void {
			
			var yPos:int = 0;
			
			for each( var thumb:FlickrThumb in _currentDisplayCollection.list ) {
				
				var xPos:int = ( thumb.fullWidth + 20 ) * ( thumb.index % 3 );
				
				if( thumb.index > 0 && xPos == 0 ) {
					yPos += thumb.fullHeight + 10; 
				} 
				
				addChild( thumb.view );
				thumb.buildIn( xPos, yPos );
				
			}
			
			Tweener.addCaller(this, {onUpdate:displayUpdate, time:0.3, count:3});
			
		}
		
		
		
		private function removeDisplay() : void {
			
			for each( var thumb:FlickrThumb in _currentDisplayCollection.list ) {
				thumb.buildOut();
			}
			_currentDisplayCollection = null;
			_currentPhotoThumbsCollection = null;
			
		}
		
		
		
		private function onSetClick( e:FlickerSetEvent ) : void {
			
			removeDisplay();
			dispatchEvent( new SectionEvent( SectionEvent.ITEM_CLICKED ) );
			_loader = new SimpleURLLoader( new URLRequest( getFlickrSetURL( e.setId ) ), onPhotoSetLoaded );
			
		}
		
		
		
		private function onPhotoClick( e:FlickrGalleryEvent ) : void {
			
			e.revealGallery = FlickrThumb( e.target ).view.parent == this;
			dispatchEvent( e );
			
		}
		
		
		
		private function getFlickrSetURL( id:String ) : String {
			
			return "http://api.flickr.com/services/rest/?api_key="+FLICKR_API_KEY+"&method=flickr.photosets.getPhotos&photoset_id="+id;
			
		}
		
		
		
	}
}