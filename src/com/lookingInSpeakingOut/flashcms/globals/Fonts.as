package com.lookingInSpeakingOut.flashcms.globals {
	
	import flash.text.Font;
	
	import fonts.BigEd;
	import fonts.GothamBold;
	import fonts.Verdana;
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class Fonts {
		
		public static const BIG_ED:Font = new BigEd();
		public static const VERDANA:Font = new Verdana();
		public static const GOTHAM:Font = new GothamBold();
		 
	}
	
}