package com.lookingInSpeakingOut.flashcms.rootView {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;

	
	/**
	 * The Background display of the application
	 * extends sprite for future interactive concerns
	 * 
	 * @author ghostmonk 28/12/2008
	 * 
	 */
	public class Sky extends Sprite {
		
		
		
		private var _display:DisplayObject;
		
		
		
		/**
		 * 
		 * @param display
		 * 
		 */
		public function Sky( display:Bitmap ) {
			
			_display = display;
			addChild( display );
			alpha = 0;
			buildIn();
			addEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			
		}
		
		
		
	    /**
		 * Positions object to the center of stage
		 * must have reference to the stage to be called 
		 */
		public function position():void {
			
			y = Math.max( -_display.height / 4, Math.min( 0, ( stage.stageHeight - _display.height ) / 2 ) );
			x = ( stage.stageWidth - width ) / 2;
			
		}
		
		
		
		private function buildIn():void {
			
			Tweener.addTween(
				this, {
					alpha:1, 
					time:1, 
					transition:Equations.easeNone
				}
			);
			
		}
		
		
		
		private function onAddedToStage( e:Event ):void {
			
			removeEventListener( Event.ADDED_TO_STAGE, onAddedToStage );
			position();
			
		}
		
		
		
	}
}