package com.lookingInSpeakingOut.flashcms.events {
	
	import flash.events.Event;

	/**
	 * 
	 * @author ghostmonk 2009-20-09
	 * 
	 */
	public class DisplayEvent extends Event {
		
		
		
		public static const CLOSE_BTN_CLICKED:String = "closeBtnClicked";
		public static const BUILD_OUT_COMPLETE:String = "buildOutComplete";
		public static const BUILD_IN_COMPLETE:String = "buildInComplete";
		
		
		private var _id:String;
		
		
		
		public function DisplayEvent( type:String, id:String = null, bubbles:Boolean = false, cancelable:Boolean = false ) {
			
			_id = id;
			super( type, bubbles, cancelable );
			
		}
		
		
		
		public function get id() : String {
			
			return _id;
			
		}
		
		
		
		override public function clone() : Event {
			
			return new DisplayEvent( type, id, bubbles, cancelable );
			
		}
		
		
		
	}
}