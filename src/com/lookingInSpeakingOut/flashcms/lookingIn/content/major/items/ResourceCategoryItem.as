package com.lookingInSpeakingOut.flashcms.lookingIn.content.major.items {
	
	import caurina.transitions.Tweener;
	
	import com.lookingInSpeakingOut.flashcms.data.sectionData.CategoryData;
	import com.lookingInSpeakingOut.flashcms.data.sectionData.parse.ResourceData;
	import com.lookingInSpeakingOut.flashcms.events.core.SectionEvent;
	
	import flash.events.DataEvent;
	import flash.events.MouseEvent;
	
	import items.resources.ResourceItemCategoryAsset;
	
	[Event ( name="animationUpdate", type="com.lookingspeakingout.flashcms.events.SectionEvent" )]
	[Event ( name="sectionOpen", type="com.lookingspeakingout.flashcms.events.SectionEvent" )]
	[Event ( name="data", type="flash.events.DataEvent" )]

	
	
	/**
	 * An expandable - retractable displayObject that lists clickable resources for a particular category
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class ResourceCategoryItem extends ResourceItemCategoryAsset {
		
		
		
		private const PADDING:int = 5;
		
		private var _resourceItems:Array;
	 	private var _openHeight:Number;
	 	private var _isOpen:Boolean;
		
		
		
		/**
		 * 
		 * @param data
		 * 
		 */
		public function ResourceCategoryItem( data:CategoryData ) { 
			
			_resourceItems = [];
			_openHeight = height;
			_isOpen = false;
			
			label.text = data.catTitle;
			createResourceItems( data.items );
			
			label.mouseEnabled = false;
			arrow.mouseEnabled = false;
			enable();
			
		}
		
		
		
		/**
		 * Animates category out of sight 
		 * 
		 */
		public function buildOut():void {
			
			Tweener.addTween (
				this, {
					x:-width,
					time:0.3,
					onComplete:parent.removeChild,
					onCompleteParams:[ this ]
				}	
			);
			
		}
		
		
		
		/**
		 * Animates in the category, re-enabling each contained ResouceItem if open 
		 * 
		 */
		public function buildIn():void {
			
			Tweener.addTween ( this, { x:0,	time:0.3 } );
			
			if( _isOpen ) {
				for each( var resource:ResourceItem in _resourceItems ) {
					resource.enable();
				}
			}
			
		}
		
		
		
		/**
		 * Animate out each resource item, and update parent container of height
		 * 
		 */
		public function hideItems():void {
			
			Tweener.addTween( arrow, { rotation:0, time:0.3 } );
			
			for each( var resource:ResourceItem in _resourceItems ) {
				resource.buildOut();
			}
			
			_isOpen = false;
			
		}
		
		
		
		//Animate in each resource item and update parent container of height
		private function showItems():void {
			
			Tweener.addTween( arrow, { rotation:180, time:0.3 } );
			
			for each( var resource:ResourceItem in _resourceItems ) {
				addChildAt( resource, 0 );
				resource.buildIn();
			}
			
			_isOpen = true;
		}
		
		
		
		//create each resource item from data list, add a click listener
		private function createResourceItems( list:Array ):void {
			
			var itemYPosition:Number = height + PADDING;
			
			for each( var data:ResourceData in list ) {
				
				var item:ResourceItem = new ResourceItem( data );
				item.addEventListener( SectionEvent.SECTION_VIEW_UPDATE, onAnimation );
				item.addEventListener( DataEvent.DATA, onItemClick );
				item.openYPosition = itemYPosition;
				
				_resourceItems.push( item );
				
				itemYPosition += item.height + PADDING;
			}
			
		}
		
		
		
		//When clicked, expand or retract based on current state and update parent container of height
		private function onClick( e:MouseEvent ):void {	
			
			if( _isOpen ) {
				hideItems();
			}
			else {
				showItems();
				dispatchEvent( new SectionEvent( SectionEvent.SECTION_OPEN ) );
			}
			
		}
		
		
		
		private function enable():void {
			
			background.buttonMode = true;
			background.addEventListener( MouseEvent.CLICK, onClick );
			
		}
		
		
		
		private function disable():void {
			
			background.buttonMode = false;
			background.removeEventListener( MouseEvent.CLICK, onClick );
			
		}
		
		
		
		private function onAnimation( e:SectionEvent ):void {
			
			dispatchEvent( e );
			
		}
		
		
		
		private function onItemClick( e:DataEvent ):void {
			
			dispatchEvent( e );
			
		}

		

	}
}