package com.lookingInSpeakingOut.flashcms.data.sectionData {
	
	import flash.net.URLRequest;
	
	/**
	 * Data struct - parses dat for display configuration of new BaseSections
	 * 
	 * @author ghostmonk 01/01/2009
	 * 
	 */
	public class SectionData {
		
		
		
		private var _label:String; 
		private var _id:String;
		private var _sectionType:int;
		
		private var _request:URLRequest;
		
		private var _width:Number;
		private var _height:Number;
		private var _alpha:Number;
		private var _color:uint;
		private var _cornerRad:Number;
		
		private var _titleColor:uint;
		private var _titleSize:Number;
		
		private var _contentWidth:Number;
		private var _contentHeight:Number;
		private var _contentType:String;
		
		private var _factoryId:String;
		
		
		
		/**
		 * 	Example: <pre>&lt;section label="news" type="1"&gt;
		 *				&lt;request url="http://www.lookinginspeakingout.com/news_feed" /&gt;
		 *				&lt;container width="470" height="400" alpha="0.5" color="0xf0ebdf" cornerRad="20" /&gt;
		 *				&lt;content width="440" height="370" type="news" /&gt;
		 *				&lt;title color="0x3b050e" size="36" /&gt;
		 *			&lt;/section&gt;</pre>
		 *  
		 * @param config - xml data containing display configuration values
		 * 
		 */
		public function SectionData( config:XML ) {
			
			_label = config.@label;
			_id = config.@id;
			_factoryId = config.@factoryId;
			_sectionType = config.@type;
			
			_request = new URLRequest( config.request.@url );
			
			_width = config.container.@width;
			_height = config.container.@height;
			_alpha = config.container.@alpha;
			_color = config.container.@color;
			_cornerRad = config.container.@cornerRad;
			
			_titleColor = config.title.@color;
			_titleSize = config.title.@size;
			
			_contentWidth = config.content.@width;
			_contentHeight = config.content.@height;
			_contentType = config.content.@type;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get id():String {
			
			return _id;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get label():String  {
			 	
			return _label;
					
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get sectionType():int {
			 	
			return _sectionType;
				
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get request():URLRequest {
			 	
			return _request;
					
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get width():Number {
			 	
			return _width;
			
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get height():Number {
			
			return _height;
			 		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get alpha():Number {
			 	
			return _alpha;
				 		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get color():uint {
			 	
			return _color;
				 		
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get cornerRad():Number {
			
			return _cornerRad;
			 		
		}
		
		
			
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get titleColor():uint {
			
			return _titleColor;
			 	
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get titleSize():Number {
			 	
			return _titleSize;
					
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get contentWidth():Number {
				
			return _contentWidth;
				
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get contentHeight():Number {
				
			return _contentHeight;
				
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get contentType():String {
			 	
			return _contentType;
			 	
		}
		
		
		
		public function get factoryId() : String {
			
			return _factoryId;
			
		}
		
		
		
		/**
		 * Use this function to check the value of the data being passed in at creation
		 * 
		 * @return - formatted string displaying value of each member
		 * 
		 */
		public function toString():String {
			
			return 	"Section: "+_label+"\n"+
					"Section Type: "+_sectionType+"\n"+
					"Request: "+_request.url+"\n"+
					"Width: "+_width+"\n"+
					"Height: "+_height+"\n"+
					"Alpha: "+_alpha+"\n"+
					"Color: "+_color+"\n"+
					"Corner: "+_cornerRad+"\n"+
					"Title Color:"+_titleColor+"\n"+
					"Title Size:"+_titleSize+"\n"+
					"Content Width:"+_contentWidth+"\n"+
					"Content Height:"+_contentHeight+"\n"+
					"Content Type: "+_contentType+"\n";
					
		}



	}
}