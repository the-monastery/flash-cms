<?php

	include '../../../modules/errors/errorReporting.php';
	include '../../../modules/connection/speakingOutConnect.php';
	include '../printXML.php';
	
	$amount = isset( $_GET[ 'amount' ] ) ? $_GET[ 'amount' ] : 20;
	$page = isset( $_GET[ 'page' ] ) ? $_GET[ 'page' ] : 1;

	$all = $mysqli->query(
		"SELECT *
		FROM messages, userdata
		WHERE isTrunk =1
		AND isApproved =1
		AND messages.userId = userdata.id" );
	
	$totalPages = ceil( $all->num_rows / $amount );
	
	$page = min( $totalPages, max( $page, 1 ) );
	$start = ( $page - 1 ) * $amount;
	
	$result = $mysqli->query(
		"SELECT messages.id, name, text, xPos, yPos, replies, messages.date
		FROM messages, userdata
		WHERE isTrunk =1
		AND isApproved =1
		AND messages.userId = userdata.id
		ORDER BY messages.date DESC
		LIMIT $start, $amount" );
	
	if( $result ) {
		printSuccess( $result, $totalPages, $page );
	}
	else {
		printError( $result );
	}
	
	$mysqli->close();
	 
?>