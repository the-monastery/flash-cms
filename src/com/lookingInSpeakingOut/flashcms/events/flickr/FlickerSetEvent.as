package com.lookingInSpeakingOut.flashcms.events.flickr {
	
	import flash.events.Event;

	public class FlickerSetEvent extends Event {
		
		
		
		public static const FLICKER_SET_CLICK:String = "flickerSetClick";
		
		private var _setId:String;
		
		
		public function FlickerSetEvent( type:String, setId:String, bubbles:Boolean=false, cancelable:Boolean=false ) {
			
			_setId = setId;
			super( type, bubbles, cancelable );
			
		}
		
		
		public function get setId() : String {
			
			return _setId;
			
		}
		
		
		
		override public function clone() : Event {
			
			return new FlickerSetEvent( type, setId, bubbles, cancelable );
			
		} 
		
		
		
	}
}