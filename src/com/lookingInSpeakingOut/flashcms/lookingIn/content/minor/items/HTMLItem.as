package com.lookingInSpeakingOut.flashcms.lookingIn.content.minor.items {
	
	import com.ghostmonk.text.EmbeddedText;
	import com.lookingInSpeakingOut.flashcms.globals.Fonts;
	
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * 
	 * @author ghostmonk
	 * 
	 */
	public class HTMLItem extends EmbeddedText {
		
		
		private var _scrollHeight:Number;
		
		/**
		 * 
		 * @param data
		 * @param w
		 * 
		 */
		public function HTMLItem( data:String, width:Number, scrollHeight:Number ) {
			
			super( Fonts.VERDANA.fontName, 11, 0xd86982, false, true, true );
			_scrollHeight = scrollHeight;
			selectable = true;
			this.width = width;
			htmlText = data;
			
			//I need a more specific way to call scroll on BaseSection... as it stands
			//there is no efficient and clear way to update the scroller without 
			// inexcusably complicating the code
			//addEventListener( MouseEvent.MOUSE_DOWN, onMouseDown );
			
		}
		
		
		
		private function onMouseDown( e:MouseEvent ):void {
			
			stage.addEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			addEventListener( Event.ENTER_FRAME, onEnterFrame );
			
		}
		
		
		private function onEnterFrame( e:Event ):void {
			
			
			var stageLoc:int = stage.mouseY - parent.parent.y;
			
			if( stageLoc > _scrollHeight + 50 ) {
				trace( "up" );
			}
			else if( stageLoc < 50 ) {
				trace( "down" );
			}
			
		}
		
		
		private function onMouseUp( e:MouseEvent ):void {
			
			stage.removeEventListener( MouseEvent.MOUSE_UP, onMouseUp );
			removeEventListener( Event.ENTER_FRAME, onEnterFrame );
			
		}
		

	}
}