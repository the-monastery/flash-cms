package com.lookingInSpeakingOut.flashcms.data.sectionData {
	
	/**
	 * This pulls out the id of a top level xml node, and then creates an array of items from the item data contained in the XMLList
	 * <p>Currently being used with Links and Resources</p> 

	 * @author ghostmonk 17/03/2009
	 * 
	 */
	public class CategoryData {
		
		
		private var _items:Array;
		private var _catTitle:String;
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get items():Array { 
			
			return _items;
			 
		}
		
		
		
		/**
		 * 
		 * @return 
		 * 
		 */
		public function get catTitle():String {
			
			return _catTitle;
			 
		}
		
		
		
		/**
		 * 
		 * @param xml Parent category with an id attribute and a number of xml children 
		 * @param ItemClass the class responsible for parsing the content of a single xml child in the Parent category
		 * 
		 */
		public function CategoryData( xml:XML, ItemClass:Class ) {
			
			_catTitle = xml.@id;
			_items = [];
			
			for each( var itemData:XML in xml.link ) {
				items.push( new ItemClass( itemData ) );
			}
			
		}
		
		

	}
}