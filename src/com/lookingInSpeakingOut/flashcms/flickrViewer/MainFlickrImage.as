package com.lookingInSpeakingOut.flashcms.flickrViewer {
	
	import caurina.transitions.Equations;
	import caurina.transitions.Tweener;
	
	import com.ghostmonk.net.IndexLoader;
	import com.ghostmonk.net.XMLLoader;
	import com.lookingInSpeakingOut.flashcms.cmsUtils.CleanHTML;
	
	import flash.display.Bitmap;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.events.ErrorEvent;
	import flash.text.TextField;

	public class MainFlickrImage extends Sprite {
		
		
		
		private var _images:Object;
		private var _currentImageId:String;
		private var _thumbs:FlickrThumbCollection;
		private var _descriptions:Object;
		
		private var _titleField:TextField;
		private var _descriptionField:TextField;
		
		
		public function MainFlickrImage( thumbs:FlickrThumbCollection, infopanel:InfoPanel ) {
			
			_thumbs = thumbs;
			_images = new Object();
			_descriptions = new Object();
			
			_titleField = infopanel.title;
			_descriptionField = infopanel.description;
			
			for( var i:Number = 0; i < thumbs.length; i++ ) {
				
				var id:String = thumbs.getThumb( i ).id;
				_images[ id ] = new Bitmap( null, PixelSnapping.ALWAYS, true );
				var loader:IndexLoader = new IndexLoader( thumbs.getThumb( i ).mainURL, Number( id ), onImageLoaded, onError );
				var data:XMLLoader = new XMLLoader(	
					"http://api.flickr.com/services/rest/" + 
					"?method=flickr.photos.getInfo" + 
					"&api_key=bf36794dc3904ca5872ce41e7e6a33e0" + 
					"&photo_id="+id, onDataLoaded );
				
			}
			
			_currentImageId = id;
			
		}
		
		
		
		public function getDescription( id:String ) : String {
			
			return _descriptions[ id ];
			
		} 
		
		
		public function set currentImageById( id:String ):void {
			
			if( _currentImageId == id ) return;
			
			if( _images[ _currentImageId ].parent == this ) {
				var outImage:Bitmap = _images[ _currentImageId ];
				alphaTween( outImage, 0, removeChild, outImage  );
			}
			
			_currentImageId = id;
			var inImage:Bitmap = _images[ _currentImageId ];
			inImage.alpha = 0;
			alphaTween( inImage, 1 );
			
			_titleField.text = _thumbs.getThumbById( id ).title;
			var desc:String = _descriptions[ id ] || "";
			
			desc = CleanHTML.removeWhiteSpace( desc );
			
			Tweener.addTween( _descriptionField, { _text:desc, time:0.3 } );
				
		}
		
		
		
		public function clean() : void {
			
			for each( var bit:Bitmap in _images ) {
				if( bit.parent == this ) {
					removeChild( bit );
				}
			}
			
		}
		
		
		
		private function onImageLoaded( bitmap:Bitmap, id:Number ):void {
			
			var targetBitmap:Bitmap = _images[ id ];
			targetBitmap.bitmapData = bitmap.bitmapData;
			
			targetBitmap.x = ( parent.width - targetBitmap.width ) * 0.5;
			targetBitmap.y = ( parent.height - targetBitmap.height ) * 0.5;
			
		}
		
		
		private function alphaTween( image:Bitmap, alpha:Number, complete:Function = null, param:Bitmap = null ):void {
			
			addChild( image );
			Tweener.addTween( image, {
					alpha:alpha,
					time:0.3,
					transition:Equations.easeNone,
					onComplete:complete,
					onCompleteParams:[param]
				}
			);
			
		}
		
		
		private function onError( e:ErrorEvent, id:Number ):void {
			
			trace( "Large Image Error: " + e.text + " ID: " + id );
			
		}
		
		
		private function onDataLoaded( data:String ) : void {
			
			var xml:XML = XML( data );
			_descriptions[ xml.photo.@id.toString() ] = xml.photo.description.toString();		
			
		}
		
		
		
		
	}
}